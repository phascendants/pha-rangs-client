# RanGS-Sources

## Website
[PHA~RanGS](http://pha-rangs.ddns.net)  

## Server Rate Features
**SERVER INFORMATIONS**  
```
- Low Exp Rate
- Low Gold Rate 
- Low Drop Rate
- All Equipment Farming / Hunting / Crafting
- Rewards tickets for PvP and PvE which can be exchanged for r-points
```  
**SERVER FEATURES**  
```
- Soft Level Cap: 260
- Max Level : 300   
- Skill : 1 ~ 237 (Official Skill Effect)  
- Level 280 Last Weapon, Armor, Accessory
- Decompose System   
- 9 Class : Brawler / Swordsman / Archer / Shaman / Extreme / Assassin / Gunner / Magician / Shaper  
```
**SKILL EFFECTS**  
```
-All Skill Effects Are From Official Nothing Changed
-All Skill Effects Functioning
-Balanced Class 
```
**OFFICIAL PVP FEATURES**  
```
-Tyranny Wars
-Capture The Flag
-Club Wars
-Club Death Match
```

