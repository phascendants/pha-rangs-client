
//--------------------------------------------------------------------------------------
// Global variables
//--------------------------------------------------------------------------------------

float4x4 g_matWorld;
float4x4 g_matWVP;
float4   g_vLightDiffuse;
float3   g_vLightAmbient;
float3   g_vLightDir;
float3   g_vCameraFrom; 
float2   g_vFOG;	// FogEnd, FogDist
float3   g_vFogColor;
int      g_nPointLightNum_TOOL;
float4   g_vPos_Range_PL_TOOL[15];
float4   g_vDiff_PL_TOOL[15];
float4   g_vAtt_PL_TOOL[15];
float4   g_vWindowSize;
float    g_fAlpha;						// Hiding piece model behind a player character
float2   g_vDayNightWeight;
float2	 g_vLightMapUV_Offset;
float2   g_vLightMapUV_Multiply; 
float3	 g_vVoxelColor; 
float    g_fTime; 
float2	 g_vReflectPower;
float 	g_fWaveDensity;		// 흔들림 밀도 ( 수치가 클 수록 세세하게 변한다. )
float 	g_fWaveScale;		// 흔들림 Scale
float 	g_fWaveSpeed;		// 흔들림 Speed
float2  g_vMoveSpeed;		// Flow 되는 방향


//--------------------------------------------------------------------------------------
// Material Parameters
//--------------------------------------------------------------------------------------

texture g_BaseTexture;
sampler BaseTexSampler = sampler_state 
{
    Texture = (g_BaseTexture);
};

texture g_NormalTexture;
sampler NormalTexSampler = sampler_state 
{
    Texture = (g_NormalTexture);
};

texture g_ReflectTexRT;
sampler ReflectTexRTSampler = sampler_state 
{
    Texture = (g_ReflectTexRT);
};

texture g_CubeTexture;
samplerCUBE CubeTexSampler = sampler_state 
{
    Texture = (g_CubeTexture);
};

float g_fRotate_UV;				//UV 회전값 (도);
float g_fScaleFactor;			//UV 크기값 (배수);

 
//--------------------------------------------------------------------------------------
// 	Basic vertex transformation
//--------------------------------------------------------------------------------------

// TOOL
struct VS_INPUT_TOOL
{
    float4 m_vPosition  : POSITION;
    float4 m_vVertColor : COLOR0;
    float3 m_vNormal    : NORMAL;
    float2 m_vTexCoord0 : TEXCOORD0;
} Vertex_tool : SEMANTIC_VS_INPUT_TOOL; 

struct VS_OUTPUT_TOOL
{
    float4 m_vPosition  : POSITION;
    float4 m_vVertColor : COLOR0;
    float2 m_vTexCoord0 : TEXCOORD0;
    float3 m_vPos       : TEXCOORD1; 
    float3 m_vNor       : TEXCOORD2; 
    float  m_fFog       : FOG;
};

// Runtime 
struct VS_INPUT		
{
    float4 m_vPosition  : POSITION;    
    float4 m_vVertColor : COLOR0;
    float3 m_vNormal    : NORMAL;
    float2 m_vTexCoord0 : TEXCOORD0;

} Vertex : SEMANTIC_VS_INPUT;

struct VS_OUTPUT_1		
{
    float4 m_vPosition  : POSITION;
    float4 m_vVertColor : COLOR0;
    float2 m_vTexCoord0 : TEXCOORD0;
    float  m_fFog       : FOG;
};
		 
struct VS_OUTPUT_2
{ 
    float4 m_vPosition       : POSITION; 
    float4 m_vVertColor      : COLOR0;
    float2 m_vTexCoord0      : TEXCOORD0;
    float2 m_vTexCoord1      : TEXCOORD1;
    float3 m_vEnumPosition   : TEXCOORD2; // CubeMap 일 경우는 m_vWorldPos, RealTimeReflect 일 경우는 m_vReflectProjPos 로 활용된다.
    float  m_fFog            : FOG; 
}; 

//----------------------------------------------------------------------------------------------------------
//	 Tool Shader
//----------------------------------------------------------------------------------------------------------

// Vertex Shader
	
VS_OUTPUT_TOOL VS_tool( VS_INPUT_TOOL In ) 
{
    VS_OUTPUT_TOOL Out;
    
    // Now that we have the calculated weight, add in the final influence
    Out.m_vPosition = mul(g_matWVP, In.m_vPosition);
    Out.m_vPos = mul(g_matWorld, In.m_vPosition);
    Out.m_vNor = normalize( mul(g_matWorld, In.m_vNormal) );

    // 텍스처 좌표 건네주기
    Out.m_vTexCoord0 = In.m_vTexCoord0;

    float fDiffuseDot = saturate( dot( g_vLightDir, normalize( Out.m_vNor ) ) );
    float3 vDiffuse = g_vLightDiffuse * fDiffuseDot + g_vLightAmbient;
	
    Out.m_vVertColor = In.m_vVertColor;
    Out.m_vVertColor.xyz *= vDiffuse.xyz;

    Out.m_fFog = saturate((g_vFOG.x - Out.m_vPosition.z) / g_vFOG.y);
   
    return Out;
}

//	Pixel Shader
	
float4 PS_tool( VS_OUTPUT_TOOL In ) : COLOR0 
{  
  
    float2 INUV = In.m_vTexCoord0.xy; 

    float4 vColor = tex2D( BaseTexSampler, INUV);    

    float3 vPointColor = 0.f;
    for ( int i=0; i<g_nPointLightNum_TOOL; ++i )
    {
        float3 afPosDir = g_vPos_Range_PL_TOOL[i].xyz - In.m_vPos;
        float fLength = length( afPosDir );
        // 내적 구함.
        afPosDir = normalize( afPosDir );
        float fDotPL = saturate( dot( afPosDir, In.m_vNor ) );
        // 선형 감쇠, 2차 감쇠, 지수 감쇠 적용
        float fAttenuationSub = g_vAtt_PL_TOOL[i].x;
        fAttenuationSub += g_vAtt_PL_TOOL[i].y * fLength;
        fAttenuationSub += g_vAtt_PL_TOOL[i].z * fLength * fLength;
        // 최종 감쇠값
        float fAttenuation = 1.f / fAttenuationSub;
        // 거리 체크
        float fEnableLength = step(fLength, g_vPos_Range_PL_TOOL[i].w);
        // 마지막
	     // Edge 부분이 저 감쇠만으로는 적용이 안되서 1 를 빼주었다.
        vPointColor += g_vDiff_PL_TOOL[i].xyz * max((fAttenuation-1.f),0) * fDotPL * fEnableLength;
    }
    vPointColor += In.m_vVertColor.xyz;

    vColor.xyz *= min(vPointColor.xyz,2.f);
    vColor.xyz = (vColor.xyz*In.m_fFog) + g_vFogColor*(1.f-In.m_fFog);
    vColor.w = g_fAlpha;

    return vColor;
}



//----------------------------------------------------------------------------------------------------------
// Runtime Shader	ps.1.1
//----------------------------------------------------------------------------------------------------------

// Vertex Shader

VS_OUTPUT_1 VS_1( VS_INPUT In ) 
{
    VS_OUTPUT_1 Out;
    
    // Now that we have the calculated weight, add in the final influence
    Out.m_vPosition = mul(g_matWVP, In.m_vPosition);

    // Pass the texture coordinate
    Out.m_vTexCoord0 = In.m_vTexCoord0;

    Out.m_vVertColor = In.m_vVertColor;

    Out.m_fFog = saturate((g_vFOG.x - Out.m_vPosition.z) / g_vFOG.y);
   
    return Out;
}


//	Pixel Shader

float4 PS_1( VS_OUTPUT_1 In ) : COLOR0 
{
    // Texture
    float4 vColor = tex2D( BaseTexSampler, In.m_vTexCoord0 );
    vColor *= In.m_vVertColor;
    vColor *= g_vReflectPower.xxxy;

    return vColor;
}



//----------------------------------------------------------------------------------------------------------
// Runtime Shader	ps.2.0 
//----------------------------------------------------------------------------------------------------------

// Vertex Shader

VS_OUTPUT_2 VS_2( VS_INPUT In, uniform bool bReflectRT  ) 
{
    VS_OUTPUT_2 Out;
    
    // Now that we have the calculated weight, add in the final influence
    Out.m_vPosition = mul(g_matWVP, In.m_vPosition);

    // Pass the texture coordinate
    Out.m_vTexCoord0 = In.m_vTexCoord0*g_fWaveDensity + float2(sin(g_fTime*g_fWaveSpeed), cos(g_fTime*g_fWaveSpeed));
    Out.m_vTexCoord1 = In.m_vTexCoord0*g_fWaveDensity + float2(sin(g_fTime*g_fWaveSpeed+(3.14f*0.5f)), cos(g_fTime*g_fWaveSpeed+(3.14f*0.5f)));
    Out.m_vTexCoord0 += float2(g_fTime*g_vMoveSpeed.x, g_fTime*g_vMoveSpeed.y);
    Out.m_vTexCoord1 += float2(g_fTime*g_vMoveSpeed.x, g_fTime*g_vMoveSpeed.y);
    Out.m_vVertColor = In.m_vVertColor;
   
    Out.m_fFog = saturate((g_vFOG.x - Out.m_vPosition.z) / g_vFOG.y);

    Out.m_vEnumPosition.xyz = 1.f;
    if ( bReflectRT )
    {
       Out.m_vEnumPosition.xyz = mul(g_matWVP, In.m_vPosition).xyw;
    }
    else
    {
       float3 vWorldPos = mul(g_matWorld, In.m_vPosition);
       Out.m_vEnumPosition = vWorldPos;
    }

    return Out;
}


//	Pixel Shader

float4 PS_2( VS_OUTPUT_2 In ) : COLOR0 
{
    float3 vBump1 = tex2D( NormalTexSampler, In.m_vTexCoord0 );
    float3 vBump2 = tex2D( NormalTexSampler, In.m_vTexCoord1 );

    vBump1 = (vBump1.rgb * 2.0f) - 1.0f;
    vBump2 = (vBump2.rgb * 2.0f) - 1.0f;
    float3 vBump = normalize(vBump1 + vBump2);
    vBump *= float3(g_fWaveScale,g_fWaveScale,1.f);
    vBump = normalize(vBump);

    float3 vEyeToVector = normalize(In.m_vEnumPosition - g_vCameraFrom);
    float3 vCubeTexcoord = reflect(vEyeToVector, vBump.xzy);
    float4 vColor = texCUBE( CubeTexSampler, vCubeTexcoord );

    vColor *= In.m_vVertColor;
    vColor *= g_vReflectPower.xxxy;

    return vColor;
}

//	Pixel Shader

float4 PS_2_REAL_TIME( VS_OUTPUT_2 In ) : COLOR0 
{
    float3 vBump1 = tex2D( NormalTexSampler, In.m_vTexCoord0 );
    float3 vBump2 = tex2D( NormalTexSampler, In.m_vTexCoord1 );

    vBump1 = (vBump1.rgb * 2.0f) - 1.0f;
    vBump2 = (vBump2.rgb * 2.0f) - 1.0f;
    float3 vBump = normalize(vBump1 + vBump2);
    vBump *= float3(g_fWaveScale,g_fWaveScale,1.f);
    vBump = normalize(vBump);

    //transform from RT space to texture space.
    float2 vProjectionUV = 0.5 * In.m_vEnumPosition.xy / In.m_vEnumPosition.z + float2( 0.5, 0.5 );
    vProjectionUV.y = 1.0f - vProjectionUV.y;
    vProjectionUV.xy += g_vWindowSize.xy;
    vProjectionUV.xy += vBump.xz;

    float4 vColor = tex2D( ReflectTexRTSampler, vProjectionUV );

    vColor *= In.m_vVertColor;
    vColor *= g_vReflectPower.xxxy;

    return vColor;
}



//---------------------------------------------------------------------------------------------------------- 
//					technique 
//----------------------------------------------------------------------------------------------------------
 
technique tool 
{ 
	pass high 
	{ 
		VertexShader = compile vs_3_0 VS_tool(); 
		PixelShader = compile ps_3_0 PS_tool(); 
	} 
}; 
 
technique runtime_1 
{ 
	pass high 
	{ 
		VertexShader = compile vs_1_1 VS_1(); 
		PixelShader = compile ps_1_1 PS_1(); 
	} 
} 

technique runtime_2 
{ 
	pass high
	{ 
		VertexShader = compile vs_2_0 VS_2(false); 
		PixelShader = compile ps_2_0 PS_2(); 
	}

	pass realtime
	{ 
		VertexShader = compile vs_2_0 VS_2(true); 
		PixelShader = compile ps_2_0 PS_2_REAL_TIME(); 
	}
};