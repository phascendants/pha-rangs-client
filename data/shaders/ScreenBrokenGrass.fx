
//--------------------------------------------------------------------------------------
// Global variables
//--------------------------------------------------------------------------------------

float    g_fAlpha;



//--------------------------------------------------------------------------------------
// Material Parameters
//--------------------------------------------------------------------------------------

texture g_BaseTexture;
sampler BaseTexSampler = sampler_state 
{
    Texture = (g_BaseTexture);
};

texture g_BackGroundTexture;
sampler BackGroundTexSampler = sampler_state 
{
    Texture = (g_BackGroundTexture);
};
 

//--------------------------------------------------------------------------------------
// 	Basic vertex transformation
//--------------------------------------------------------------------------------------

struct VS_OUTPUT
{
    float4 m_vPosition  : POSITION;
    float2 m_vTexCoord0 : TEXCOORD0;
};


//	Pixel Shader
float4 PS( VS_OUTPUT In ) : COLOR0 
{
    float4 vUV = tex2D( BaseTexSampler, In.m_vTexCoord0 );
    vUV.xy -= 0.5f;

    // Texture
    float4 vColor = tex2D( BackGroundTexSampler, In.m_vTexCoord0 );
    float4 vColorA = tex2D( BackGroundTexSampler, In.m_vTexCoord0 + (vUV.xy*0.1f) );

    vColor = (vColor*(1.f-g_fAlpha)) + (vColorA*g_fAlpha);

    return vColor;
}



//---------------------------------------------------------------------------------------------------------- 
//					technique 
//----------------------------------------------------------------------------------------------------------
 
technique tool_deffered
{ 
	pass high 
	{ 
		PixelShader = compile ps_2_0 PS(); 
	} 
};