//--------------------------------------------------------------------------------------
// Textured - 텍스쳐를 라이트나 버텍스의 노멀과는 무관하게 있는 그대로 렌더링한다.
//--------------------------------------------------------------------------------------


//--------------------------------------------------------------------------------------
// Global variables
//--------------------------------------------------------------------------------------

float4x4 g_matWVP;       // 텍스쳐UV행렬

// Material Parameters
//--------------------------------------------------------------------------------------

texture g_BaseTexture;
sampler BaseTexSampler = sampler_state 
{
    Texture = (g_BaseTexture);
};

//--------------------------------------------------------------------------------------
// 	Basic vertex transformation
//--------------------------------------------------------------------------------------

// TOOL
struct VS_INPUT_TOOL
{
    float4 m_vPosition  : POSITION;
    float2 m_vTexCoord0 : TEXCOORD0;
} Vertex_tool : SEMANTIC_VS_INPUT_TOOL; 

struct VS_OUTPUT_TOOL
{
    float4 m_vPosition  : POSITION;
    float2 m_vTexCoord0 : TEXCOORD0;
};

// Runtime 
struct VS_INPUT		
{
    float4 m_vPosition  : POSITION;    
    float2 m_vTexCoord0 : TEXCOORD0;

} Vertex : SEMANTIC_VS_INPUT;

struct VS_OUTPUT_1		
{
    float4 m_vPosition  : POSITION;
    float2 m_vTexCoord0 : TEXCOORD0;
};

//---------------------------------------------------------------------------------------

// Vertex Shader
	
VS_OUTPUT_TOOL VS_tool( VS_INPUT_TOOL In ) 
{
    VS_OUTPUT_TOOL Out;
    
    // Now that we have the calculated weight, add in the final influence
    Out.m_vPosition = mul(g_matWVP, In.m_vPosition);


   // 텍스처 좌표 건네주기
    Out.m_vTexCoord0 = In.m_vTexCoord0;
    return Out;
}



//	Pixel Shader

float4 PS_tool( VS_OUTPUT_TOOL In ) : COLOR0 

{
    // Texture
    float4 vColor = tex2D( BaseTexSampler, In.m_vTexCoord0 );
    return vColor;
}

//-------------------------------------------------------------------

// Vertex Shader
	
VS_OUTPUT_1 VS_1( VS_INPUT In ) 
{
    VS_OUTPUT_1 Out;
    
    // Now that we have the calculated weight, add in the final influence
    Out.m_vPosition = mul(g_matWVP, In.m_vPosition);


   // 텍스처 좌표 건네주기
    Out.m_vTexCoord0 = In.m_vTexCoord0;
	
    return Out;
}

//	Pixel Shader

float4 PS_1( VS_OUTPUT_1 In ) : COLOR0 

{
    // Texture
    float4 vColor = tex2D( BaseTexSampler, In.m_vTexCoord0 );
    return vColor;
}



//--------------------------------------------------------------//
// Technique Section for Textured
//--------------------------------------------------------------//


technique tool_deffered
{ 
	pass high 
	{ 
		VertexShader = compile vs_3_0 VS_tool(); 
		PixelShader = compile ps_3_0 PS_tool();
		ALPHABLENDENABLE = TRUE;
		SRCBLEND = ONE;
		DESTBLEND = ONE;
	} 
}; 

// C++ Code 상에서도 1.1 만 사용하도록 작업되었다.
// 2.0 도 사용하길 원하면, C++ Code 도 변경해야 한다.
technique runtime_1 
{ 
	pass high 
	{ 
		VertexShader = compile vs_1_1 VS_1(); 
		PixelShader = compile ps_1_1 PS_1();
		FOGENABLE = FALSE;
		ALPHABLENDENABLE = TRUE;
		SRCBLEND = ONE;
		DESTBLEND = ONE;
	} 
};
