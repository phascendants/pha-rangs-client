float2 g_screen_size = 1.f;
float2 g_vFog = 1.f;
//float g_random_size = 100.f;
//float g_sample_rad = 0.03f;
//float g_intensity = 1.f;
//float g_scale = 1.f;
//float g_bias = 0.f;
float4x4 Projection;

float g_random_size	= 64;
float g_sample_rad	= 5;
float g_intensity	= 0.125;
float g_scale		= 0.16;
//float g_sample_rad	= 2;
//float g_intensity	= 6;
//float g_scale		= 1;
//float g_sample_rad	= 5;	//2;	//5;	//5;		//2;
//float g_intensity	= 30;	//6;	//30;	//14000;	//20;
//float g_scale		= 5;	//1;	////5;	//2000;		//4;
float g_bias		= 0.2;	//0;	////0;	//0.2

texture DepthTexture;
texture NormalTexture;
texture RandomTexture;
texture SSAOTexture;

sampler2D g_buffer_depth = sampler_state
{
	Texture = (DepthTexture);
	ADDRESSU = CLAMP;
	ADDRESSV = CLAMP;
	MAGFILTER = LINEAR;
	MINFILTER = LINEAR;
};

sampler2D g_buffer_norm = sampler_state
{
	Texture = (NormalTexture);
	ADDRESSU = CLAMP;
	ADDRESSV = CLAMP;
	MAGFILTER = LINEAR;
	MINFILTER = LINEAR;
};

sampler2D g_random = sampler_state
{
	Texture = (RandomTexture);
	ADDRESSU = WRAP;
	ADDRESSV = WRAP;
	MAGFILTER = LINEAR;
	MINFILTER = LINEAR;
};

sampler2D g_ssao = sampler_state
{
	Texture = (SSAOTexture);
	ADDRESSU = CLAMP;
	ADDRESSV = CLAMP;
	MAGFILTER = LINEAR;
	MINFILTER = LINEAR;
};

float3 getPosition(in float2 uv)
{
	// Construct a position for the rendered오후 5:42 2018-07-06 fragment
	float depth = tex2D(g_buffer_depth, uv).r;
	//float3 screenPosition = float3(uv, depth);

	// Go from [0, 1] to [-1, 1]
	//float3 clipPosition = (screenPosition * 2.0f) - float3(1.0f,1.0f,1.0f);

	float4 clipPosition = float4(uv.x * 2 - 1, (1 - uv.y) * 2 - 1, depth, 1);

	// Undo the projection to get back to view coordinates
	float4 viewPosition = mul( clipPosition, Projection );
	viewPosition /= viewPosition.w;

	return viewPosition.xyz;
}

float3 getNormal(in float2 uv)
{
	//return normalize(tex2D(g_buffer_norm, uv).xyz * 2.0f - 1.0f);
	return normalize(tex2D(g_buffer_norm, uv).xyz);
}

float2 getRandom(in float2 uv)
{
	return normalize(tex2D(g_random, g_screen_size * uv / g_random_size).xy * 2.0f - 1.0f);
}

float doAmbientOcclusion(in float2 tcoord,in float2 uv, in float3 p, in float3 cnorm, float scale, float intensity)
{
	float3 diff = getPosition(tcoord + uv) - p;
	float3 v = normalize(diff);
	float d = length(diff)*scale;
//	return (1.0/(1.0+d))*intensity;
	return max(0.0,dot(cnorm,v)-g_bias)*(1.0/(1.0+d*d))*intensity;
//	return max(0.0,dot(cnorm,v)-g_bias)*(sqrt(1.0/((1.0+d)*(1 + d))))*intensity;
}

struct VS_OUTPUT
{
	float4 pos : POSITION;
	float2 uv : TEXCOORD0;
};

VS_OUTPUT VS(float4 Position : POSITION, float2 TexCoord : TEXCOORD0)
{
	VS_OUTPUT Out = (VS_OUTPUT)0;

	Out.pos = Position;
	Out.uv = TexCoord; 
	return Out;
}

float4 PS(VS_OUTPUT i) : COLOR0
{
	const float2 vec[4] = {float2(1,0),float2(-1,0),float2(0,1),float2(0,-1)};

	float4 normal_blend = tex2D(g_buffer_norm, i.uv);

	float3 p = getPosition(i.uv);
	float3 n = normalize(normal_blend.xyz);
	float2 rand = getRandom(i.uv);

	float ao = 0.0f;
	float rad = g_sample_rad/p.z;

	float intensity = g_intensity * normal_blend.w;
	float scale = g_scale;
	float blendpow2 = 1.f;

	// normal_blend.w 값 : 지형 1, 캐릭터 0.4
	// 1. 지형에 지형 관련 수치를 정해야 함. ( 영향받는 거리가 늘어남 (자동차밑, 의자밑) )
	// 2. 지형에 캐릭터 관련 수치를 정함 ( scale = g_scale/blendpow2(0.16); 영향거리가 줄어듬 ) ( intensity/blendpow2(0.16) 많이 어두워짐. )
	// 3. 캐릭터에 캐릭터 관련 수치를 정함 ( scale = g_scale/blendpow2(0.16); 영향거리가 줄어듬 ) ( intensity/normal_blend.w(0.4) 약간 어두워짐.  )

	//**SSAO Calculation**//
	int iterations = 4;
	for (int j = 0; j < iterations; ++j)
	{
		float2 coord1 = reflect(vec[j],rand)*rad;
		float2 coord2 = float2(coord1.x*0.707 - coord1.y*0.707,coord1.x*0.707 + coord1.y*0.707);
		float2 coord3 = float2(coord1.x*0.707 + coord1.y*0.707,coord1.x*0.707 - coord1.y*0.707);
		coord1 += rand * 0.001f;
		coord2 += rand * 0.001f;

		normal_blend = tex2D(g_buffer_norm, i.uv + coord1*0.25);
		blendpow2 = normal_blend.w * normal_blend.w;
		scale = g_scale / blendpow2;	// 지형에 영향받으면 길게, 캐릭터에 영향받으면 짧게
		ao += doAmbientOcclusion(i.uv, coord1*0.25, p, n, scale, intensity/blendpow2 );

		normal_blend = tex2D(g_buffer_norm, i.uv + coord2*0.5);
		scale = g_scale / blendpow2;	// 지형에 영향받으면 길게, 캐릭터에 영향받으면 짧게
		ao += doAmbientOcclusion(i.uv, coord2*0.5, p, n, scale, intensity/blendpow2 );

		normal_blend = tex2D(g_buffer_norm, i.uv + coord1*0.75);
		scale = g_scale / blendpow2;	// 지형에 영향받으면 길게, 캐릭터에 영향받으면 짧게
		ao += doAmbientOcclusion(i.uv, coord1*0.75, p, n, scale, intensity/blendpow2 );

		normal_blend = tex2D(g_buffer_norm, i.uv + coord2);
		scale = g_scale / blendpow2;	// 지형에 영향받으면 길게, 캐릭터에 영향받으면 짧게
		ao += doAmbientOcclusion(i.uv, coord2, p, n, scale, intensity/blendpow2 );
	}
	//ao /= (float)iterations*4.f;
	//**END**//

	ao = pow( ao, 1.f );	// /(normal_blend.w*normal_blend.w) );
	ao = 1.f - ao;

	ao = ao * ao;

	//Do stuff here with your occlusion value “ao”: modulate ambient lighting, write it to a buffer for later //use, etc.
	return float4(ao, ao, ao, 1.0f);
//	return float4(rand, 1.f, 1.0f);
//	return float4(n, 1.0f);
}

float4 PS_Blur(VS_OUTPUT i) : COLOR0
{
	float3 outColor = 0.0f;

	float2 newUV = i.uv + 0.5 / g_screen_size;
	float2 pixelsizeHalf = 0.5f / g_screen_size;
	float2 pixelsize = 1.f / g_screen_size;
	float2 pixelsize2 = 2.f / g_screen_size;

//	return float4( tex2D(g_ssao, newUV).xyz, 1.f );

//	outColor = tex2D(g_ssao, i.uv).xyz * 0.28;

//	outColor += tex2D(g_ssao, newUV+float2(0,pixelsize.y)).xyz * 0.18f;
//	outColor += tex2D(g_ssao, newUV+float2(pixelsize.x,-pixelsize.y)).xyz * 0.18f;
//	outColor += tex2D(g_ssao, newUV+float2(-pixelsize2.x,0)).xyz * 0.18f;
//	outColor += tex2D(g_ssao, newUV+float2(-pixelsize.x,-pixelsize2.y)).xyz * 0.18f;

	outColor = tex2D(g_ssao, newUV).xyz*0.25;
	outColor += tex2D(g_ssao, newUV+float2(pixelsize2.x,0)).xyz * 0.125;
	outColor += tex2D(g_ssao, newUV+float2(-pixelsize2.x,0)).xyz * 0.125;
	outColor += tex2D(g_ssao, newUV+float2(0,pixelsize2.y)).xyz * 0.125;
	outColor += tex2D(g_ssao, newUV+float2(0,-pixelsize2.y)).xyz * 0.125;
	outColor += tex2D(g_ssao, newUV+float2(pixelsize2.x,pixelsize2.y)).xyz * 0.0625;
	outColor += tex2D(g_ssao, newUV+float2(pixelsize2.x,-pixelsize2.y)).xyz * 0.0625;
	outColor += tex2D(g_ssao, newUV+float2(-pixelsize2.x,pixelsize2.y)).xyz * 0.0625;
	outColor += tex2D(g_ssao, newUV+float2(-pixelsize2.x,-pixelsize2.y)).xyz * 0.0625;

	float3 p = getPosition(i.uv);
	float fFog = saturate((g_vFog.x - p.z) / g_vFog.y);
	outColor = (outColor*fFog) + float3( 1.f-fFog, 1.f-fFog, 1.f-fFog );

	return float4(outColor,1.f);
}

technique SSAO
{
	pass P0
	{ 
		VertexShader = compile vs_3_0 VS();
		PixelShader = compile ps_3_0 PS();
		ZEnable = false;
	}

	pass Blur
	{ 
		VertexShader = compile vs_3_0 VS();
		PixelShader = compile ps_3_0 PS_Blur();
		ZEnable = false;
		AlphaBlendEnable = true;
		SrcBlend = Zero;
		DestBlend = SrcColor;
	}
}