
//--------------------------------------------------------------------------------------
// Global variables
//--------------------------------------------------------------------------------------

float4x4 g_matWV;
float4x4 g_matWVP;
float4x4 g_matShadow;
float4   g_vLightDiffuse;
float3   g_vLightAmbient;
float3   g_vLightDir;
float3   g_vLightDirViewSpace;
float3   g_vCameraFrom; 
float2   g_vFOG;	// FogEnd, FogDist
float3   g_vFogColor;
int      g_nPointLightNum_TOOL;
float4   g_vPos_Range_PL_TOOL[12];
float4   g_vDiff_OverLighting_PL_TOOL[12];
float4   g_vAtt_PL_TOOL[12];
int      g_nConeLightNum_TOOL;
float4   g_vPos_Range_CL_TOOL[3];
float4   g_vDiff_CosPhiHalf_CL_TOOL[3];
float4   g_vAtt_CosHalfThetaSubPhi_CL_TOOL[3];
float4   g_vDirect_Falloff_CL_TOOL[3];
float    g_fSpecularPower1 = 256.f;		// 외부에서 제어가능
float    g_fSpecularIntensity1 = 2.f;		// 외부에서 제어가능
float    g_fSpecularPower2 = 256.f;		// 외부에서 제어가능
float    g_fSpecularIntensity2 = 2.f;		// 외부에서 제어가능
float    g_fOverLightPowerLM = 3.f;
float    g_fOverLightPower_PS_2_0 = 1.f;
float    g_fVS_1_1_ColorMulti = 0.75f;
float4   g_vWindowSize;
float    g_fAlpha;						// Hiding piece model behind a player character
float2   g_vDayNightWeight;
float2	 g_vLightMapUV_Offset;
float2   g_vLightMapUV_Multiply; 
float3	 g_vVoxelColor; 
float    g_fTime; 
float3   g_vShadow;	// x-SIZE, y-EPSILON, z-COLOR

//--------------------------------------------------------------------------------------
// UV 회전 함수
//--------------------------------------------------------------------------------------

float2 rotateUVs(float2 Texcoords, float2 center, float theta)
{ 
 
	//  theta(Radian)에 대한 sin 과 cos 값 계산
	float2 sc; 
	sincos( theta, sc.x, sc.y ); 
	// 회전중심을 원점으로 이동
	float2 uv = Texcoords - center; 
	// UV 회전 
	float2 rotateduv; 
	rotateduv.x = dot( uv, float2( sc.y, -sc.x ) ); 
	rotateduv.y = dot( uv, sc.xy );	 
	// 회전된 UV를 정확한 위치로 다시 이동
	rotateduv += center; 
 
	return rotateduv; 
} 


//--------------------------------------------------------------------------------------
// Material Parameters
//--------------------------------------------------------------------------------------

texture g_BackBufferTexture; 
sampler BackBufferSampler = sampler_state  
{
    Texture = (g_BackBufferTexture); 	
};

texture g_BaseTexture;
sampler BaseTexSampler = sampler_state 
{
    Texture = (g_BaseTexture);
};

texture g_BaseTexture2;
sampler BaseTex2Sampler = sampler_state 
{
    Texture = (g_BaseTexture2);
};

texture g_LightMapDayTex;
sampler LightMapTexSampler_1st = sampler_state 
{
    Texture = (g_LightMapDayTex);	
};

texture g_LightMapTex_DS_1st; 
sampler LightMapTexSampler_DS_1st = sampler_state  
{ 
	MinFilter = Linear; 
	MagFilter = Linear; 
	Texture = (g_LightMapTex_DS_1st); 
}; 
 
texture g_LightMapTex_DS_2nd; 
sampler LightMapTexSampler_DS_2nd = sampler_state  
{ 
	MinFilter = Linear; 
	MagFilter = Linear; 
	Texture = (g_LightMapTex_DS_2nd); 
}; 

texture g_ShadowTexture;
sampler ShadowTexSampler = sampler_state 
{
    Texture = (g_ShadowTexture);

    MinFilter = Point;
    MagFilter = Point;
    AddressU = Clamp;
    AddressV = Clamp;
};

texture g_GlobalCubeTexture;
samplerCUBE CubeTexSampler = sampler_state 
{
    Texture = (g_GlobalCubeTexture);

//    AddressU = Wrap;
//    AddressV = Wrap;
//    AddressW = Wrap;

    AddressU = Wrap;
    AddressV = Wrap;
 //   AddressW = Wrap;

//   MinFilter = Point;
//   MagFilter = Point;
   // MipFilter = Point;
};

texture g_MaskTexture;
sampler MaskTexSampler = sampler_state
{
	Texture = (g_MaskTexture);
};

float g_fRotate_UV;				//UV 회전값 (도);
float g_fScaleFactor;			//UV 크기값 (배수);

 
//--------------------------------------------------------------------------------------
// 	Basic vertex transformation
//--------------------------------------------------------------------------------------

// TOOL
struct VS_INPUT_TOOL
{
    float4 m_vPosition  : POSITION;
    float4 m_vVertColor : COLOR0;
    float3 m_vNormal    : NORMAL;
    float3 m_vBinormal	: BINORMAL;
    float3 m_vTangent	: TANGENT;
    float2 m_vTexCoord0 : TEXCOORD0;
} Vertex_tool : SEMANTIC_VS_INPUT_TOOL; 

struct VS_OUTPUT_TOOL
{
    float4 m_vPosition  	: POSITION;
    float4 m_vVertColor    	: COLOR0;
    float2 m_vTexCoord0 	: TEXCOORD0;
    float3 m_vNormal		: TEXCOORD1;
    float3 m_vBinormal		: TEXCOORD2;
    float3 m_vTangent		: TEXCOORD3;
    float3 m_vPosViewSpace      : TEXCOORD4; 
};

struct PS_DEFFERED_TOOL_OUT	
{ 
    float4 m_vColor0		: COLOR0; 	// Albedo
    float4 m_vColor1		: COLOR1; 	// Normal
    float4 m_vColor2		: COLOR2; 	// Specular(Intensity,Power), CubeMap
    float4 m_vColor3		: COLOR3; 	// Position
}; 

// Runtime 
struct VS_INPUT		
{
    float4 m_vPosition   : POSITION;
    float4 m_vVertColor  : COLOR0;
    float3 m_vNormal     : NORMAL;
    float3 m_vBinormal	: BINORMAL;
    float3 m_vTangent	 : TANGENT;
    float2 m_vTexCoord0  : TEXCOORD0;
    float2 m_vLightmapUV : TEXCOORD1;	// LightMap를 사용할 경우, m_vLightmapUV 이름을 유지하며, m_vLightmapUV 이름이 있으면, LightMap을 생성해준다.

} Vertex : SEMANTIC_VS_INPUT;

struct VS_OUTPUT_1		
{
    float4 m_vPosition  : POSITION;
    float4 m_vVertColor : COLOR0;
    float2 m_vTexCoord0 : TEXCOORD0;
    float2 m_vTexCoord1 : TEXCOORD1;
    float2 m_vLightmapUV  : TEXCOORD2;
    float2 m_vLightmapUV1 : TEXCOORD3;
    float  m_fFog         : FOG;
};

struct VS_OUTPUT_ALBEDO	
{ 
    float4 m_vPosition  : POSITION; 
    float4 m_vVertColor : COLOR0;
    float2 m_vTexCoord0 : TEXCOORD0; 
    float2 m_vLightmapUV  : TEXCOORD1;
    float3 m_vPosViewSpace : TEXCOORD2;
    float  m_fFog         : FOG;
}; 

struct VS_OUTPUT_ALBEDO_3
{ 
    float4 m_vPosition  : POSITION; 
    float4 m_vVertColor : COLOR0;
    float2 m_vTexCoord0 : TEXCOORD0;
    float2 m_vLightmapUV  : TEXCOORD1; 
    float3 m_vNormal 	: TEXCOORD2;
    float3 m_vBinormal	: TEXCOORD3;
    float3 m_vTangent	: TEXCOORD4;
    float  m_fFog         : FOG;
}; 
			 
struct PS_DEFFERED_OUT		// game 렌더용 
{ 
    float4 m_vColor0	: COLOR0; 
    float4 m_vColor1	: COLOR1; 
    float4 m_vColor2	: COLOR2; 
}; 
 
struct PS_DEFFERED_HIGH_OUT		// game 렌더용 
{ 
    float4 m_vColor0	: COLOR0; 
    float4 m_vColor1	: COLOR1; 
    float4 m_vColor2	: COLOR2;
    float4 m_vColor3	: COLOR3;
};


//---------------------------------------------------------------------------------------------------------- 
//					N o r m a l 
float3 GetNormal_Tool( VS_OUTPUT_TOOL In, float4 vMaskColor ) 
{ 
    // If using a signed texture, we must unbias the normal map data 
    float3 Normal; 
    Normal.xy = (vMaskColor.wy * 2) - 1; 
	 Normal.z = sqrt( 1.f - dot( Normal.xy, Normal.xy ) ); 
    
    // Move the normal from tangent space to world space 
    float3x3 mTangentFrame = { In.m_vTangent, In.m_vBinormal, In.m_vNormal }; 
    Normal = mul( Normal, mTangentFrame ); 
    Normal = normalize( Normal ); 
 
    return Normal;  
}  

//---------------------------------------------------------------------------------------------------------- 
//					N o r m a l 
float3 GetNormal( VS_OUTPUT_ALBEDO_3 In, float4 vMaskColor ) 
{ 
    // If using a signed texture, we must unbias the normal map data 
    float3 Normal; 
    Normal.xy = (vMaskColor.wy * 2) - 1; 
	 Normal.z = sqrt( 1.f - dot( Normal.xy, Normal.xy ) ); 
    
    // Move the normal from tangent space to world space 
    float3x3 mTangentFrame = { In.m_vTangent, In.m_vBinormal, In.m_vNormal }; 
    Normal = mul( Normal, mTangentFrame ); 
    Normal = normalize( Normal ); 
 
    return Normal;  
}  

//----------------------------------------------------------------------------------------------------------
//	 Tool Shader
//----------------------------------------------------------------------------------------------------------

// Vertex Shader
	
VS_OUTPUT_TOOL VS_tool( VS_INPUT_TOOL In ) 
{
    VS_OUTPUT_TOOL Out;
    
    // Now that we have the calculated weight, add in the final influence
    Out.m_vPosition = mul(g_matWVP, In.m_vPosition);
    Out.m_vPosViewSpace = mul(g_matWV, In.m_vPosition);
    Out.m_vNormal = normalize( mul(g_matWV, In.m_vNormal) );
    Out.m_vBinormal = normalize( mul(g_matWV, In.m_vBinormal) );
    Out.m_vTangent = normalize( mul(g_matWV, In.m_vTangent) );

    // 텍스처 좌표 건네주기
    Out.m_vTexCoord0 = In.m_vTexCoord0;

    Out.m_vVertColor = In.m_vVertColor;
   
    return Out;
}

//	Pixel Shader
	
PS_DEFFERED_TOOL_OUT PS_tool( VS_OUTPUT_TOOL In )
{  
    PS_DEFFERED_TOOL_OUT vOut; 
 
    float2 INUV = In.m_vTexCoord0.xy; 
    float2 UVRotator_center = float2(0.0, 0.0);	 //no center values wired to UV Rotate Node - using defaults 
    float UVRotAngle = radians(g_fRotate_UV);	//  DegToRad
    float2 UVRotator = rotateUVs(INUV, UVRotator_center, UVRotAngle); 

    float4 vColor = tex2D( BaseTexSampler, INUV);    
    float4 vColor2 = tex2D(BaseTex2Sampler, (g_fScaleFactor * UVRotator).xy);	
    vColor = lerp(vColor2, vColor, In.m_vVertColor.w);
 
    vColor.xyz *= In.m_vVertColor.xyz; 
 
    float4 vMaskColor = tex2D( MaskTexSampler, In.m_vTexCoord0 );

    float3 vNor1 = GetNormal_Tool( In, vMaskColor );
    float3 vNor2 = normalize( In.m_vNormal );
    float3 vNor = lerp(vNor2, vNor1, In.m_vVertColor.w);

    float2 vSpec1 = float2( g_fSpecularIntensity1*vMaskColor.r, g_fSpecularPower1 );
    float2 vSpec2 = float2( g_fSpecularIntensity2*vMaskColor.r, g_fSpecularPower2 );
    float2 vSpec = lerp(vSpec2, vSpec1, In.m_vVertColor.w);
 
    vOut.m_vColor0 = vColor; 
    vOut.m_vColor1 = float4(((vNor+1.f)*0.5f),1.f); 
    vOut.m_vColor2 = float4( vSpec.x*0.5f, vSpec.y/256.f, 0.f, 1.f); 
    vOut.m_vColor3 = 1.f;
    vOut.m_vColor3.x = 0.f;
 
    return vOut; 
}



//----------------------------------------------------------------------------------------------------------
// Runtime Shader	ps.1.1
//----------------------------------------------------------------------------------------------------------

// Vertex Shader

VS_OUTPUT_1 VS_1( VS_INPUT In ) 
{
    VS_OUTPUT_1 Out;
    
    // Now that we have the calculated weight, add in the final influence
    Out.m_vPosition = mul(g_matWVP, In.m_vPosition);

    // Pass the texture coordinate
    Out.m_vTexCoord0 = In.m_vTexCoord0;

    float2 UVRotator_center = float2(0.0, 0.0);	 //no center values wired to UV Rotate Node - using defaults 
    float UVRotAngle = radians(g_fRotate_UV);	//  DegToRad
    float2 UVRotator = rotateUVs(In.m_vTexCoord0, UVRotator_center, UVRotAngle); 
    Out.m_vTexCoord1 = (g_fScaleFactor * UVRotator).xy;

    Out.m_vLightmapUV = In.m_vLightmapUV;
    Out.m_vLightmapUV.xy += g_vLightMapUV_Offset;
    Out.m_vLightmapUV1 = Out.m_vLightmapUV;

    Out.m_vVertColor = In.m_vVertColor;
    Out.m_vVertColor.xyz *= g_fVS_1_1_ColorMulti;

    Out.m_fFog = saturate((g_vFOG.x - Out.m_vPosition.z) / g_vFOG.y);
   
    return Out;
}

//	Pixel Shader

float4 PS_1( VS_OUTPUT_1 In ) : COLOR0 
{
    // Texture
    float4 vColor = tex2D( BaseTexSampler, In.m_vTexCoord0 );
    float4 vColor2 = tex2D(BaseTex2Sampler, In.m_vTexCoord1 );
    vColor = lerp(vColor2, vColor, In.m_vVertColor.w);

	float4 vLightColor = tex2D( LightMapTexSampler_1st, In.m_vLightmapUV );

	vColor.xyz *= vLightColor.xyz * In.m_vVertColor.xyz * 4.f;
    vColor.w = g_fAlpha;

    return vColor;
}



//----------------------------------------------------------------------------------------------------------
// Runtime Shader	ps.2.0 Albedo
//----------------------------------------------------------------------------------------------------------

// Vertex Shader

VS_OUTPUT_ALBEDO VS_2( VS_INPUT In ) 
{
    VS_OUTPUT_ALBEDO Out;
    
    // Now that we have the calculated weight, add in the final influence
    Out.m_vPosition = mul(g_matWVP, In.m_vPosition);
    Out.m_vPosViewSpace = mul(g_matWV, In.m_vPosition).xyz;

    // Pass the texture coordinate
    Out.m_vTexCoord0 = In.m_vTexCoord0;
    Out.m_vLightmapUV = In.m_vLightmapUV;
    Out.m_vLightmapUV += g_vLightMapUV_Offset;
    Out.m_vVertColor = In.m_vVertColor;
   
    Out.m_fFog = 1.f;

    return Out;
}


//	Pixel Shader

PS_DEFFERED_OUT PS_2( VS_OUTPUT_ALBEDO In )
{
    PS_DEFFERED_OUT vOut; 

    // Texture
    float2 INUV = In.m_vTexCoord0.xy; 
    float2 UVRotator_center = float2(0.0, 0.0);	 //no center values wired to UV Rotate Node - using defaults 
    float UVRotAngle = radians(g_fRotate_UV);	//  DegToRad
    float2 UVRotator = rotateUVs(INUV, UVRotator_center, UVRotAngle); 

    float4 vColor = tex2D( BaseTexSampler, INUV);
    float4 vColor2 = tex2D(BaseTex2Sampler, (g_fScaleFactor * UVRotator).xy);

    vColor = lerp(vColor2, vColor, In.m_vVertColor.w);
    vColor.xyz *= In.m_vVertColor.xyz;
    vColor.w = g_fAlpha;

    vOut.m_vColor0 = vColor; 
    vOut.m_vColor1 = 1.f;
    vOut.m_vColor1.xyz = tex2D( LightMapTexSampler_1st, In.m_vLightmapUV ) * g_fOverLightPowerLM * g_fOverLightPower_PS_2_0 / 3;
    vOut.m_vColor2 = 1.f;
    vOut.m_vColor2.xyz = In.m_vPosViewSpace;

    return vOut;
}

//---------------------------------------------------------------------------------------------------------- 
//					game 렌더용	ps.3.0 Albedo 
VS_OUTPUT_ALBEDO_3 VS_3( VS_INPUT In )  
{ 
    VS_OUTPUT_ALBEDO_3 Out;
    
    // Now that we have the calculated weight, add in the final influence
    Out.m_vPosition = mul(g_matWVP, In.m_vPosition);
    Out.m_vNormal = normalize( mul(g_matWV, In.m_vNormal) ); 
    Out.m_vBinormal = normalize( mul(g_matWV, In.m_vBinormal) );
    Out.m_vTangent = normalize( mul(g_matWV, In.m_vTangent) );

    // Pass the texture coordinate
    Out.m_vTexCoord0 = In.m_vTexCoord0;
    Out.m_vLightmapUV = In.m_vLightmapUV;
    Out.m_vLightmapUV += g_vLightMapUV_Offset;
    Out.m_vVertColor = In.m_vVertColor;
   
    Out.m_fFog = 1.f;

    return Out;
} 

float ExportVector2ToFloat( float fX, float fY ) // fX 와 fY 는 1 이하여야 한다. 	
{ 
    float2 vecEncode = floor( float2( fX * 255.0f, fY * 255.0f ) ); 
    float fResult = ( ( vecEncode.x * 256.0f ) + vecEncode.y ) / 65535.0f; 
    return fResult; 
} 

PS_DEFFERED_HIGH_OUT PS_3( VS_OUTPUT_ALBEDO_3 In, uniform bool bAlphaTex, uniform int nPL1, uniform int nPL2 ) 
{ 
    PS_DEFFERED_HIGH_OUT vOut; 

    // Texture
    float2 INUV = In.m_vTexCoord0.xy; 
    float2 UVRotator_center = float2(0.0, 0.0);	 //no center values wired to UV Rotate Node - using defaults 
    float UVRotAngle = radians(g_fRotate_UV);	//  DegToRad
    float2 UVRotator = rotateUVs(INUV, UVRotator_center, UVRotAngle); 

    float4 vColor = tex2D( BaseTexSampler, INUV);
    float4 vColor2 = tex2D(BaseTex2Sampler, (g_fScaleFactor * UVRotator).xy);

    float3 vNorNormalize = normalize( In.m_vNormal ); 
    float3 vNorConvert = (vNorNormalize+1.f)*0.5f; 

    vColor = lerp(vColor2, vColor, In.m_vVertColor.w);
    vColor.xyz *= In.m_vVertColor.xyz;
    vColor.w = g_fAlpha;
 
    float4 vMaskColor = tex2D( MaskTexSampler, In.m_vTexCoord0 );

    float3 vNor1 = GetNormal( In, vMaskColor );
    float3 vNor2 = normalize( In.m_vNormal );
    float3 vNor = lerp(vNor2, vNor1, In.m_vVertColor.w);

    float2 vSpec1 = float2( g_fSpecularIntensity1*vMaskColor.r, g_fSpecularPower1 );
    float2 vSpec2 = float2( g_fSpecularIntensity2*vMaskColor.r, g_fSpecularPower2 );
    float2 vSpec = lerp(vSpec2, vSpec1, In.m_vVertColor.w);

    // 라이트맵 
    float4 vLightMap_Indirect = tex2D( LightMapTexSampler_1st, In.m_vLightmapUV ) * 3.f;
    float4 vLightMap_DS_1st = tex2D( LightMapTexSampler_DS_1st, In.m_vLightmapUV );
    float4 vLightMap_DS_2nd = tex2D( LightMapTexSampler_DS_2nd, In.m_vLightmapUV );

    float3 vAddColor = vLightMap_Indirect.xyz;
    if ( nPL1==1 )
    {
    	float fLightAmount = vLightMap_DS_1st.z;
    	fLightAmount *= vLightMap_DS_1st.x*3.f; 
    	vAddColor += g_vDiff_CosPhiHalf_CL_TOOL[0].xyz * fLightAmount;
    }
    if ( nPL2==1 )
    {
    	float fLightAmount = vLightMap_DS_2nd.z;
    	fLightAmount *= vLightMap_DS_2nd.x*3.f; 
    	vAddColor += g_vDiff_CosPhiHalf_CL_TOOL[1].xyz * fLightAmount;
    }

    vOut.m_vColor0 = vColor; 
    vOut.m_vColor1 = float4(normalize(vNor),vColor.w); 
    vOut.m_vColor2 = float4(vSpec.x*0.5f,vSpec.y/256.f,0.f,vColor.w); 
    vOut.m_vColor3.x = ExportVector2ToFloat( min(vAddColor.x * 0.33333333f, 1.f), min(vAddColor.y * 0.33333333f, 1.f) ); 
    vOut.m_vColor3.y = ExportVector2ToFloat( min(vAddColor.z * 0.33333333f, 1.f), vNorConvert.x ); 
    vOut.m_vColor3.z = ExportVector2ToFloat( vNorConvert.y, vNorConvert.z ); 
    if ( bAlphaTex ) 
    { 
	vOut.m_vColor3.w = 1.f; 
    } 
    else 
    { 
	vOut.m_vColor3.w = vLightMap_DS_1st.z + 0.00001f;	// 00001f - 라이트맵은 PS_LSPASS 에서 빛계산을 해야하기 때문에 Zero 로 만들지 못하게 한다.
    } 

    return vOut; 
} 

//---------------------------------------------------------------------------------------------------------- 
//					technique 
//----------------------------------------------------------------------------------------------------------
 
technique tool 
{ 
	pass high 
	{ 
		VertexShader = compile vs_3_0 VS_tool(); 
		PixelShader = compile ps_3_0 PS_tool(); 
	} 
}; 
 
technique runtime_1 
{ 
	//------------------------------------------------------------------------------------- 
	// LightMap 
	pass high 
	{ 
		VertexShader = compile vs_1_1 VS_1(); 
		PixelShader = compile ps_1_1 PS_1(); 
	} 
} 

technique runtime_2 
{ 
	//------------------------------------------------------------------------------------- 
	// LightMap 
	pass albedo 
	{ 
		VertexShader = compile vs_3_0 VS_2(); 
		PixelShader = compile ps_3_0 PS_2(); 
	} 
}

technique runtime_3 
{ 
	//------------------------------------------------------------------------------------- 
	// LightMap 
	pass albedo 				// 0
	{ 
		VertexShader = compile vs_3_0 VS_3(); 
		PixelShader = compile ps_3_0 PS_3( false, 0, 0 ); 
	} 
	pass albedo_PL1_ON 		// 1
	{ 
		VertexShader = compile vs_3_0 VS_3(); 
		PixelShader = compile ps_3_0 PS_3( false, 1, 0 ); 
	} 
	pass albedo_PL2_ON 		// 2
	{ 
		VertexShader = compile vs_3_0 VS_3(); 
		PixelShader = compile ps_3_0 PS_3( false, 0, 1 ); 
	} 
	pass albedo_PL1_PL2_ON 	// 3
	{ 
		VertexShader = compile vs_3_0 VS_3(); 
		PixelShader = compile ps_3_0 PS_3( false, 1, 1 ); 
	} 
	pass albedo_AlphTex 				// 0
	{ 
		VertexShader = compile vs_3_0 VS_3(); 
		PixelShader = compile ps_3_0 PS_3( true, 0, 0 ); 
	} 
	pass albedo_PL1_ON_AlphTex 		// 1
	{ 
		VertexShader = compile vs_3_0 VS_3(); 
		PixelShader = compile ps_3_0 PS_3( true, 1, 0 ); 
	} 
	pass albedo_PL2_ON_AlphTex 		// 2
	{ 
		VertexShader = compile vs_3_0 VS_3(); 
		PixelShader = compile ps_3_0 PS_3( true, 0, 1 ); 
	} 
	pass albedo_PL1_PL2_ON_AlphTex 	// 3
	{ 
		VertexShader = compile vs_3_0 VS_3(); 
		PixelShader = compile ps_3_0 PS_3( true, 1, 1 ); 
	} 
}
