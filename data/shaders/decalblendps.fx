//-------------------------------------------------------------------------------------- 
//       shhan 
//-------------------------------------------------------------------------------------- 
 
//-------------------------------------------------------------------------------------- 
// Global variables 
//-------------------------------------------------------------------------------------- 
float4x4 g_matWV; 
float4x4 g_matWVP; 
float4   g_vLightDiffuse; 
float3   g_vLightAmbient; 
float3   g_vLightDirViewSpace; 
float2   g_vFOG;	// FogEnd, FogDist 
float3   g_vFogColor; 
int      g_nPointLightNum_TOOL; 
float4   g_vPos_Range_PL_TOOL[12]; 
float4   g_vDiff_OverLighting_PL_TOOL[12]; 
float4   g_vAtt_PL_TOOL[12]; 
int      g_nConeLightNum_TOOL; 
float4   g_vPos_Range_CL_TOOL[3]; 
float4   g_vDiff_CosPhiHalf_CL_TOOL[3]; 
float4   g_vAtt_CosHalfThetaSubPhi_CL_TOOL[3]; 
float4   g_vDirect_Falloff_CL_TOOL[3]; 
float    g_fAttEndLineValue_CL_TOOL[3]; 
float    g_fOverLighting_CL_TOOL[3]; 
float    g_fSpecularPower = 256.f; 
float    g_fSpecularIntensity = 0.f; 
float    g_fAlpha; 
float2   g_vLightMapUV_Offset; 
float2   g_vLightMapUV_Multiply; 
float3	  g_vVoxelColor; 
float3	  g_vDecalBelndColor; 
float    g_fTime; 
float	  g_fCubeMapValue;
float	  g_fDecalNormal=1.f;
float	g_fSeamless_Near_Distance;
float	g_fSeamless_Far_Distance;
float	g_fSeamless_Far_Alpha;
float	g_fSeamless_Near_Alpha;

 
texture g_BaseTexture; 
sampler BaseTexSampler = sampler_state  
{ 
	Texture = (g_BaseTexture); 
}; 

texture g_MaskTexture; 
sampler MaskTexSampler = sampler_state  
{ 
	Texture = (g_MaskTexture); 
}; 
 
//-------------------------------------------------------------------------------------- 
// Basic vertex transformation 
//-------------------------------------------------------------------------------------- 
struct VS_INPUT_TOOL		// editer ������ 
{ 
	float4 m_vPosition  : POSITION; 
	float3 m_vNormal    : NORMAL; 
	float4 m_vColor0    : COLOR0; 
	float2 m_vTexCoord0 : TEXCOORD0; 
} Vertex_tool : SEMANTIC_VS_INPUT_TOOL; 
 
struct VS_OUTPUT_TOOL	// editer ������ 
{ 
	float4 m_vPosition  	: POSITION; 
	float2 m_vTexCoord0 	: TEXCOORD0; 
	float3 m_vPosViewSpace	: TEXCOORD1; 
	float3 m_vNorViewSpace	: TEXCOORD2; 
	float4 m_vColor0    	: TEXCOORD3; 
}; 

struct PS_DEFFERED_TOOL_OUT	
{ 
    float4 m_vColor0		: COLOR0; 	// Albedo
    float4 m_vColor1		: COLOR1; 	// Normal
    float4 m_vColor2		: COLOR2; 	// Specular(Intensity,Power), CubeMap
    float4 m_vColor3		: COLOR3; 	// Position
}; 
 
struct VS_INPUT_DECAL		// game ������ 
{ 
	float4 m_vPosition  : POSITION; 
	float3 m_vNormal    : NORMAL; 
	float4 m_vColor0    : COLOR0; 
	float2 m_vTexCoord0 : TEXCOORD0; 
} Vertex : SEMANTIC_VS_INPUT; 
 
struct VS_OUTPUT_DECAL		// game ������ 
{ 
	float4 m_vPosition		: POSITION; 
	float4 m_vColor0		: COLOR0; 
	float2 m_vTexCoord0		: TEXCOORD0; 
	float3 m_vNormal		: TEXCOORD1; 
	float3 m_vPositionViewSpace	: TEXCOORD2; 
	float  m_fFog			: FOG; 
}; 
 
struct PS_DEFFERED_OUT		// game ������ 
{ 
	float4 m_vColor0	: COLOR0; 
	float4 m_vColor1	: COLOR1; 
	float4 m_vColor2	: COLOR2; 
}; 
 
//---------------------------------------------------------------------------------------------------------- 
//				TOOL		editer ������ 
VS_OUTPUT_TOOL VS_TOOL( VS_INPUT_TOOL In )  
{ 
	VS_OUTPUT_TOOL Out; 
 
	// Now that we have the calculated weight, add in the final influence 
	Out.m_vPosition = mul(g_matWVP, In.m_vPosition); 
	Out.m_vPosViewSpace = mul(g_matWV, In.m_vPosition); 
	Out.m_vNorViewSpace = normalize( mul(g_matWV, In.m_vNormal) ); 
 
	// Get Color 
	Out.m_vColor0 = In.m_vColor0; 
 
	// Pass the texture coordinate 
	Out.m_vTexCoord0 = In.m_vTexCoord0; 
 
	return Out; 
} 
 
PS_DEFFERED_TOOL_OUT PS_TOOL( VS_OUTPUT_TOOL In, uniform int nDecalMode )
{ 
	PS_DEFFERED_TOOL_OUT vOut; 

	// Texture 
	float4 vColor = tex2D( BaseTexSampler, In.m_vTexCoord0 ); 
 
        vColor *= In.m_vColor0; 

	float3 vNor = normalize( In.m_vNorViewSpace ); 

        float3 vLength = In.m_vPosViewSpace;
        float fLength = length( vLength );

        float fRate = (fLength - g_fSeamless_Near_Distance) / (g_fSeamless_Far_Distance - g_fSeamless_Near_Distance); // ������ 0, �ּ��� 1
        fRate = saturate( fRate );
        float fSeamlessAlpha = (g_fSeamless_Far_Alpha - g_fSeamless_Near_Alpha) * fRate + g_fSeamless_Near_Alpha;
	vColor.w *= fSeamlessAlpha;

	vOut.m_vColor0 = vColor; 
	vOut.m_vColor1 = float4( ((vNor+1.f)*0.5f), vColor.w*g_fDecalNormal ); 
   	vOut.m_vColor2 = float4( g_fSpecularIntensity*0.5f, g_fSpecularPower/256.f, 0.f, vColor.w*g_fDecalNormal ); 
   	vOut.m_vColor3 = 1.f;
   	vOut.m_vColor3.xyz = In.m_vPosViewSpace; 

   	if ( nDecalMode == 1 )	// Multiply
	{
		vOut.m_vColor1 = 1.f;
		vOut.m_vColor2 = 1.f;
		vOut.m_vColor3 = 1.f;
	}
   	else if ( nDecalMode == 2 )	// Add
	{
		vOut.m_vColor1 = 0.f;
		vOut.m_vColor2 = 0.f;
		vOut.m_vColor3 = 0.f;
	}

 	return vOut; 
} 

//---------------------------------------------------------------------------------------------------------- 
//					P	S		game ������ Decal 
 
VS_OUTPUT_DECAL VS_DECAL( VS_INPUT_DECAL In, uniform bool bFogEnable )  
{ 
	VS_OUTPUT_DECAL Out; 
 
	// Now that we have the calculated weight, add in the final influence 
	Out.m_vPosition = mul(g_matWVP, In.m_vPosition); 
	Out.m_vNormal = normalize( mul(g_matWV, In.m_vNormal) ); 
	Out.m_vPositionViewSpace = mul(g_matWV, In.m_vPosition);
 
	// Get Color 
	Out.m_vColor0 = In.m_vColor0; 
 
	// Pass the texture coordinate 
	Out.m_vTexCoord0 = In.m_vTexCoord0; 
 
	Out.m_fFog = 1.f; 
	if ( bFogEnable ) 
	{ 
		Out.m_fFog = saturate((g_vFOG.x - Out.m_vPosition.z) / g_vFOG.y); 
	} 
 
	return Out; 
} 
 
PS_DEFFERED_OUT PS_DECAL( VS_OUTPUT_DECAL In, uniform bool bBlendColor, uniform int nDecalMode )  
{ 
	PS_DEFFERED_OUT vOut; 
 
	// Texture 
	float4 vColor = tex2D( BaseTexSampler, In.m_vTexCoord0 ); 
 
	vColor *= In.m_vColor0; 
 
	if ( bBlendColor ) 
	{ 
		vColor.xyz = (vColor.xyz*vColor.w)+(g_vDecalBelndColor*(1.f-vColor.w)); 
	} 

	float3 vLength = In.m_vPositionViewSpace;
        float fLength = length( vLength );

        float fRate = (fLength - g_fSeamless_Near_Distance) / (g_fSeamless_Far_Distance - g_fSeamless_Near_Distance); // ������ 0, �ּ��� 1
        fRate = saturate( fRate );
        float fSeamlessAlpha = (g_fSeamless_Far_Alpha - g_fSeamless_Near_Alpha) * fRate + g_fSeamless_Near_Alpha;
	vColor.w *= fSeamlessAlpha;
 
	vOut.m_vColor0 = vColor; 
	vOut.m_vColor1 = float4(normalize(In.m_vNormal),vColor.w*g_fDecalNormal); 
        //vOut.m_vColor2 = float4(g_fSpecularIntensity*0.5f,g_fSpecularPower/256.f,g_fCubeMapValue,vColor.w*g_fDecalNormal); 
        vOut.m_vColor2 = float4( g_fSpecularIntensity*0.5f, g_fSpecularPower/256.f, 0.f, vColor.w*g_fDecalNormal ); 
 
   	if ( nDecalMode == 1 )	// Multiply
	{
		vOut.m_vColor1 = 1.f;
		vOut.m_vColor2 = 1.f;
	}
   	else if ( nDecalMode == 2 )	// Add
	{
		vOut.m_vColor1 = 0.f;
		vOut.m_vColor2 = 0.f;
	}

	return vOut; 
} 
 
//---------------------------------------------------------------------------------------------------------- 
//					technique 
 
technique tool 
{ 
	pass high 
	{ 
		VertexShader = compile vs_3_0 VS_TOOL(); 
		PixelShader = compile ps_3_0 PS_TOOL(0); 
	} 
	pass high_multiply
	{ 
		VertexShader = compile vs_3_0 VS_TOOL(); 
		PixelShader = compile ps_3_0 PS_TOOL(1); 
	}
	pass high_add
	{ 
		VertexShader = compile vs_3_0 VS_TOOL(); 
		PixelShader = compile ps_3_0 PS_TOOL(2); 
	} 
};

technique runtime_3 
{ 
	//------------------------------------------------------------------------------------- 
	// LightMap 
	pass high
	{ 
		VertexShader = compile vs_3_0 VS_DECAL(false); 
		PixelShader = compile ps_3_0 PS_DECAL(false,0); 
	}
	pass high_decal_multiply
	{ 
		VertexShader = compile vs_3_0 VS_DECAL(false); 
		PixelShader = compile ps_3_0 PS_DECAL(false,1); 
	}
	pass pass2_decal_multiply
	{ 
		VertexShader = compile vs_3_0 VS_DECAL(true); 
		PixelShader = compile ps_3_0 PS_DECAL(true,1); 
	}
	pass pass2_decal_add
	{ 
		VertexShader = compile vs_3_0 VS_DECAL(true); 
		PixelShader = compile ps_3_0 PS_DECAL(true,2); 
	}
}; 