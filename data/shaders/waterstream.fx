// 굴곡지는 물에서 사용되는 것임.
// 실시간 반사는 사용되지 않음.
// CubeMap은 사용가능함. 원한다면 작업하면 됨.


//--------------------------------------------------------------------------------------
// Global variables
//--------------------------------------------------------------------------------------

float4x4 g_matWVP;
float4   g_vLightDiffuse;
float3   g_vLightAmbient;
float2   g_vFOG;	// FogEnd, FogDist
float3   g_vFogColor;
float    g_fAlpha;						// Hiding piece model behind a player character
float    g_fTime;
float 	g_fImageScale0;	// 이미지 Scale
float 	g_fImageScale1;	// 이미지 Scale
float2  g_vMoveSpeed0;	// Flow 되는 방향
float2  g_vMoveSpeed1;	// Flow 되는 방향


//--------------------------------------------------------------------------------------
// Material Parameters
//--------------------------------------------------------------------------------------

texture g_BaseTexture;
sampler BaseTexSampler = sampler_state 
{
    Texture = (g_BaseTexture);
};

texture g_BaseTexture2;
sampler BaseTex2Sampler = sampler_state 
{
    Texture = (g_BaseTexture2);
};

 
//--------------------------------------------------------------------------------------
// 	Basic vertex transformation
//--------------------------------------------------------------------------------------

// TOOL
struct VS_INPUT_TOOL
{
    float4 m_vPosition  : POSITION;
    float4 m_vVertColor : COLOR0;
    float2 m_vTexCoord0 : TEXCOORD0;
} Vertex_tool : SEMANTIC_VS_INPUT_TOOL; 

struct VS_OUTPUT_TOOL
{
    float4 m_vPosition  : POSITION;
    float4 m_vVertColor : COLOR0;
    float2 m_vTexCoord0 : TEXCOORD0;
    float2 m_vTexCoord1 : TEXCOORD1;
    float  m_fFog       : FOG;
};

// Runtime 
struct VS_INPUT		
{
    float4 m_vPosition  : POSITION;    
    float4 m_vVertColor : COLOR0;
    float2 m_vTexCoord0 : TEXCOORD0;

} Vertex : SEMANTIC_VS_INPUT;

struct VS_OUTPUT_1		
{
    float4 m_vPosition  : POSITION;
    float4 m_vVertColor : COLOR0;
    float2 m_vTexCoord0 : TEXCOORD0;
    float2 m_vTexCoord1 : TEXCOORD1;
    float  m_fFog       : FOG;
};
		 
//----------------------------------------------------------------------------------------------------------
//	 Tool Shader
//----------------------------------------------------------------------------------------------------------

// Vertex Shader
	
VS_OUTPUT_TOOL VS_tool( VS_INPUT_TOOL In ) 
{
    VS_OUTPUT_TOOL Out;
    
    // Now that we have the calculated weight, add in the final influence
    Out.m_vPosition = mul(g_matWVP, In.m_vPosition);

    // 텍스처 좌표 건네주기
    Out.m_vTexCoord0 = (In.m_vTexCoord0 * g_fImageScale0);
    Out.m_vTexCoord1 = (In.m_vTexCoord0 * g_fImageScale1);
    Out.m_vTexCoord0 += float2(g_fTime*g_vMoveSpeed0.x, g_fTime*g_vMoveSpeed0.y);
    Out.m_vTexCoord1 += float2(g_fTime*g_vMoveSpeed1.x, g_fTime*g_vMoveSpeed1.y);

    Out.m_vVertColor = In.m_vVertColor;
    Out.m_vVertColor.xyz *= g_vLightDiffuse.xyz + g_vLightAmbient.xyz;

    Out.m_fFog = saturate((g_vFOG.x - Out.m_vPosition.z) / g_vFOG.y);
   
    return Out;
}

//	Pixel Shader
	
float4 PS_tool( VS_OUTPUT_TOOL In ) : COLOR0 
{  
    // Texture
    float4 vColor = tex2D( BaseTexSampler, In.m_vTexCoord0 );
    float4 vColor2 = tex2D( BaseTex2Sampler, In.m_vTexCoord1 );
    vColor += vColor2;

    vColor.w = g_fAlpha;

    // ps_3_0 에서는 Fog 를 직접 구현해줘야함.
    vColor.xyz = (vColor.xyz*In.m_fFog) + g_vFogColor*(1.f-In.m_fFog);

    return vColor;
}



//----------------------------------------------------------------------------------------------------------
// Runtime Shader	ps.1.1
//----------------------------------------------------------------------------------------------------------

// Vertex Shader

VS_OUTPUT_1 VS_1( VS_INPUT In ) 
{
    VS_OUTPUT_1 Out;
    
    // Now that we have the calculated weight, add in the final influence
    Out.m_vPosition = mul(g_matWVP, In.m_vPosition);

    // Pass the texture coordinate
    Out.m_vTexCoord0 = (In.m_vTexCoord0 * g_fImageScale0);
    Out.m_vTexCoord1 = (In.m_vTexCoord0 * g_fImageScale1);
    Out.m_vTexCoord0 += float2(g_fTime*g_vMoveSpeed0.x, g_fTime*g_vMoveSpeed0.y);
    Out.m_vTexCoord1 += float2(g_fTime*g_vMoveSpeed1.x, g_fTime*g_vMoveSpeed1.y);

    Out.m_vVertColor = In.m_vVertColor;
    Out.m_vVertColor.xyz *= g_vLightDiffuse.xyz + g_vLightAmbient.xyz;

    Out.m_fFog = saturate((g_vFOG.x - Out.m_vPosition.z) / g_vFOG.y);
   
    return Out;
}


//	Pixel Shader

float4 PS_1( VS_OUTPUT_1 In ) : COLOR0 
{
    // Texture
    float4 vColor = tex2D( BaseTexSampler, In.m_vTexCoord0 );
    float4 vColor2 = tex2D( BaseTex2Sampler, In.m_vTexCoord1 );
    vColor += vColor2;

    vColor.w = g_fAlpha;

    return vColor;
}


//---------------------------------------------------------------------------------------------------------- 
//					technique 
//----------------------------------------------------------------------------------------------------------
 
technique tool 
{ 
	pass high 
	{ 
		VertexShader = compile vs_3_0 VS_tool(); 
		PixelShader = compile ps_3_0 PS_tool(); 
	} 
}; 

// C++ Code 상에서도 1.1 만 사용하도록 작업되었다.
// 2.0 도 사용하길 원하면, C++ Code 도 변경해야 한다.
technique runtime_1 
{ 
	pass high 
	{ 
		VertexShader = compile vs_1_1 VS_1(); 
		PixelShader = compile ps_1_1 PS_1(); 
	} 
} 