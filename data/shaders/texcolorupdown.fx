

//--------------------------------------------------------------------------------------
// Global variables
//--------------------------------------------------------------------------------------

float4x4 g_matWVP;
float4   g_vLightDiffuse;
float3   g_vLightAmbient;
float2   g_vFOG;	// FogEnd, FogDist
float3   g_vFogColor;
float    g_fAlpha;					// Hiding piece model behind a player character
float    g_fTime;
float    g_fTexColorUpDown = 1.f;		// 프로그램상에서 쓰이는 것.
float    g_fTexColorUpDownMin = 0.f;		// mat 파일에서 셋팅하는 것. Texture 의 세기 조절 최소값
float    g_fTexColorUpDownAdd = 1.f;		// mat 파일에서 셋팅하는 것. Texture 의 세기 조절 추가 색 변화치
float	 g_fTexColorUpDownSpeed = 5.f;		// mat 파일에서 셋팅하는 것. Texture 의 색 변화 속도


//--------------------------------------------------------------------------------------
// Material Parameters
//--------------------------------------------------------------------------------------

texture g_BaseTexture;
sampler BaseTexSampler = sampler_state 
{
    Texture = (g_BaseTexture);
};

 
//--------------------------------------------------------------------------------------
// 	Basic vertex transformation
//--------------------------------------------------------------------------------------

// TOOL
struct VS_INPUT_TOOL
{
    float4 m_vPosition  : POSITION;
    float2 m_vTexCoord0 : TEXCOORD0;
} Vertex_tool : SEMANTIC_VS_INPUT_TOOL; 

struct VS_OUTPUT_TOOL
{
    float4 m_vPosition  : POSITION;
    float4 m_vColor	: COLOR0;
    float2 m_vTexCoord0 : TEXCOORD0;
    float  m_fFog       : FOG;
};

// Runtime 
struct VS_INPUT		
{
    float4 m_vPosition  : POSITION;  
    float2 m_vTexCoord0 : TEXCOORD0;

} Vertex : SEMANTIC_VS_INPUT;

struct VS_OUTPUT_1		
{
    float4 m_vPosition  : POSITION;
    float4 m_vColor	: COLOR0;
    float2 m_vTexCoord0 : TEXCOORD0;
    float  m_fFog       : FOG;
};


 
//----------------------------------------------------------------------------------------------------------
//	 Tool Shader
//----------------------------------------------------------------------------------------------------------

// Vertex Shader
	
VS_OUTPUT_TOOL VS_tool( VS_INPUT_TOOL In ) 
{
    VS_OUTPUT_TOOL Out;
    
    // Now that we have the calculated weight, add in the final influence
    Out.m_vPosition = mul(g_matWVP, In.m_vPosition);

    // 텍스처 좌표 건네주기
    Out.m_vTexCoord0 = In.m_vTexCoord0;

    Out.m_vColor = (g_fTexColorUpDownMin + (((sin(g_fTime*g_fTexColorUpDownSpeed)+1)*0.25f) * g_fTexColorUpDownAdd));

    Out.m_fFog = saturate((g_vFOG.x - Out.m_vPosition.z) / g_vFOG.y);

    return Out;
}

//	Pixel Shader
	
float4 PS_tool( VS_OUTPUT_TOOL In ) : COLOR0 
{  
    // Texture
    float4 vColor = tex2D( BaseTexSampler, In.m_vTexCoord0 ) * In.m_vColor * 2.f;

    // ps_3_0 에서는 Fog 를 직접 구현해줘야함.
    vColor.xyz = (vColor.xyz*In.m_fFog) + g_vFogColor*(1.f-In.m_fFog);

    return vColor;
}



//----------------------------------------------------------------------------------------------------------
// Runtime Shader	ps.1.1
//----------------------------------------------------------------------------------------------------------

// Vertex Shader

VS_OUTPUT_1 VS_1( VS_INPUT In ) 
{
    VS_OUTPUT_1 Out;
    
    // Now that we have the calculated weight, add in the final influence
    Out.m_vPosition = mul(g_matWVP, In.m_vPosition);

    // Pass the texture coordinate
    Out.m_vTexCoord0 = In.m_vTexCoord0;

    Out.m_vColor = (g_fTexColorUpDownMin + (((sin(g_fTime*g_fTexColorUpDownSpeed)+1)*0.25f) * g_fTexColorUpDownAdd));

    Out.m_fFog = saturate((g_vFOG.x - Out.m_vPosition.z) / g_vFOG.y);
   
    return Out;
}


//	Pixel Shader

float4 PS_1( VS_OUTPUT_1 In ) : COLOR0 
{
    // Texture
    float4 vColor = tex2D( BaseTexSampler, In.m_vTexCoord0 ) * In.m_vColor * 2.f;
    return vColor;
}


//---------------------------------------------------------------------------------------------------------- 
//					technique 
//----------------------------------------------------------------------------------------------------------
 
technique tool 
{ 
	pass high 
	{ 
		VertexShader = compile vs_3_0 VS_tool(); 
		PixelShader = compile ps_3_0 PS_tool();

		SRCBLEND = ONE;
		DESTBLEND = ONE;
                FOGCOLOR = 0L;
	} 
}; 

// C++ Code 상에서도 1.1을 최저 기준으로 작업되었다.
// 기준을 2.0으로 하려면, C++ Code 도 변경해야 한다.
technique runtime_1 
{ 
	pass high 
	{ 
		VertexShader = compile vs_1_1 VS_1(); 
		PixelShader = compile ps_1_1 PS_1();


		SRCBLEND = ONE;
		DESTBLEND = ONE;
                FOGCOLOR = 0L;
	} 
} 