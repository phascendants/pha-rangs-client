
//--------------------------------------------------------------------------------------
// Global variables
//--------------------------------------------------------------------------------------

float4x4 g_matWV;
float4x4 g_matWVP;
float4x4 g_matShadow;
float4   g_vLightDiffuse;
float3   g_vLightAmbient;
float3   g_vLightDir;
float3   g_vLightDirViewSpace;
float3   g_vCameraFrom; 
float2   g_vFOG;	// FogEnd, FogDist
float3   g_vFogColor;
int      g_nPointLightNum_TOOL;
float4   g_vPos_Range_PL_TOOL[12];
float4   g_vDiff_OverLighting_PL_TOOL[12];
float4   g_vAtt_PL_TOOL[12];
int      g_nConeLightNum_TOOL;
float4   g_vPos_Range_CL_TOOL[3];
float4   g_vDiff_CosPhiHalf_CL_TOOL[3];
float4   g_vAtt_CosHalfThetaSubPhi_CL_TOOL[3];
float4   g_vDirect_Falloff_CL_TOOL[3];
float    g_fSpecularPower = 256.f;		// 외부에서 제어가능
float    g_fSpecularIntensity = 2.f;		// 외부에서 제어가능
float    g_fOverLightPowerLM = 3.f;
float    g_fOverLightPower_PS_2_0 = 1.f;
float    g_fVS_1_1_ColorMulti = 0.75f;
float4   g_vWindowSize;
float    g_fAlpha;						// Hiding piece model behind a player character
float2   g_vDayNightWeight;
float2	 g_vLightMapUV_Offset;
float2   g_vLightMapUV_Multiply; 
float3	 g_vVoxelColor; 
float    g_fTime; 
float3   g_vShadow;	// x-SIZE, y-EPSILON, z-COLOR

//--------------------------------------------------------------------------------------
// UV 회전 함수
//--------------------------------------------------------------------------------------

float2 rotateUVs(float2 Texcoords, float2 center, float theta)
{ 
 
	//  theta(Radian)에 대한 sin 과 cos 값 계산
	float2 sc; 
	sincos( theta, sc.x, sc.y ); 
	// 회전중심을 원점으로 이동
	float2 uv = Texcoords - center; 
	// UV 회전 
	float2 rotateduv; 
	rotateduv.x = dot( uv, float2( sc.y, -sc.x ) ); 
	rotateduv.y = dot( uv, sc.xy );	 
	// 회전된 UV를 정확한 위치로 다시 이동
	rotateduv += center; 
 
	return rotateduv; 
} 


//--------------------------------------------------------------------------------------
// Material Parameters
//--------------------------------------------------------------------------------------

texture g_BackBufferTexture; 
sampler BackBufferSampler = sampler_state  
{
    Texture = (g_BackBufferTexture); 	
};

texture g_BaseTexture;
sampler BaseTexSampler = sampler_state 
{
    Texture = (g_BaseTexture);
	
};

texture g_BaseTexture2;
sampler BaseTex2Sampler = sampler_state 
{
    Texture = (g_BaseTexture2);
};

texture g_LightMapDayTex;
sampler LightMapTexSampler_1st = sampler_state 
{
    Texture = (g_LightMapDayTex);	
};

texture g_ShadowTexture;
sampler ShadowTexSampler = sampler_state 
{
    Texture = (g_ShadowTexture);

    MinFilter = Point;
    MagFilter = Point;
    AddressU = Clamp;
    AddressV = Clamp;
};

texture g_GlobalCubeTexture;
samplerCUBE CubeTexSampler = sampler_state 
{
    Texture = (g_GlobalCubeTexture);

//    AddressU = Wrap;
//    AddressV = Wrap;
//    AddressW = Wrap;

    AddressU = Wrap;
    AddressV = Wrap;
 //   AddressW = Wrap;

//   MinFilter = Point;
//   MagFilter = Point;
   // MipFilter = Point;
};


float g_fRotate_UV;				//UV 회전값 (도);
float g_fScaleFactor;			//UV 크기값 (배수);

 
//--------------------------------------------------------------------------------------
// 	Basic vertex transformation
//--------------------------------------------------------------------------------------

// TOOL
struct VS_INPUT_TOOL
{
    float4 m_vPosition  : POSITION;
    float4 m_vVertColor : COLOR0;
    float3 m_vNormal    : NORMAL;
    float2 m_vTexCoord0 : TEXCOORD0;
} Vertex_tool : SEMANTIC_VS_INPUT_TOOL; 

struct VS_OUTPUT_TOOL
{
    float4 m_vPosition  : POSITION;
    float4 m_vVertColor    : COLOR0;
    float2 m_vTexCoord0 : TEXCOORD0;
    float3 m_vPos       : TEXCOORD1; 
	float3 m_vNor       : TEXCOORD2; 
	float  m_fFog       : FOG;
};

// Runtime 
struct VS_INPUT		
{
    float4 m_vPosition   : POSITION;
    float4 m_vVertColor  : COLOR0;
    float3 m_vNormal     : NORMAL;
    float2 m_vTexCoord0  : TEXCOORD0;
    float2 m_vLightmapUV : TEXCOORD1;	// LightMap를 사용할 경우, m_vLightmapUV 이름을 유지하며, m_vLightmapUV 이름이 있으면, LightMap을 생성해준다.

} Vertex : SEMANTIC_VS_INPUT;

struct VS_OUTPUT_1		
{
    float4 m_vPosition  : POSITION;
    float4 m_vVertColor : COLOR0;
    float2 m_vTexCoord0 : TEXCOORD0;
    float2 m_vTexCoord1 : TEXCOORD1;
    float2 m_vLightmapUV  : TEXCOORD2;
    float2 m_vLightmapUV1 : TEXCOORD3;
    float  m_fFog         : FOG;
};

struct VS_OUTPUT_ALBEDO	
{ 
    float4 m_vPosition  : POSITION; 
    float4 m_vVertColor : COLOR0;
    float2 m_vTexCoord0 : TEXCOORD0; 
    float  m_fFog         : FOG;
}; 

struct VS_OUTPUT_ALBEDO_3
{ 
    float4 m_vPosition  : POSITION; 
    float4 m_vVertColor : COLOR0;
    float2 m_vTexCoord0 : TEXCOORD0; 
    float3 m_vNormal 	: TEXCOORD1; 
    float  m_fFog         : FOG;
}; 
			 
struct PS_DEFFERED_OUT		// game 렌더용 
{ 
	float4 m_vColor0	: COLOR0; 
	float4 m_vColor1	: COLOR1; 
	float4 m_vColor2	: COLOR2; 
}; 
 
//----------------------------------------------------------------------------------------------------------
//	 Tool Shader
//----------------------------------------------------------------------------------------------------------

// Vertex Shader
	
VS_OUTPUT_TOOL VS_tool( VS_INPUT_TOOL In ) 
{
    VS_OUTPUT_TOOL Out;
    
    // Now that we have the calculated weight, add in the final influence
    Out.m_vPosition = mul(g_matWVP, In.m_vPosition);
    Out.m_vPos = mul(g_matWV, In.m_vPosition);
    Out.m_vNor = normalize( mul(g_matWV, In.m_vNormal) );

    // 텍스처 좌표 건네주기
    Out.m_vTexCoord0 = In.m_vTexCoord0;

    float fDiffuseDot = saturate( dot( g_vLightDirViewSpace, normalize( Out.m_vNor ) ) );
    float3 vDiffuse = g_vLightDiffuse * fDiffuseDot + g_vLightAmbient;
	
    Out.m_vVertColor = In.m_vVertColor;
    Out.m_vVertColor.xyz *= vDiffuse.xyz;

    Out.m_fFog = saturate((g_vFOG.x - Out.m_vPosition.z) / g_vFOG.y);
   
    return Out;
}

//	Pixel Shader
	
float4 PS_tool( VS_OUTPUT_TOOL In ) : COLOR0 
{  
  
    float2 INUV = In.m_vTexCoord0.xy; 
    float2 UVRotator_center = float2(0.0, 0.0);	 //no center values wired to UV Rotate Node - using defaults 
    float UVRotAngle = radians(g_fRotate_UV);	//  DegToRad
    float2 UVRotator = rotateUVs(INUV, UVRotator_center, UVRotAngle); 

    float4 vColor = tex2D( BaseTexSampler, INUV);    
    float4 vColor2 = tex2D(BaseTex2Sampler, (g_fScaleFactor * UVRotator).xy);	
    vColor = lerp(vColor2, vColor, In.m_vVertColor.w);

    float3 vNor = normalize( In.m_vNor );

    float3 vPointColor = 0.f;
    for ( int i=0; i<g_nPointLightNum_TOOL; ++i )
    {
        float3 afPosDir = g_vPos_Range_PL_TOOL[i].xyz - In.m_vPos;
        float fLength = length( afPosDir );
        // 내적 구함.
        afPosDir = normalize( afPosDir );
        float fDotPL = saturate( dot( afPosDir, In.m_vNor ) );
        // 선형 감쇠, 2차 감쇠, 지수 감쇠 적용
        float fAttenuationSub = g_vAtt_PL_TOOL[i].x;
        fAttenuationSub += g_vAtt_PL_TOOL[i].y * fLength;
        fAttenuationSub += g_vAtt_PL_TOOL[i].z * fLength * fLength;
        // 최종 감쇠값
        float fAttenuation = 1.f / fAttenuationSub;
        // 거리 체크
        float fEnableLength = step(fLength, g_vPos_Range_PL_TOOL[i].w);
        // 마지막
	// Edge 부분이 저 감쇠만으로는 적용이 안되서 1 를 빼주었다.
	fAttenuation = max((fAttenuation-1.f),0) * fDotPL * fEnableLength;
	fAttenuation = min(fAttenuation,g_vDiff_OverLighting_PL_TOOL[i].w);
	vPointColor += g_vDiff_OverLighting_PL_TOOL[i].xyz * fAttenuation;
    }

    for ( int i=0; i<g_nConeLightNum_TOOL; ++i )
    {
        float3 afPosDir = g_vPos_Range_CL_TOOL[i].xyz - In.m_vPos;
        float fLength = length( afPosDir );
        // 내적 구함.
        afPosDir = normalize( afPosDir );
        float fDotPL = saturate( dot( afPosDir, vNor ) );
        // 선형 감쇠, 2차 감쇠, 지수 감쇠 적용
        float fAttenuationSub = g_vAtt_CosHalfThetaSubPhi_CL_TOOL[i].x;
        fAttenuationSub += g_vAtt_CosHalfThetaSubPhi_CL_TOOL[i].y * fLength;
        fAttenuationSub += g_vAtt_CosHalfThetaSubPhi_CL_TOOL[i].z * fLength * fLength;
        // 최종 감쇠값
        float fAttenuation = 1.f / fAttenuationSub;
        // 거리 체크
        float fEnableLength = step(fLength, g_vPos_Range_CL_TOOL[i].w);
   
	//Cone쪽 계산
        float3 fRho = dot(afPosDir,g_vDirect_Falloff_CL_TOOL[i].xyz);
        fEnableLength *= step(g_vDiff_CosPhiHalf_CL_TOOL[i].w, fRho);
        fEnableLength *= pow( (fRho - g_vDiff_CosPhiHalf_CL_TOOL[i].w) / g_vAtt_CosHalfThetaSubPhi_CL_TOOL[i].w, g_vDirect_Falloff_CL_TOOL[i].w );
        fEnableLength = min(1.f,fEnableLength);
   
	// 마지막
	float3 vHalf = g_vCameraFrom - In.m_vPos;
	vHalf = normalize( vHalf );
	vHalf += normalize( g_vPos_Range_CL_TOOL[i].xyz - In.m_vPos );
	float fSpec = pow( saturate( dot( normalize(vHalf), vNor ) ), g_fSpecularPower) * g_fSpecularIntensity;

	// Edge 부분이 저 감쇠만으로는 적용이 안되서 1 를 빼주었다.
	//vPointColor += g_vDiff_CosPhiHalf_CL_TOOL[i].xyz * max((fAttenuation-1.f),0) * (fDotPL+fSpec) * fEnableLength;
	float vLightAmountNEW = max((fAttenuation-1.f),0) * fEnableLength;
	float vLightAmountDIFF = vLightAmountNEW * fDotPL;
	float vLightAmountSPEC = vLightAmountNEW * fSpec;
	vLightAmountNEW = min(vLightAmountDIFF,2.f);
	vLightAmountNEW += min(vLightAmountSPEC,2.f);
	vPointColor += g_vDiff_CosPhiHalf_CL_TOOL[i].xyz * vLightAmountNEW;
    }

    vPointColor += In.m_vVertColor.xyz;

    vColor.xyz *= min(vPointColor.xyz,3.f);
    vColor.xyz = (vColor.xyz*In.m_fFog) + g_vFogColor*(1.f-In.m_fFog);
    vColor.w = g_fAlpha;

    return vColor;
}



//----------------------------------------------------------------------------------------------------------
// Runtime Shader	ps.1.1
//----------------------------------------------------------------------------------------------------------

// Vertex Shader

VS_OUTPUT_1 VS_1( VS_INPUT In ) 
{
    VS_OUTPUT_1 Out;
    
    // Now that we have the calculated weight, add in the final influence
    Out.m_vPosition = mul(g_matWVP, In.m_vPosition);

    // Pass the texture coordinate
    Out.m_vTexCoord0 = In.m_vTexCoord0;

    float2 UVRotator_center = float2(0.0, 0.0);	 //no center values wired to UV Rotate Node - using defaults 
    float UVRotAngle = radians(g_fRotate_UV);	//  DegToRad
    float2 UVRotator = rotateUVs(In.m_vTexCoord0, UVRotator_center, UVRotAngle); 
    Out.m_vTexCoord1 = (g_fScaleFactor * UVRotator).xy;

    Out.m_vLightmapUV = In.m_vLightmapUV;
    Out.m_vLightmapUV.xy += g_vLightMapUV_Offset;
    Out.m_vLightmapUV1 = Out.m_vLightmapUV;

    Out.m_vVertColor = In.m_vVertColor;
    Out.m_vVertColor.xyz *= g_fVS_1_1_ColorMulti;

    Out.m_fFog = saturate((g_vFOG.x - Out.m_vPosition.z) / g_vFOG.y);
   
    return Out;
}

//	Pixel Shader

float4 PS_1( VS_OUTPUT_1 In ) : COLOR0 
{
    // Texture
    float4 vColor = tex2D( BaseTexSampler, In.m_vTexCoord0 );
    float4 vColor2 = tex2D(BaseTex2Sampler, In.m_vTexCoord1 );
    vColor = lerp(vColor2, vColor, In.m_vVertColor.w);

	float4 vLightColor = tex2D( LightMapTexSampler_1st, In.m_vLightmapUV );

	vColor.xyz *= vLightColor.xyz * In.m_vVertColor.xyz * 4.f;
    vColor.w = g_fAlpha;

    return vColor;
}



//----------------------------------------------------------------------------------------------------------
// Runtime Shader	ps.2.0 Albedo
//----------------------------------------------------------------------------------------------------------

// Vertex Shader

VS_OUTPUT_ALBEDO VS_2( VS_INPUT In ) 
{
    VS_OUTPUT_ALBEDO Out;
    
    // Now that we have the calculated weight, add in the final influence
    Out.m_vPosition = mul(g_matWVP, In.m_vPosition);

    // Pass the texture coordinate
    Out.m_vTexCoord0 = In.m_vTexCoord0;
    Out.m_vVertColor = In.m_vVertColor;
   
    Out.m_fFog = 1.f;

    return Out;
}


//	Pixel Shader

float4 PS_2( VS_OUTPUT_ALBEDO In ) : COLOR0 
{
    // Texture
    float2 INUV = In.m_vTexCoord0.xy; 
    float2 UVRotator_center = float2(0.0, 0.0);	 //no center values wired to UV Rotate Node - using defaults 
    float UVRotAngle = radians(g_fRotate_UV);	//  DegToRad
    float2 UVRotator = rotateUVs(INUV, UVRotator_center, UVRotAngle); 

    float4 vColor = tex2D( BaseTexSampler, INUV);
    float4 vColor2 = tex2D(BaseTex2Sampler, (g_fScaleFactor * UVRotator).xy);

    vColor = lerp(vColor2, vColor, In.m_vVertColor.w);
    vColor.xyz *= In.m_vVertColor.xyz;
    vColor.w = g_fAlpha;

    return vColor;
}

//---------------------------------------------------------------------------------------------------------- 
//					game 렌더용	ps.3.0 Albedo 
VS_OUTPUT_ALBEDO_3 VS_3( VS_INPUT In )  
{ 
    VS_OUTPUT_ALBEDO_3 Out;
    
    // Now that we have the calculated weight, add in the final influence
    Out.m_vPosition = mul(g_matWVP, In.m_vPosition);
    Out.m_vNormal = normalize( mul(g_matWV, In.m_vNormal) ); 

    // Pass the texture coordinate
    Out.m_vTexCoord0 = In.m_vTexCoord0;
    Out.m_vVertColor = In.m_vVertColor;
   
    Out.m_fFog = 1.f;

    return Out;
} 
 
PS_DEFFERED_OUT PS_3( VS_OUTPUT_ALBEDO_3 In ) 
{ 
    PS_DEFFERED_OUT vOut; 

    // Texture
    float2 INUV = In.m_vTexCoord0.xy; 
    float2 UVRotator_center = float2(0.0, 0.0);	 //no center values wired to UV Rotate Node - using defaults 
    float UVRotAngle = radians(g_fRotate_UV);	//  DegToRad
    float2 UVRotator = rotateUVs(INUV, UVRotator_center, UVRotAngle); 

    float4 vColor = tex2D( BaseTexSampler, INUV);
    float4 vColor2 = tex2D(BaseTex2Sampler, (g_fScaleFactor * UVRotator).xy);

    vColor = lerp(vColor2, vColor, In.m_vVertColor.w);
    vColor.xyz *= In.m_vVertColor.xyz;
    vColor.w = g_fAlpha;
 
    vOut.m_vColor0 = vColor; 
    vOut.m_vColor1 = float4(((normalize(In.m_vNormal)+1.f)*0.5f),vColor.w); 
//    vOut.m_vColor2 = float4(g_fSpecularIntensity*0.5f,g_fSpecularPower/256.f,g_fCubeMapValue,vColor.w); 
    vOut.m_vColor2 = float4(g_fSpecularIntensity*0.5f,g_fSpecularPower/256.f,0.f,vColor.w); 
 
    return vOut; 
} 

//---------------------------------------------------------------------------------------------------------- 
//					technique 
//----------------------------------------------------------------------------------------------------------
 
technique tool_deffered
{ 
	pass high 
	{ 
		VertexShader = compile vs_3_0 VS_tool(); 
		PixelShader = compile ps_3_0 PS_tool(); 
	} 
}; 
 
technique runtime_1 
{ 
	//------------------------------------------------------------------------------------- 
	// LightMap 
	pass high 
	{ 
		VertexShader = compile vs_1_1 VS_1(); 
		PixelShader = compile ps_1_1 PS_1(); 
	} 
} 

technique runtime_2 
{ 
	//------------------------------------------------------------------------------------- 
	// LightMap 
	pass albedo 
	{ 
		VertexShader = compile vs_2_0 VS_2(); 
		PixelShader = compile ps_2_0 PS_2(); 
	} 
}

technique runtime_3 
{ 
	//------------------------------------------------------------------------------------- 
	// LightMap 
	pass albedo 
	{ 
		VertexShader = compile vs_2_0 VS_3(); 
		PixelShader = compile ps_3_0 PS_3(); 
	} 
}
