--
-- 주의) 
-- 1) 대/소 문자를 확실하게 구분해야 합니다.
-- 2) true, false 를 확실하게 구분해야 합니다.
-- 3) UCS-2 Little Endian 로 저장해야 합니다.
-------------------------------------------------------------------------------
-- 버젼;
Country_System_Version = 100;

-- 대륙이름 ;
ContinentName = "Asia"
Continent = 1

-- 국가;
Country= 
{
	-- 국가 ID | 국가 이름 | 국가 이미지 XML ID; (국가 ID는 절대 변경하면 안된다.)
	--[[
	{ 1, "Bangladesh", "COUNTRY_BAN", "MINI_COUNTRY_BAN", "BAN.DDS" },		-- 방글라데시
	{ 2, "Bhutan", "COUNTRY_UN", "MINI_COUNTRY_BHU", "UN.DDS" },		-- 부탄
	{ 3, "British Indian Ocean Territory", "COUNTRY_UN", "MINI_COUNTRY_IOT", "UN.DDS" },		-- 영국령 인도양 지역
	{ 4, "Brunei", "COUNTRY_BRU", "MINI_COUNTRY_BRU", "BRU.DDS" },		-- 브루나이
	{ 5, "Cambodia", "COUNTRY_UN", "MINI_COUNTRY_CAM", "UN.DDS" },		-- 캄보디아
	]]--
	{ 6, "China", "COUNTRY_CHN", "MINI_COUNTRY_CHN", "CHN.DDS" },		-- 중국
	--[[
	{ 7, "Christmas Island", "COUNTRY_CXR", "MINI_COUNTRY_CXR", "CXR.DDS" },		-- 크리스마스 섬
	{ 8, "Gaza Strip", "COUNTRY_UN", "MINI_COUNTRY_PSE", "UN.DDS" },		-- 가자 지구
	{ 9, "Hong Kong", "COUNTRY_HKG", "MINI_COUNTRY_HKG", "HKG.DDS" },		-- 홍콩
	{ 10, "India", "COUNTRY_IND", "MINI_COUNTRY_IND", "IND.DDS" },		-- 인도
	]]--
	{ 11, "Indonesia", "COUNTRY_INA", "MINI_COUNTRY_INA", "INA.DDS" },		-- 인도네시아
	--[[
	{ 12, "Japan", "COUNTRY_JPN", "MINI_COUNTRY_JPN", "JPN.DDS" },		-- 일본
	{ 13, "Kazakhstan", "COUNTRY_KAZ", "MINI_COUNTRY_KAZ", "KAZ.DDS" },		-- 카자흐스탄
	{ 14, "Kyrgyzstan", "COUNTRY_KGZ", "MINI_COUNTRY_KGZ", "KGZ.DDS" },		-- 키르기스스탄
	{ 15, "Laos", "COUNTRY_UN", "MINI_COUNTRY_LAO", "UN.DDS" },		-- 라오스
	{ 16, "Macau", "COUNTRY_MAC", "MINI_COUNTRY_MAC", "MAC.DDS" },		-- 마카오
	]]--
	{ 17, "Malaysia", "COUNTRY_MAS", "MINI_COUNTRY_MAS", "MAS.DDS" },		-- 말레이시아
	--[[
	{ 18, "Maldives", "COUNTRY_MDV", "MINI_COUNTRY_MDV", "MDV.DDS" },		-- 몰디브
	{ 19, "Mongolia", "COUNTRY_UN", "MINI_COUNTRY_MGL", "UN.DDS" },		-- 몽골
	{ 20, "Myanmar", "COUNTRY_UN", "MINI_COUNTRY_MMR", "UN.DDS" },		-- 미얀마
	{ 21, "Nepal", "COUNTRY_UN", "MINI_COUNTRY_NEP", "UN.DDS" },		-- 네팔
	{ 22, "North Korea", "COUNTRY_UN", "MINI_COUNTRY_PRK", "UN.DDS" },		-- 북한
	{ 23, "Pakistan", "COUNTRY_PAK", "MINI_COUNTRY_PAK", "PAK.DDS" },		-- 파키스탄
	]]--
	{ 24, "Philippines", "COUNTRY_PHI", "MINI_COUNTRY_PHI", "PHI.DDS" },		-- 필리핀
	--[[
	{ 25, "Russia", "COUNTRY_RUS", "MINI_COUNTRY_RUS", "RUS.DDS" },		-- 러시아
	{ 26, "Singapore", "COUNTRY_SIN", "MINI_COUNTRY_SIN", "SIN.DDS" },		-- 싱가포르
	{ 27, "South Korea", "COUNTRY_KOR", "MINI_COUNTRY_KOR", "KOR.DDS" },		-- 한국
	{ 28, "Sri Lanka", "COUNTRY_SRI", "MINI_COUNTRY_SRI", "SRI.DDS" },		-- 스리랑카
	]]--
	{ 29, "Taiwan", "COUNTRY_TWN", "MINI_COUNTRY_TWN", "TWN.DDS" },		-- 대만
	--[[
	{ 30, "Tajikistan", "COUNTRY_UN", "MINI_COUNTRY_TJK", "UN.DDS" },		-- 타지키스탄
	]]--
	{ 31, "Thailand", "COUNTRY_THA", "MINI_COUNTRY_THA", "THA.DDS" },		-- 태국
	--[[
	{ 32, "Timor-Leste", "COUNTRY_UN", "MINI_COUNTRY_TLS", "UN.DDS" },		-- 동티모르
	{ 33, "Turkmenistan", "COUNTRY_UN", "MINI_COUNTRY_TKM", "UN.DDS" },		-- 투르크메니스탄
	{ 34, "Uzbekistan", "COUNTRY_UZB", "MINI_COUNTRY_UZB", "UZB.DDS" },		-- 우즈베키스탄
	]]--
	{ 35, "Vietnam", "COUNTRY_VIE", "MINI_COUNTRY_VIE", "VIE.DDS" },		-- 베트남
	{ 36, "Union", "COUNTRY_UNION", "MINI_COUNTRY_UNION", "UNION.DDS" },		-- Union
}