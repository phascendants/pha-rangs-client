--
-- 주의) 
-- 1) 대/소 문자를 확실하게 구분해야 합니다.
-- 2) true, false 를 확실하게 구분해야 합니다.
-- 3) UCS-2 Little Endian 로 저장해야 합니다.
-------------------------------------------------------------------------------
-- 버젼;
Country_System_Version = 100;

-- 대륙이름 ;
ContinentName = "Africa"
Continent = 4

-- 국가;
Country= 
{
	-- 국가 ID | 국가 이름 | 국가 이미지 XML ID;
	{ 1, "Algeria", "COUNTRY_ALG", "MINI_COUNTRY_ALG", "ALG.DDS" },		-- 알제리
	{ 2, "Angola", "COUNTRY_UN", "MINI_COUNTRY_AGO", "UN.DDS" },		-- 앙골라
	{ 3, "Benin", "COUNTRY_UN", "MINI_COUNTRY_BEN", "UN.DDS" },		-- 베냉
	{ 4, "Botswana", "COUNTRY_UN", "MINI_COUNTRY_BOT", "UN.DDS" },		-- 보츠와나
	{ 5, "Burkina Faso", "COUNTRY_UN", "MINI_COUNTRY_BUR", "UN.DDS" },		-- 부르키나파소
	{ 6, "Burundi", "COUNTRY_UN", "MINI_COUNTRY_BDI", "UN.DDS" },		-- 부룬디
	{ 7, "Cameroon", "COUNTRY_UN", "MINI_COUNTRY_CMR", "UN.DDS" },		-- 카메룬
	{ 8, "Cape Verde", "COUNTRY_CPV", "MINI_COUNTRY_CPV", "CPV.DDS" },		-- 카보베르데
	{ 9, "Central African Republic", "COUNTRY_UN", "MINI_COUNTRY_CAF", "UN.DDS" },		-- 중앙아프리카공화국
	{ 10, "Chad", "COUNTRY_UN", "MINI_COUNTRY_CHA", "UN.DDS" },		-- 차드
	{ 11, "Comoros", "COUNTRY_UN", "MINI_COUNTRY_COM", "UN.DDS" },		-- 코모로
	{ 12, "Congo", "COUNTRY_UN", "MINI_COUNTRY_CGO", "UN.DDS" },		-- 콩고
	{ 13, "Cote d'Ivoire", "COUNTRY_UN", "MINI_COUNTRY_CIV", "UN.DDS" },		-- 코트디부아르
	{ 14, "Democratic Republic of the Congo", "COUNTRY_UN", "MINI_COUNTRY_DOD", "UN.DDS" },		-- 콩고민주공화국
	{ 15, "Djibouti", "COUNTRY_UN", "MINI_COUNTRY_DJI", "UN.DDS" },		-- 지부티
	{ 16, "Egypt", "COUNTRY_EGY", "MINI_COUNTRY_EGY", "EGY.DDS" },		-- 이집트
	{ 17, "Equatorial Guinea", "COUNTRY_UN", "MINI_COUNTRY_GEQ", "UN.DDS" },		-- 적도기니
	{ 18, "Eritrea", "COUNTRY_UN", "MINI_COUNTRY_ERI", "UN.DDS" },		-- 에리트레아
	{ 19, "Ethiopia", "COUNTRY_UN", "MINI_COUNTRY_ETH", "UN.DDS" },		-- 에티오피아
	{ 20, "Gabon", "COUNTRY_UN", "MINI_COUNTRY_GAB", "UN.DDS" },		-- 가봉
	{ 21, "Gambia", "COUNTRY_UN", "MINI_COUNTRY_GAM", "UN.DDS" },		-- 감비아
	{ 22, "Ghana", "COUNTRY_UN", "MINI_COUNTRY_GHA", "UN.DDS" },		-- 가나
	{ 23, "Guinea", "COUNTRY_UN", "MINI_COUNTRY_GUI", "UN.DDS" },		-- 기니
	{ 24, "Guinea-Bissau", "COUNTRY_UN", "MINI_COUNTRY_GBS", "UN.DDS" },		-- 기니비사우
	{ 25, "Guyana", "COUNTRY_GUY", "MINI_COUNTRY_GUY", "GUY.DDS" },		-- 가이아나
	{ 26, "Kenya", "COUNTRY_KEN", "MINI_COUNTRY_KEN", "KEN.DDS" },		-- 케냐
	{ 27, "Lesotho", "COUNTRY_UN", "MINI_COUNTRY_LES", "UN.DDS" },		-- 레소토
	{ 28, "Liberia", "COUNTRY_UN", "MINI_COUNTRY_LBR", "UN.DDS" },		-- 라이베리아
	{ 29, "Libya", "COUNTRY_UN", "MINI_COUNTRY_LBA", "UN.DDS" },		-- 리비아
	{ 30, "Madagascar", "COUNTRY_UN", "MINI_COUNTRY_MAD", "UN.DDS" },		-- 마다가스카르
	{ 31, "Malawi", "COUNTRY_UN", "MINI_COUNTRY_MAW", "UN.DDS" },		-- 말라위
	{ 32, "Mali", "COUNTRY_UN", "MINI_COUNTRY_MLI", "UN.DDS" },		-- 말리
	{ 33, "Mauritania", "COUNTRY_UN", "MINI_COUNTRY_MTN", "UN.DDS" },		-- 모리타니
	{ 34, "Mauritius", "COUNTRY_MRI", "MINI_COUNTRY_MRI", "MRI.DDS" },		-- 모리셔스
	{ 35, "Mayotte", "COUNTRY_UN", "MINI_COUNTRY_MYT", "UN.DDS" },		-- 마요트 섬
	{ 36, "Morocco", "COUNTRY_MAR", "MINI_COUNTRY_MAR", "MAR.DDS" },		-- 모로코
	{ 37, "Mozambique", "COUNTRY_UN", "MINI_COUNTRY_MOZ", "UN.DDS" },		-- 모잠비크
	{ 38, "Namibia", "COUNTRY_UN", "MINI_COUNTRY_NAM", "UN.DDS" },		-- 나미비아
	{ 39, "Niger", "COUNTRY_UN", "MINI_COUNTRY_NIG", "UN.DDS" },		-- 니제르
	{ 40, "Nigeria", "COUNTRY_NGR", "MINI_COUNTRY_NGR", "NGR.DDS" },		-- 나이지리아
	{ 41, "Republic of South Africa", "COUNTRY_ZAF", "MINI_COUNTRY_ZAF", "ZAF.DDS" },		-- 남아프리카공화국
	{ 42, "Rwanda", "COUNTRY_UN", "MINI_COUNTRY_RWA", "UN.DDS" },		-- 르완다
	{ 43, "Saint Helena", "COUNTRY_SHN", "MINI_COUNTRY_SHN", "SHN.DDS" },		-- 세인트헬레나 섬
	{ 44, "Sao Tome and Principe", "COUNTRY_STP", "MINI_COUNTRY_STP", "STP.DDS" },		-- 상투메프린시페
	{ 45, "Senegal", "COUNTRY_SEN", "MINI_COUNTRY_SEN", "SEN.DDS" },		-- 세네갈
	{ 46, "Seychelles", "COUNTRY_SEY", "MINI_COUNTRY_SEY", "SEY.DDS" },		-- 세이셸
	{ 47, "Sierra Leone", "COUNTRY_UN", "MINI_COUNTRY_SLE", "UN.DDS" },		-- 시에라리온
	{ 48, "Somalia", "COUNTRY_UN", "MINI_COUNTRY_SOM", "UN.DDS" },		-- 소말리아
	{ 49, "French Southern Territories", "COUNTRY_UN", "MINI_COUNTRY_ATF", "UN.DDS" },		-- 프랑스령 남쪽 지역
	{ 50, "Sudan", "COUNTRY_UN", "MINI_COUNTRY_SUD", "UN.DDS" },		-- 수단
	{ 51, "Swaziland", "COUNTRY_UN", "MINI_COUNTRY_SWZ", "UN.DDS" },		-- 스와질란드
	{ 52, "Tanzania", "COUNTRY_TZA", "MINI_COUNTRY_TZA", "TZA.DDS" },		-- 탄자니아
	{ 53, "Togo", "COUNTRY_UN", "MINI_COUNTRY_TOG", "UN.DDS" },		-- 토고
	{ 54, "Tunisia", "COUNTRY_TUN", "MINI_COUNTRY_TUN", "TUN.DDS" },		-- 튀니지
	{ 55, "Uganda", "COUNTRY_UGA", "MINI_COUNTRY_UGA", "UGA.DDS" },		-- 우간다
	{ 56, "Western Sahara", "COUNTRY_UN", "MINI_COUNTRY_ESH", "UN.DDS" },		-- 서사하라
	{ 57, "Zambia", "COUNTRY_UN", "MINI_COUNTRY_ZAM", "UN.DDS" },		-- 잠비아
	{ 58, "Zimbabwe", "COUNTRY_ZIM", "MINI_COUNTRY_ZIM", "ZIM.DDS" },		-- 짐바브웨
}