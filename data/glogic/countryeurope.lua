--
-- 주의) 
-- 1) 대/소 문자를 확실하게 구분해야 합니다.
-- 2) true, false 를 확실하게 구분해야 합니다.
-- 3) UCS-2 Little Endian 로 저장해야 합니다.
-------------------------------------------------------------------------------
-- 버젼;
Country_System_Version = 100;

-- 대륙이름 ;
ContinentName = "Europe"
Continent = 2

-- 국가;
Country= 
{
	-- 국가 ID | 국가 이름 | 국가 이미지 XML ID;
	{ 1, "Albania", "COUNTRY_ALB", "MINI_COUNTRY_ALB", "ALB.DDS" },		-- 알바니아
	{ 2, "Andorra", "COUNTRY_AND", "MINI_COUNTRY_AND", "AND.DDS" },		-- 안도라
	{ 3, "Austria", "COUNTRY_AUT", "MINI_COUNTRY_AUT", "AUT.DDS" },		-- 오스트리아
	{ 4, "Azerbaijan", "COUNTRY_AZE", "MINI_COUNTRY_AZE", "AZE.DDS" },		-- 아제르바이잔
	{ 5, "Belarus", "COUNTRY_BLR", "MINI_COUNTRY_BLR", "BLR.DDS" },		-- 벨라루스
	{ 6, "Belgium", "COUNTRY_BEL", "MINI_COUNTRY_BEL", "BEL.DDS" },		-- 벨기에
	{ 7, "Bosnia and Herzegovina", "COUNTRY_BIH", "MINI_COUNTRY_BIH", "BIH.DDS" },		-- 보스니아헤르체고비나
	{ 8, "Bouvet Island", "COUNTRY_UN", "MINI_COUNTRY_BVT", "UN.DDS" },		-- 부베 섬
	{ 9, "Bulgaria", "COUNTRY_BUL", "MINI_COUNTRY_BUL", "BUL.DDS" },		-- 불가리아
	{ 10, "Croatia", "COUNTRY_CRO", "MINI_COUNTRY_CRO", "CRO.DDS" },		-- 크로아티아
	{ 11, "Cyprus", "COUNTRY_CYP", "MINI_COUNTRY_CYP", "CYP.DDS" },		-- 키프로스
	{ 12, "Czech Republic", "COUNTRY_CZE", "MINI_COUNTRY_CZE", "CZE.DDS" },		-- 체코 공화국
	{ 13, "Denmark", "COUNTRY_DEN", "MINI_COUNTRY_DEN", "DEN.DDS" },		-- 덴마크
	{ 14, "Estonia", "COUNTRY_EST", "MINI_COUNTRY_EST", "EST.DDS" },		-- 에스토니아
	{ 15, "Faroe Islands", "COUNTRY_FRO", "MINI_COUNTRY_FRO", "FRO.DDS" },		-- 페로 제도
	{ 16, "Finland", "COUNTRY_FIN", "MINI_COUNTRY_FIN", "FIN.DDS" },		-- 핀란드
	{ 17, "France", "COUNTRY_FRA", "MINI_COUNTRY_FRA", "FRA.DDS" },		-- 프랑스
	{ 18, "Georgia", "COUNTRY_GEO", "MINI_COUNTRY_GEO", "GEO.DDS" },		-- 조지아
	{ 19, "Germany", "COUNTRY_GER", "MINI_COUNTRY_GER", "GER.DDS" },		-- 독일
	{ 20, "Gibraltar", "COUNTRY_GIB", "MINI_COUNTRY_GIB", "GIB.DDS" },		-- 지브롤터
	{ 21, "Greece", "COUNTRY_GRE", "MINI_COUNTRY_GRE", "GRE.DDS" },		-- 그리스
	{ 22, "Holy See (Vatican City)", "COUNTRY_VAT", "MINI_COUNTRY_VAT", "VAT.DDS" },		-- 바티칸시국
	{ 23, "Hungary", "COUNTRY_HUN", "MINI_COUNTRY_HUN", "HUN.DDS" },		-- 헝가리
	{ 24, "Iceland", "COUNTRY_ISL", "MINI_COUNTRY_ISL", "ISL.DDS" },		-- 아이슬란드
	{ 25, "Ireland", "COUNTRY_IRL", "MINI_COUNTRY_IRL", "IRL.DDS" },		-- 아일랜드
	{ 26, "Isle of Man", "COUNTRY_UN", "MINI_COUNTRY_IMN", "UN.DDS" },		-- 맨 섬
	{ 27, "Italy", "COUNTRY_ITA", "MINI_COUNTRY_ITA", "ITA.DDS" },		-- 이탈리아
	{ 28, "Latvia", "COUNTRY_LAT", "MINI_COUNTRY_LAT", "LAT.DDS" },		-- 라트비아
	{ 29, "Liechtenstein", "COUNTRY_LIE", "MINI_COUNTRY_LIE", "LIE.DDS" },		-- 리히텐슈타인
	{ 30, "Lithuania", "COUNTRY_LTU", "MINI_COUNTRY_LTU", "LTU.DDS" },		-- 리투아니아
	{ 31, "Luxembourg", "COUNTRY_LUX", "MINI_COUNTRY_LUX", "LUX.DDS" },		-- 룩셈부르크
	{ 32, "Macedonia", "COUNTRY_MKD", "MINI_COUNTRY_MKD", "MKD.DDS" },		-- 마케도니아
	{ 33, "Malta", "COUNTRY_MLT", "MINI_COUNTRY_MLT", "MLT.DDS" },		-- 몰타
	{ 34, "Moldova", "COUNTRY_MDA", "MINI_COUNTRY_MDA", "MDA.DDS" },		-- 몰도바
	{ 35, "Monaco", "COUNTRY_MON", "MINI_COUNTRY_MON", "MON.DDS" },		-- 모나코
	{ 36, "Netherlands", "COUNTRY_NED", "MINI_COUNTRY_NED", "NED.DDS" },		-- 네덜란드
	{ 37, "Norway", "COUNTRY_NOR", "MINI_COUNTRY_NOR", "NOR.DDS" },		-- 노르웨이
	{ 38, "Poland", "COUNTRY_POL", "MINI_COUNTRY_POL", "POL.DDS" },		-- 폴란드
	{ 39, "Portugal", "COUNTRY_POR", "MINI_COUNTRY_POR", "POR.DDS" },		-- 포르투갈
	{ 40, "Romania", "COUNTRY_ROM", "MINI_COUNTRY_ROM", "ROM.DDS" },		-- 루마니아
	{ 41, "San Marino", "COUNTRY_SMR", "MINI_COUNTRY_SMR", "SMR.DDS" },		-- 산마리노
	{ 42, "Serbia", "COUNTRY_SCG", "MINI_COUNTRY_SCG", "SCG.DDS" },		-- 세르비아
	{ 43, "Slovakia", "COUNTRY_SVK", "MINI_COUNTRY_SVK", "SVK.DDS" },		-- 슬로바키아
	{ 44, "Slovenia", "COUNTRY_SLO", "MINI_COUNTRY_SLO", "SLO.DDS" },		-- 슬로베니아
	{ 45, "Spain", "COUNTRY_ESP", "MINI_COUNTRY_ESP", "ESP.DDS" },		-- 스페인
	{ 46, "Svalbard", "COUNTRY_UN", "MINI_COUNTRY_SJM", "UN.DDS" },		-- 스발바르 제도
	{ 47, "Sweden", "COUNTRY_SWE", "MINI_COUNTRY_SWE", "SWE.DDS" },		-- 스웨덴
	{ 48, "Switzerland", "COUNTRY_SUI", "MINI_COUNTRY_SUI", "SUI.DDS" },		-- 스위스
	{ 49, "Ukraine", "COUNTRY_UKR", "MINI_COUNTRY_UKR", "UKR.DDS" },		-- 우크라이나
	{ 50, "United Kingdom", "COUNTRY_GBR", "MINI_COUNTRY_GBR", "GBR.DDS" },		-- 영국
}