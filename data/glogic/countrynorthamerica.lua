--
-- 주의) 
-- 1) 대/소 문자를 확실하게 구분해야 합니다.
-- 2) true, false 를 확실하게 구분해야 합니다.
-- 3) UCS-2 Little Endian 로 저장해야 합니다.
-------------------------------------------------------------------------------
-- 버젼;
Country_System_Version = 100;

-- 대륙이름 ;
ContinentName = "NorthAmerica"
Continent = 5

-- 국가;
Country= 
{
	-- 국가 ID | 국가 이름 | 국가 이미지 XML ID;
	{ 1, "Canada", "COUNTRY_CAN", "MINI_COUNTRY_CAN", "CAN.DDS" },		-- 캐나다
	{ 2, "Greenland", "COUNTRY_GRL", "MINI_COUNTRY_GRL", "GRL.DDS" },		-- 그린란드
	{ 3, "United States", "COUNTRY_USA", "MINI_COUNTRY_USA", "USA.DDS" },		-- 미국
}