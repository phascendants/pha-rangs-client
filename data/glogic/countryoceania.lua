--
-- 주의) 
-- 1) 대/소 문자를 확실하게 구분해야 합니다.
-- 2) true, false 를 확실하게 구분해야 합니다.
-- 3) UCS-2 Little Endian 로 저장해야 합니다.
-------------------------------------------------------------------------------
-- 버젼;
Country_System_Version = 100;

-- 대륙이름 ;
ContinentName = "Oceania"
Continent = 7

-- 국가;
Country= 
{
	-- 국가 ID | 국가 이름 | 국가 이미지 XML ID;
	{ 1, "American Samoa", "COUNTRY_UN", "MINI_COUNTRY_ASA", "UN.DDS" },		-- 아메리칸사모아
	{ 2, "Australia", "COUNTRY_AUS", "MINI_COUNTRY_AUS", "AUS.DDS" },		-- 오스트레일리아
	{ 3, "Cocos (Keeling) Islands", "COUNTRY_UN", "MINI_COUNTRY_CCK", "UN.DDS" },		-- 코코스 제도
	{ 4, "Cook Islands", "COUNTRY_COK", "MINI_COUNTRY_COK", "COK.DDS" },		-- 쿡 제도
	{ 5, "Federated States of Micronesia", "COUNTRY_FSM", "MINI_COUNTRY_FSM", "FSM.DDS" },		-- 미크로네시아
	{ 6, "Fiji", "COUNTRY_UN", "MINI_COUNTRY_FIJ", "UN.DDS" },		-- 피지
	{ 7, "French Polynesia", "COUNTRY_PYF", "MINI_COUNTRY_PYF", "PYF.DDS" },		-- 프랑스령폴리네시아
	{ 8, "Guam", "COUNTRY_GUM", "MINI_COUNTRY_GUM", "GUM.DDS" },		-- 괌
	{ 9, "Heard Island and McDonald Islands", "COUNTRY_UN", "MINI_COUNTRY_HMD", "UN.DDS" },		-- 허드 맥도널드 제도
	{ 10, "Kiribati", "COUNTRY_UN", "MINI_COUNTRY_KIR", "UN.DDS" },		-- 키리바시
	{ 11, "Marshall Islands", "COUNTRY_UN", "MINI_COUNTRY_MHL", "UN.DDS" },		-- 마셜 제도
	{ 12, "Nauru", "COUNTRY_UN", "MINI_COUNTRY_NRU", "UN.DDS" },		-- 나우루
	{ 13, "New Caledonia", "COUNTRY_UN", "MINI_COUNTRY_NCL", "UN.DDS" },		-- 뉴칼레도니아
	{ 14, "New Zealand", "COUNTRY_NZL", "MINI_COUNTRY_NZL", "NZL.DDS" },		-- 뉴질랜드
	{ 15, "Niue", "COUNTRY_NIU", "MINI_COUNTRY_NIU", "NIU.DDS" },		-- 니우에
	{ 16, "Norfolk Island", "COUNTRY_NFK", "MINI_COUNTRY_NFK", "NFK.DDS" },		-- 노퍽 섬
	{ 17, "Northern Mariana Islands", "COUNTRY_MNP", "MINI_COUNTRY_MNP", "MNP.DDS" },		-- 북 마리아나 제도
	{ 18, "Palau", "COUNTRY_PLW", "MINI_COUNTRY_PLW", "PLW.DDS" },		-- 팔라우
	{ 19, "Papua New Guinea", "COUNTRY_UN", "MINI_COUNTRY_PNG", "UN.DDS" },		-- 파푸아뉴기니
	{ 20, "Pitcairn Islands", "COUNTRY_UN", "MINI_COUNTRY_PCN", "UN.DDS" },		-- 핏케언 제도
	{ 21, "Samoa", "COUNTRY_UN", "MINI_COUNTRY_SAM", "UN.DDS" },		-- 사모아
	{ 22, "Solomon Islands", "COUNTRY_UN", "MINI_COUNTRY_SOL", "UN.DDS" },		-- 솔로몬 제도
	{ 23, "Tokelau", "COUNTRY_TKL", "MINI_COUNTRY_TKL", "TKL.DDS" },		-- 토켈라우제도
	{ 24, "Tonga", "COUNTRY_UN", "MINI_COUNTRY_TGA", "UN.DDS" },		-- 통가
	{ 25, "Tuvalu", "COUNTRY_TUV", "MINI_COUNTRY_TUV", "TUV.DDS" },		-- 투발루
	{ 26, "Vanuatu", "COUNTRY_UN", "MINI_COUNTRY_VAN", "UN.DDS" },		-- 바누아투
	{ 27, "Wallis and Futuna", "COUNTRY_UN", "MINI_COUNTRY_WLF", "UN.DDS" },		-- 월리스푸투나 제도
}