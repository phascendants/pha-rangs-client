--
-- 주의) 
-- 1) 대/소 문자를 확실하게 구분해야 합니다.
-- 2) true, false 를 확실하게 구분해야 합니다.
-- 3) UCS-2 Little Endian 로 저장해야 합니다.
-------------------------------------------------------------------------------
-- 버젼;
Country_System_Version = 100;

-- 대륙이름 ;
ContinentName = "SouthAmerica"
Continent = 6

-- 국가;
Country= 
{
	-- 국가 ID | 국가 이름 | 국가 이미지 XML ID;
	{ 1, "Anguilla", "COUNTRY_AIA", "MINI_COUNTRY_AIA", "AIA.DDS" },		-- 앙귈라
	{ 2, "Antigua and Barbuda", "COUNTRY_ATG", "MINI_COUNTRY_ATG", "ATG.DDS" },		-- 앤티가바부다
	{ 3, "Argentina", "COUNTRY_ARG", "MINI_COUNTRY_ARG", "ARG.DDS" },		-- 아르헨티나
	{ 4, "Aruba", "COUNTRY_ARU", "MINI_COUNTRY_ARU", "ARU.DDS" },		-- 아루바
	{ 5, "Bahamas", "COUNTRY_BHA", "MINI_COUNTRY_BHA", "BHA.DDS" },		-- 바하마
	{ 6, "Barbados", "COUNTRY_BAR", "MINI_COUNTRY_BAR", "BAR.DDS" },		-- 바베이도스
	{ 7, "Belize", "COUNTRY_UN", "MINI_COUNTRY_BIZ", "UN.DDS" },		-- 벨리즈
	{ 8, "Bermuda", "COUNTRY_BER", "MINI_COUNTRY_BER", "BER.DDS" },		-- 버뮤다
	{ 9, "Bolivia", "COUNTRY_UN", "MINI_COUNTRY_BOL", "UN.DDS" },		-- 볼리비아
	{ 10, "Brazil", "COUNTRY_BRA", "MINI_COUNTRY_BRA", "BRA.DDS" },		-- 브라질
	{ 11, "British Virgin Islands", "COUNTRY_UN", "MINI_COUNTRY_IVB", "UN.DDS" },		-- 영국령 버진 아일랜드
	{ 12, "Cayman Islands", "COUNTRY_CAY", "MINI_COUNTRY_CAY", "CAY.DDS" },		-- 케이맨 제도
	{ 13, "Chile", "COUNTRY_CHI", "MINI_COUNTRY_CHI", "CHI.DDS" },		-- 칠레
	{ 14, "Colombia", "COUNTRY_COL", "MINI_COUNTRY_COL", "COL.DDS" },		-- 콜롬비아
	{ 15, "Costa Rica", "COUNTRY_CRI", "MINI_COUNTRY_CRI", "CRI.DDS" },		-- 코스타리카
	{ 16, "Cuba", "COUNTRY_CUB", "MINI_COUNTRY_CUB", "CUB.DDS" },		-- 쿠바
	{ 17, "Commonwealth of Dominica", "COUNTRY_DMA", "MINI_COUNTRY_DMA", "DMA.DDS" },		-- 도미니카 연방
	{ 18, "Dominican Republic", "COUNTRY_UN", "MINI_COUNTRY_DOM", "UN.DDS" },		-- 도미니카 공화국
	{ 19, "Ecuador", "COUNTRY_ECU", "MINI_COUNTRY_ECU", "ECU.DDS" },		-- 에콰도르
	{ 20, "El Salvador", "COUNTRY_UN", "MINI_COUNTRY_ESA", "UN.DDS" },		-- 엘살바도르
	{ 21, "Falkland Islands (Islas Malvinas)", "COUNTRY_FLK", "MINI_COUNTRY_FLK", "FLK.DDS" },		-- 포클랜드 제도
	{ 22, "Grenada", "COUNTRY_GRN", "MINI_COUNTRY_GRN", "GRN.DDS" },		-- 그레나다
	{ 23, "Guatemala", "COUNTRY_UN", "MINI_COUNTRY_GUA", "UN.DDS" },		-- 과테말라
	{ 24, "Haiti", "COUNTRY_UN", "MINI_COUNTRY_HAI", "UN.DDS" },		-- 아이티
	{ 25, "Honduras", "COUNTRY_UN", "MINI_COUNTRY_HON", "UN.DDS" },		-- 온두라스
	{ 26, "Jamaica", "COUNTRY_JAM", "MINI_COUNTRY_JAM", "JAM.DDS" },		-- 자메이카
	{ 27, "Mexico", "COUNTRY_MEX", "MINI_COUNTRY_MEX", "MEX.DDS" },		-- 멕시코
	{ 28, "Montserrat", "COUNTRY_UN", "MINI_COUNTRY_MSR", "UN.DDS" },		-- 몬세라트
	{ 29, "Netherlands Antilles", "COUNTRY_UN", "MINI_COUNTRY_AHO", "UN.DDS" },		-- 네덜란드령 안틸레스
	{ 30, "Nicaragua", "COUNTRY_UN", "MINI_COUNTRY_NCA", "UN.DDS" },		-- 니카라과
	{ 31, "Panama", "COUNTRY_PAN", "MINI_COUNTRY_PAN", "PAN.DDS" },		-- 파나마
	{ 32, "Paraguay", "COUNTRY_UN", "MINI_COUNTRY_PAR", "UN.DDS" },		-- 파라과이
	{ 33, "Peru", "COUNTRY_PER", "MINI_COUNTRY_PER", "PER.DDS" },		-- 페루
	{ 34, "Puerto Rico", "COUNTRY_PUR", "MINI_COUNTRY_PUR", "PUR.DDS" },		-- 푸에르토리코
	{ 35, "Saint Kitts and Nevis", "COUNTRY_SKN", "MINI_COUNTRY_SKN", "SKN.DDS" },		-- 세인트키츠네비스
	{ 36, "Saint Lucia", "COUNTRY_LCA", "MINI_COUNTRY_LCA", "LCA.DDS" },		-- 세인트루시아
	{ 37, "Saint Pierre and Miquelon", "COUNTRY_UN", "MINI_COUNTRY_SPM", "UN.DDS" },		-- 생피에르 미클롱
	{ 38, "Saint Vincent and the Grenadines", "COUNTRY_VIN", "MINI_COUNTRY_VIN", "VIN.DDS" },		-- 세인트빈센트 그레나딘
	{ 39, "South Georgia and the South Sandwich Islands", "COUNTRY_UN", "MINI_COUNTRY_SGS", "UN.DDS" },		-- 사우스조지아 사우스샌드위치 제도
	{ 40, "Suriname", "COUNTRY_SUR", "MINI_COUNTRY_SUR", "SUR.DDS" },		-- 수리남
	{ 41, "Trinidad and Tobago", "COUNTRY_TRI", "MINI_COUNTRY_TRI", "TRI.DDS" },		-- 트리니다드토바고
	{ 42, "Turks and Caicos Islands", "COUNTRY_TCA", "MINI_COUNTRY_TCA", "TCA.DDS" },		-- 터크스 케이커스 제도
	{ 43, "Uruguay", "COUNTRY_URU", "MINI_COUNTRY_URU", "URU.DDS" },		-- 우루과이
	{ 44, "Venezuela", "COUNTRY_VEN", "MINI_COUNTRY_VEN", "VEN.DDS" },		-- 베네수엘라
	{ 45, "Virgin Islands", "COUNTRY_ISV", "MINI_COUNTRY_ISV", "ISV.DDS" },		-- 버진 제도
}