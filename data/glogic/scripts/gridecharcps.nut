// 방어구 강화이펙트 설정 190508 이익세 : 인게임 강화 +11~15 에서만 이펙트 나옴

function GradeFunc(arg1)
{
//-----------------------------------------------------------------
// 남자
	arg1.SetGradeUpMan( [ 		"",	// CheEdit_Lev 1
					"",	// CheEdit_Lev 2
					"",	// CheEdit_Lev 3
					"",	// CheEdit_Lev 4
					"",	// CheEdit_Lev 5
					"m_bs_body_enchent+6.cps",	// CheEdit_Lev 6
					"m_bs_body_enchent+7.cps",	// CheEdit_Lev 7
					"",	// CheEdit_Lev 8
					"",	// CheEdit_Lev 9
					"",	// CheEdit_Lev 10
					"" ])	// CheEdit_Lev 11

	arg1.SetGradeDownMan( [ 	"",	// CheEdit_Lev 1
					"",	// CheEdit_Lev 2
					"",	// CheEdit_Lev 3
					"",	// CheEdit_Lev 4
					"",	// CheEdit_Lev 5
					"m_bs_leg_enchent+6.cps",	// CheEdit_Lev 6
					"m_bs_leg_enchent+7.cps",	// CheEdit_Lev 7
					"",	// CheEdit_Lev 8
					"",	// CheEdit_Lev 9
					"",	// CheEdit_Lev 10
					"" ])	// CheEdit_Lev 11

	arg1.SetGradeHandMan( [ 	"",	// CheEdit_Lev 1
					"",	// CheEdit_Lev 2
					"",	// CheEdit_Lev 3
					"",	// CheEdit_Lev 4
					"",	// CheEdit_Lev 5
					"m_bs_hand_enchent+6.cps",	// CheEdit_Lev 6
					"m_bs_hand_enchent+7.cps",	// CheEdit_Lev 7
					"",	// CheEdit_Lev 8
					"",	// CheEdit_Lev 9
					"",	// CheEdit_Lev 10
					"" ])	// CheEdit_Lev 11

	arg1.SetGradeFootMan( [ 	"",	// CheEdit_Lev 1
					"",	// CheEdit_Lev 2
					"",	// CheEdit_Lev 3
					"",	// CheEdit_Lev 4
					"",	// CheEdit_Lev 5
					"m_bs_foot_enchent+6.cps",	// CheEdit_Lev 6
					"m_bs_foot_enchent+7.cps",	// CheEdit_Lev 7
					"",	// CheEdit_Lev 8
					"",	// CheEdit_Lev 9
					"",	// CheEdit_Lev 10
					"" ])	// CheEdit_Lev 11


//-----------------------------------------------------------------
// 여자
	arg1.SetGradeUpWoman( [ 	"",	// CheEdit_Lev 1
					"",	// CheEdit_Lev 2
					"",	// CheEdit_Lev 3
					"",	// CheEdit_Lev 4
					"",	// CheEdit_Lev 5
					"w_bs_body_enchent+6.cps",	// CheEdit_Lev 6
					"w_bs_body_enchent+7.cps",	// CheEdit_Lev 7
					"",	// CheEdit_Lev 8
					"",	// CheEdit_Lev 9
					"",	// CheEdit_Lev 10
					"" ])	// CheEdit_Lev 11

	arg1.SetGradeDownWoman( [ 	"",	// CheEdit_Lev 1
					"",	// CheEdit_Lev 2
					"",	// CheEdit_Lev 3
					"",	// CheEdit_Lev 4
					"",	// CheEdit_Lev 5
					"w_bs_leg_enchent+6.cps",	// CheEdit_Lev 6
					"w_bs_leg_enchent+7.cps",	// CheEdit_Lev 7
					"",	// CheEdit_Lev 8
					"",	// CheEdit_Lev 9
					"",	// CheEdit_Lev 10
					"" ])	// CheEdit_Lev 11

	arg1.SetGradeHandWoman( [ 	"",	// CheEdit_Lev 1
					"",	// CheEdit_Lev 2
					"",	// CheEdit_Lev 3
					"",	// CheEdit_Lev 4
					"",	// CheEdit_Lev 5
					"w_bs_hand_enchent+6.cps",	// CheEdit_Lev 6
					"w_bs_hand_enchent+7.cps",	// CheEdit_Lev 7
					"",	// CheEdit_Lev 8
					"",	// CheEdit_Lev 9
					"",	// CheEdit_Lev 10
					"" ])	// CheEdit_Lev 11

	arg1.SetGradeFootWoman( [ 	"",	// CheEdit_Lev 1
					"",	// CheEdit_Lev 2
					"",	// CheEdit_Lev 3
					"",	// CheEdit_Lev 4
					"",	// CheEdit_Lev 5
					"w_bs_foot_enchent+6.cps",	// CheEdit_Lev 6
					"w_bs_foot_enchent+7.cps",	// CheEdit_Lev 7
					"",	// CheEdit_Lev 8
					"",	// CheEdit_Lev 9
					"",	// CheEdit_Lev 10
					"" ])	// CheEdit_Lev 11

}
