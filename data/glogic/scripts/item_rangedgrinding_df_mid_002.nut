
GrindingOption <-
{
	Type      = EMGRINDING_TYPE_DEFENSE, // EMGRINDING_TYPE_DAMAGE, EMGRINDING_TYPE_DEFENSE
	Level     = EMGRINDING_LEVEL_HIGH, // NORMAL, HIGH, TOP
	Purpose   = EMGRINDING_PURPOSE_CLOTH, // EMGRINDING_PURPOSE_ARM, EMGRINDING_PURPOSE_CLOTH
	Attribute = ATTR_RANDOM 
		| ATTR_IGNORE_DEFAULTREQ 
		| ATTR_IGNORE_PROTECTED 
		| ATTR_IGNORE_RATEINC // ATTR_IGNORE_RATEINC, ATTR_NODEGRATION, ATTR_ADDED
		| ATTR_NODEGRATION,
	
	Consume 	= 1,
	Enchant_Min = 0,
	Enchant_Max = 10,
	Range_Min 	= 2,
	Range_Max 	= 6,
	
	Chance = [
		{ Grade = 0, Percent = 0.0 }
		{ Grade = 1, Percent = 0.0 }
		{ Grade = 2, Percent = 25.0 }
		{ Grade = 3, Percent = 25.0  }
		{ Grade = 4, Percent = 25.0 }
		{ Grade = 5, Percent = 15.0  }
		{ Grade = 6, Percent = 10.0  }
		{ Grade = 7, Percent = 0.0  }
		{ Grade = 8, Percent = 0.0  }
		{ Grade = 9, Percent = 0.0  }
		{ Grade = 10, Percent = 0.0  }
		{ Grade = 11, Percent = 0.0  }
		{ Grade = 12, Percent = 0.0  }
		{ Grade = 13, Percent = 0.0  }
		{ Grade = 14, Percent = 0.0  }
		{ Grade = 15, Percent = 0.0  }
	]
}

