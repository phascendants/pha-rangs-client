/**
	PVE 무한의 계단 
	클라 메인 스크립트        작성자 이익세
	
 */

// 맵 정보
root <- MapInfoTrigger();
root.SetMap(233, 0);									// 맵ID
root.SetGateID(2);									// 맵의 게이트ID
root.SetMapName("COMPETITION_PVE_MAPNAME");			// 맵명 XML
root.SetObject("WAIL_OF_THE_BUILDING_OBJECT_TEXT"); // 맵목표 XML //통합전장 UI에서만 사용
root.SetMapInfo("COMPETITION_PVE_INFO_TEXT");		// 맵정보 XML
root.SetMapInfoRn("WAIL_OF_THE_BUILDING_INFO");		// 맵정보 XML //통합전장 UI에서만 사용
root.SetMapReward("COMPETITION_PVE_REWARD_TEXT");	// 맵보상 XML
root.SetDailyLimit(1);								// 하루 입장 제한
root.SetOwnTime(4800);								// 귀속 시간
root.SetPerson(5);									// 인원 제한
root.SetWaitEntrance(10);							// 입장 확인 대기 시간

// 난이도 일반
level0 <- LevelTrigger();
level0.SetFile("InfinitystairsLev0.nut");				
level0.SetLevel(0);							
level0.SetUserLevel(190, 300);
level0.Load();			
root.InsertSibling(level0);

// 난이도 중
//level1 <- LevelTrigger();
//level1.SetFile("InfinitystairsLev1.nut");
//level1.SetLevel(1);
//level1.SetUserLevel(220, 260);
//level1.Load();
//root.InsertSibling(level1);

// 난이도 상
//level2 <- LevelTrigger();
//level2.SetFile("InfinitystairsLev2.nut");
//level2.SetLevel(2);
//level2.SetUserLevel(230, 260);
//level2.Load();
//root.InsertSibling(level2);
