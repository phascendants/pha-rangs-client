/**
	PVE 외벽대전
	클라 메인 스크립트        작성자 이익세
	
 */

// 맵 정보
root <- MapInfoTrigger();
root.SetMap(245, 0);						// 맵ID
root.SetGateID(2);						// 맵의 게이트ID
root.SetMapName("EXTERNALW_PVE_MAPNAME");			// 맵명 XML
root.SetObject("EXTERNALW_OF_THE_BUILDING_OBJECT_TEXT"); 	// 맵목표 XML //통합전장 UI에서만 사용
root.SetMapInfo("COMPETITION_PVE_INFO_TEXT");			// 맵정보 XML
root.SetMapInfoRn("EXTERNALW_OF_THE_BUILDING_INFO");		// 맵정보 XML //통합전장 UI에서만 사용
root.SetMapReward("EXTERNALW_PVE_REWARD_TEXT");			// 맵보상 XML
root.SetDailyLimit(1);						// 하루 입장횟수 (라이브 적용 시 -> 1)
root.SetOwnTime(2400);						// 귀속 시간  40분 (테스트 및 라이브 40분)
root.SetPerson(2);						// 인원 제한 (테스트 및 라이브 2)
root.SetWaitEntrance(10);					// 입장 확인 대기 시간

// 난이도 일반
level0 <- LevelTrigger();
level0.SetFile("InfinitystairsALev0.nut");				
level0.SetLevel(0);							
level0.SetUserLevel(210, 300);
level0.Load();			
root.InsertSibling(level0);

