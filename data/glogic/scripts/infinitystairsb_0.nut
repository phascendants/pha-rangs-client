/**
	PVE 한계의 사막
	클라 공통 콤포넌트 스크립트        작성자 김판수
	
 */
 
// 입장 대기 표시
root <- ReadyTrigger();

// 게이트 닫음 처리
close1 <- PortalCloseTrigger();
close1.SetFromGateID(0);
root.InsertSibling(close1);
close2 <- PortalCloseTrigger();
close2.SetFromGateID(1);
root.InsertSibling(close2);
close3 <- PortalCloseTrigger();
close3.SetFromGateID(2);
root.InsertSibling(close3);

// 스테이지 시작 정보 표시
start <- StartStageTrigger();
root.InsertSibling(start);

// 메시지 출력 표시
msg <- MsgTrigger();
root.InsertSibling(msg);

// 스테이지 표시(*)
stage <- StageTrigger();
stage.SetRange(2);
stage.SetEndStage(2);
root.InsertSibling(stage);

// 몬스터 미션 표시
clear <- MonsterClearTrigger();
root.InsertSibling(clear);

// 이동 미션 표시
pos <- PositioningClearTrigger();
root.InsertSibling(pos);

// 재도전 제어 표시
retry <- RetryTrigger();
retry.SetTime(60);
retry.SetMoney(2500);
retry.SetControl(0.5);
root.InsertSibling(retry);
