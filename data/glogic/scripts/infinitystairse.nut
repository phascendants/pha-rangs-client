// 맵 정보
root <- MapInfoTrigger();
root.SetMap(350, 0);						// 맵ID
root.SetGateID(2);						// 맵의 게이트ID
root.SetMapName("BLOODYRAID_PVE_MAPNAME");			// 맵명 XML
root.SetObject("BLOODYRAID_OF_THE_BUILDING_OBJECT_TEXT"); 	// 맵목표 XML //통합전장 UI에서만 사용
root.SetMapInfo("COMPETITION_PVE_INFO_TEXT");			// 맵정보 XML
root.SetMapInfoRn("BLOODYRAID_OF_THE_BUILDING_INFO");		// 맵정보 XML //통합전장 UI에서만 사용
root.SetMapReward("BLOODYRAID_PVE_REWARD_TEXT");			// 맵보상 XML
root.SetDailyLimit(1);						// 하루 입장횟수 (라이브 적용 시 -> 2)
root.SetOwnTime(6000);						// 귀속 시간  100분 (테스트 및 라이브 100분)
root.SetPerson(2);						// 인원 제한 (테스트 및 라이브 2)
root.SetWaitEntrance(10);					// 입장 확인 대기 시간

// 난이도 일반
level0 <- LevelTrigger();
level0.SetFile("InfinitystairsELev0.nut");				
level0.SetLevel(0);							
level0.SetUserLevel(260, 300);
level0.Load();			
root.InsertSibling(level0);

