/**
	PVE 한계의 사막
	클라 메인 스크립트        작성자 김판수
	
 */

// 맵 정보
root <- MapInfoTrigger();
root.SetMap(246, 0);						// 맵ID
root.SetGateID(2);						// 맵의 게이트ID
root.SetMapName("LIMITDESERT_PVE_MAPNAME");			// 맵명 XML
root.SetObject("LIMIT_DESERT_OBJECT_TEXT"); 	// 맵목표 XML //통합전장 UI에서만 사용
root.SetMapInfo("COMPETITION_PVE_INFO_TEXT");			// 맵정보 XML
root.SetMapInfoRn("LIMIT_DESERT_INFO");		// 맵정보 XML //통합전장 UI에서만 사용
root.SetMapReward("LIMITDESERT_PVE_REWARD_TEXT");			// 맵보상 XML
root.SetDailyLimit(1);						// 하루 입장횟수 (라이브 적용 시 -> 2)
root.SetOwnTime(6000);						// 귀속 시간  100분 (테스트 및 라이브 100분)
root.SetPerson(2);						// 인원 제한 (테스트 및 라이브 2)
root.SetWaitEntrance(10);					// 입장 확인 대기 시간

// 난이도 일반
level0 <- LevelTrigger();
level0.SetFile("InfinitystairsBLev0.nut");				
level0.SetLevel(0);							
level0.SetUserLevel(245, 300);
level0.Load();			
root.InsertSibling(level0);

