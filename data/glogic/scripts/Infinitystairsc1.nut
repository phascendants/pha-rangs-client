/**
	PVE 비밀의 피라미드 (보물의방)
	클라 메인 스크립트        작성자 이익세 170725
	
 */

// 맵 정보
root <- MapInfoTrigger();
root.SetMap(293, 0);						// 맵ID
root.SetGateID(2);						// 맵의 게이트ID
root.SetMapName("Pyramid2_PVE_MAPNAME");			// 맵명 XML
root.SetObject("Pyramid2_OF_THE_BUILDING_OBJECT_TEXT"); 	// 맵목표 XML //통합전장 UI에서만 사용
root.SetMapInfo("COMPETITION_PVE_INFO_TEXT");			// 맵정보 XML
root.SetMapInfoRn("Pyramid2_OF_THE_BUILDING_INFO");		// 맵정보 XML //통합전장 UI에서만 사용
root.SetMapReward("Pyramid2_PVE_REWARD_TEXT");			// 맵보상 XML
root.SetDailyLimit(1);						// 하루 입장횟수 (라이브/테섭/알파 : 1/1/50회)
root.SetOwnTime(3600);						// 귀속 시간 (라이브/테섭 : 1시간)
root.SetPerson(2);						// 인원 제한 (라이브/테섭 : 2인 이상)
root.SetWaitEntrance(10);					// 입장 확인 대기 시간

// 난이도 일반
level0 <- LevelTrigger();
level0.SetFile("InfinitystairsC1Lev0.nut");				
level0.SetLevel(0);							
level0.SetUserLevel(190, 300);
level0.Load();			
root.InsertSibling(level0);

