
GrindingOption <-
{
	Type      = EMGRINDING_TYPE_DEFENSE, // EMGRINDING_TYPE_DAMAGE, EMGRINDING_TYPE_DEFENSE
	Level     = EMGRINDING_LEVEL_NORMAL, // NORMAL, HIGH, TOP
	Purpose   = EMGRINDING_PURPOSE_CLOTH, // EMGRINDING_PURPOSE_ARM, EMGRINDING_PURPOSE_CLOTH
	Attribute = ATTR_RANDOM 
		| ATTR_IGNORE_DEFAULTREQ 
		| ATTR_IGNORE_PROTECTED 
		| ATTR_IGNORE_RATEINC, // ATTR_IGNORE_RATEINC, ATTR_NODEGRATION, ATTR_ADDED
	
	Consume 	= 1,
	Enchant_Min = 0,
	Enchant_Max = 10,
	Range_Min 	= 5,
	Range_Max 	= 10,
	
	Chance = [
		{ Grade = 0, Percent = 0.0 }
		{ Grade = 1, Percent = 0.0 }
		{ Grade = 2, Percent = 0.0 }
		{ Grade = 3, Percent = 0.0  }
		{ Grade = 4, Percent = 0.0 }
		{ Grade = 5, Percent = 25.0  }
		{ Grade = 6, Percent = 25.0  }
		{ Grade = 7, Percent = 20.0  }
		{ Grade = 8, Percent = 15.0  }
		{ Grade = 9, Percent = 10.0  }
		{ Grade = 10, Percent = 5.0  }
		{ Grade = 11, Percent = 0.0  }
		{ Grade = 12, Percent = 0.0  }
		{ Grade = 13, Percent = 0.0  }
		{ Grade = 14, Percent = 0.0  }
		{ Grade = 15, Percent = 0.0  }
	]
}