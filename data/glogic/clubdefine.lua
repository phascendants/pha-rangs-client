-------------------------------------------------------------------------------
-- Club 세팅 파일
-- 2012-03-09 Jgkim
--
-- 주의) 
-- 1) 대/소 문자를 확실하게 구분해야 합니다.
-- 2) 1, 0, true, false, null 을 확실하게 구분해야 합니다.
-- 3) UCS-2 Little Endian 로 저장해야 합니다.
-- 4) 문자열을 작성할때는 L"문자열" 로 작성하십시오. (unicode)
-------------------------------------------------------------------------------

-- 클럽 권한 문자열
ClubAuthorityDesc =
{
	{  0, L"Invite Member", }, -- 클럽원 가입
	{  1, L"Oust Member", }, -- 클럽원 추방 
	{  2, L"Edit Announcement", }, -- 공지작성/변경	  
	{  3, L"Edit Club Mark", }, -- 클럽 마크 변경
	{  4, L"CDM Qualification", }, -- CDM 참가 자격
	{  5, L"Club Rank Up", }, -- 클럽 랭크업
	{  6, L"Edit Member Rank", }, -- 클럽원 계급 편집/변경
	{  7, L"Club Locker Usage", }, -- 클럽 창고/아이템 넣기/꺼내기
	{  8, L"Club Locker Gold", }, -- 클럽 창고/입출금
	{  9, L"Club Alliance", }, -- 클럽 동맹 신청하기/받기
	{ 10, L"Terminate Ally", }, -- 클럽 동맹 해지
	{ 11, L"Club Battle", }, -- 클럽 배틀 신청 하기/받기/항복/휴전
}

-- 초기 클럽 권한 설정
ClubAuthorityBase = 
{
	-- 0 번 계급
    { L"Newbie", -- 계급 이름
      false, -- 클럽원 가입
	  false, -- 클럽원 추방 
	  false, -- 공지작성/변경	  
	  false, -- 클럽 마크 변경
	  false, -- CDM 참가 자격
	  false, -- 클럽 랭크업
	  false, -- 클럽원 계급 편집/변경
	  false, -- 클럽 창고/아이템 넣기/꺼내기
	  false, -- 클럽 창고/입출금
	  false, -- 클럽 동맹 신청하기/받기
	  false, -- 클럽 동맹 해지
	  false, -- 클럽 배틀 신청 하기/받기/항복/휴전
	}, 
	
	{ L"Private",    true, false,  false, false, false, false, false, false, false, false, false, false, }, -- 1 번 계급
	{ L"Corporal",   true,  true,  false, false, false, false, false, false, false, false, false, false, }, -- 2 번 계급
	{ L"Sergeant",   true,  true,   true, false, false, false, false, false, false, false, false, false, }, -- 3 번 계급
	{ L"Chief",      true,  true,   true,  true, false, false, false, false, false, false, false, false, }, -- 4 번 계급
	{ L"Lieutenant", true,  true,   true,  true,  true, false, false, false, false, false, false, false, }, -- 5 번 계급
	{ L"Captain",    true,  true,   true,  true,  true,  true, false, false, false, false, false, false, }, -- 6 번 계급
	{ L"Major",      true,  true,   true,  true,  true,  true,  true, false, false, false, false, false, }, -- 7 번 계급
	{ L"Colonel",    true,  true,   true,  true,  true,  true,  true,  true, false, false, false, false, }, -- 8 번 계급
	{ L"Deputy CM",    true,  true,   true,  true,  true,  true,  true,  true,  true,  true,  true,  true, }, -- 9 번 계급
}

-- 클럽 로그 지우기 시간
ClubLogDefine =
{
    -- 서버는 매2분마다 현재시간에서 해당 시간이 지난 data 를 삭제합니다.
	-- 1일 = 60 * 60 * 24 = 86400
	DeleteTime = 86401,
}

-- 특정 몬스터 사망시 로그
-- 아래에 기록된 Monster 는 해당 Monster 에게 사망시 클럽로그에 남는다.
-- GM 명령어 /crow_list 참고
ClubRecordKilledByMonster = 
{
	{40,  17}, -- 파이어 마스터
	{40,  18}, -- 염력술사
	{40,  27}, -- 후디니
	{40,  28}, -- 타임 마스터
	{42,  10}, -- 제타
	{44,  35}, -- 리플렉터
	{44,  13}, -- 미스터 팻
	{44,  14}, -- 리볼버
	{44,  15}, -- 미스 캐미컬
	{44,  34}, -- 풍수사(회장)
	{48,  22}, -- 가이넌
	{48,  23}, -- 루시안
	{48,  9}, -- 격노의 사제
	{57, 27}, -- 최종 정신격류체
}

-- 특정 몬스터를 쓰러뜨릴 시 로그
-- 아래에 기록된 Monster 는 해당 Monster 를 쓰러뜨린경우 클럽로그에 남는다.
-- GM 명령어 /crow_list 참고
ClubRecordKillMonster =
{
	{40,  17}, -- 파이어 마스터
	{40,  18}, -- 염력술사
	{40,  27}, -- 후디니
	{40,  28}, -- 타임 마스터
	{42,  10}, -- 제타
	{44,  35}, -- 리플렉터
	{44,  13}, -- 미스터 팻
	{44,  14}, -- 리볼버
	{44,  15}, -- 미스 캐미컬
	{44,  34}, -- 풍수사(회장)
	{48,  22}, -- 가이넌
	{48,  23}, -- 루시안
	{48,  9}, -- 격노의 사제
	{57, 27}, -- 최종 정신격류체
}

-- 특정 레벨 이상 달성 시 로그
ClubRecordMemberLevel =
{
	LevelOver = 197, -- 해당 레벨 이상이면 로그를 남긴다.
	LevelReach = { 40, 80, 110, 140, 190, 207 }, -- 해당 레벨에 도달하면 로그를 남긴다.
}