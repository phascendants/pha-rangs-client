--
-- 주의) 
-- 1) 대/소 문자를 확실하게 구분해야 합니다.
-- 2) true, false 를 확실하게 구분해야 합니다.
-- 3) UCS-2 Little Endian 로 저장해야 합니다.
-------------------------------------------------------------------------------
-- 버젼;
Country_System_Version = 100;

-- 대륙이름 ;
ContinentName = "MiddleEast"
Continent = 3

-- 국가;
Country= 
{
	-- 국가 ID | 국가 이름 | 국가 이미지 XML ID;
	{ 1, "Afghanistan", "COUNTRY_UN", "MINI_COUNTRY_AFG", "UN.DDS" },		-- 아프가니스탄
	{ 2, "Armenia", "COUNTRY_ARM", "MINI_COUNTRY_ARM", "ARM.DDS" },		-- 아르메니아
	{ 3, "Bahrain", "COUNTRY_BRN", "MINI_COUNTRY_BRN", "BRN.DDS" },		-- 바레인
	{ 4, "Iran", "COUNTRY_IRN", "MINI_COUNTRY_IRN", "IRN.DDS" },		-- 이란
	{ 5, "Iraq", "COUNTRY_UN", "MINI_COUNTRY_IRQ", "UN.DDS" },		-- 이라크
	{ 6, "Israel", "COUNTRY_ISR", "MINI_COUNTRY_ISR", "ISR.DDS" },		-- 이스라엘
	{ 7, "Jordan", "COUNTRY_JOR", "MINI_COUNTRY_JOR", "JOR.DDS" },		-- 요르단
	{ 8, "Kuwait", "COUNTRY_KUW", "MINI_COUNTRY_KUW", "KUW.DDS" },		-- 쿠웨이트
	{ 9, "Lebanon", "COUNTRY_LIB", "MINI_COUNTRY_LIB", "LIB.DDS" },		-- 레바논
	{ 10, "Oman", "COUNTRY_OMA", "MINI_COUNTRY_OMA", "OMA.DDS" },		-- 오만
	{ 11, "Qatar", "COUNTRY_QAT", "MINI_COUNTRY_QAT", "QAT.DDS" },		-- 카타르
	{ 12, "Saudi Arabia", "COUNTRY_KSA", "MINI_COUNTRY_KSA", "KSA.DDS" },		-- 사우디아라비아
	{ 13, "Syria", "COUNTRY_UN", "MINI_COUNTRY_SYR", "UN.DDS" },		-- 시리아
	{ 14, "Turkey", "COUNTRY_TUR", "MINI_COUNTRY_TUR", "TUR.DDS" },		-- 터키
	{ 15, "Yemen", "COUNTRY_UN", "MINI_COUNTRY_YEM", "UN.DDS" },		-- 예멘
}