--
-- 주의) 
-- 1) 대/소 문자를 확실하게 구분해야 합니다.
-- 2) true, false 를 확실하게 구분해야 합니다.
-- 3) UCS-2 Little Endian 로 저장해야 합니다.
-------------------------------------------------------------------------------
-- 버젼;
Country_System_Version = 100;

-- 대륙;
Continent = 
{
	"CountryAsia.lua",
	--"CountryEurope.lua",
	--"CountryMiddleEast.lua",
	--"CountryAfrica.lua",
	--"CountryNorthAmerica.lua",
	--"CountrySouthAmerica.lua",
	--"CountryOceania.lua",
}