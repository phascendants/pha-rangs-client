
print("------->  Attendance script starts.  <-------")
print(" ")
print(" ")

//
// Note: 
// 1. ,(comma) is Optional
//


//------------------------------------------------------------------------
//												customization of  values
//------------------------------------------------------------------------


//
// 하루에 수행가능한 과제의 수 설정
//
NumberOfTasksPerOneDay <- 
{
	Min	= 4		//최소 과제수
	Max	= 4		//최대 과제수
}


NumberOfItemsPerOneTask <- 3



//
// Type 설정
//
Types <-
{
	Connection		= 0
	SparringSolo	= 1
	SparringParty	= 2
	Whisper			= 3
	Party				= 4
	EnchantW		= 5
	EnchantWC		= 6
	EnchantA			= 7
	EnchantAC		= 8
	PointItem			= 9
	PostSend			= 10
}


ValueRanges <-	{}
ValueRanges[ Types.Connection ] 	<- { min = 10,		max=30 };
ValueRanges[ Types.SparringSolo ]	<- { min = 1,		max=5 };
ValueRanges[ Types.SparringParty ]	<- { min = 1,		max=2 };
ValueRanges[ Types.Whisper ]		<- { min = 3,		max=7};
ValueRanges[ Types.Party ]			<- { min = 2,		max=6 };
ValueRanges[ Types.EnchantW ]		<- { min = 1,		max=5};
ValueRanges[ Types.EnchantWC ]		<- { min = 2, 		max=4 };
ValueRanges[ Types.EnchantA ]		<- { min = 1,		max=5 };
ValueRanges[ Types.EnchantAC ]		<- { min = 2,		max=4 };
ValueRanges[ Types.PointItem ]		<- { min = 2000, 	max=100000 };
ValueRanges[ Types.PostSend ]		<- { min = 1,		max=3 };



//
// Default 값 설정
//
defaultValues <- {}
defaultValues[ Types.Connection ] 	<- 60;
defaultValues[ Types.SparringSolo ]	<- 2;
defaultValues[ Types.SparringParty ]	<- 2;
defaultValues[ Types.Whisper ]			<- 1;
defaultValues[ Types.Party ]				<- 3;
defaultValues[ Types.EnchantW ]		<- 1;
defaultValues[ Types.EnchantWC ]		<- 2;
defaultValues[ Types.EnchantA ]		<- 1;
defaultValues[ Types.EnchantAC ]		<- 2;
defaultValues[ Types.PointItem ]			<- 12000;
defaultValues[ Types.PostSend ]		<- 3;



//
// RewardType 설정
//
RewardTypes <-
{
	Exp		= 0
	Point	= 1
	Money	= 2
	Item		= 3
}


//
//	보상 아이템 설정
//

//Note: Squirrel은 zero-base이므로 혼란을 줄이기 위해 모든 코드는 0-base로 작성하도록 한다.

RewardItems <- {}

RewardItems[0]		<- {} // 1일 연속 출석
RewardItems[0][0] <- {mid=261,sid=1} 	// 1일 출석 선물상자

// ...


RewardItems[1]		<- {} // 2일 연속 출석
RewardItems[1][0] <- {mid=261,sid=2} 	// 2일 출석 선물상자

// ...


RewardItems[2]		<- {} // 3일 연속 출석
RewardItems[2][0] <- {mid=261,sid=3} 	// 3일 출석 선물상자
// ...


RewardItems[3]		<- {} // 4일 연속 출석
RewardItems[3][0] <- {mid=261,sid=4} 	// 4일 출석 선물상자
// ...


RewardItems[4] 		<- {} // 5일 연속 출석
RewardItems[4][0] <- {mid=261,sid=5} 	// 5일 출석 선물상자
// ...


RewardItems[5] 		<- {} // 6일 연속 출석
RewardItems[5][0] <- {mid=261,sid=6} 	// 6일 출석 선물상자
// ...


RewardItems[6] 		<- {} // 7일 연속 출석
RewardItems[6][0] <- {mid=261,sid=7} 	// 7일 출석 선물상자

// ...



//
// 보상 생활 점수
//
RewardPoint 		<- {}
RewardPoint[0]	<- {min=1,	max=10}	//연속 1일 출석
RewardPoint[1]	<- {min=11,	max=20}	//연속 2일 출석
RewardPoint[2]	<- {min=21,	max=30}	//연속 3일 출석
RewardPoint[3]	<- {min=31,	max=40}	//연속 4일 출석
RewardPoint[4]	<- {min=41,	max=50}	//연속 5일 출석
RewardPoint[5]	<- {min=51,	max=80}	//연속 6일 출석
RewardPoint[6]	<- {min=81,	max=100}	//연속 7일 출석





//------------------------------------------------------------------------
//														functions
//------------------------------------------------------------------------


/*
	main function that is called by C++

	dayN: 연속 일 수
	level: 해당 캐릭터 현재 레벨	
	maxLevel: 현재 최고 레벨(시스템 상으로 허용하는 최고 레벨)
*/
function SetUpDayN(nDay, level, maxLevel)		// N: 1~7		//<===========================================================
{
	print("function SetUpDayN starts with parameters");
	print("  nDay: " + nDay);
	print("  level: " + level);	
	print("  maxLevel: " + maxLevel);
	print("{\n");
	
	
	::srand( ::time() );
	
	local objDayN = DayN();	
	
	objDayN.SetN(nDay);
	
	//
	//1. 과제의 수를 결정
	//	
	local nTasks = GetRandomNumOfTasks();
	
	print("  -The number of Tasks: " + nTasks + "\n" );
	
	
	objDayN.SetNumOfTasks(nTasks);
	
	
	local nType = 0;
	local nValueBase = 0;
	
	
	//
	//2. 보상 처리 과정에서 사용되는 변수들 미리 선언
	//
	local rewardSortNum;
	local rewardType;
	local rewardExpRate;
	local rewardPoint	;
	local rewardMoney;
	local rewardItemNum;


	
	
	for (local i = 0; i< nTasks; i+=1)		// iterate all Tasks
	{
		local sharedobj = SharedAmongTasks(nDay);	//과제끼리 공유되어야 하는 정보들을 관리하는 객체 (ex> 보상 아이템의 갯수는 모든 과제들 사이에서 공유되면서, 최대값이 반복해서 나오는 것을 방지해야 한다.)
		
		//
		//3. 과제 Type 결정
		//
		if ( i != 0 )
		{
			nType = GetRandomType();
		}
		else
		{
			nType = Types.Connection;
		}
		
		//
		//4. 과제의 Type에 기반한 수행(랜덤)값 결정
		//
		nValueBase = GetRandomValue(nType);
		
		objDayN.SetTaskBasic(i, nType, nValueBase);
		
		print("");
		print("    -Task( "  + i + " ) Type : " + nType + " (ValueBase: " + nValueBase + ")");		

		//
		//5. 보상(Reward) 객체 생성 - Task당 1개의 Reward객체를 생성 및 사용한다. (Reward객체 내부의 random table들(properties)에 변형을 가하므로 반드시 Task당 1개의 Reward객체를 사용하자.)
		//		
		local reward = Reward(nDay, level, maxLevel, nTasks);
	
		rewardSortNum= reward.GetRandomRewardSortNum();
		rewardType		= 0;
		rewardExpRate= 0.0;
		rewardPoint		= 0;
		rewardMoney	= 0;
		rewardItemNum= 0;
		
		print("      -Tasks( "  + i + " ) has " + rewardSortNum + " rewards." );
		
		for (local j=0; j<rewardSortNum; j+=1)	//iterate all sorts of rewards
		{
			rewardType = reward.GetRandomFilteredRewardType( i );//GetRandomRewardType( i );
			
			switch (rewardType)
			{
				case RewardTypes.Exp:
					rewardExpRate = reward.GetRewardExpRate();
					print("        -Reward( "  + j + " ) Type :  Exp ( " + rewardExpRate.tofloat() + " )." );
					break;
					
				case RewardTypes.Point:					
					rewardPoint = reward.GetRewardPoint();
					print("        -Reward( "  + j + " ) Type :  Point ( " + rewardPoint + " )." );
					break;
					
				case RewardTypes.Money:					
					rewardMoney = reward.GetRewardMoney();
					print("        -Reward( "  + j + " ) Type :  Money ( " + rewardMoney + " )." );
					break;
					
				case RewardTypes.Item:					
					rewardItemNum = sharedobj.GetRandomRewardItemNum();
					print("        -Reward( "  + j + " ) Type :  Item ( " + rewardItemNum + " )." );					

					if (rewardItemNum > NumberOfItemsPerOneTask)
					{
						print("      !!! rewardItemNum exceeds the NumberOfItemsPerOneTask(" + NumberOfItemsPerOneTask + ") !!!");
					}
					
					for (local k=0; k<rewardItemNum; k+=1)
					{
						local rewardItemID = reward.GetRewardItem();
						objDayN.SetRewardItem(i, k, rewardItemID.mid, rewardItemID.sid);
					
						print("          -Item( "  + k + " )'s ID: <mid: "  + rewardItemID.mid + ", sid: " + rewardItemID.sid + " >." );
					}
					break;
					
				default:
					break;
			}
		}	// for(;;)
		
		objDayN.SetRewardNumeric(i, rewardExpRate.tofloat(), rewardPoint, rewardMoney);		
	}	// for(;;)
	
	
	print("\n}");
	print("function SetUpDayN ends.\n\n");

	return objDayN;
}



//
// SharedAmongTasks Class	-  과제들 사이에서 공유되어야할 정보들을 관리하는 클래스 - 즉, 이 객체는 Day당 1개가 생성된다.
//
class SharedAmongTasks
{
	constructor(dayN)
	{
		m_dayN = dayN;
		//
		// 과제 당 지급할 보상 아이템의 개수 pickup용 랜덤 테이블 (최대 3개)
		//	
		m_arrRandomRewardItemNum		=			//array: 과제당 보상 아이템의 개수 랜덤 결정		->  이 array는 모든 Task들 사이에서 공유(Remove가 반영)되어야 하므로 생성자가 아닌 이곳에서 초기화 한다.

		[
				[1,1,1,1,1,1,1,1,1],	// 1일자
				[1,1,1,1,1,1,1],			// 2일자
				[1,1,1,1,1,1],			// 3일자
				[1,1,1,1,1,1],			// 4일자
				[1,1,1,1,1,1],			// 5일자
				[1,1,1,1,1,1],			// 6일자
				[1,1,1,1,1,1]				// 7일자
			]	
	}
	
	//
	// 과제에 대한 보상 아이템의 개수 결정
	//
	function GetRandomRewardItemNum()
	{
		local idx = GetRandom(0, m_arrRandomRewardItemNum[m_dayN].len() - 1);
		
		local nRewardItems = m_arrRandomRewardItemNum[m_dayN][idx];
		//m_nRewardItems = m_arrRandomRewardItemNum[m_dayN][idx];
		
		m_arrRandomRewardItemNum[m_dayN].remove(idx);
		
		return nRewardItems;
		//return m_nRewardItems;
	}
	
	m_dayN										= null;
	m_arrRandomRewardItemNum		= null;
}


//
// Reward Class
//
class Reward
{
	constructor(dayN, level, maxLevel, nTasks)
	{
		m_dayN				= dayN;
		m_level				= level;		
		m_maxLevel			= maxLevel;
		
		m_nTasks			= nTasks;	
		
		m_arrRandomRewardSortNum 	=	[1,1,1,1,2,2,2,2,3,4]  //array: 과제당 보상 종류 수 랜덤 결정
		
		m_arrRandomRewardType  		=
		[
			[RewardTypes.Exp, RewardTypes.Point, RewardTypes.Money, RewardTypes.Item, RewardTypes.Item],			// 과제1	== [0, 1, 2, 3]
			[RewardTypes.Exp, RewardTypes.Point, RewardTypes.Money, RewardTypes.Item, RewardTypes.Item],			// 과제2
			[RewardTypes.Exp, RewardTypes.Point, RewardTypes.Money, RewardTypes.Item, RewardTypes.Item],			// 과제3
			[RewardTypes.Exp, RewardTypes.Point, RewardTypes.Money, RewardTypes.Item, RewardTypes.Item]				// 과제4
		]
		
		m_arrRandomRewardTypeFilter	= 	[]
	}
	

	//
	// 경험치 결정 함수 
	//
	function GetRewardExpRate()
	{
		local rewardExpRate = (m_maxLevel - m_level) / (m_maxLevel * (1.0 + floor(m_level/10.0) )) * ( (m_dayN + 1.0) /10.0 );		//m_dayN is zero-base
		
		return rewardExpRate;
	}


	//
	// 생활점수 보상 결정
	//
	function GetRewardPoint() //dayN)
	{
		local rewardPoint = GetRandom( RewardPoint[m_dayN].min, RewardPoint[m_dayN].max );

		return rewardPoint;
	}


	//
	// 게임머니 보상 결정
	//
	function GetRewardMoney()
	{
		local moneyPerLevelSquare = GetRandom(50.0, 150.0) * 0.1;

		local x = ( m_level * m_level * moneyPerLevelSquare * (m_dayN + 1.0) / 10.0) + m_level;
		
		local rewardMoney = floor( x ).tointeger();	//m_dayN is zero-base
		
		return rewardMoney;
	}
	
	
	//
	// 보상 Item 생성
	//
	function GetRewardItem() //dayN)	
	{
		local idxItem = 0;
		
		idxItem = GetRandom(0, RewardItems[m_dayN].len() -1);
		
		return RewardItems[m_dayN][idxItem];
	}
	
	
	//
	// 과제에 대한 보상 종류의 수를  결정
	//
	function GetRandomRewardSortNum()
	{
		local idx = GetRandom(0, m_arrRandomRewardSortNum.len() - 1);
		
		local nSortOfRewards = m_arrRandomRewardSortNum[idx];

		
		m_arrRandomRewardSortNum.remove(idx);
		


		return nSortOfRewards;		
	}
	
	
	//
	// 보상의 종류를 결정 (같은 종류의 type이 나올 수 있음)
	//	
	function GetRandomRewardType(idxTask)
	{
		local idx = GetRandom(0, m_arrRandomRewardType[idxTask].len() -1 );
		
		if (m_arrRandomRewardType[idxTask].len() > 4)
		{
			idx = 4;
		}
		else if (m_arrRandomRewardType[idxTask].len() == 4)
		{
			idx = GetRandom(0, m_arrRandomRewardType[idxTask].len() - 2 );
		}
		
		local type = m_arrRandomRewardType[idxTask][idx];
		
		m_arrRandomRewardType[idxTask].remove(idx);
		
		return type;
	}
	
		
	//
	// 보상의 종류를 결정 (같은 종류의 type이 나오지 않음) 	<------------------- NEW
	//
	function GetRandomFilteredRewardType(idxTask)
	{
		local type = GetRandomRewardType(idxTask);
		
		local len = m_arrRandomRewardType.len();
		
		for(local i=0; i<len; i+=1)
		{
			if (IsFilteredType(type))		// 이미 발생한 보상 type에 대해서 filtering
			{
				print("[DEBUG] Filtering RewardType(" + type + ")" );
				type = GetRandomRewardType(idxTask);
			}
			else
				return type;
		}
		
		print("[ERROR] GetRandomFilteredRewardType(" + idxTask + ")" );	//여기로 흘러들어오면 문제가 있다.
			
		return 0;	//경험치
	}
	
	//
	// 보상type 제외 filter에 특정 type이 등록되어 있는지 조사 (1: 존재함, 0:존재안함)	
	//
	function IsFilteredType(type)
	{
		local lenFilter = m_arrRandomRewardTypeFilter.len();
	
		if (lenFilter != 0)	//filter에 제외해야할 type이 존재한다.
		{
			for (local i=0; i<lenFilter; i+=1)
			{
				if (m_arrRandomRewardTypeFilter[i] == type)
					return 1;
			}
		}
		else
		{
			m_arrRandomRewardTypeFilter.push(type);
		}
		
		return 0;
	}
	
	//
	// Properties
	//	
	/*
	Note:
	
		FOR C# and Java programmers:
		Squirrel doesn't clone member's default values nor executes the member declaration for each
		instace(as C# or java). So consider this example:
		
		class Foo {
			myarray = [1,2,3]
			mytable = {}
		}
		
		local a = Foo();
		local b = Foo();
		
		In the snippet above both instances will refer to the same array and same table.To archieve
		what a C# or Java programmer would exepect, the following approach should be taken.
		
		class Foo {
			myarray = null
			mytable = null
			constructor()
			{
				myarray = [1,2,3]
				mytable = {}
			}
		}
		
		local a = Foo();
		local b = Foo();		
	*/
	
	m_dayN										= null;
	m_level										= null;	
	m_maxLevel									= null;
	
	m_nTasks									= null;
	
	
	//
	// m_arrRandomRewardNum 과 m_arrRandomRewardItemNum으로 부터 pickup된 값을 가진다.
	//
	//m_nSortOfRewards						=	null;	//보상의 종류 개수
	//m_nRewardItems							=	null;	//보상 아이템 개수
	
	
	//
	// 과제 당 지급할 보상의 종류수 pickup용 랜덤 테이블  (최대 4가지: exp, point, item, money)
	// 아래의 table에서 임의의 값을 pickup한 후, pickup한 값은 삭제하므로써 다음에 동일한 값이 선택되는 것을 방지한다.
	//
	m_arrRandomRewardSortNum		=	null;  //array: 과제당 보상 종류 수 랜덤 결정
	
	//
	// member variable array이므로 <-가 아니라 = 로 초기화
	//
	
	
	//
	//보상 종류 1 경험치, 2 생활점수, 3 게임머니, 4 아이템
	//
	m_arrRandomRewardType  		=	null
	
	m_arrRandomRewardTypeFilter	= 	null
}


//
// 랜덤 과제의 개수
//
function GetRandomNumOfTasks()
{
	return GetRandom(NumberOfTasksPerOneDay.Min, NumberOfTasksPerOneDay.Max);
}


//
// 랜덤 과제 Type
//
function GetRandomType()
{	
	//return GetRandom(0, Types.len() - 1);
	local tempType = GetRandom(0, Types.len() - 1);

	
	if ( tempType == 9 )
	{
		tempType = GetRandom(0, Types.len() - 3);
	}
	
	return tempType
}



//
// 과제 Type별 ValueBase 랜덤값
//
function GetRandomValue(nType)
{
	return GetRandom( ValueRanges[nType].min, ValueRanges[nType].max );
}



//
// 난수 발생 (min ~ max 범위)
//
function GetRandom(min, max)
{
	return ( ::rand() % (max - min + 1) + min );
}

/*
	constructor(dayN)
	{
		m_dayN = dayN;
		//
		// 과제 당 지급할 보상 아이템의 개수 pickup용 랜덤 테이블 (최대 3개)
		//	
		m_arrRandomRewardItemNum		=			//array: 과제당 보상 아이템의 개수 랜덤 결정		->  이 array는 모든 Task들 사이에서 공유(Remove가 반영)되어야 하므로 생성자가 아닌 이곳에서 초기화 한다.

		[
				[1,1,1,1,1,1,1,1,2],	// 1일자
				[1,1,1,1,1,1,2],			// 2일자
				[1,1,1,1,1,2],			// 3일자
				[1,1,1,1,2,2],			// 4일자
				[1,1,1,2,2,3],			// 5일자
				[1,1,2,2,2,3],			// 6일자
				[1,2,2,2,3,3]				// 7일자
			]	
	}
*/