/**
	PVE 무한의 계단 
	서버 스크립트
	작성자 이익세 (수정하지말것)
 */
 
/*kill 20 mobs*/
script1 <- ::import("data/glogicserver/scripts/infinitystairsC0_0.nut", {});
script1.difficulty.SetMultipleHP(2.5);
script1.difficulty.SetMultipleAttack(1.2);
script1.difficulty.SetMultipleDefense(1);
script1.difficulty.SetMultipleExp(0.7);
script1.difficulty.SetMultipleDrop(1);
script1.layer.SetLayer(0); /*es_pyramid1_a.lev, 0*/
script1.info.SetMessage("INFINITYSTAIRS_MESSAGE_63"); /* set inflitration* */
script1.info.SetLifeTime(4);
script1.stage.SetLifeTime(120); /* 1:20 min. */
script1.start.InsertChild(script1.stage);
script1.start.InsertChild(script1.clear);
script1.clear.SetCount(20); /*kill 20 mobs*/
script1.postinfo.SetMessage("INFINITYSTAIRS_MESSAGE_60"); /*Please proceed to the next area through the gate.*/
script1.postinfo.SetLifeTime(4);
script1.open.SetFromGateID(1);
script1.open.SetToMapID(285, 0);
script1.open.SetToGateID(2);
script1.reward.SetMVPIncrease(2);
script1.reward.SetEXPIncrease(5);
stage1 <- script1.root;

/*kill 11 mobs*/
script2 <- ::import("data/glogicserver/scripts/infinitystairsC0_0.nut", {});
script2.difficulty.SetMultipleHP(2.5);
script2.difficulty.SetMultipleAttack(1.2);
script2.difficulty.SetMultipleDefense(1);
script2.difficulty.SetMultipleExp(0.7);
script2.difficulty.SetMultipleDrop(1);
script2.layer.SetLayer(1); /*es_pyramid1_a.lev, 1*/
script2.info.SetMessage("INFINITYSTAIRS_MESSAGE_64"); /*Please recover your health by purchasing the MP+SP potion lunchbox from NPC*/
script2.info.SetLifeTime(4);
script2.stage.SetLifeTime(120); /* 120 sec. */
script2.start.InsertChild(script2.stage);
script2.start.InsertChild(script2.clear);
script2.clear.SetCount(11); /*kill 11 mobs*/
script2.postinfo.SetMessage("INFINITYSTAIRS_MESSAGE_60"); /*Please proceed to the next area through the gate.*/
script2.postinfo.SetLifeTime(4);
script2.open.SetFromGateID(1);
script2.open.SetToMapID(286, 0);
script2.open.SetToGateID(2);
script2.reward.SetMVPIncrease(2);
script2.reward.SetEXPIncrease(5);
stage2 <- script2.root;

/*don't touch robot and kill 30 mobs*/
script3 <- ::import("data/glogicserver/scripts/infinitystairsC0_0.nut", {});
script3.difficulty.SetMultipleHP(2.5);
script3.difficulty.SetMultipleAttack(2);
script3.difficulty.SetMultipleDefense(1);
script3.difficulty.SetMultipleExp(0.7);
script3.difficulty.SetMultipleDrop(1);
script3.layer.SetLayer(0); /*es_pyramid1_b.lev, 0*/
script3.info.SetMessage("INFINITYSTAIRS_MESSAGE_65"); /*Don't wake up the sleeping special robot. If you attack it, your life will be in danger.*/
script3.info.SetLifeTime(4);
script3.stage.SetLifeTime(120); /* 120 sec. */
script3.start.InsertChild(script3.stage);
script3.start.InsertChild(script3.clear);
script3.clear.SetCount(30); /*kill 30 mobs*/
script3.postinfo.SetMessage("INFINITYSTAIRS_MESSAGE_60"); /*Please proceed to the next area through the gate.*/
script3.postinfo.SetLifeTime(4);
script3.open.SetFromGateID(1);
script3.open.SetToMapID(286, 0);
script3.open.SetToGateID(2);
script3.reward.SetMVPIncrease(2);
script3.reward.SetEXPIncrease(5);
stage3 <- script3.root;

/*kill 23 mobs*/
script4 <- ::import("data/glogicserver/scripts/infinitystairsC0_0.nut", {});
script4.difficulty.SetMultipleHP(2.5);
script4.difficulty.SetMultipleAttack(2);
script4.difficulty.SetMultipleDefense(1);
script4.difficulty.SetMultipleExp(1);
script4.difficulty.SetMultipleDrop(1);
script4.layer.SetLayer(1); /*es_pyramid1_b.lev, 1*/
script4.info.SetMessage("INFINITYSTAIRS_MESSAGE_43"); /*Please get yourself going. Starting from here, it's going to get harder.*/
script4.info.SetLifeTime(4);
script4.stage.SetLifeTime(115); /* 1:15 min. */
script4.start.InsertChild(script4.stage);
script4.start.InsertChild(script4.clear);
script4.clear.SetCount(23); /*kill 23 mobs*/
script4.postinfo.SetMessage("INFINITYSTAIRS_MESSAGE_60"); /*Please proceed to the next area through the gate.*/
script4.postinfo.SetLifeTime(4);
script4.open.SetFromGateID(1);
script4.open.SetToMapID(287, 0);
script4.open.SetToGateID(2);
script4.reward.SetMVPIncrease(2);
script4.reward.SetEXPIncrease(5);
stage4 <- script4.root;

/* evade trap and move to pos */
script5 <- ::import("data/glogicserver/scripts/infinitystairsC0_0.nut", {});
script5.difficulty.SetMultipleHP(2.5);
script5.difficulty.SetMultipleAttack(2);
script5.difficulty.SetMultipleDefense(1);
script5.difficulty.SetMultipleExp(1);
script5.difficulty.SetMultipleDrop(1);
script5.layer.SetLayer(2); /*es_pyramid1_c.lev, 2*/
script5.info.SetMessage("INFINITYSTAIRS_MESSAGE_44"); /*Try to evade the traps and reach the destination. This is going to be a rough day.*/
script5.info.SetLifeTime(4);
script5.stage.SetLifeTime(80);
script5.start.InsertChild(script5.stage);
script5.start.InsertChild(script5.pos);
script5.pos.SetMin(12, 28); /*x,y coordinate*/
script5.pos.SetMax(12, 28); /*x,y coordinate*/
script5.postinfo.SetMessage("INFINITYSTAIRS_MESSAGE_60"); /*Please proceed to the next area through the gate.*/
script5.postinfo.SetLifeTime(4);
script5.open.SetFromGateID(1);
script5.open.SetToMapID(287, 0);
script5.open.SetToGateID(2);
script5.reward.SetMVPIncrease(2);
script5.reward.SetEXPIncrease(5);
stage5 <- script5.root;

/*kill 30 mobs, finish task before trap arrive*/
script6 <- ::import("data/glogicserver/scripts/infinitystairsC0_0.nut", {});
script6.difficulty.SetMultipleHP(2.5);
script6.difficulty.SetMultipleAttack(2);
script6.difficulty.SetMultipleDefense(1);
script6.difficulty.SetMultipleExp(0.8);
script6.difficulty.SetMultipleDrop(1);
script6.layer.SetLayer(1); /*es_pyramid1_c.lev, 1*/
script6.info.SetMessage("INFINITYSTAIRS_MESSAGE_46"); /*Please finish your task before the trap arrives.*/
script6.info.SetLifeTime(4);
script6.stage.SetLifeTime(120);
script6.start.InsertChild(script6.stage);
script6.start.InsertChild(script6.clear);
script6.clear.SetCount(30); /*kill 30 mobs*/
script6.postinfo.SetMessage("INFINITYSTAIRS_MESSAGE_60"); /*Please proceed to the next area through the gate.*/
script6.postinfo.SetLifeTime(4);
script6.open.SetFromGateID(1);
script6.open.SetToMapID(285, 0);
script6.open.SetToGateID(2);
script6.reward.SetMVPIncrease(2);
script6.reward.SetEXPIncrease(5);
stage6 <- script6.root;

/*kill 1 boss mobs*/
script7 <- ::import("data/glogicserver/scripts/infinitystairsC0_0.nut", {});
script7.difficulty.SetMultipleHP(3);
script7.difficulty.SetMultipleAttack(2);
script7.difficulty.SetMultipleDefense(1);
script7.difficulty.SetMultipleExp(0.8);
script7.difficulty.SetMultipleDrop(1);
script7.layer.SetLayer(2); /*es_pyramid1_a.lev, 2*/
script7.info.SetMessage("INFINITYSTAIRS_MESSAGE_47"); /*Please watch your back.*/
script7.info.SetLifeTime(4);
script7.stage.SetLifeTime(90);
script7.start.InsertChild(script7.stage);
script7.start.InsertChild(script7.clear);
script7.clear.SetCount(1);  /*kill 1 boss mobs*/
script7.postinfo.SetMessage("INFINITYSTAIRS_MESSAGE_60"); /*Please proceed to the next area through the gate.*/
script7.postinfo.SetLifeTime(4);
script7.open.SetFromGateID(1);
script7.open.SetToMapID(286, 0);
script7.open.SetToGateID(2);
script7.reward.SetMVPIncrease(2);
script7.reward.SetEXPIncrease(5);
stage7 <- script7.root;

/*kill 35 mobs*/
script8 <- ::import("data/glogicserver/scripts/infinitystairsC0_0.nut", {});
script8.difficulty.SetMultipleHP(2);
script8.difficulty.SetMultipleAttack(2);
script8.difficulty.SetMultipleDefense(1);
script8.difficulty.SetMultipleExp(1);
script8.difficulty.SetMultipleDrop(1);
script8.layer.SetLayer(2); /*es_pyramid1_b.lev, 2*/
script8.info.SetMessage("INFINITYSTAIRS_MESSAGE_48"); /*Evade the traps and defeat the monsters.*/
script8.info.SetLifeTime(4);
script8.stage.SetLifeTime(140);
script8.start.InsertChild(script8.stage);
script8.start.InsertChild(script8.clear);
script8.clear.SetCount(35);  /*kill 35 mobs*/
script8.postinfo.SetMessage("INFINITYSTAIRS_MESSAGE_60"); /*Please proceed to the next area through the gate.*/
script8.postinfo.SetLifeTime(4);
script8.open.SetFromGateID(1);
script8.open.SetToMapID(287, 0);
script8.open.SetToGateID(2);
script8.reward.SetMVPIncrease(2);
script8.reward.SetEXPIncrease(5);
stage8 <- script8.root;

/* evade trap and move to pos */
script9 <- ::import("data/glogicserver/scripts/infinitystairsC0_0.nut", {});
script9.difficulty.SetMultipleHP(4);
script9.difficulty.SetMultipleAttack(2);
script9.difficulty.SetMultipleDefense(1);
script9.difficulty.SetMultipleExp(1);
script9.difficulty.SetMultipleDrop(1);
script9.layer.SetLayer(0); /*es_pyramid1_c.lev, 0*/
script9.info.SetMessage("INFINITYSTAIRS_MESSAGE_45"); /*This time your mouse controlling prowess will be tested. With your level of skill, you might not reach the next stage.*/
script9.info.SetLifeTime(4);
script9.stage.SetLifeTime(60); /*60 sec*/
script9.start.InsertChild(script9.stage);
script9.start.InsertChild(script9.pos);
script9.pos.SetMin(12, 28); /*x,y coordinate*/
script9.pos.SetMax(12, 28); /*x,y coordinate*/
script9.postinfo.SetMessage("INFINITYSTAIRS_MESSAGE_60"); /*Please proceed to the next area through the gate.*/
script9.postinfo.SetLifeTime(4);
script9.open.SetFromGateID(1);
script9.open.SetToMapID(291, 0);
script9.open.SetToGateID(2);
script9.reward.SetMVPIncrease(2);
script9.reward.SetEXPIncrease(5);
stage9 <- script9.root;

/* kill 1/3 boss */
script10 <- ::import("data/glogicserver/scripts/infinitystairsC0_0.nut", {});
script10.difficulty.SetMultipleHP(2);
script10.difficulty.SetMultipleAttack(1.2);
script10.difficulty.SetMultipleDefense(1);
script10.difficulty.SetMultipleExp(1);
script10.difficulty.SetMultipleDrop(1);
script10.layer.SetLayer(0); /*es_pyramid1_g.lev, 0*/
script10.info.SetMessage("INFINITYSTAIRS_MESSAGE_49"); /*Please try to defeat at least 1 of the 3 bosses. Each boss has different rewards.*/
script10.info.SetLifeTime(4);
script10.stage.SetLifeTime(200);
script10.start.InsertChild(script10.stage);
script10.start.InsertChild(script10.clear);
script10.clear.SetCount(1); /* kill 1/3 boss */
script10.postinfo.SetMessage("INFINITYSTAIRS_MESSAGE_60"); /*Please proceed to the next area through the gate.*/
script10.postinfo.SetLifeTime(4);
script10.open.SetFromGateID(1);
script10.open.SetToMapID(288, 0);
script10.open.SetToGateID(2);
script10.reward.SetMVPIncrease(2);
script10.reward.SetEXPIncrease(5);
stage10 <- script10.root;

/*kill 23 mobs*/
script11 <- ::import("data/glogicserver/scripts/infinitystairsC0_0.nut", {});
script11.difficulty.SetMultipleHP(3.5);
script11.difficulty.SetMultipleAttack(2);
script11.difficulty.SetMultipleDefense(1);
script11.difficulty.SetMultipleExp(0.8);
script11.difficulty.SetMultipleDrop(1);
script11.layer.SetLayer(0); /*es_pyramid1_d.lev, 0*/
script11.info.SetMessage("INFINITYSTAIRS_MESSAGE_50"); /*A way to quickly defeat the monsters is needed.*/
script11.info.SetLifeTime(4);
script11.stage.SetLifeTime(120);
script11.start.InsertChild(script11.stage);
script11.start.InsertChild(script11.clear);
script11.clear.SetCount(23); /*kill 23 mobs*/
script11.postinfo.SetMessage("INFINITYSTAIRS_MESSAGE_60"); /*Please proceed to the next area through the gate.*/
script11.postinfo.SetLifeTime(4);
script11.open.SetFromGateID(1);
script11.open.SetToMapID(288, 0);
script11.open.SetToGateID(2);
script11.reward.SetMVPIncrease(2);
script11.reward.SetEXPIncrease(5);
stage11 <- script11.root;

/*kill 13 mobs*/
script12 <- ::import("data/glogicserver/scripts/infinitystairsC0_0.nut", {});
script12.difficulty.SetMultipleHP(3);
script12.difficulty.SetMultipleAttack(1.5);
script12.difficulty.SetMultipleDefense(1);
script12.difficulty.SetMultipleExp(0.4);
script12.difficulty.SetMultipleDrop(1);
script12.layer.SetLayer(1); /*es_pyramid1_d.lev, 1*/
script12.info.SetMessage("INFINITYSTAIRS_MESSAGE_43"); /*Please get yourself going. Starting from here, it's going to get harder.*/
script12.info.SetLifeTime(4);
script12.stage.SetLifeTime(120);
script12.start.InsertChild(script12.stage);
script12.start.InsertChild(script12.clear);
script12.clear.SetCount(13); /*kill 13 mobs*/
script12.postinfo.SetMessage("INFINITYSTAIRS_MESSAGE_60"); /*Please proceed to the next area through the gate.*/
script12.postinfo.SetLifeTime(4);
script12.open.SetFromGateID(1);
script12.open.SetToMapID(289, 0);
script12.open.SetToGateID(2);
script12.reward.SetMVPIncrease(2);
script12.reward.SetEXPIncrease(5);
stage12 <- script12.root;

/* evade trap and move to pos */
script13 <- ::import("data/glogicserver/scripts/infinitystairsC0_0.nut", {});
script13.difficulty.SetMultipleHP(2);
script13.difficulty.SetMultipleAttack(2);
script13.difficulty.SetMultipleDefense(1);
script13.difficulty.SetMultipleExp(0.8);
script13.difficulty.SetMultipleDrop(1);
script13.layer.SetLayer(0); /*es_pyramid1_e.lev, 0*/
script13.info.SetMessage("INFINITYSTAIRS_MESSAGE_44"); /*Try to evade the traps and reach the destination. This is going to be a rough day.*/
script13.info.SetLifeTime(4);
script13.stage.SetLifeTime(100);
script13.start.InsertChild(script13.stage);
script13.start.InsertChild(script13.pos);
script13.pos.SetMin(24, 20); /*x,y coordinate*/
script13.pos.SetMax(24, 20); /*x,y coordinate*/
script13.postinfo.SetMessage("INFINITYSTAIRS_MESSAGE_60"); /*Please proceed to the next area through the gate.*/
script13.postinfo.SetLifeTime(4);
script13.open.SetFromGateID(1);
script13.open.SetToMapID(289, 0);
script13.open.SetToGateID(2);
script13.reward.SetMVPIncrease(2);
script13.reward.SetEXPIncrease(5);
stage13 <- script13.root;

/* kill 3 mobs */
script14 <- ::import("data/glogicserver/scripts/infinitystairsC0_0.nut", {});
script14.difficulty.SetMultipleHP(2);
script14.difficulty.SetMultipleAttack(2);
script14.difficulty.SetMultipleDefense(1);
script14.difficulty.SetMultipleExp(1);
script14.difficulty.SetMultipleDrop(1);
script14.layer.SetLayer(1); /*es_pyramid1_e.lev, 1*/
script14.info.SetMessage("INFINITYSTAIRS_MESSAGE_51"); /*Please be careful of the repetitively moving traps.*/
script14.info.SetLifeTime(4);
script14.stage.SetLifeTime(140);
script14.start.InsertChild(script14.stage);
script14.start.InsertChild(script14.clear);
script14.clear.SetCount(3); /*kill 3 mobs*/
script14.postinfo.SetMessage("INFINITYSTAIRS_MESSAGE_60"); /*Please proceed to the next area through the gate.*/ 
script14.postinfo.SetLifeTime(4);
script14.open.SetFromGateID(1);
script14.open.SetToMapID(290, 0);
script14.open.SetToGateID(2);
script14.reward.SetMVPIncrease(2);
script14.reward.SetEXPIncrease(5);
stage14 <- script14.root;

/* kill 12 mobs */
script15 <- ::import("data/glogicserver/scripts/infinitystairsC0_0.nut", {});
script15.difficulty.SetMultipleHP(3);
script15.difficulty.SetMultipleAttack(2);
script15.difficulty.SetMultipleDefense(1);
script15.difficulty.SetMultipleExp(0.9);
script15.difficulty.SetMultipleDrop(1);
script15.layer.SetLayer(0); /*es_pyramid1_f.lev, 0*/
script15.info.SetMessage("INFINITYSTAIRS_MESSAGE_52"); /*Almost reach the top floor. Keep it up.*/
script15.info.SetLifeTime(4);
script15.stage.SetLifeTime(140);
script15.start.InsertChild(script15.stage);
script15.start.InsertChild(script15.clear);
script15.clear.SetCount(12); /* kill 12 mobs */
script15.postinfo.SetMessage("INFINITYSTAIRS_MESSAGE_60"); /*Please proceed to the next area through the gate.*/ 
script15.postinfo.SetLifeTime(4);
script15.open.SetFromGateID(1);
script15.open.SetToMapID(290, 0);
script15.open.SetToGateID(2);
script15.reward.SetMVPIncrease(2);
script15.reward.SetEXPIncrease(5);
stage15 <- script15.root;

/* evade trap and move to pos */
script16 <- ::import("data/glogicserver/scripts/infinitystairsC0_0.nut", {});
script16.difficulty.SetMultipleHP(3.5);
script16.difficulty.SetMultipleAttack(2);
script16.difficulty.SetMultipleDefense(1);
script16.difficulty.SetMultipleExp(0.9);
script16.difficulty.SetMultipleDrop(1);
script16.layer.SetLayer(1); /*es_pyramid1_f.lev, 1*/
script16.info.SetMessage("INFINITYSTAIRS_MESSAGE_53"); /*Please move according to the special trap. The more people make mistake, the harder it will be. Most people just give up here.*/
script16.info.SetLifeTime(4);
script16.stage.SetLifeTime(100);
script16.start.InsertChild(script16.stage);
script16.start.InsertChild(script16.pos);
script16.pos.SetMin(12, 28); /*x,y coordinate*/
script16.pos.SetMax(12, 28); /*x,y coordinate*/
script16.postinfo.SetMessage("INFINITYSTAIRS_MESSAGE_60"); /*Please proceed to the next area through the gate.*/ 
script16.postinfo.SetLifeTime(4);
script16.open.SetFromGateID(1);
script16.open.SetToMapID(288, 0);
script16.open.SetToGateID(2);
script16.reward.SetMVPIncrease(2);
script16.reward.SetEXPIncrease(5);
stage16 <- script16.root;

/* evade trap and move to pos */
script17 <- ::import("data/glogicserver/scripts/infinitystairsC0_0.nut", {});
script17.difficulty.SetMultipleHP(2.5);
script17.difficulty.SetMultipleAttack(2);
script17.difficulty.SetMultipleDefense(1);
script17.difficulty.SetMultipleExp(2);
script17.difficulty.SetMultipleDrop(1);
script17.layer.SetLayer(2); /*es_pyramid1_d.lev, 2*/
script17.info.SetMessage("INFINITYSTAIRS_MESSAGE_54"); /*Show everyone how hardworking you are. You can do it.*/
script17.info.SetLifeTime(4);
script17.stage.SetLifeTime(40);
script17.start.InsertChild(script17.stage);
script17.start.InsertChild(script17.pos);
script17.pos.SetMin(15, 16); /*x,y coordinate*/
script17.pos.SetMax(15, 16); /*x,y coordinate*/
script17.postinfo.SetMessage("INFINITYSTAIRS_MESSAGE_60"); /*Please proceed to the next area through the gate.*/ 
script17.postinfo.SetLifeTime(4);
script17.open.SetFromGateID(1);
script17.open.SetToMapID(289, 0);
script17.open.SetToGateID(2);
script17.reward.SetMVPIncrease(2);
script17.reward.SetEXPIncrease(5);
stage17 <- script17.root;

/*kill 1 boss mob*/
script18 <- ::import("data/glogicserver/scripts/infinitystairsC0_0.nut", {});
script18.difficulty.SetMultipleHP(2);
script18.difficulty.SetMultipleAttack(2);
script18.difficulty.SetMultipleDefense(1);
script18.difficulty.SetMultipleExp(1);
script18.difficulty.SetMultipleDrop(1);
script18.layer.SetLayer(2); /*es_pyramid1_e.lev, 2*/
script18.info.SetMessage("INFINITYSTAIRS_MESSAGE_55"); /*Please evade the traps and shoot the enemies. And finish them before the enemy's traps come close.*/
script18.info.SetLifeTime(4);
script18.stage.SetLifeTime(130);
script18.start.InsertChild(script18.stage);
script18.start.InsertChild(script18.clear);
script18.clear.SetCount(1); /*kill 1 boss mobs*/
script18.postinfo.SetMessage("INFINITYSTAIRS_MESSAGE_60"); /*Please proceed to the next area through the gate.*/ 
script18.postinfo.SetLifeTime(4);
script18.open.SetFromGateID(1);
script18.open.SetToMapID(290, 0);
script18.open.SetToGateID(2);
script18.reward.SetMVPIncrease(2);
script18.reward.SetEXPIncrease(5);
stage18 <- script18.root;

/* evade trap and move to pos */
script19 <- ::import("data/glogicserver/scripts/infinitystairsC0_0.nut", {});
script19.difficulty.SetMultipleHP(2.5);
script19.difficulty.SetMultipleAttack(2);
script19.difficulty.SetMultipleDefense(1);
script19.difficulty.SetMultipleExp(2);
script19.difficulty.SetMultipleDrop(1);
script19.layer.SetLayer(2); /*es_pyramid1_f.lev, 2*/
script19.info.SetMessage("INFINITYSTAIRS_MESSAGE_45"); /*This time your mouse controlling prowess will be tested. With your level of skill, you might not reach the next stage.*/
script19.info.SetLifeTime(4);
script19.stage.SetLifeTime(180);
script19.start.InsertChild(script19.stage);
script19.start.InsertChild(script19.pos);
script19.pos.SetMin(12, 28); /*x,y coordinate*/
script19.pos.SetMax(12, 28); /*x,y coordinate*/
script19.postinfo.SetMessage("INFINITYSTAIRS_MESSAGE_60"); /*Please proceed to the next area through the gate.*/ 
script19.postinfo.SetLifeTime(4);
script19.open.SetFromGateID(1);
script19.open.SetToMapID(292, 0);
script19.open.SetToGateID(2);
script19.reward.SetMVPIncrease(2);
script19.reward.SetEXPIncrease(5);
stage19 <- script19.root;

/* kill 5 mobs */
script20 <- ::import("data/glogicserver/scripts/infinitystairsC0_0.nut", {});
script20.difficulty.SetMultipleHP(1);
script20.difficulty.SetMultipleAttack(1);
script20.difficulty.SetMultipleDefense(1);
script20.difficulty.SetMultipleExp(1.5);
script20.difficulty.SetMultipleDrop(1);
script20.layer.SetLayer(0); /*es_pyramid1_h.lev, 0*/
script20.info.SetMessage("INFINITYSTAIRS_MESSAGE_56"); /*Finally, it's the last stage. Gather your remaining spirit and senses and finish it.*/
script20.info.SetLifeTime(4);
script20.stage.SetLifeTime(600); /* 10 mins */
script20.start.InsertChild(script20.stage);
script20.start.InsertChild(script20.clear);
script20.clear.SetCount(5); /* kill 5 mobs */
script20.postinfo.SetMessage("INFINITYSTAIRS_MESSAGE_61"); /*ALL CLEAR! Your hard work deserves respect. Press 'ESC' button to close the result window and take the drop items.*/
script20.postinfo.SetLifeTime(60);
script20.reward.SetMVPIncrease(2);
script20.reward.SetEXPIncrease(5);
script20.stage.SetLastStage(); /*end stage*/
stage20 <- script20.root;
