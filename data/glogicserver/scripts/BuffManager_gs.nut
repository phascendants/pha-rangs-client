

BuffManagerType <- 
{
	//필드 점령전
	CTF = 
	{
		// 점령전 전용 버프 MID
		nBuffMID = 51
		
		// 점령전 참여용 보상 버프 갯수
		nBuffReward_Player = 1
		
		// 점령전 보상 버프 갯수
		nBuffReward_School = 2
		
		// 점령전 기본 보상 버프 갯수
		nBuffReward_Default = 1
	
		// 점령전 참여전용 보상버프
		arrRewardBuffTypes_Player = 
		[
			BUFF(51,3,0,0,true),
			BUFF(51,4,0,0,true),
			BUFF(51,9,0,0,true)
		]
		
		// 모든 유저의 랜덤 보상 버프
		arrRewardBuffTypes_School = 
		[
			BUFF(51,5,0,0,true),
			BUFF(51,6,0,0,true),
			BUFF(51,7,0,0,true),
			BUFF(51,8,0,0,true),
			BUFF(51,10,0,0,true),
			BUFF(51,11,0,0,true),
			BUFF(51,12,0,0,true)
		]
			
		// 모든 유저의 기본 보상 버프
		arrRewardBuffTypes_Default = 
		[
			BUFF(51,13,0,0,true)
		]
		
		// 점령전 밸런스 버프
		arrBalanceBuffTypes = 
		[
			BUFF(51,0,0,0,true),
			BUFF(51,0,1,0,true),
			BUFF(51,0,2,0,true),
			BUFF(51,0,3,0,true),
			BUFF(51,0,4,0,true),
			BUFF(51,0,5,0,true),
			BUFF(51,0,6,0,true),
			BUFF(51,0,7,0,true),
			BUFF(51,0,8,0,true),
			BUFF(51,1,0,0,true),
			BUFF(51,1,1,0,true),
			BUFF(51,1,2,0,true),
			BUFF(51,1,3,0,true),
			BUFF(51,1,4,0,true),
			BUFF(51,1,5,0,true)
		]

		//점령전 부활 버프 개수
		nBuffRebirth_Num = 1
		
		//부활 버프 종류 세팅
		arrRebirthBuffTypes =
		[
			BUFF(51, 2, 0, 0, true)
		]

		//부활 버프를 가져온다
		function GetBuffRebirth()
		{
			local nCountBuff = 0;
			local arrRebirthBuffList = [];
			
			nCountBuff = BuffManagerType.CTF.nBuffRebirth_Num;
			arrRebirthBuffList.extend(BuffManagerType.CTF.arrRebirthBuffTypes);		

			local arrResult = [];
			
			for(local i = 0; i < nCountBuff; ++i)
			{
				arrResult.append(arrRebirthBuffList[i]);
			}

			return arrResult;
		}			

		// 점령전 버프의 리스트를 가져온다.
		function GetBuffTypes()
		{
			local arrBuffTypes = [];
			arrBuffTypes.append( BuffManagerType.CTF.arrRewardBuffTypes_Player );
			arrBuffTypes.append( BuffManagerType.CTF.arrRewardBuffTypes_School );
			arrBuffTypes.append( BuffManagerType.CTF.arrRewardBuffTypes_Default );
			return arrBuffTypes;
		}
		
		// 점령전 보상 버프의 총 갯수를 가져온다
		function GetRewardBuffNums()
		{
			local nNums = 0;
			
			nNums += BuffManagerType.CTF.nBuffReward_Player;
			nNums += BuffManagerType.CTF.nBuffReward_School;
			nNums += BuffManagerType.CTF.nBuffReward_Default;
		}
		
		//필드점령전 보상버프를 가져온다.
		function GetBuffReward()
		{
			::srand( ::time() );
			
			local nRandomRewardBuff = 0;
			local nRandomCount     	= 0;
			
			nRandomRewardBuff += BuffManagerType.CTF.nBuffReward_Player;
			nRandomRewardBuff += BuffManagerType.CTF.nBuffReward_School;
			
			nRandomCount += BuffManagerType.CTF.arrRewardBuffTypes_Player.len();
			nRandomCount += BuffManagerType.CTF.arrRewardBuffTypes_School.len();
			
			local arrRewardBuffList = [];
			arrRewardBuffList.extend( BuffManagerType.CTF.arrRewardBuffTypes_Player );
			arrRewardBuffList.extend( BuffManagerType.CTF.arrRewardBuffTypes_School );
			
			//배열의 인덱스를 랜덤하게 섞는다.
			local arrBuffRewardIndex = [];
			
			for ( local i=0; i<nRandomCount; ++i )
			{
				arrBuffRewardIndex.append(i);
			}
			
			for ( local i=0; i<nRandomCount; ++i )
			{
				if ( rand()%2 )
				{
					local nDest = rand()%nRandomCount;
					local nSwap = arrBuffRewardIndex[i];
					arrBuffRewardIndex[i]     = arrBuffRewardIndex[nDest];
					arrBuffRewardIndex[nDest] = nSwap;
				}
			}
			
			//섞은 인덱스를 바탕으로 버프를 가져온다
			local arrResult = [];
	
			for ( local i=0; i<nRandomRewardBuff; ++i )
			{
				local nIndex = arrBuffRewardIndex[i];
				arrResult.append( arrRewardBuffList[nIndex] );
			}
			
			// 기본 보상 버프를 붙인다.
			arrResult.extend(BuffManagerType.CTF.arrRewardBuffTypes_Default);

			return arrResult;
		}
				
		// 밸런스 버프를 계산, 가져온다
		function GetBuffBalance( nStudentCount, nStandardCount )
		{
			local fPercent = 1.0 * nStudentCount / nStandardCount;
		
			//10% 이하
			if ( fPercent <= 0.1 )
			{
				return [
					BuffManagerType.CTF.arrBalanceBuffTypes[8], 
					BuffManagerType.CTF.arrBalanceBuffTypes[14] 
				];
			}
			//20% 이하
			else if ( fPercent <= 0.2 )
			{
				return [
					BuffManagerType.CTF.arrBalanceBuffTypes[7], 
					BuffManagerType.CTF.arrBalanceBuffTypes[13] 
				];
			}
			//30% 이하
			else if ( fPercent <= 0.3 )
			{
				return [
					BuffManagerType.CTF.arrBalanceBuffTypes[6], 
					BuffManagerType.CTF.arrBalanceBuffTypes[12] 
				];
			}
			//40% 이하
			else if ( fPercent <= 0.4 )
			{
				return [
					BuffManagerType.CTF.arrBalanceBuffTypes[5], 
					BuffManagerType.CTF.arrBalanceBuffTypes[11] 
				];
			}
			//50% 이하
			else if ( fPercent <= 0.5 )
			{
				return [
					BuffManagerType.CTF.arrBalanceBuffTypes[4], 
					BuffManagerType.CTF.arrBalanceBuffTypes[10] 
				];
			}
			//60% 이하
			else if ( fPercent <= 0.6 )
			{
				return [
					BuffManagerType.CTF.arrBalanceBuffTypes[3], 
					BuffManagerType.CTF.arrBalanceBuffTypes[9] 
				];
			}
			//70% 이하
			else if ( fPercent <= 0.7 )
			{
				return [
					BuffManagerType.CTF.arrBalanceBuffTypes[2], 
					BuffManagerType.CTF.arrBalanceBuffTypes[9] 
				];
			}
			//80% 이하
			else if ( fPercent <= 0.8 )
			{
				return [
					BuffManagerType.CTF.arrBalanceBuffTypes[1]
					BuffManagerType.CTF.arrBalanceBuffTypes[9]
				];
			}
			
			// 그 이상
			return [
					BuffManagerType.CTF.arrBalanceBuffTypes[0]
					BuffManagerType.CTF.arrBalanceBuffTypes[9]
				];
		}
	}
}
