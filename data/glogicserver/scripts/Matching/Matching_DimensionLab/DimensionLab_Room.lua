-------------------------------------------------------------------------------
-- 주의) 
-- 1) 대/소 문자를 확실하게 구분해야 합니다.
-- 2) true, false 를 확실하게 구분해야 합니다.
-- 3) UCS-2 Little Endian 로 저장해야 합니다.
-------------------------------------------------------------------------------

-- UI Text;
ContentsMainType 	= 	L"RNCOMPETITION_MAIN_TYPE_PVE";			-- 트리 첫번째 이름;
ContentsSubType 	= 	L"RNCOMPETITION_SUB_TYPE_WAIL_1";		-- 트리 두번째 이름;
ContentsName 		= 	L"DIMENSION_LABORATORY_PVE_MAPNAME";	-- 리스트 이름 (UI중앙);
ContentsMissionInfo = 	L"DIMENSION_LABORATORY_INFO";			-- 맵목표;
ContentsDetailInfo 	= 	L"DIMENSION_LABORATORY_DETAIL";			-- 맵정보;
ContentsRewardInfo 	= 	L"DIMENSION_LABORATORY_REWARD";			-- 맵보상; 

-- 컨텐츠 타입;
-- 0 => 일반;
-- 1 => 토너먼트;
-- 2 => 재입장 가능 일반;
ContentsType	= 	2;

-- 유저에게 알릴 토너먼트 타입입니다.;
-- 0 => 정시
-- 1 => 상시
-- 2 => 임시
ContentsDateType 	= 	1;	--	상시

-- Map ID;
ContentsMapID 		= 	261;

-- Time;
ContentsDate =
{
	--   요일, 시간, 분, 진행시간(분);
	{ 0, 09, 00, 1440 }, -- 매일 09시00분에 600분 동안 신청가능;
}

-- 정보보기 기능 사용여부;
ContentsUseInfoButton 		= 	false;

-- 입장제한;
-- 0일 경우 무제한;
ContentsMaxJoinCount 		= 	1;		-- 10회 (충전권 설정할 때 MID에 matchignScript.lua의 순서 0부터 n중에 몇번째, SID에 횟수해서 만들면됨.)
-------------------------------------------------------

function CreateComplete()
end