-------------------------------------------------------------------------------
-- 주의) 
-- 1) 대/소 문자를 확실하게 구분해야 합니다.
-- 2) true, false 를 확실하게 구분해야 합니다.
-- 3) UCS-2 Little Endian 로 저장해야 합니다.
-------------------------------------------------------------------------------

-- ColorCode
COLOR_BLACK				= {0,0,0};
COLOR_WHITE				= {255,255,255};
COLOR_RED					= {255,0,0};
COLOR_GREEN				= {0,255,0};
COLOR_BLUE				= {128,194,235};
COLOR_MAGENTA			= {240,100,170}; 
COLOR_YELLOW			= {255,200,0};
--------------------------------------------------------------------------------

-- 매칭이 가능한지 확인하는 Callback Function;
-- 프레임마다 돌아가기 때문에 무거운 코드는 심지 않는다;
function reqTournamentReady ( fElpaseTime )
	return true;
end

-- 입장 체크;
function functionCheckJoin ( nPlayerDbNum )
	-- 파티가 아니라면 입장 불가;
    if ( false == IsParty( nPlayerDbNum ) ) then
        return false;
    end
	
	-- 이미 해당 게임에 신청한 상태인지 검사;
	if ( true == IsJoinGame( nPlayerDbNum ) ) then	
		PrintChatXmlMsg( nPlayerDbNum, { "RNCDM_SYSTEM_MESSAGE", 12 }, COLOR_RED );		-- (이미 신청 중입니다)
		return false;
	end
	
	-- 다른 전장 컨텐츠를 신청,이용 중인지 검사;
	if ( ( true == IsJoinGameofALL( nPlayerDbNum ) ) or ( true == IsJoinOtherContents( nPlayerDbNum ) ) ) then
		PrintChatXmlMsg( nPlayerDbNum, { "RNCDM_SYSTEM_MESSAGE", 13 }, COLOR_RED );		-- (다른 참가 신청이 있습니다.)
		return false;
	end
    
	-- 필요 레벨 검사;
	local nLevel = GetLevel( nPlayerDbNum );
	if ( nLevel < 250 ) then
		PrintChatXmlMsg( nPlayerDbNum, { "RNCDM_SYSTEM_MESSAGE", 21 }, COLOR_RED ); -- (입장 가능레벨을 확인하세요.)
		return false;
	end
	
	-- 입장 가능;
	return true;
end

-- 입장 요청시 호출되는 Callback Function;
function TournamentRequestJoin ( nPlayerDbNum )
	-- 파티 상태 확인;
	local nPartyID = GetPartyID( nPlayerDbNum );
	if ( nil == nPartyID ) then
		-- 파티원이 아닙니다. 파티 상태에서 신청가능합니다;
		PrintChatXmlMsg( nPlayerDbNum, { "RNCDM_SYSTEM_MESSAGE", 16 }, COLOR_RED ); -- 5/29 xml로 교체.
		return false;
	end
	
	-- 입장 요청자의 입장 체크;
	functionCheckJoin( nPlayerDbNum );
	
	-- 파티장인지 검사;
	local nPartyMasterDBNum = GetPartyMaster( nPartyID );
	if ( nPlayerDbNum ~= nPartyMasterDBNum ) then
		PrintChatXmlMsg( nPlayerDbNum, { "RNCDM_SYSTEM_MESSAGE", 19 }, COLOR_RED ); -- 5/29 xml로 교체.
		return false;
	else
		-- 파티원의 입장 체크;
		local nPartySize = GetPartyMemberNum( nPartyID );
		for i = 0, nPartySize-1, 1 do
			local nPartyMemberDBNum = GetPartyMember( nPartyID, i );
			
			if ( false == functionCheckJoin( nPartyMemberDBNum ) ) then
				return false;
			end
		end
	end
	
	ServerLog( "[ Matching Log ] [ Request DimensionLab ... ]" );
    
	-- 참가 신청;
	-- 파티원들을 모두 하나의 그룹으로 넣어 신청한다;
	local nPartyMemberList = GetParty( nPlayerDbNum );
	for key, nPartyMemberDBNum in pairs( nPartyMemberList ) do
		SendMatchingQueue( nPartyMemberDBNum );
	end
	EndTournamentQueue();
	
	-- 하나의 그룹만 받기 때문에 바로 매칭을 시작한다;
    TournamentStart();
	
	return true;
end

-- 재입장 여부 설정;
function TournamentRequestReJoin ( nPlayerDbNum, InstanceMapID )
	ServerLog( "[ Matching Log ] [ Rejoin DimensionLab ... ]" );
	
    return true;
end

-- 신청 취소시 호출되는 Callback Function;
function MatchingJoinCansel ( nPlayerDbNum )
    local nMatchingGroupID = FindGroup( nPlayerDbNum );
    DeleteGroup( nMatchingGroupID );
end

-- 캐릭터가 Drop Out 될 시 호출되는 Callback Function;
function OnDropOutChar ( nPlayerDbNum )
	MatchingJoinCansel( nPlayerDbNum );
end