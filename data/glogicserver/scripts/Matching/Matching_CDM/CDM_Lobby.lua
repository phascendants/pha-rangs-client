--
-- 주의) 
-- 1) 대/소 문자를 확실하게 구분해야 합니다.
-- 2) true, false 를 확실하게 구분해야 합니다.
-- 3) UCS-2 Little Endian 로 저장해야 합니다.
-------------------------------------------------------------------------------
--------------------------------------------------------------------------------
-- ColorCode
	COLOR_BLACK				= {0,0,0};
	COLOR_WHITE				= {255,255,255};
	COLOR_RED				= {255,0,0};
	COLOR_GREEN				= {0,255,0};
	COLOR_BLUE				= {128,194,235};
--------------------------------------------------------------------------------
-- CDM_ROOM.lua파일의 정보 참조 (실제 인던이 생성되는 시점);
ContentsDate =
{
--   요일, 시간, 분 , Duration (진행시간)
	{ 2, 23, 10, {30,20,10,9,8,7,6,5,4,3,2,1} }, -- 월요일 23시 10분에 CDM 시작;
	--{ 3, 14, 10, {30,20,10,9,8,7,6,5,4,3,2,1} }, -- 화요일 14시 10분에 CDM 시작;
	{ 4, 23, 10, {30,20,10,9,8,7,6,5,4,3,2,1} }, -- 수요일 23시 10분에 CDM 시작;
	--{ 5, 2, 0, {30,20,10,9,8,7,6,5,4,3,2,1} },   -- 목요일 02시 0분에 CDM 시작;
	{ 6, 23, 10, {30,20,10,9,8,7,6,5,4,3,2,1} },   -- 금요일 23시 10분에 CDM 시작;
	--{ 7, 22, 10, {30,20,10,9,8,7,6,5,4,3,2,1} },   -- 토요일 22시 10분에 CDM 시작;
	{ 1, 23, 10, {30,20,10,9,8,7,6,5,4,3,2,1} }, -- 일요일 23시 10분에 CDM 시작;
	--{ 0, 22, 0, {30,20,10,9,8,7,6,5,4,3,2,1} }, -- 매일 22시 0분에 CDM 시작;		
}
--------------------------------------------------------------------------------
OpenGameDateNum = nil;
ClubList = {};
--------------------------------------------------------------------------------

function reqTournamentReady(fElpaseTime)
	local _day = GetDay();
	local _weekDay = GetWeekDay();
	local _hour = GetHour();
	local _min = GetMinute();
	
	-- 현재 진행중인 게임이 있는지 체크;
	if ( OpenGameDateNum ~= nil ) then
		local ContentOpenDate = ContentsDate[OpenGameDateNum];
		
		if ( ( ContentOpenDate[2] ~= _hour ) or ( ContentOpenDate[3] ~= _min ) )then
			OpenGameDateNum = nil;
		end
		
		-- 진행중인 게임이 있다면 1분 동안은 혹시모를 상황을 위해 신청자들을 계속 난입가능하도록 수정;
		TournamentStart();
	else
		-- 현재 진행중인 게임이 없다면 열려야하는 게임이 있는지 체크;
		for dateKey, dateValue in pairs( ContentsDate ) do
			-- 해당 시간까지 남은 시간;
			local _leftHour = dateValue[2] - _hour;
			local _leftMinute = ( _leftHour * 60 ) + ( dateValue[3] - _min );
			-- 해당 요일까지 남은 일수;
			local _nDayInterval = 0;
			if ( dateValue[1] ~= 0 ) then
				_nDayInterval = dateValue[1] - _weekDay;
				if ( _nDayInterval < 0 ) then
					_nDayInterval = _nDayInterval + 7;
				elseif ( _nDayInterval == 0 ) and ( _leftMinute < 0 ) then
					_nDayInterval = 7;
				end
			elseif ( _leftMinute < 0 ) then
				_nDayInterval = 1;
			end
			_leftMinute = _leftMinute + ( _nDayInterval * 24 * 60 );
			
			-- 현재는 1일 이전에 한해서 알림;
			if ( _nDayInterval <= 1 ) then
				-- 알림이 있다면 알림 시간 체크;
				if ( dateValue[4] ~= nil ) then
					-- 최초 알림 초기화;
					if ( dateValue["currentNotify"] == nil ) then
						for NotifyKey, NotifyValue in pairs( dateValue[4] ) do
							dateValue["currentNotify"] = NotifyKey;
							if ( _leftMinute >= NotifyValue ) then
								break;
							end
						end
					end
					-- 알림 시간 체크;
					local notifyMinute = dateValue[4][dateValue["currentNotify"]];
					if ( notifyMinute ~= nil ) then
						if ( _leftMinute == notifyMinute ) then
							PrintSimpleXmlMsg_Broad(10, {"RNCDM_SYSTEM_MESSAGE", 26, _leftMinute}, COLOR_WHITE);
							PrintChatXmlMsg_Broad({"RNCDM_SYSTEM_MESSAGE", 26, _leftMinute}, COLOR_BLUE);
							dateValue["currentNotify"] = dateValue["currentNotify"] + 1;
							break;
						elseif ( _leftMinute < notifyMinute ) then
							dateValue["currentNotify"] = dateValue["currentNotify"] + 1;
						end
					end
				end
				-- 진행시간 체크;
				if ( _leftMinute == 0 ) then
					-- CDM 시작;
					EndTournamentQueue();
					TournamentStart();				
					
					-- 해당 클럽의 등록된 파티들 전부 해제;
					ClubList = nil;
					ClubList = {};
					
					OpenGameDateNum = dateKey;
					dateValue["currentNotify"] = nil;
					
					-- CDM을 시작합니다.
					PrintSimpleXmlMsg_Broad(10, {"RNCDM_SYSTEM_MESSAGE", 11},COLOR_WHITE);
					PrintChatXmlMsg_Broad({"RNCDM_SYSTEM_MESSAGE", 11},COLOR_BLUE);
					
					return true;
				end
			end
		end
	end
	
	return false;
end

-- 토너먼트 입장 요청을 하였다! 확인해주자!
function TournamentRequestJoin(nPlayerDbNum)

	-- CDM 진행중에 입장 신청을 했다는 뜻 ( 패킷 조작 가능성 );
	if ( OpenGameDateNum ~= nil ) then
		return false;
	end
	
	if ( functionChekJoin(nPlayerDbNum) == false ) then
		return false;
	end

	local _partyID = GetPartyID(nPlayerDbNum);
	local _nJoinClubID = GetClubID(nPlayerDbNum);
	-- 해당 클럽의 등록자 파티Num을 저장해 놓음 ( 한 클럽에서 2개 이상의 파티는 입장 불가 )
	ClubList[_nJoinClubID] = _partyID;
	
	-- 파티 인원 정보를 테이블로 가져 옵니다.
	local partygroup = GetParty(nPlayerDbNum);	
	-- 파티원에 대한 모든 입장조건을 체크한다.
	for key, value in pairs( partygroup ) do
		SendMatchingQueue(value); -- 참가
		--클럽데스매치 참가신청을 하였습니다.;
		PrintChatXmlMsg(value, { "RNCDM_SYSTEM_MESSAGE", 14 },COLOR_BLUE); -- 5/29 xml로 교체.
		PrintChatXmlMsg(value, { "RNCDM_SYSTEM_MESSAGE", 15 },COLOR_BLUE); -- 5/29 xml로 교체.
	end	
	EndTournamentQueue();
	--SendToClientMessageBox(L"데스메치 입장 대기 중", nPlayerDbNum);
	--SendToClientMessageXMLGROUP(partygroup, { "RNCDM_SYSTEM_MESSAGE", 15 }, COLOR_BLUE);
	return true;
end

function functionChekJoin(nPlayerDbNum)
	-- 파티중인가?
	local _partyID = GetPartyID(nPlayerDbNum);
	if ( _partyID == nil ) then
		-- 파티원이 아닙니다. 파티 상태에서 신청가능합니다.
		PrintChatXmlMsg(nPlayerDbNum, { "RNCDM_SYSTEM_MESSAGE", 16 },COLOR_RED); -- 5/29 xml로 교체.
		return false;
	end
	
	-- 소속 클럽이 있는가?
	local _nJoinClubID = GetClubID(nPlayerDbNum);
	if ( _nJoinClubID == nil ) then
		-- 소속클럽이 없습니다.
		PrintChatXmlMsg(nPlayerDbNum, { "RNCDM_SYSTEM_MESSAGE", 17 },COLOR_RED); -- 5/29 xml로 교체.
		return false;
	end
	
	-- 클럽등급이 S등급 (4등급) 이상인가?
	local _nClubRank = GetClubRank(_nJoinClubID);	
	if ( _nClubRank == nil ) or ( _nClubRank < 4 ) then
		-- 랭크가 부족합니다. S랭크 이상 신청가능합니다.
		PrintChatXmlMsg(nPlayerDbNum, { "RNCDM_SYSTEM_MESSAGE", 18 },COLOR_RED); -- 5/29 xml로 교체.
		return false;
	end
	
	-- 이미 해당클럽으로 등록된 파티가 있는가?
	if ( ClubList[_nJoinClubID] ~= nil ) then
		-- 이미 해당 클럽으로 신청한 파티가 있습니다.
		PrintChatXmlMsg(nPlayerDbNum, { "RNCDM_SYSTEM_MESSAGE", 27 },COLOR_RED); -- 5/29 xml로 교체.
		return false;
	end
	
	-- 파티원들의 입장 조건 검사;
	local _partyMaster = GetPartyMaster(_partyID);
	if ( nPlayerDbNum ~= _partyMaster ) then
		-- 파티장이 아닙니다. 파티장만 신청가능합니다.
		PrintChatXmlMsg(nPlayerDbNum, { "RNCDM_SYSTEM_MESSAGE", 19 },COLOR_RED); -- 5/29 xml로 교체.
		return false;
	else
		local _partySize = GetPartyMemberNum(_partyID);
		for i = 0, _partySize-1, 1 do
			local _partyMember = GetPartyMember(_partyID, i);
			local _partyMemberClubID = GetClubID(_partyMember);
			if ( _nJoinClubID ~= _partyMemberClubID ) then
				-- 클럽원들로 구성된 파티가 아닙니다.
				PrintChatXmlMsg(nPlayerDbNum, { "RNCDM_SYSTEM_MESSAGE", 20 },COLOR_RED); -- 5/29 xml로 교체.
				return false;
			else
				requestPlayer = nPlayerDbNum;
				-- 파티원중 입장 불가 인원이 있는가?
				if ( functionCheckRequire(_partyMember) == false ) then
					return false;
				end
			end
		end
	end
	return true;
end

function functionCheckRequire(nPlayerDbNum)

	-- 이미 해당 게임에 신청한 상태인지 검사;
	if(IsJoinGame(nPlayerDbNum) == true) then	
		PrintChatXmlMsg(requestPlayer, { "RNCDM_SYSTEM_MESSAGE", 12 },COLOR_RED); -- (이미 신청 중입니다) 신청자에게 보냄
		if ( requestPlayer ~= nPlayerDbNum ) then
			PrintChatXmlMsg(nPlayerDbNum, { "RNCDM_SYSTEM_MESSAGE", 12 },COLOR_RED); -- (이미 신청 중입니다) 해당 검사자에게 보냄;
		end
		return false;
	end
	
	-- 다른 전장 컨텐츠를 신청,이용 중인지 검사;
	if ( (IsJoinGameofALL(nPlayerDbNum) == true) or (IsJoinOtherContents(nPlayerDbNum) == true) )then
		PrintChatXmlMsg(requestPlayer, { "RNCDM_SYSTEM_MESSAGE", 13 },COLOR_RED); -- (다른 참가 신청이 있습니다.) 신청자에게 보냄
		if ( requestPlayer ~= nPlayerDbNum ) then
			PrintChatXmlMsg(nPlayerDbNum, { "RNCDM_SYSTEM_MESSAGE", 13 },COLOR_RED); -- (다른 참가 신청이 있습니다.) 해당 검사자에게 보냄;
		end
		return false;
	end

	-- 필요 레벨 검사
	local _nLevel = GetLevel(nPlayerDbNum);
	if ( _nLevel < 195) then
		PrintChatXmlMsg(requestPlayer, { "RNCDM_SYSTEM_MESSAGE", 21 },COLOR_RED); -- (입장 가능레벨을 확인하세요.) 신청자에게 보냄
		if ( requestPlayer ~= nPlayerDbNum ) then
			PrintChatXmlMsg(nPlayerDbNum, { "RNCDM_SYSTEM_MESSAGE", 21 },COLOR_RED); -- (입장 가능레벨을 확인하세요.) 해당 검사자에게 보냄;
		end
		return false;
	end
	
	-- CDM 참가 권한이 있는가?
	local bAuthority = HaveCDM_Authority(nPlayerDbNum);
	if ( bAuthority == false) then
		PrintChatXmlMsg(requestPlayer, { "RNCDM_SYSTEM_MESSAGE", 22 },COLOR_RED); -- (클럽권한(클럽데스매치 참가자격)을 확인하세요.) 신청자에게 보냄
		if ( requestPlayer ~= nPlayerDbNum ) then
			PrintChatXmlMsg(nPlayerDbNum, { "RNCDM_SYSTEM_MESSAGE", 22 },COLOR_RED); -- (클럽권한(클럽데스매치 참가자격)을 확인하세요.) 해당 검사자에게 보냄
		end
		return false;
	end
	
	return true;
end

function MatchingJoinCansel(nPlayerDbNum) -- 신청 취소시;
	-- 파티가 없는지 검사.
	if IsParty(nPlayerDbNum) == false then  
		-- 파티상태가 아닙니다.
		PrintChatXmlMsg(nPlayerDbNum, { "RNCDM_SYSTEM_MESSAGE", 23 },COLOR_RED); -- 5/29 xml로 교체.
		return false
	end
	
	-- 파티 장인지 체크
	local _partyID = GetPartyID(nPlayerDbNum);
	local _masterID = GetPartyMaster(_partyID);
	if ( _masterID ~= nPlayerDbNum ) then   
		--SendToClientMessageBox(L"취소는 파티장만이 할수있습니다.",nPlayerDbNum) -- 파티마스터만 신청취소가 가능합니다.
		PrintChatXmlMsg(nPlayerDbNum, { "RNCDM_SYSTEM_MESSAGE", 24 },COLOR_RED); -- 5/29 xml로 교체.
		return false
	end
	
	--PrintChatXmlMsg(nPlayerDbNum, { "RNCDM_SYSTEM_MESSAGE", 25 },COLOR_RED); -- 5/29 xml로 교체.
	OnDropOutChar(nPlayerDbNum);
end

function OnDropOutChar(ClietDBID)
	if(IsJoinGameofALL(ClietDBID) == true) then
		local GroupId = FindGroup(ClietDBID);
		
		if( GroupId == -1)then
			return false;
		end

		local GroupTable = GetGroup(GroupId);
		
		for value in pairs( GroupTable ) do
			-- 클럽데스매치 신청이 취소되었습니다.
			PrintChatXmlMsg(value, { "RNCDM_SYSTEM_MESSAGE", 25 },COLOR_RED); -- 5/29 xml로 교체.
		end
		DeleteGroup(GroupId); 
		
		-- 해당 클럽의 등록된 파티에서 해제;
		local _nClubID = GetClubID(ClietDBID);
		ClubList[_nClubID] = nil;
		
		return true;
	end
	return true;
end

-- 아래의 주석을 해지 하면 변경 조건에 따라 다르게 처리 가능하다.
-- 하지만 주석을 풀지 않는것을 권장함.
--function PlayerPartyChanged(nPlayerDbNum)
--	OnDropOutChar(nPlayerDbNum);
--end