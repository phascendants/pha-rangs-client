-- 트리 첫번째 이름
ContentsMainType = L"RNCOMPETITION_MAIN_TYPE_PVP";
-- 트리 두번째 이름
ContentsSubType = L"RNCOMPETITION_SUB_TYPE_CDM";
-- 리스트 이름 (UI중앙)
ContentsName = L"MATCHING_CDM_MAPNAME";
-- 맵목표
ContentsMissionInfo = L"MATCHING_CDM_INFO";
-- 맵정보 
ContentsDetailInfo = L"MATCHING_CDM_DETAIL";
-- 맵보상 
ContentsRewardInfo = L"MATCHING_CDM_REWARD";

ContentsType = 0;

-- 유저에게 알릴 토너먼트 타입입니다. 
-- 0 => 정시
-- 1 => 상시
-- 2 => 임시
ContentsDateType = 0;
ContentsMapID = 180;

ContentsDate =
{
--   요일, 시간, 분 , Duration (진행시간)
	{ 2, 23, 00, 10 }, -- 월요일 23시00분에 10분 동안 신청가능;
	--{ 3, 14, 00, 10 }, -- 화요일 14시00분에 10분 동안 신청가능;
	{ 4, 23, 00, 10 }, -- 수요일 23시00분에 10분 동안 신청가능;
	--{ 5, 1, 50, 10 },  -- 목요일 01시50분에 10분 동안 신청가능;
	{ 6, 23, 00, 10 },  -- 금요일 23시00분에 10분 동안 신청가능;
	--{ 7, 22, 00, 10 },  -- 토요일 22시00분에 10분 동안 신청가능;
	{ 1, 23, 00, 10 },  -- 일요일 23시00분에 10분 동안 신청가능;	
	--{ 0, 21, 50, 10 }, -- 매일 21시50분에 10분 동안 신청가능;	
}
-------------------------------------------------------

function CreateComplete()
--	NotifyCreateComplete();
end

function EventCustomMessage( ClientDbNum, CustomParam1, CustomParam2 )
	if ( CustomParam1 == 1 ) then
		Send_CDM_History(ClientDbNum);
	end
end