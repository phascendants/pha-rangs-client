----- 클럽 데스매치용 매칭 스크립트
----- 실제 하는 역할은 인던을 생성하고 지금까지 대기중이었던 인원들을 입장시키는 일을 한다.
--------------------------------------------------------------------------------
-- ColorCode
	COLOR_BLACK				= {0,0,0};
	COLOR_WHITE				= {255,255,255};
	COLOR_RED				= {255,0,0};
	COLOR_GREEN				= {0,255,0};
	COLOR_BLUE				= {128,194,235};
--------------------------------------------------------------------------------
EMMATCHING_CONTENTS_OPENED = 0         --// - 신청가능: 해당 토너먼트에 신청이 가능한 시간인 경우;
EMMATCHING_CONTENTS_CLOSED = 1         --// - 신청불가: 해당 토너먼트가 신청이 불가능한 상태인 경우;
EMMATCHING_CONTENTS_STANDBY_JOIN = 2   --// - 입장대기: 해당 토너먼트가 신청 마감 후 입장대기로 변경;
EMMATCHING_CONTENTS_IN_PROGRESS = 3    --// - 진행 중: 해당 토너먼트가 현재 진행중인 경우;
--------------------------------------------------------------------------------

--GroupTable = {};
bCanStart = true;
bCanDestroy = true;

function OnReset( )
	GroupTable = {};
	bCanStart = true;
	bCanDestroy = true;
end

bOk = false;
InstanceTable = nil;
_instanceid = nil;
function OnStartMatching()
	if ( bOk == false ) then
		bOk = true;
		Serverlog("GetMatchGroup OK");

		local mapid = GetSNATIVEID(180, 0);
		
		InstanceTable = MapMultyCreate(0, 1, mapid);
		local IsMapCreate = false;
		for inID in pairs( InstanceTable ) do
			Serverlog("<MapRequestResualt " .. "GroupID=" .. inID  .. "/>"  )
			IsMapCreate = true;
		end
		
		if IsMapCreate == false then
			InstanceTable = MapMultyCreate(0, 1, mapid);
			for inID in pairs( InstanceTable ) do
				Serverlog("<MapRequestResualt " .. "GroupID=" .. inID  .. "/>"  )
				IsMapCreate = true;
			end
		
			if IsMapCreate == false then
				Serverlog("MapCreate Fail");
				bOk = false;
				return true;
			end
		end
		
		bOk = true;
		Serverlog("MapCreate OK");
		
		for inID in pairs( InstanceTable ) do
			_instanceid = inID;
		end
        
        SetTournamentStatus(EMMATCHING_CONTENTS_IN_PROGRESS);
	end
	
	local WaitingQueuSize = GetMatchingWaitingGroupCount();
	for  i = 0, WaitingQueuSize, 1 do
		local group = GetGroupToMatching();
		--SendToClientMessageBoxGROUP(L"클럽데스매치 존으로 입장합니다.(plz Change XmlString)", group); -- 태혁씨 작업 후 xml로 교체.
		SendToClientMessageXMLGROUP(group, {"RNCDM_ENTER_TEXT", 0}, COLOR_BLUE);
		SendJoin(group, _instanceid);
		FinishGroup(group);
	end
		
	return true;
end
function OnDestroy(InstanceID, isSucsess)
	bOk = false;
	SetTournamentStatus(EMMATCHING_CONTENTS_CLOSED);
	return false; -- 이맵에 있는 모든 녀석들 나가게 한다.
end
	
function NoticeResualt(InstanceID,FactionID,_Rating,_IsWinner)
	return true;
end

function isReadyToStart()
	return bCanStart;
end

function isReadyToDistroy()
	return bCanDestroy;
end