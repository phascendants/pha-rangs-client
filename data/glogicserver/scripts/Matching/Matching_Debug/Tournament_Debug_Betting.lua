--
-- 주의) 
-- 1) 대/소 문자를 확실하게 구분해야 합니다.
-- 2) true, false 를 확실하게 구분해야 합니다.
-- 3) UCS-2 Little Endian 로 저장해야 합니다.
-------------------------------------------------------------------------------
TournamentAnyTime = true;

Joinsize = 0;

-- 토너먼트 시작.
function reqTournamentStart()

	if(TournamentAnyTime == true) then 
		TournamentStart()
	end
		
	return true; 
end


function reqTournamentReady(fElpaseTime)

	if(TournamentAnyTime == true) then 
		if(Joinsize >= 2) then
			TournamentStart();
			Joinsize = 0;
			return true;
		end
	end
	return false;
end

-- 토너먼트 입장 요청을 하였다! 확인해주자!
function SetMatchingQueue(nPlayerDbNum)
			
	local resualt = false;
	
	resualt = SendMatchingQueue(nPlayerDbNum)
	
	Joinsize = Joinsize + 1;
	
	return true;
end

-- 토너먼트 입장 요청을 하였다! 확인해주자!
function TournamentRequestJoin(nPlayerDbNum)
	
	
	
	if(IsJoinGame(nPlayerDbNum) == true) then
		SendToClientMessageBox(L"이미 신청중에 있습니다.",nPlayerDbNum)
		return false;
	end
	
	if(IsJoinGameofALL(nPlayerDbNum) == true) then
		SendToClientMessageBox(L"딴거에 이미 신청중에 있습니다.",nPlayerDbNum)
		return false;
	end
	
	SetMatchingQueue(nPlayerDbNum) -- 참가 
	EndTournamentQueue();
	

	SendToClientMessageBox(L"메칭을 찾고 있습니다...",nPlayerDbNum)
	
	reqTournamentReady(0);
end

function TournamentRequestREJoinGroup(nGroupID)
	--SendToClientMessageBoxALL(L"나 다시 로비로 왔음");
	Joinsize = Joinsize + 1;
	return true;
end


function OnDropOutChar(ClietDBID)
	
	SendToClientMessageBox(L"너 팅김",ClietDBID);
	
	if(IsJoinGameofALL(ClietDBID) == true) then
		local GroupId = FindGroup(ClietDBID);
		
		if( GroupId == -1)then
			return false;
		end
		-- 그룹은 살리고 사람만 지운다.를 원한다면 아래의 주석 삭제
		--GroupOut(GroupId, ClietDBID);
		
		-- 그룹을 지운다.를 원한다면 아래의 주석 삭제
		DeleteGroup(GroupId); 
		
		Joinsize = Joinsize - 1;
		
		return true;
	end
	return true;
end

function MatchingCansel()
	SendToClientMessageBoxALL(L"메칭 취소됨");
end











