----- 개인전이던 팀전이던 무조건 5:5 매칭을 해주는 예제 스크립트
----- 1:1 를 만들어줄뿐 그외 예외에 대해선 알아주지 않는다.
-----

MatchingtableA = { };
MatchingCountA = 0;

MatchingtableB = { };
MatchingCountB = 0;

MatchingResualt = { };

MapRequestResualt = { };
MapRequestSize = 0;

resualtCount = 2;

isCanStart = true;
isFinish = false;


LaterStart = false;

isGameRun = false;


TotalJoinMessageCount = 0;
TotalReadyMessageCount = 0;
TotalNotReadyMessageCount = 0

AskJoinTable = {}



function OnReset( )
	MatchingtableA = { };
	MatchingCountA = 0;
	MatchingtableB = { };
	MatchingCountB = 0;
	MatchingResualt = { };
	MapRequestResualt = { };
	MapRequestSize = 0;
	resualtCount = 2;
	TotalJoinMessageCount = 0;
	TotalReadyMessageCount = 0;
	TotalNotReadyMessageCount = {};
	isGameRun = true;
	return true;
end

function OnLuaFrameMove(fElpaseTime)
	if( TotalJoinMessageCount ~= 0 and isGameRun == false) then
		if(fElpaseTime > 30) then
			NotAnswer();			
		end
		return fElpaseTime;
	end

	return 0;	
end

-- 메칭 실패하면 자동으로 다시 큐에 넣어줌
function OnStartMatching( )

	local WaitingQueuSize = GetTournamentWaitingGroupCount();
	
	if WaitingQueuSize ==  0 then
		return false;
	end
	
	local GroupsA = { };
	
	local GroupsB = { };
	
	local Groupstabls=	GetGroupToMatching();
	
	if Groupstabls == -1 then
		return false;
	end
	
	table.insert(GroupsA , Groupstabls);
	
	
	Groupstabls=	GetGroupToMatching();
	
	if Groupstabls == -1 then
		return false;
	end
	
	table.insert(GroupsB , Groupstabls);
	
-------------------------------------------------A 팀
	
	Serverlog("GetMatchGroup OK");
	local LeftCount = 1;
	local ResualtGroupID;
	
	for key in pairs( GroupsA ) do
		
		local idA = GroupsA[key];
		
		local sizeA = GetGroupSize(idA);
		
		if sizeA <= LeftCount  then 
			MatchingtableA[MatchingCountA] = idA;
			MatchingCountA = MatchingCountA + 1;
			LeftCount = LeftCount - sizeA;			
			SendToClientMessageBoxGROUP(L"헿~ 난 A팀이다.",idA);
		else
		-- 다시 큐에 넣어줌
			SetGroupToLobby(idA);
			Serverlog("SetGroupToLobby");
		end
	end
	

	if(LeftCount > 0) then 
		return false;
	end
	
	Serverlog("Groups Generated Team");
	
	local MargeA  =-1;
	for key in pairs( MatchingtableA ) do
	
		if( MargeA == -1) then 
			MargeA = MatchingtableA[key];
		else
			MargeA = GetSumGroup(MargeA,MatchingtableA[key]);
			Serverlog("GetSumGroup");
		end
		
	end
	MatchingResualt[0] = MargeA;
-------------------------------------------------A팀 끝
	Serverlog("A Team Create Finished");

-------------------------------------------------B팀
	
	LeftCount = 1;
	for Bkey in pairs( GroupsB ) do
		local idB = GroupsB[Bkey];
		
		local sizeB = GetGroupSize(idB);
		
		
		if(sizeB <= LeftCount) then 
			MatchingtableB[MatchingCountB] = idB;
			MatchingCountB = MatchingCountB + 1;
			LeftCount = LeftCount - sizeB;
			SendToClientMessageBoxGROUP(L"헿~ 난 B팀이다.",idB);
		else
		-- 다시 큐에 넣어줌
			SetGroupToLobby(idB);
		end
	end
	
	if(LeftCount > 0) then 
		Serverlog("B Team Create Fail! ");
		return false;
	end
	
	Serverlog("Groups Generated Team");
	
	local MargeB  =-1;
	for key in pairs( MatchingtableB ) do
		if( MargeB == -1) then 
			MargeB = MatchingtableB[key]
		else
			MargeB = GetSumGroup(MargeB,MatchingtableB[key]);
		end
	end
	MatchingResualt[1] = MargeB;
--------------------------------------------------B팀 끝

	Serverlog("B Team Create Finished");

	local A_AskJoinTable = GetGroup(MatchingResualt[0]);

	for ClientDBid in pairs( A_AskJoinTable ) do
		local DieString = string.format("%s is A ASKING", ClientDBid);
		Serverlog(DieString);
		AskJoinTable[ClientDBid] = false;
	end

	A_AskJoinTable = nil;

	local B_AskJoinTable = GetGroup(MatchingResualt[1]);

	for ClientDBid in pairs( B_AskJoinTable ) do
		local DieString = string.format("%s is B ASKING", ClientDBid);
		Serverlog(DieString);
		AskJoinTable[ClientDBid] = false;
	end

	B_AskJoinTable = nil;
	
	TotalJoinMessageCount = 2;

	Serverlog("ASK BY YOU!!");

	
	isCanStart = false;
	
	if IsValidGroup(MatchingResualt[0]) == true or IsValidGroup(MatchingResualt[1]) == true then
		TotalJoinMessageCount = 2;
		SendToClientMessageBoxGROUP(L"너임마 들어올꺼니?..",MatchingResualt[0]);
		SendToClientMessageBoxGROUP(L"너임마 들어올꺼니?..",MatchingResualt[1]);

		SendToGroupAskJoin(MatchingResualt[0]);
		SendToGroupAskJoin(MatchingResualt[1]);
	end

	return true;
	
end



function MapRquest(MapSize, MapID)


	Serverlog("MapRquest");
-- 순서 반드시 시켜야한다, MapSize 먼저 설정하세요
	MapRequestSize = MapSize;
	MapRequestResualt = {};
	
	if(MapSize == -1) then 
		return false;
	end

-- 갯수 대로 만들어준다.
	for i =1, MapRequestSize, 1 do
		RequestMapCreate(MapID );
		Serverlog("RequestMapCreate GOING ON");
	end
	
	return true;
end

-- 본격적 리소스는 모두 요청 완료 했다.
-- 시작하자.
function StartMachedGame(InstanceID)


-- 편가르고
	SendFaction(MatchingResualt[0], 0  , InstanceID);
	SendFaction(MatchingResualt[1], 1 , InstanceID );
	
	SendToClientMessageBoxGROUP(L"A팀 으로 쪼인.",MatchingResualt[0]);
	SendToClientMessageBoxGROUP(L"B팀 으로 쪼인",MatchingResualt[1]);
	
-- 시작하소 고마.
	SendJoin(MatchingResualt[0], InstanceID);
	SendJoin(MatchingResualt[1], InstanceID);
		
	return true;
end

function NoticeResualt(InstanceID,FactionID,_IsWinner)

	Serverlog(string.format("%d Faction",FactionID));
	if(_IsWinner == true) then 
		--SetRankScore(MatchingResualt[FactionID],_Rating +10 );
		resualtCount  = resualtCount - 1;
		
		SendToClientMessageBoxGROUP(L"랭킹 포인트 상승!",MatchingResualt[FactionID]);
	else
		--SetRankScore(MatchingResualt[FactionID],_Rating * (-1) );
		resualtCount  = resualtCount - 1;
		SendToClientMessageBoxGROUP(L"랭킹 포인트 하락",MatchingResualt[FactionID]);
	end
	
	return true;
end

function IsMyMap(InstanceID)
	for key in pairs( MapRequestResualt ) do
		if( MapRequestResualt[key] == InstanceID) then 
			return true;
		end
	end
	
	return false;
end




function ErrorCode(errorcode)
	
	return true;
end

function EndMessage()
	
	if(PlayTime ~= nil) then
		local DieString = string.format("%s(sec) Time left", PlayTime);
		Serverlog(DieString);
		if(PlayTime >50) then
			SendToClientMessageBoxGROUP(L"조건 맞음 끝남" ,MatchingResualt[0]);
			SendToClientMessageBoxGROUP(L"조건 맞음 끝남",MatchingResualt[1]);
			FinishGroup(MatchingResualt[0]);
			FinishGroup(MatchingResualt[1]);
			isCanStart = true;
			DoDestroy();
		else
			SendToClientMessageBoxGROUP(L"싱거워 다시 싸워" ,MatchingResualt[0]);
			SendToClientMessageBoxGROUP(L"싱거워 다시 싸워",MatchingResualt[1]);
			local mapid = GetSNATIVEID(23,0);
			LaterStart = true;
		
		end
	end
	return true;
end

function RatingUp(nPlayerDbNum,_Rating)
	SetRating(nPlayerDbNum,_Rating);
end


function OnReadyAnswer( Message_Client , OKMessage)

	Serverlog("OnReadyAnswer");
	
	
	if( OKMessage == true)then
		TotalReadyMessageCount = TotalReadyMessageCount +1;
	else
		TotalNotReadyMessageCount = TotalNotReadyMessageCount +1;
	end

	for ClientDBid in pairs( AskJoinTable ) do
		if(ClientDBid == Message_Client)then
			AskJoinTable[ClientDBid] = true;
		end
	end

	TotalJoinMessageCount = TotalJoinMessageCount -1;

	if(TotalJoinMessageCount < 1)then
		if TotalNotReadyMessageCount ~= 0 then
			OnReset();
			SetGroupToLobbyALL();
			DoDestroy();
		else
			isGameRun = true;
			FinishRUOK();
		end
	end	
	
end


function OnMapCreate( InstanceID, isSucsess )

	if(isSucsess == false) then 
		Serverlog("MapRequestSize Fail3");
		return false;
	end
		
 -- 만들어진 맵 갯수 알아온다.
	table.insert(MapRequestResualt,InstanceID);
	
	local size = table.getn(MapRequestResualt)
	
	Serverlog("OnMapCreate GOOD");
	
	if(MapRequestSize  == size) then 
		StartMachedGame(InstanceID);
		MapRequestSize = -1;
		Serverlog("MapRequestSize Sucess");
	else
		Serverlog("MapRequestSize Fail");
		return false
	end

	return true;
end


function NotAnswer()

	if(isGameRun == true) then
		return
	end
	TotalNotReadyMessageCount = TotalNotReadyMessageCount + TotalJoinMessageCount;
	TotalJoinMessageCount = 0;

	SetGroupToLobbyALL();
	for ClientDBid in pairs( AskJoinTable ) do
		if AskJoinTable[ClientDBid] == false then
			GroupAskJoinTimeOut(ClientDBid);
		end
	end
	--MatchingCansel();
	DoDestroy(TotalReadyMessageCount);

end

function FinishRUOK()

	local returnvalue = false;
	
	local mapid = GetSNATIVEID(23,0);
	
	returnvalue = MapRquest(1,mapid); -- 매칭 성공이야 성공
	
	Groupstabls = nil;
end


function OnDestroy(InstanceID, isSucsess)
	SendToClientMessageBoxGROUP(L"맵이 부셔졌다!!" ,MatchingResualt[0]);
	SendToClientMessageBoxGROUP(L"맵이 부셔졌다!!",MatchingResualt[1]);
		
	if LaterStart == true then
		LaterStart = false;
		local mapid = GetSNATIVEID(23,0);
		MapRquest(1,mapid); -- 다시 맵으로
		return 0;
	end
end

function isReadyToStart()
	return isCanStart;
end