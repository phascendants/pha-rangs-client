--
-- 주의) 
-- 1) 대/소 문자를 확실하게 구분해야 합니다.
-- 2) true, false 를 확실하게 구분해야 합니다.
-- 3) UCS-2 Little Endian 로 저장해야 합니다.
-- Matching_Content_Type는 는 해당 컨텐츠의 점수관리를 위한 컨텐츠의 고유이름입니다. 없을경우 해당컨텐츠는 전장 점수를 사용할수 없습니다.;
-------------------------------------------------------------------------------

MAX_Matching_SIZE = 1;

Room_Admin_Script =
{
	L"Matching_DimensionLab\\DimensionLab_Room.lua",
};

Lobby_Manager =	
{
	L"Matching_DimensionLab\\DimensionLab_Lobby.lua",
};

Matching_Script = 	
{
	L"Matching_DimensionLab\\DimensionLab_Matching.lua",
};

-- << Matching Mode ID Lists >>
-- 0. 스크립트를 읽어 매칭을 하는 모드 ( 될 수 있으면 사용하지 말자 / 테스트 용도로만 사용하길 권한다 );
-- 1. 한정된 그룹들을 참여시켜 이 그룹들을 이용해 팀을 나누는 매칭, 계속해서 이루어진다 ( 현재 사용중인 인던 : CTF, PVE );
-- 2. 대기열의 그룹들을 모두 참여시켜 바로 시작하는 매칭, 매칭은 한번만 이루어진다 ( 현재 사용중인 인던 : CDM );
Matching_Mode_ID = 1;