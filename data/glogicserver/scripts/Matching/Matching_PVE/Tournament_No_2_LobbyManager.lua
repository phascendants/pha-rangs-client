--
-- 주의) 
-- 1) 대/소 문자를 확실하게 구분해야 합니다.
-- 2) true, false 를 확실하게 구분해야 합니다.
-- 3) UCS-2 Little Endian 로 저장해야 합니다.
-------------------------------------------------------------------------------

-- 지원하는 함수 목록 입니다.
--// 한명에게 알리기.
--SendToClientMessageBox(메세지 내용 ,유저번호 );
--
--// 모두에게 알리기
-- SendToClientMessageBoxALL(메세지 내용);
--
--//토너먼트 시작
-- TournamentStart();
--
--// 토너먼트 현제 인원
--숫자결과 GetCountTournamentPeople();
--
--// 유저 돈 얼마나됨?
--숫자결과 GetGameMoney(유저번호);
--
--// 유저 캐쉬는 얼마나 남았음?
--숫자결과 GetCashMoney(유저번호);
--
--// 유저 돈을 써버리자.
-- SpendingGameMoney(long long price);
--
--// 토너먼트 큐에 박아버리자.
-- SetMatchingQueue(DWORD Playerdbnum);
--
--// 유저 레벨 요청
--숫자결과 GetLevel(유저번호);
--
--// 유저 랭킹 포인트 요청
--숫자결과 GetRankPoint(유저번호);
--
--// 유저 기여도 점수 요청
--숫자결과 GetContribution(유저번호);
--
--// 유저 학교 정보 요청
--숫자결과 GetSchool(유저번호);
--
--// 학원별 인원 정보 요청
--숫자결과 GetSchoolParticipation();
--
--// 유저 파티 있음?
--참/거짓 HasParty(유저번호);
--
--// 유저가 파티 장임?
--참/거짓 IsPartyMaster(유저번호);
--
--// 파티그룹 알고싶은데 내놔
--// 1 -> 유저번호, 2-> 유저번호 ....
-- 테이블  GetPartyGroup(lua_State* pState);

--
---  
-------------------- 커스텀이 가능한 토너먼트 경기 입니다. ------------------
--								최대한 모든 함수나 변수를 활용하게 작성했습니다.
---       ----------------------------------------------------Written by thkwon
--

-- 일(1), 월(2), 화(3), 수(4), 목(5), 금(6), 토(7)
--일요일 18:00 시작 {요일,시,분}
Battle_Date = {{1, 18 }};

--토너먼트 진행시간 시간(분)
Battle_Time = 3600; 			

-- CTF 설정파일 연결
File = L"Tournament_CTF_1_kr.lua";

-- 최소 입장 가능 레벨
Required_Level_Min = 0;		

-- 최대 입장 가능 레벨
Required_Level_Max = 999;		

-- 입장 가능한 파티 최소  크기
Required_Capacity_MIN = 6;

-- 입장 가능한 파티 최대  크기
Required_Capacity_MAX = 32;		

-- 토너먼트 시작하기위한 최소 크기
Required_Tournament_Capacity_Min = 8;

-- 토너먼트 시작하기 위한 최대 크기
Required_Tournament_Capacity_Max = 32;


-- 승자 예상 머니 타입 (게임머니, 란 포인트)
--0: 게임머니, 1: 란포인트
Winner_Bet_Money_Type = 0; 		

-- 승자 예상 금액 처리
-- 승자 예상 금액 범위 
--   사용자는 승자 예상 금액을 정해진 범위(최소 ~ 최대)내에서 입력할 수 있다.

-- 최소 승자 예상 금액 
Winner_Bet_Money_Min = 100000;

-- 최대 승자 예상 금액 
Winner_Bet_Money_Min = 10000000;

-- 시스템 수수료 
-- 시스템은 사용자의 승자 예상 금액 중 일정액을 수수료로 회수한다..
-- 사용자가 건 금액의 10%를 수수료로 회수
Winner_forecast_fee = 0.1; 		

-- 결과후 휴식 시간
RoundWaitTime = 120; -- 휴식시간 설정

-- 참가자 이상 인원 받기 ( 상시 토너먼트일경우 매칭하기위하여 만들어 놓음 )
TournamentAnyTime = false;


-- 파티 조건에 따른 입장 옵션 ON
Use_Condition_Party_Status = true;

-- 클럽 상태에 대한 입장 옵션 ON
Use_Condition_Club_Status = true;

-- 특정학원 입장 옵션  ON
Use_Condition_SpacificSchool = true;

-- 학원별 입장 가능 인원수 제한 ON
Use_Condition_School_Count_Litmit = true;

-- 학원별 팀수제한. { 성문, 현암,  봉황 } 순으로 진행.
Condition_School_litmit = {10,10,10}

-- 학원별 최대입장 인원 제한
SpacificSchool = 1

-- 토너먼트 사용료
GameMoney_Price = 100

-- 토너먼트 케쉬 사용료
GameCash_Price = 0

-- 랭킹 최소 제한 값.
Tournament_Rank_Point_MIN = 0

-- 랭킹 최대 제한 값.
Tournament_Rank_Point_MAX = 99999

-- 기여도 최소 제한값.
Tournament_Contribution_Point_MIN = 0

-- 기여도 최대 제한 값
Tournament_Contribution_Point_MAX = 99999


--- 스크립트에서만 쓰는 변수들 ---

-- 토너먼트 시작 플레그
Tournament_isReady = false

-- 시간체크를 위하여 저장함
SavedTotalTime  = 0;

-- 몇번째의 시작 시간을 사용했는지 저장함.
SavedStartGame = -1;



-- 토너먼트 시작.
function reqTournamentStart()

	if(TournamentAnyTime == true) then 
		TournamentStart()
	end
		
	-- 최소 인원 제한.
	if( GetCountTournamentPeople() < Required_Tournament_Capacity_Min ) then 
		SendToClientMessageBoxALL(L"신청인원이 작아 토너먼트가 취소가 된것을 알려 드립니다.", GetTournamentNumber())
		return false; -- 시작못함.
	end
	
	TournamentStart()
	return true; 
end


function reqTournamentReady(fElpaseTime)


	-- 상시 메칭일경우 무조건 OK
	if(TournamentAnyTime == true)  then 
		Tournament_isReady = true
	else
		-- 토너먼트가 준비중이지 않을때.
		if(Tournament_isReady == false) then 
			-- 시작 시간 제한.
			for value in pairs( Battle_Date ) do
				-- 중복 실행되지 않게 하기위하여 설정. (같은 시간을 설정하지는 .. 않겠지요?!)
				if(SavedStartGame == value ) then 
					-- 요일 체크
					if(Battle_Date[value][0] == GetWeek())  then 
						-- 시(時) 체크
						if(Battle_Date[value][1] == GetHoure()) then 
							Tournament_isReady = true;
							SavedStartGame = value;
						end
					end
				end
			end	
		end
	end
	
	
	if(TournamentAnyTime == true) then 
		-- 허용인원보다 크거나 같으면 곧바로 시작한다.
		if(GetCountTournamentPeople() >= Required_Tournament_Capacity_Max) then 
			Tournament_isReady = false
			reqTournamentStart()
			return true;
		end
	end
	
	
	SavedTotalTime = SavedTotalTime + fElpaseTime
	
	-- 분 체크
	if(Tournament_isReady == true) then 
		if(SavedTotalTime > Battle_Time) then  -- 시간아 꽉차면 시작한다.
			Tournament_isReady = false
			reqTournamentStart()
			return true
		else
			return Tournament_isReady
		end
	end
	
	return false
end

-- 토너먼트 입장 요청을 하였다! 확인해주자!
function SetMatchingQueue(nPlayerDbNum)
	
	-- 토너먼트 게임비용 
	if(GetGameMoney(partygroup[value]) >= GameMoney_Price) then 
		SpendingGameMoney(GameMoney_Price)
	else
	--하지만 요기 올일은 없다!
		SendToClientMessageBox(L"파티원의 토너먼트 참가비용(게임머니) 모자랍니다.",nPlayerDbNum)
		return false
	end
	
	-- 토너먼트 케쉬 게임비용 
	if(GetCashMoney(partygroup[value]) >= GameCash_Price) then 
		UseGameCash(GameCash_Price)
	else
	--하지만 요기 올일은 없다!
		SendToClientMessageBox(L"파티원의 토너먼트 참가비용(란포인트) 모자랍니다.",nPlayerDbNum)
		return false
	end
		
	SendMatchingQueue(nPlayerDbNum)
end

-- 토너먼트 입장 요청을 하였다! 확인해주자!
function TournamentRequestJoin(nPlayerDbNum)

	if(IsJoinGame(nPlayerDbNum) == true) then
		SendToClientMessageBoxBox(L"이미 신청중에 있습니다.",nPlayerDbNum)
		return false;
	end
	
	if(IsJoinGameofALL(nPlayerDbNum) == true) then
		SendToClientMessageBoxBox(L"다른 토너먼트를 이미 신청중에 있습니다.",nPlayerDbNum)
		return false;
	end
	
	--신청 시간 제한
	if(reqTournamentReady(0) == false) then 
		SendToClientMessageBoxBox(L"아직 신청할수있는 시간이 아닙니다.",nPlayerDbNum)
		return false
	end
	-- 기본 캐릭터에 대한 조건 체크 
	
	-- 레벨 하행선 제한
	 if( GetLevel(nPlayerDbNum) < Required_Level_Min ) then 
		 SendToClientMessageBoxBox(L"신청자의  레벨이 낮습니다.",nPlayerDbNum)
		return false
	 end
	 
	 -- 레벨 상행선 제한
	 if( GetLevel(nPlayerDbNum) > Required_Level_Max ) then 
		 SendToClientMessageBoxBox(L"신청자의 레벨이 높습니다.",nPlayerDbNum)
		return false
	 end
	 
	 
	 -- 랭크 포인트 하행선 제한
	 if( GetRankPoint(nPlayerDbNum) < Tournament_Rank_Point_MIN ) then 
		 SendToClientMessageBoxBox(L"신청자의  랭크 포인트가 낮습니다.",nPlayerDbNum)
		return false
	 end
	 
	 -- 랭크 포인트 상행선 제한
	 if( GetRankPoint(nPlayerDbNum) > Tournament_Rank_Point_MAX ) then 
		 SendToClientMessageBoxBox(L"신청자의 랭크 포인트가 높습니다.",nPlayerDbNum)
		return false
	 end
	 
	 -- 기여도 하행선 제한
	 if( GetContribution(nPlayerDbNum) < Tournament_Contribution_Point_MIN ) then 
		 SendToClientMessageBoxBox(L"신청자의 기여도가 낮습니다.",nPlayerDbNum)
		return false
	 end
	 
	 -- 기여도 상행선 제한
	 if( GetContribution(nPlayerDbNum) > Tournament_Contribution_Point_MAX ) then 
		 SendToClientMessageBoxBox(L"신청자의 기여도가 높습니다.",nPlayerDbNum)
		return false
	 end
	 
	 
	
	-- 신청자 학교 제한
	
	if(Use_Condition_SpacificSchool == true) then 
		if( GetSchool(nPlayerDbNum) ~= SpacificSchool) then 
			SendToClientMessageBoxBox(L"신청자의 학원이 조건과 맞지 않습니다.",nPlayerDbNum)
			return false
		end
	end
	
	
	-- 학교별 인원 제한
	if(TournamentAnyTime ~= true) then 
		if(Use_Condition_School_Count_Litmit == true) then 
			--참가자 인원수 가져온다.( 학원 번호 가져오고 ) == 학교 제한 수 가져오기 (학교 번호 가져오기)
			if(GetSchoolParticipation(GetSchool(nPlayerDbNum) )+ 1  > Condition_School_litmit[GetSchool(nPlayerDbNum)]) then 
				SendToClientMessageBoxBox(L"신청자의 학원이 많이 신청하여 제한에 걸렸습니다.",nPlayerDbNum)
				return false
			end
		end
	end
	
	-- 토너먼트 게임비용 
	if(GetGameMoney(nPlayerDbNum) < GameMoney_Price) then 
		SendToClientMessageBox(L"토너먼트 참가비용(게임머니) 모자랍니다.",nPlayerDbNum)
		return false
	end
	
	-- 토너먼트 케쉬 게임비용 
	if(GetCashMoney(nPlayerDbNum) < GameCash_Price) then 
		SendToClientMessageBox(L"토너먼트 참가비용(란포인트) 모자랍니다.",nPlayerDbNum)
		return false
	end
	
	if(TournamentAnyTime ~= true) then 
		 -- 토너먼트 최대 인원 제한.
		 if( GetCountTournamentPeople() + 1 > Required_Tournament_Capacity_Max ) then 
			 SendToClientMessageBoxBox(L"토너먼트 참가인원 가득 차서 참여 하실수 없습니다.",nPlayerDbNum)
			 return false
		 end
	end
	
	
	
	
	 -- 파티 제한
	if(Use_Condition_Party_Status == true) then 
		if( HasParty(nPlayerDbNum) ~= true) then  -- 파티가 없다면.
			if(Required_Capacity_MIN == 1 or Required_Capacity_MAX == 1) then  -- 참가 제한이 1인경우 파티  없어도 됨
				SetMatchingQueue(nPlayerDbNum) 
				EndTournamentQueue();
				return true
			else
				SendToClientMessageBox(L"파티가 없으면 참여가 불가능합니다.",nPlayerDbNum)
				return false
			end
		elseif(IsPartyMaster(nPlayerDbNum) == false) then   -- 파티 장인지 체크
			SendToClientMessageBox(L"파티장만이 토너먼트 참가 신청이 가능합니다.",nPlayerDbNum)
			return false
		end
	else
		if( HasParty(nPlayerDbNum) == true) then  -- 파티가 있다면
			SendToClientMessageBox(L"파티탈퇴후 참가 하셔야 합니다.",nPlayerDbNum)
			return false
		else
			SetMatchingQueue(nPlayerDbNum) -- 참가 가능
			EndTournamentQueue();
			return true
		end
	end


	 -- 파티 인원 정보를 테이블로 가져 옵니다.
	 local partygroup = GetParty(nPlayerDbNum)
	 
	 -- 파티 사이즈 알아온다.
	 local size = table.getn(partygroup)
	 
	-- 파티 수 인원 제한. 
	if( size < Required_Capacity_MIN ) then 
		SendToClientMessageBoxBox(L"신청자의 파티 인원이 작습니다.",nPlayerDbNum)
		return false
	elseif( size < Required_Capacity_MAX ) then 
		SendToClientMessageBoxBox(L"신청자의 파티 인원이 많습니다.",nPlayerDbNum)
		return false
	end
	
	 -- 파티원에 대한 모든 입장조건을 체크한다.
	 for value in pairs( partygroup ) do
	 	
		-- 랭크 포인트 하행선 제한
		 if( GetRankPoint(partygroup[value]) < Tournament_Rank_Point_MIN ) then 
			 SendToClientMessageBoxBox(L"파티원의  랭크 포인트가 낮습니다.",nPlayerDbNum)
			return false
		 end
		 
		 -- 랭크 포인트 상행선 제한
		 if( GetRankPoint(partygroup[value]) > Tournament_Rank_Point_MAX ) then 
			 SendToClientMessageBoxBox(L"파티원의 랭크 포인트가 높습니다.",nPlayerDbNum)
			return false
		 end
		 
		 -- 기여도 하행선 제한
		 if( GetContribution(partygroup[value])< Tournament_Contribution_Point_MIN ) then 
			 SendToClientMessageBoxBox(L"파티원의 기여도가 낮습니다.",nPlayerDbNum)
			return false
		 end
		 
		 -- 기여도 상행선 제한
		 if( GetContribution(partygroup[value]) > Tournament_Contribution_Point_MAX ) then 
			 SendToClientMessageBoxBox(L"파티원의 기여도가 높습니다.",nPlayerDbNum)
			return false
		 end


		 -- 레벨 하행선 제한
		 if( GetLevel(partygroup[value]) < Required_Level_Min ) then 
			 SendToClientMessageBoxBox(L"파티원중 한명이 레벨이 낮습니다.",nPlayerDbNum)
			return false
		 end
		 
		 -- 레벨 상행선 제한
		 if( GetLevel(partygroup[value]) > Required_Level_Max ) then 
			 SendToClientMessageBoxBox(L"파티원중 한명이 레벨이 높습니다.",nPlayerDbNum)
			return false
		 end
		 
		 -- 특정 학교 조건 확인
		 if(Use_Condition_SpacificSchool == true) then 
			if( GetSchool(partygroup[value]) ~= SpacificSchool) then 
				SendToClientMessageBoxBox(L"파티원의 학원이 조건과 맞지 않습니다.",nPlayerDbNum)
				return false
			end
		end
		
		-- 토너먼트 게임비용 
		if(GetGameMoney(partygroup[value]) < GameMoney_Price) then 
			SendToClientMessageBox(L"파티원의 토너먼트 참가비용(게임머니) 모자랍니다.",nPlayerDbNum)
			return false
		end
		
		-- 토너먼트 케쉬 게임비용 
		if(GetCashMoney(partygroup[value]) < GameCash_Price) then 
			SendToClientMessageBox(L"파티원의 토너먼트 참가비용(란포인트) 모자랍니다.",nPlayerDbNum)
			return false
		end
		
		if(Use_Condition_School_Count_Litmit == true) then 
			if(GetSchool(nPlayerDbNum) ~=GetSchool(partygroup[value])) then 
				SendToClientMessageBox(L"학원별 제한 설정으로 인하여 파티인원 모두가 학원이 같지 않으면 등록할수 없습니다.",nPlayerDbNum)
				return false
			end
		end
		
		if(Use_Condition_Club_Status == true) then 
			if(GetClubNum(nPlayerDbNum) ~= GetClubNum(partygroup[value]) ) then 
				SendToClientMessageBox(L"클럽이 같지 않다면 입장이 허용 되지 않습니다.",nPlayerDbNum)
			end
		end
	
	end
	
	

	 -- 파티 드루와 드루와
	 for value in pairs( partygroup ) do
		 SetMatchingQueue(partygroup[value])
	 end
	 EndTournamentQueue();
	return true;
end













