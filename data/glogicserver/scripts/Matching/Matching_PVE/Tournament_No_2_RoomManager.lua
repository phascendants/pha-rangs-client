


-- 맵이름
MapName = L"TOURNAMENT_PVP_CTF_MAPNAME";

-- 맵정보 ( 한칸당 한줄 xml에 여러줄 넣으면 안된다!!)
-- 정보에 따르면 굉장히 위험함.
-- 따라서 한줄 한줄 넣어주시기 바랍니다.
MapInfo = 
{ 
L"TOURNAMENT_TEST_PVP_INFO"
}
-- 맵 정보 줄수
MapInfoCount = 4;

-- 맵보상 ( 한칸당 한줄 xml에 여러줄 넣으면 안된다!!)
-- 정보에 따르면 굉장히 위험함.
-- 따라서 한줄 한줄 넣어주시기 바랍니다.
MapReward =
{
L"TOURNAMENT_TEST_PVP_REWARD",
}

-- 정보 보상 정보 줄수
MapRewardCount = 3;

-- 유저에게 알릴 토너먼트 타입입니다. 
-- 0 => 정시
-- 1 => 상시
-- 2 => 임시
TournamentType = 1;

-- 0 신청가능 : 해당 토너먼트에 신청이 가능한 시간인 경우
-- 1 신청 중  : 나의 상태가 해당 토너먼트를 신청하여 대기중인 경우
-- 2 입장대기 : 해당 토너먼트가 신청 마감 후 입장대기로 변경 
-- 3 신청불가 : 해당 토너먼트가 신청이 불가능한 상태인 경우 (일, 시)
-- 4 진행 중  : 해당 토너먼트가 현재 진행중인 경우

TournamentStatus = 3;

TournamentMapID = 14;


function CreateComplete()
	NotifyCreateComplete();
end