--------------------------------------------------------------------------------
-- ColorCode
	COLOR_BLACK				= {0,0,0};
	COLOR_WHITE				= {255,255,255};
	COLOR_RED				= {255,0,0};
	COLOR_GREEN				= {0,255,0};
	COLOR_BLUE				= {128,194,235};
--------------------------------------------------------------------------------

-- 조작 금지;
EMPOSTTYPE_WINNER = 0
EMPOSTTYPE_TOTO_WINNER = 1
EMPOSTTYPE_WINNER_POINT =2
EMPOSTTYPE_TOTO_WINNER_POINT=3

function TournamentRequestJoin( ClietDBID )

	-- 현재 인던시스템에서 채널이동이 어렵다 그래서 채널 0번으로 고정한다.
	-- 만약 채널을 바꾸고 싶으면 메칭스크립트중 Mapcreate를 고치시면 됩니다.
	-- In Now Enviroment, Instance System Can not Move Channel. So it Fixed by 0.
	-- If you want Changing Channel, then please do change "THE MATCHING SCRIPT in MAPCREATE".
	
	Serverlog("TournamentRequestJoin:" .. ClietDBID  );
	
	local severchannel = GetServerChannel(ClietDBID);
	if severchannel == 0 then
		return true;
	end
	
	Serverlog("Sever Channel = " .. severchannel );
	
	PrintChatXmlMsg(ClietDBID,{"TOURNAMENT_WARNING_JOIN_WRONG_CHANNEL", 0},COLOR_BLUE); 
	return false;
end

function TournamentPostMessage(dwDbNum, dwKang, winMoney, emPostType)

	local TitleString ;
	
	if(emPostType == EMPOSTTYPE_WINNER) then 
	
		if(dwKang > 1) then
		
			SendSystemMail( dwDbNum, { "TOURNAMENT_POST_TITLE", 0,ID2GAMEINTEXT("TOURNAMENT_PVP_SINGLE_MAPNAME") }, { "TOURNAMENT_POST_CONTENT", 0, ID2GAMEINTEXT("TOURNAMENT_PVP_SINGLE_MAPNAME"),dwKang }, winMoney);
		else
		
			SendSystemMail( dwDbNum, { "TOURNAMENT_POST_TITLE", 0,ID2GAMEINTEXT("TOURNAMENT_PVP_SINGLE_MAPNAME") }, { "TOURNAMENT_POST_CONTENT_WINNER", 0, ID2GAMEINTEXT("TOURNAMENT_PVP_SINGLE_MAPNAME")}, winMoney);
		end	

		

	elseif (emPostType == EMPOSTTYPE_TOTO_WINNER) then 
		
		SendSystemMail( dwDbNum, { "TOURNAMENT_POST_TITLE_BETTING", 0,ID2GAMEINTEXT("TOURNAMENT_PVP_SINGLE_MAPNAME") }, { "TOURNAMENT_POST_CONTENT_BETTING_WINNER", 0,ID2GAMEINTEXT("TOURNAMENT_PVP_SINGLE_MAPNAME") }, winMoney); -- 보상을 
		
	end
	PrintChatMsg(dwDbNum, {"TOURNAMENT_POST_CHAT_NOTY", 0,ID2GAMEINTEXT("TOURNAMENT_PVP_SINGLE_MAPNAME") }, COLOR_RED);
	
end