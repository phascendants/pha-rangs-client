--
-- 주의) 
-- 1) 대/소 문자를 확실하게 구분해야 합니다.
-- 2) true, false 를 확실하게 구분해야 합니다.
-- 3) UCS-2 Little Endian 로 저장해야 합니다.
-- 제작자 Thkwon - GTPK
-- 제작한 사람이 제작함. 2014 1 10
-- 넘어온 그룹크기대로 바로 토너먼트 만들어주기.
-------------------------------------------------------------------------------
-- ColorCode
	COLOR_BLACK				= {0,0,0};
	COLOR_WHITE				= {255,255,255};
	COLOR_RED				= {255,0,0};
	COLOR_GREEN				= {0,255,0};
	COLOR_BLUE				= {128,194,235};
--------------------------------------------------------------------------------

EMMATCHING_CONTENTS_OPENED =0         --// - 신청가능: 해당 토너먼트에 신청이 가능한 시간인 경우;
EMMATCHING_CONTENTS_CLOSED = 1         --// - 신청불가: 해당 토너먼트가 신청이 불가능한 상태인 경우;
EMMATCHING_CONTENTS_STANDBY_JOIN = 2   --// - 입장대기: 해당 토너먼트가 신청 마감 후 입장대기로 변경;
EMMATCHING_CONTENTS_IN_PROGRESS = 3    --// - 진행 중: 해당 토너먼트가 현재 진행중인 경우;
EMMATCHING_CONTENTS_UNKWON = 4			--// - 알수없음: 초기화가 안됨.
	
	
ANSWER_OK = 0				--// 대답함
ANSWER_CANSEL = 1		--// 싫다고함
ANSWER_NO_RESPONS = 2 --// 왠지모르겠지만 대답을 안함

-------------------------------------------------------------------------------

BettingDuration = 0;  --// 분 단위로 베팅시간을 입력한다.

-------------------------------------------------------------------------------

Matchingtable = { };
MatchingCount = 0;
MatchingResualt = { };
MapRequestResualt = { };
IndexOfMatchingStart = 0;
IndexOfMatchingWiner = 0;
TournamentNodeCount = 0;
MatchingDashboardTable = {};
RoundCount = 1;
readyforRestart = false;


--1. 개인대항전만 오픈 (베팅 off, 관전 on)
--2. 195이상 참여가능
--3. 상금설정 (구간별 설정)
--   우승 - 5억
--  준우승 - 1억
-- 4강진출 - 1천만 (= 4강 탈락을 의미함)
-- 8강진출 - 3백만
-- 16강진출 - 2백만
-- 32강진출 - 1백만
--64강진출 - 5십만
--128강진출 - 2십만
--4. 매일 2회 (19:00, 22:30분 시작) 
-- - 시작 30분전부터 10분전까지 10분간격으로 시작 알림
-- - 시작 10분부터 1분간격으로 시작알림.
--

RewardInfo = 
{
	500000000,
	100000000,
	10000000,
	3000000,
	2000000,
	1000000,
	500000,
	200000,
}

-- MID, SID , 갯수
RewardItem = 
{
	
}


m_fElpaseTime = 0.0;
function OnLuaFrameMove(fElpaseTime)
	
	
	if readyforRestart == true then
		m_fElpaseTime = m_fElpaseTime + fElpaseTime;
		if(m_fElpaseTime > 40) then
			
			StartMatching();
			m_fElpaseTime = 0;
		end
	else
		m_fElpaseTime = 0;
	end
	return;	
end

-- 예를들어 8팀이면 8강 4강 결승 총 3번
function GetSizeofTournament( value )
	--local m_log = (math.log(value) / math.log(2));
	--resualt =  (1-math.pow(2,m_log-1))/(1- m_log-1);
	if value <= 2 then
		return 0
	elseif value <= 4 then
		return 1
	elseif value <= 8 then
		return 2
	elseif value <= 16 then
		return 3
	elseif value <= 32 then
		return 4
	elseif value <= 64 then
		return 5
	elseif value <= 128 then
		return 6
	elseif value <= 256 then
		return 7
	elseif value <= 512 then
		return 8
	elseif value <= 1024 then
		return 9
	elseif value <= 2048 then
		return 10
	end	
	
	return 11;
end

function shuffled(tab)
	local n, order, res = #tab, {}, {}

	for i=1,n do order[i] = { rnd = math.random(), idx = i } end
	table.sort(order, function(a,b) return a.rnd < b.rnd end)
	for i=1,n do res[i] = tab[order[i].idx] end
	return res
end 

-- 예를들어 3팀이 참가하면 4개로 만들어서 토너먼트 만듦
function GetSizeOfTotalNodeCount(value)
	local powcount = 2;
	local powcountresualt = 2;
	local beforcount = 2;
	for i = 0, 10, 1 do
		if(powcountresualt >= value)then
			return powcountresualt;
		else
			beforcount = powcountresualt;
			powcountresualt = powcountresualt* powcount;
		end
	end
	return 1024;
end

-- 최종승자에게 이 함수를 사용한다.
function SetFinialWinner(WinnerID)

	if IsValidGroup(WinnerID) == false then
		Serverlog("Winner Disapear :: ".. WinnerID  );
	end
			
	
	local GroupTable = GetGroup(WinnerID);
	for key, Userdbid in pairs( GroupTable ) do
		PrintChatXmlMsg_Broad({"TOURNAMENT_MATCHING_SYSTEM_MESSAGE", 8,ID2GAMEINTEXT("TOURNAMENT_PVP_SINGLE_MAPNAME"),GetName(key)},COLOR_BLUE);
		PrintChatXmlMsg_Broad({"TOURNAMENT_MATCHING_SYSTEM_MESSAGE", 11,GetName(key)},COLOR_GREEN);
		
		PrintSimpleXmlMsg_Broad(10, {"TOURNAMENT_MATCHING_SYSTEM_MESSAGE", 8,ID2GAMEINTEXT("TOURNAMENT_PVP_SINGLE_MAPNAME"),GetName(key)},COLOR_WHITE);
		break;
	end
    
	SetTournamentRound(RoundCount);
	SetTournamentPlayers(Matchingtable);
	SetFinalWinner(WinnerID);
	
	for key,value in pairs( Matchingtable ) do
		FinalWinner = value;
		FinishGroup(value);
	end
	WaitTime(60);
	
	SetTournamentStatus(EMMATCHING_CONTENTS_CLOSED);
	FinishGroupALL();

	DoDestroy();
	
	return 
end

function MatchingRealStart()
	SetTournamentStatus(EMMATCHING_CONTENTS_IN_PROGRESS);
	
	-- 나갔는지 체크
	for i = IndexOfMatchingStart,  TournamentNodeCount , 1 do
		if Matchingtable[i] ~= -1 then
			if IsValidGroup(Matchingtable[i]) == false then
				Matchingtable[i] = -1
			end
		end
	end
	
	-- 필요한 맵갯수 알아오기
	local MapSize = 0;
	for i =  IndexOfMatchingStart , TournamentNodeCount , 2 do
		if Matchingtable[i] ~= -1 and  Matchingtable[i+1] ~= -1 then
			MapSize = MapSize + 1;
		end
	end
	local MapID = GetSNATIVEID(24,0);
	MapRequestResualt =  MapMultyCreate(0,MapSize,MapID);
	
	-- 맵 키와 아이디 위치 바꿔줌
	local MapList = {};
	for key in pairs( MapRequestResualt ) do
		for key,value in pairs( MapRequestResualt ) do
			MapList[value] = key;
		end
	end
	MapRequestResualt = {};
	MapRequestResualt =MapList;
	local Indexing = 0;
	local MapIndexing = 0;
	
	
	for i =  IndexOfMatchingStart , TournamentNodeCount , 2 do
		-- -1 이면 기권패 한놈임
		if Matchingtable[i] ~= -1 and  Matchingtable[i+1] ~= -1 then
			-- 모두 있다. 
			StartMachedGame(i,i+1,TournamentNodeCount,MapRequestResualt[MapIndexing],Indexing);
			MapIndexing = MapIndexing +1
		else
			-- 누가 나갔다.
			StartMachedGame(i,i+1,TournamentNodeCount,-1,Indexing);
			
		end
		
		Indexing = Indexing+1;
	end
	
	
	local PlusCount = TournamentNodeCount - IndexOfMatchingStart
	IndexOfMatchingStart = TournamentNodeCount+ 1;
	TournamentNodeCount = TournamentNodeCount + math.floor( PlusCount/2 );

	if ( MapSize  == 0) then
		readyforRestart = true;
	end
	
end




-- 메칭 실패하면 자동으로 다시 큐에 넣어줌
function OnStartMatching( )

	-- 맵 ID 초기화
	

	Serverlog("OnStartMatching");
	
	if readyforRestart == true then
	
		--
		MatchingDashboardTable = {};
		
		TournamentSize = TournamentSize - 1;
		RoundCount = RoundCount + 1;
		
		readyforRestart = false;
		
		m_fElpaseTime = 0;

		if(TournamentSize ==  0)then
			local FinalWinner = 0
			for key,value in pairs( Matchingtable ) do
				FinalWinner = value;
			end
			SetFinialWinner(FinalWinner);
			return true;
		end
		TournamentNodeCount = TournamentNodeCount + 1;
		
	else
		local WaitingQueuSize = GetMatchingWaitingGroupCount();
		
		if WaitingQueuSize == 0 then
			return false;
		end
		
		SetTournamentPlayersReset();
	
		-- Lua는 1부터 시작입니다.
		IndexOfMatchingStart = 1;
		-- 지금 신청한 사람 숫자를 세어 보자
		local WaitingQueuSize = GetMatchingWaitingGroupCount();
		
		-- 총몇번의 토너먼트가 필요한지 알림
		TournamentSize = GetSizeofTournament(WaitingQueuSize);
		
		-- 내림으로 소수점 날려버림
		TournamentSize = math.floor(TournamentSize)+1
		
		-- 토너먼트 총 참여 팀수 (부전승 포함)
		TournamentNodeCount = GetSizeOfTotalNodeCount(WaitingQueuSize);
		
		-- 토너먼트 시작시 참가인원 및 경기수 표시
		TournamentStartLog(WaitingQueuSize, TournamentSize, TournamentNodeCount);
		
		Matchingtable = {}
		-- 출전자 목록. 
		for i = 1, TournamentNodeCount, 1 do
			local GroupID=	GetGroupToMatching();
			table.insert(Matchingtable,GroupID);
		end
		
		Matchingtable = shuffled(Matchingtable);
		
		local JoCounting = 1;
		local IndexCount = 0;
		for key,value in pairs( Matchingtable ) do
			if value ~= -1 then
				SendToClientMessageXMLGROUP(value, {"TOURNAMENT_MATCHING_SYSTEM_MESSAGE", 5,TournamentNodeCount,JoCounting}, COLOR_BLUE);
			end
			IndexCount = IndexCount + 1;
			if(IndexCount%8 == 0) then
				JoCounting = JoCounting + 1;
			end
		end
		
		-- 토너먼트 크기
		SetTournamentSize(TournamentNodeCount);
		
		-- 시작은 항상 1라운드
		RoundCount = 1;
	end
	
	SetTournamentRound(RoundCount);
	SetTournamentPlayers(Matchingtable);
	
	-- 상태를 입장 대기로 변경
	SetTournamentStatus(EMMATCHING_CONTENTS_STANDBY_JOIN);
	
	if RoundCount == 1 then
		
		
		--30초 기다려 주자.
		
		if BettingDuration ~= 0  then
			PrintSimpleXmlMsg_Broad(10, {"TOURNAMENT_SYSTEM_BROAD", 1}, COLOR_WHITE);
			PrintChatXmlMsg_Broad({"TOURNAMENT_SYSTEM_BROAD", 1}, COLOR_BLUE);
			SetCanBetting(true);
			WaitTime(60*BettingDuration);
			PrintSimpleXmlMsg_Broad(10, {"TOURNAMENT_SYSTEM_BROAD", 2}, COLOR_WHITE);
			PrintChatXmlMsg_Broad({"TOURNAMENT_SYSTEM_BROAD", 2}, COLOR_BLUE);
			SetCanBetting(false);
		end
		
		PrintSimpleXmlMsg_Broad(10, {"TOURNAMENT_SYSTEM_BROAD", 0, 1,ID2GAMEINTEXT("TOURNAMENT_PVP_SINGLE_MAPNAME")}, COLOR_WHITE);
		PrintChatXmlMsg_Broad({"TOURNAMENT_SYSTEM_BROAD", 0, 1,ID2GAMEINTEXT("TOURNAMENT_PVP_SINGLE_MAPNAME")}, COLOR_BLUE);
							

		PrintSimpleXmlMsg_Broad(10, {"TOURNAMENT_MATCHING_SYSTEM_MESSAGE", 3,ID2GAMEINTEXT("TOURNAMENT_PVP_SINGLE_MAPNAME")},COLOR_WHITE);
		PrintChatXmlMsg_Broad({"TOURNAMENT_MATCHING_SYSTEM_MESSAGE", 3,ID2GAMEINTEXT("TOURNAMENT_PVP_SINGLE_MAPNAME")},COLOR_BLUE);
	end	

	MatchingRealStart();

end

function CreateMatchingTableNode(UserFactionStart,UserFactionEnd,TournamentEndIndex,InstanceID,Indexing)

	if( InstanceID == nil)then
		InstanceID = -1
	end
	
	local TempNode = 
	{ 
		faction = { } ,
		IsfinishedGame = false,
		MapInstanceID  = InstanceID,
		matchingCount = TournamentEndIndex + Indexing +1,
		LeagueCount = TournamentSize;
	}
	TempNode.faction[0] = Matchingtable[UserFactionStart];
	TempNode.faction[1] = Matchingtable[UserFactionEnd];
	
	return TempNode;
	
end

-- 본격적 리소스는 모두 요청 완료 했다.
-- 시작하자.
function StartMachedGame(UserFactionStart,UserFactionEnd,TournamentEndIndex , InstanceID,Indexing)
	
	if( InstanceID == nil)then
		--Serverlog("StartMachedGame : Nil");
	end
	
	
	-- 토너먼트 대진표 노드 만들어줌
	local Node = CreateMatchingTableNode(UserFactionStart,UserFactionEnd,TournamentEndIndex,InstanceID,Indexing);
	table.insert(MatchingDashboardTable,Node);
	
	if( InstanceID == -1) then
		--적어도 한녀석이 기권했다.
		--아닌쪽을 이긴걸로 처리
		if Matchingtable[UserFactionStart] == -1 then
			WinTournamentGame(Matchingtable[UserFactionEnd] , true)
			return true;
		else -- 둘다 나간경우 걍 둘중 아무나.
			WinTournamentGame(Matchingtable[UserFactionStart] , true)
			return true;		
		end
	end

-- 편가르고
	SendFaction(Node.faction[0], 0  , InstanceID);
	SendFaction(Node.faction[1], 1 , InstanceID );
--  방에 입장 시킨다.
	SendJoin(Node.faction[0], InstanceID);
	SendJoin(Node.faction[1], InstanceID);
	return true;
end

--240/50 1등 ~ 부터 (순서대로)
--240/59 10등 
--넣어주고욤 
--게임머니도 같이 줄 수 있으면 테스트 용도로 100만(1등) ~ 10만(10등) 

function WinTournamentGame(GroupID, ForceWin)

	local GroupTable = GetGroup(GroupID);
	
	
	-- 메치를 찾는다. 그리곤 우승한 녀셕이 있어야 할곳으로 집어 넣는다.
	for matchingNode in pairs( MatchingDashboardTable ) do
		if MatchingDashboardTable[matchingNode].faction[0] == GroupID or MatchingDashboardTable[matchingNode].faction[1] == GroupID  then
			if MatchingDashboardTable[matchingNode].IsfinishedGame  == false then
            
				Matchingtable[MatchingDashboardTable[matchingNode].matchingCount] = GroupID;
				MatchingDashboardTable[matchingNode].IsfinishedGame = true;
				SetTournamentPlayers(Matchingtable);

                if ( TournamentSize == 1 ) then
                    local GroupTable = GetGroup(GroupID);
                    for value in pairs( GroupTable ) do
                        PrintChatXmlMsg(value, {"TOURNAMENT_MATCHING_SYSTEM_MESSAGE", 11,GetName(value)},COLOR_BLUE);
                        if RewardItem[1] ~= nil then
                            SendSystemMail( value, { "TOURNAMENT_PVP_SENDER", 0 }, { "TOURNAMENT_WIN_MAIL_MESSAGE", 0, ID2GAMEINTEXT("TOURNAMENT_PVP_SINGLE_MAPNAME") },  RewardInfo[1],RewardItem[1]); -- 보상을 메일로 보냄;
                        else
                            SendSystemMail( value, { "TOURNAMENT_PVP_SENDER", 0 }, { "TOURNAMENT_WIN_MAIL_MESSAGE", 0, ID2GAMEINTEXT("TOURNAMENT_PVP_SINGLE_MAPNAME") },  RewardInfo[1]); -- 보상을 메일로 보냄;
                        end
                    end
                else
                    if ForceWin == true then
                        for value in pairs( GroupTable ) do
                            PrintSimpleXmlMsg(Value,10,{"TOURNAMENT_MATCHING_SYSTEM_MESSAGE", 13}, COLOR_WHITE);
                            -- 부전승인 경우에는 실제로 경기를 진행하지 않으므로 경기 결과 로그를 남길수 없음.
                            -- 따라서 별도로 부전승 로그를 남김
                            TournamentRoundEndLog(value, GroupID, 1);
                        end
                        SendToClientMessageXMLGROUP(GroupID, {"TOURNAMENT_MATCHING_SYSTEM_MESSAGE", 13}, COLOR_BLUE);
                    else
                        SendToClientMessageXMLGROUP(GroupID, {"TOURNAMENT_MATCHING_SYSTEM_MESSAGE", 1}, COLOR_BLUE);
                    end
				end
			end
		end
	end
				
end


function NoticeResualt(InstanceID,FactionID,_IsWIN)
	local GroupID;
	Serverlog(string.format("%d InstanceID, %d Faction, %s _IsWIN",InstanceID,FactionID,tostring(_IsWIN)));
	if(_IsWIN == true) then 
		--이기면 이곳으로 
		for matchingNode in pairs( MatchingDashboardTable ) do
			
			if MatchingDashboardTable[matchingNode].MapInstanceID == InstanceID then
				
				if MatchingDashboardTable[matchingNode].IsfinishedGame == true then
					return true;
				end
				for key,Fid in pairs( MatchingDashboardTable[matchingNode].faction ) do
					if key == FactionID then
						--GroupID = MatchingDashboardTable[matchingNode].faction[key];
						WinTournamentGame(Fid,false);
					else
						SendToClientMessageXMLGROUP(Fid, {"TOURNAMENT_MATCHING_SYSTEM_MESSAGE", 2}, COLOR_BLUE);
						--240/50 1등 ~ 부터 (순서대로)
						--240/59 10등 
						local GroupTable = GetGroup(Fid);
						Serverlog(string.format("%d Faction",key));
						if( TournamentSize < 10) then
							if TournamentSize == 1 then
								for value in pairs( GroupTable ) do
									if RewardItem[2] ~= nil then
										SendSystemMail( value, { "TOURNAMENT_PVP_SENDER", 0 }, { "TOURNAMENT_WIN_MAIL_MESSAGE", 1, ID2GAMEINTEXT("TOURNAMENT_PVP_SINGLE_MAPNAME") },  RewardInfo[2],RewardItem[2]); -- 보상을 메일로 보냄;
									else
										SendSystemMail( value, { "TOURNAMENT_PVP_SENDER", 0 }, { "TOURNAMENT_WIN_MAIL_MESSAGE", 1, ID2GAMEINTEXT("TOURNAMENT_PVP_SINGLE_MAPNAME") },  RewardInfo[2]); -- 보상을 메일로 보냄;
									end
								end
							else
								for value in pairs( GroupTable ) do
									if RewardInfo[TournamentSize+1] ~= nil then
										if RewardItem[TournamentSize+1] ~= nil then
											SendSystemMail( value, { "TOURNAMENT_PVP_SENDER", 0 },{ "TOURNAMENT_WIN_MAIL_MESSAGE", 2,ID2GAMEINTEXT("TOURNAMENT_PVP_SINGLE_MAPNAME") ,math.pow(2,TournamentSize) },  RewardInfo[TournamentSize+1],RewardItem[TournamentSize+1]); -- 보상을 메일로 보냄;
										else
											SendSystemMail( value, { "TOURNAMENT_PVP_SENDER", 0 }, { "TOURNAMENT_WIN_MAIL_MESSAGE", 2,ID2GAMEINTEXT("TOURNAMENT_PVP_SINGLE_MAPNAME") ,math.pow(2,TournamentSize) },  RewardInfo[TournamentSize+1]); -- 보상을 메일로 보냄;
										end
									end
								end
							end
							
						end	
					end
				end
				
				return true; 
			end
		end
		
	else
		-- 토너먼트에 지면 이곳으로 돌아옴
		for matchingNode in pairs( MatchingDashboardTable ) do
			if MatchingDashboardTable[matchingNode].MapInstanceID == InstanceID then
			
				if MatchingDashboardTable[matchingNode].IsfinishedGame == true then
					return true;
				end
				
				GroupID = MatchingDashboardTable[matchingNode].faction[FactionID];
				
				SendToClientMessageXMLGROUP(GroupID, {"TOURNAMENT_MATCHING_SYSTEM_MESSAGE", 2}, COLOR_BLUE);
				for key,value in pairs( Matchingtable ) do
					if GroupID == value then
						Matchingtable[key] = -1;
					end
				end
				--DeleteGroup(GroupID)
				
				--FinishGroup(GroupID);
				return true; 
			end
		end
		
		
	end
	
	return true;
end

function OnDestroy(InstanceID, isSucsess)
	
	local count = 0;
	local MaxCount = 0;
	
	local ExsistInstance = false;
	-- 인던이 파괴 되었는데 결과가 없다? 둘다 나간경우다!
	for matchingNode in pairs( MatchingDashboardTable ) do
		
		if MatchingDashboardTable[matchingNode].MapInstanceID == InstanceID then
			----Serverlog("Tournament : matchingNode " .. MatchingDashboardTable[matchingNode].IsfinishedGame);
			if MatchingDashboardTable[matchingNode].IsfinishedGame == false then
				NoticeResualt(InstanceID,1,true);
			end
			
			ExsistInstance = true;
		end
	end
	
	if ExsistInstance == false then
		return true;
	end
	
	-- 다끝이 났는지 체크하자.
	for matchingNode in pairs( MatchingDashboardTable ) do
		if MatchingDashboardTable[matchingNode].IsfinishedGame == false then
			count = count + 1;
		end
	end
	
	if count ~= 0 then
		return true;
	end
	
	if readyforRestart == true  then
		return true;
	end
	
	readyforRestart = true;
	m_fElpaseTime = 0 ;	
	if TournamentSize == 1 then
		return true;
	end
	
	local CountLeage = math.pow(2,TournamentSize)
	PrintChatXmlMsg_Broad({"TOURNAMENT_MATCHING_SYSTEM_MESSAGE", 6,CountLeage},COLOR_BLUE);
	
	
	if TournamentSize >= 4 then 
		CountLeage = math.pow(2,TournamentSize -1 )
		PrintChatXmlMsg_Broad({"TOURNAMENT_MATCHING_SYSTEM_MESSAGE", 7,CountLeage},COLOR_WHITE);
	elseif TournamentSize == 3 then
		PrintChatXmlMsg_Broad({"TOURNAMENT_MATCHING_SYSTEM_MESSAGE", 9,ID2GAMEINTEXT("TOURNAMENT_PVP_SINGLE_MAPNAME")},COLOR_BLUE);
	elseif TournamentSize == 2 then
		PrintChatXmlMsg_Broad({"TOURNAMENT_MATCHING_SYSTEM_MESSAGE", 10,ID2GAMEINTEXT("TOURNAMENT_PVP_SINGLE_MAPNAME")},COLOR_BLUE);
	end


	
	return true;
end

function isReadyToStart()
	return false;
end