--
-- 주의) 
-- 1) 대/소 문자를 확실하게 구분해야 합니다.
-- 2) true, false 를 확실하게 구분해야 합니다.
-- 3) UCS-2 Little Endian 로 저장해야 합니다.
-- 제작자 Thkwon - GTPK
-- 제작한 사람이 제작함. 2014 1 10
-------------------------------------------------------------------------------
-- ColorCode
	COLOR_BLACK				= {0,0,0};
	COLOR_WHITE				= {255,255,255};
	COLOR_RED				= {255,0,0};
	COLOR_GREEN				= {0,255,0};
	COLOR_BLUE				= {128,194,235};
--------------------------------------------------------------------------------
-- CDM_ROOM.lua파일의 정보 참조 (실제 인던이 생성되는 시점);
ContentsDate =
{
--   요일, 시간, 분 , Duration (진행시간)
	{ 0, 19, 0, {30,20,10,9,8,7,6,5,4,3,2,1} }, -- 매일 19시 0분에 토너먼트 시작;
	{ 0, 22, 30, {30,20,10,9,8,7,6,5,4,3,2,1} }, -- 매일 22시 30분에 토너먼트 시작;	
}

--// 분 단위로 베팅시간을 입력한다.
BettingDuration = 1;

--------------------------------------------------------------------------------
OpenGameDateNum = nil;
--------------------------------------------------------------------------------
-- 최소 입장 가능 레벨
Required_Level_Min = 195;		

-- 최대 입장 가능 레벨
Required_Level_Max = 999;		

-- 입장 가능한 파티 최소  크기
Required_Capacity_MIN = 2;

-- 입장 가능한 파티 최대  크기
Required_Capacity_MAX = 8;		

-- 토너먼트 시작하기위한 최소 크기
Required_Tournament_Capacity_Min = 100;

-- 토너먼트 시작하기 위한 최대 크기
Required_Tournament_Capacity_Max = 100;



Joinsize = 0;
TOTAL_TIME= 0 ;
-- 토너먼트 시작.
function reqTournamentReady(fElpaseTime)

	local _day = GetDay();
	local _weekDay = GetWeekDay();
	local _hour = GetHour();
	local _min = GetMinute();
	
	-- 현재 진행중인 게임이 있는지 체크;
	if ( OpenGameDateNum ~= nil ) then
		local ContentOpenDate = ContentsDate[OpenGameDateNum];
		
		if ( ( ContentOpenDate[2] ~= _hour ) or ( ContentOpenDate[3] ~= _min ) )then
			OpenGameDateNum = nil;
		end
		-- 진행중인 게임이 있다면 1분 동안은 혹시모를 상황을 위해 신청자들을 계속 난입가능하도록 수정;
		--TournamentStart();
	else
		-- 현재 진행중인 게임이 없다면 열려야하는 게임이 있는지 체크;
		for dateKey, dateValue in pairs( ContentsDate ) do
			-- 해당 시간까지 남은 시간;
			local _leftHour = dateValue[2] - _hour;
			local _leftMinute = ( _leftHour * 60 ) + ( dateValue[3] - _min );
			-- 해당 요일까지 남은 일수;
			local _nDayInterval = 0;
			if ( dateValue[1] ~= 0 ) then
				_nDayInterval = dateValue[1] - _weekDay;
				if ( _nDayInterval < 0 ) then
					_nDayInterval = _nDayInterval + 7;
				elseif ( _nDayInterval == 0 ) and ( _leftMinute < 0 ) then
					_nDayInterval = 7;
				end
			elseif ( _leftMinute < 0 ) then
				_nDayInterval = 1;
			end
			_leftMinute = _leftMinute + ( _nDayInterval * 24 * 60 );
			
			-- 현재는 1일 이전에 한해서 알림;
			if ( _nDayInterval <= 1 ) then
				-- 알림이 있다면 알림 시간 체크;
				if ( dateValue[4] ~= nil ) then
					-- 최초 알림 초기화;
					if ( dateValue["currentNotify"] == nil ) then
						for NotifyKey, NotifyValue in pairs( dateValue[4] ) do
							dateValue["currentNotify"] = NotifyKey;
							if ( _leftMinute >= NotifyValue ) then
								break;
							end
						end
					end
					-- 알림 시간 체크;
					local notifyMinute = dateValue[4][dateValue["currentNotify"]];
					if ( notifyMinute ~= nil ) then
						if ( _leftMinute == notifyMinute ) then
							PrintSimpleXmlMsg_Broad(10, {"TOURNAMENT_SYSTEM_BROAD", 0, _leftMinute + BettingDuration,ID2GAMEINTEXT("TOURNAMENT_PVP_SINGLE_MAPNAME")}, COLOR_WHITE);
							PrintChatXmlMsg_Broad({"TOURNAMENT_SYSTEM_BROAD", 0, _leftMinute + BettingDuration,ID2GAMEINTEXT("TOURNAMENT_PVP_SINGLE_MAPNAME")}, COLOR_BLUE);
							dateValue["currentNotify"] = dateValue["currentNotify"] + 1;
							break;
						elseif ( _leftMinute < notifyMinute ) then
							dateValue["currentNotify"] = dateValue["currentNotify"] + 1;
						end
					end
				end
				-- 진행시간 체크;
				if ( _leftMinute == 0 ) then
					------------------------------------------------------------------
					-- 토너먼트 시작
					TournamentStart();					
					------------------------------------------------------------------
					OpenGameDateNum = dateKey;
					dateValue["currentNotify"] = nil;
					return true;
				end
			end
		end
	end
	return false;
end

-- 토너먼트 입장 요청을 하였다! 확인해주자!
function SetMatchingQueue(nPlayerDbNum)
	local resualt = false;
	resualt = SendMatchingQueue(nPlayerDbNum)
	PrintChatXmlMsg(nPlayerDbNum,{"TOURNAMENT_SYSTEM_MESSAGE", 0},COLOR_BLUE); 
	Joinsize = Joinsize + 1;
	EndTournamentQueue();	
	return true;
end

-- 토너먼트 입장 요청을 하였다! 확인해주자!
function TournamentRequestJoin(nPlayerDbNum)

	--이미 참가한경우 막힘
	if(IsJoinGame(nPlayerDbNum) == true) then
		PrintChatXmlMsg(nPlayerDbNum,{"TOURNAMENT_SYSTEM_MESSAGE", 2},COLOR_BLUE); 
		return false;
	end
	
	-- 다른게임에 참가중일때 막힘
	if ( (IsJoinGameofALL(nPlayerDbNum) == true) or (IsJoinOtherContents(nPlayerDbNum) == true) )then
		PrintChatXmlMsg(nPlayerDbNum, { "RNCDM_SYSTEM_MESSAGE", 13 },COLOR_RED); -- 5/29 xml로 교체.
		return false;
	end
	
	-- 레벨 하행선 제한
	if( GetLevel(nPlayerDbNum) < Required_Level_Min ) then 
		PrintChatXmlMsg(nPlayerDbNum,{"TOURNAMENT_SYSTEM_MESSAGE", 4},COLOR_BLUE);
		--신청자의 레벨이 낮습니다.
		return false
	end
	 
	 -- 레벨 상행선 제한
	if( GetLevel(nPlayerDbNum) > Required_Level_Max ) then 
		PrintChatXmlMsg(nPlayerDbNum,{"TOURNAMENT_SYSTEM_MESSAGE", 5},COLOR_BLUE);
		--신청자의 레벨이 높습니다.
		return false
	end
	
	-- 파티가 있다면
	if( IsParty(nPlayerDbNum) == true) then  
		PrintChatXmlMsg(nPlayerDbNum,{"TOURNAMENT_SYSTEM_MESSAGE", 13},COLOR_BLUE);
		--SendToClientMessageBox(L"파티탈퇴후 참가 하셔야 합니다.",nPlayerDbNum)
		return false
	else
	
		--for i = 1, 4, 1 do
			SetMatchingQueue(nPlayerDbNum) -- 참가 가능
		--end
		
		return true
	end
	
end

--  메칭 실패 다시 대기 해야될때 이곳으로 넘어온다.
function TournamentRequestREJoinGroup(nGroupID)
	Joinsize = Joinsize + 1;
	return true;
end


function OnDropOutChar(ClietDBID)
	MatchingJoinCansel(ClietDBID)
	return true;
end

-- 파티 변경으로 인한 취소.
function MatchingCansel(ClietDBID)
	PrintChatXmlMsg(ClietDBID,{"TOURNAMENT_SYSTEM_MESSAGE", 31},COLOR_BLUE);
	
	MatchingJoinCansel(ClietDBID);
end

function MatchingJoinCansel(ClietDBID)

	local partygroup = GetParty(ClietDBID)
	
	if partygroup == nil then
		
		local GroupIds = FindGroup(ClietDBID);
			
		if( GroupIds == -1)then
			return false;
		end
		
		local GroupTables = GetGroup(GroupIds);
		for key, Userdbid in pairs( GroupTables ) do
			PrintChatXmlMsg(Userdbid,{"TOURNAMENT_SYSTEM_MESSAGE", 30},COLOR_BLUE);
		end
		
		Joinsize = Joinsize - GetGroupSize(GroupIds);
		DeleteGroup(GroupIds)
	else
		for key, value in pairs( partygroup ) do
		   
			local GroupId = FindGroup(value);
			
			if( GroupId == -1)then
				return false;
			end
			
			local GroupTable = GetGroup(GroupId);
			
			for key, Userdbid in pairs( GroupTable ) do
				PrintChatXmlMsg(Userdbid,{"TOURNAMENT_SYSTEM_MESSAGE", 30},COLOR_BLUE);
			end
			
			Joinsize = Joinsize - GetGroupSize(GroupId);
			DeleteGroup(GroupId)
		end
	end
	
	
	
	
end










