--
-- 주의) 
-- 1) 대/소 문자를 확실하게 구분해야 합니다.
-- 2) true, false 를 확실하게 구분해야 합니다.
-- 3) UCS-2 Little Endian 로 저장해야 합니다.
-------------------------------------------------------------------------------

--사용자는 승자 예상 금액을 정해진 범위(최소 ~ 최대)내에서 입력할 수 있다.

Winner_Bet_Money_Min = 100000;    -- 최소 승자 예상 금액 

-- 최대 승자 예상 금액 
Winner_Bet_Money_Max = 10000000; -- 최대 승자 예상 금액 

-- 승자 예상 머니 타입 (게임머니, 란포인트)
Winner_Bet_Money_Type  = 0;			--0 : 게임머니, 1: 란포인트

-- 시스템 수수료 
Winner_forecast_fee = 0.1               --사용자가 건 금액의 10%를 수수료로 회수

--- 스크립트에서만 쓰는 변수들 ---

Total_Betting_Money = 0

BettingTable  = {};

function Reset()
	Total_Betting_Money = 0;
	BettingTable = nil;
end

function SetBetting(UserDBid, GroupID, Money)
	Total_Betting_Money = Total_Betting_Money+ Money;
	if ( BettingTable[GroupID] == nil ) then
		BettingTable[GroupID] = Money;
	else
		BettingTable[GroupID] = BettingTable[GroupID] + Money;
	end		
end

function CanselBetting(UserDBid, GroupID, Money)
	Total_Betting_Money = Total_Betting_Money -  Money;
	if ( BettingTable[GroupID] == nil ) then
		BettingTable[GroupID] = 0;
	else
		BettingTable[GroupID] = BettingTable[GroupID] - Money;
	end
end

--배당률 = ((총 누적금액 * (1 – 수수료)) / (내가 예상한 선수에 누적된 금액)) * 100 (%)
function GetBettingPerRatio(GroupID)

	if BettingTable[GroupID] == nil then
		return 0;
	end
		
		ServerLog("GetBettingPerRatio::Total_Betting_Money"..Total_Betting_Money);
		ServerLog("GroupID"..GroupID);
		ServerLog("BettingTable[GroupID]"..BettingTable[GroupID]);
		ServerLog("..");
		
	return ((Total_Betting_Money * (1 - Winner_forecast_fee)) / BettingTable[GroupID])*100
end

function ResualtWinner(UserDBid,GroupID,BettingCount)
	--Serverlog("ResualtWinner".. BettingCount );
	local bettingRatio = GetBettingPerRatio(GroupID);
	SendSystemMail( UserDBid, { "TOURNAMENT_PVP_SENDER", 0 }, { "TOURNAMENT_BETTING_WINNER",0 }, BettingCount*bettingRatio/100); -- 보상을 메일로 보냄;
end


