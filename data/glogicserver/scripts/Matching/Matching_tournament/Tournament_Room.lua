-- 트리 첫번째 이름
ContentsMainType = L"RNCOMPETITION_MAIN_TYPE_PVP";
-- 트리 두번째 이름
ContentsSubType = L"RNC_SUB_TYPE_TOURNAMENT";

ContentsName = L"TOURNAMENT_PVP_CTF_MAPNAME";
-- 맵목표
ContentsMissionInfo = L"TOURNAMENT_TEST_PVP_INFO";
-- 맵정보 
ContentsDetailInfo = L"TOURNAMENT_TEST_PVP_REWARD";
-- 맵보상 
ContentsRewardInfo = L"TOURNAMENT_TEST_PVP_REWARD";

-- 메칭 타입을 설정한다.
-- 0 => 일반
-- 1 => 토너먼트
ContentsType = 1;

-- 유저에게 알릴 토너먼트 타입입니다. 
-- MATCHING_TIME_TYPE
-- 0 => 정시
-- 1 => 상시
-- 2 => 이벤트
ContentsDateType = 0;

ContentsMapID = 186;

ContentsDate =
{
	
	{ 0, 1, 0, 2250 }, -- 월요일 11시에 30분 동안 진행;
	{ 0, 1, 0, 2250 }, -- 일요일 14시에 60분 동안 진행;
}

TournamentCanToTo = true;

TournamentCanObserve = true;

TournamentObserverSize = 5;

------------------------지역 변수 ---------------------------------------

ObserveTable = { 
-- 방번호 InstanceMapID
-- ReQuester {  
--      ReQuester
-- }.
 }

------------------------지역 변수 ---------------------------------------

function RequestObserve(ReQuester,GroupID, InstanceMapID)
	if(IsJoinGame(ReQuester) == true) then
		PrintChatXmlMsg(ReQuester,{"TOURNAMENT_SYSTEM_ OBSERVE", 1},COLOR_BLUE); 
		-- 토너먼트 참가중입니다. 관전중에는 관전할수 없습니다.
		return false;
	end
	
	local obserSize = 0;
	for ReQuestertable,key in pairs( ObserveTable ) do
		-- 만약 같은 인던 아이디가있으면.
		if InstanceMapID == key then
			for who in pairs( ReQuestertable ) do
				-- 들어있는 녀석 수를 세자
				obserSize = obserSize  + 1;
			end
		end
	end
	
	if obserSize >  TournamentObserverSize then
		PrintChatXmlMsg(ReQuester,{"TOURNAMENT_SYSTEM_ OBSERVE", 0, obserSize, TournamentObserverSize},COLOR_BLUE); 
		return false;
	end
	
	local obserSize = 0;
	for ReQuestertable,key in pairs( ObserveTable ) do
		-- 만약 같은 인던 아이디가있으면.
		if InstanceMapID == key then
			for who in pairs( ReQuestertable ) do
				-- 들어있는 녀석 수를 세자
				obserSize = obserSize  + 1;
			end
		end
	end
	
	if ObserveTable[InstanceMapID] == nil then
		local UserTable = { };
		table.insert(UserTable,ReQuester);
		ObserveTable[InstanceMapID] = UserTable;
	else
		table.insert(ObserveTable[InstanceMapID], ReQuester);
	end
	
	PrintChatXmlMsg(ReQuester,{"TOURNAMENT_SYSTEM_ OBSERVE", 2,ID2GAMEINTEXT("TOURNAMENT_PVP_CTF_MAPNAME")},COLOR_BLUE);
	
	return true;	
end

function DestroyComplete(InstanceMapID)
	ObserveTable[InstanceMapID] = nil;
end

function OutObserve(RequestID, InstanceMapID)

	
	local isJoined = false;

	for ReQuestertable,key in pairs( ObserveTable ) do
		-- 만약 같은 인던 아이디가있으면.
		if InstanceMapID == key then
			for who in pairs( ReQuestertable ) do
				if who == RequestID then
					isJoined = true;
					PrintChatXmlMsg(ReQuester,{"TOURNAMENT_SYSTEM_ OBSERVE", 3},COLOR_BLUE); 
				end
			end
		end
	end
	
	if isJoined == true then
		if ObserveTable[InstanceMapID] ~= nil then
			table.remove(ObserveTable[InstanceMapID], ReQuester);
		end
	end
	
	Serverlog("OutObserve" );
end





















