--
-- 주의) 
-- 1) 대/소 문자를 확실하게 구분해야 합니다.
-- 2) true, false 를 확실하게 구분해야 합니다.
-- 3) UCS-2 Little Endian 로 저장해야 합니다.
-- 제작자 Thkwon - GTPK
-- 제작한 사람이 제작함. 2014 1 10
-- 넘어온 그룹크기대로 바로 토너먼트 만들어주기.
-------------------------------------------------------------------------------

EMMATCHING_CONTENTS_OPENED =0         --// - 신청가능: 해당 토너먼트에 신청이 가능한 시간인 경우;
EMMATCHING_CONTENTS_CLOSED = 1         --// - 신청불가: 해당 토너먼트가 신청이 불가능한 상태인 경우;
EMMATCHING_CONTENTS_STANDBY_JOIN = 2   --// - 입장대기: 해당 토너먼트가 신청 마감 후 입장대기로 변경;
EMMATCHING_CONTENTS_IN_PROGRESS = 3    --// - 진행 중: 해당 토너먼트가 현재 진행중인 경우;
EMMATCHING_CONTENTS_UNKWON = 4			--// - 알수없음: 초기화가 안됨.
	
	
ANSWER_OK = 0				--// 대답함
ANSWER_CANSEL = 1		--// 싫다고함
ANSWER_NO_RESPONS = 2 --// 왠지모르겠지만 대답을 안함

-------------------------------------------------------------------------------

Matchingtable = { };
MatchingCount = 0;
MatchingResualt = { };

MapRequestResualt = { };
MapRequestSize = 0;

resualtCount = 2;

isCanStart = true;
isFinish = false;


LaterStart = false;

isGameRun = false;


TotalJoinMessageCount = 0;
TotalReadyMessageCount = 0;
AskJoinTable = {}

MapID = 0;

IndexOfMatchingStart = 0;
IndexOfMatchingWiner = 0;

TournamentNodeCount = 0;
TournamentTotalMatchingSize = 0;

MatchingDashboardTable = {};

RoundCount = 1;

readyforRestart = false;



function OnReset( )
	return true;
end

m_fElpaseTime = 0.0;
function OnLuaFrameMove(fElpaseTime)
	
	m_fElpaseTime = m_fElpaseTime + fElpaseTime;
	if readyforRestart == true then
		if(m_fElpaseTime > 10) then
			StartMatching();
			m_fElpaseTime = 0;
		end
	end
	return;	
end

-- 예를들어 8팀이면 8강 4강 결승 총 3번
function GetSizeofTournament( value )
	--local m_log = (math.log(value) / math.log(2));
	--resualt =  (1-math.pow(2,m_log-1))/(1- m_log-1);
	if value == 2 then
		return 0
	elseif value == 4 then
		return 1
	elseif value == 8 then
		return 2
	end
	return (math.log(value) / math.log(2));
end

-- 예를들어 3팀이 참가하면 4개로 만들어서 토너먼트 만듦
function GetSizeOfTotalNodeCount(value)
	local powcount = 2;
	local powcountresualt = 2;
	local beforcount = 2;
	for i = 0, 10, 1 do
		if(powcountresualt >= value)then
			return powcountresualt;
		else
			beforcount = powcountresualt;
			powcountresualt = powcountresualt* powcount;
		end
	end
	return 1024;
end

function SetFinialWinner(WinnerID)
	
	--SendToClientMessageXMLGROUP(group, {"TOURNAMENT_MATCHING_SYSTEM_MESSAGE", 0}, COLOR_BLUE);
	
	if IsValidGroup(WinnerID) == false then
		--Serverlog("Winner Disapear :: ".. WinnerID  );
	end
			
	local GroupTable = GetGroup(WinnerID);
	
	
	for value in pairs( GroupTable ) do
		PrintChatXmlMsg_Broad({"TOURNAMENT_MATCHING_SYSTEM_MESSAGE", 0,GetName(value),ID2GAMEINTEXT("TOURNAMENT_PVP_CTF_MAPNAME")},COLOR_BLUE);
		--PrintChatXmlMsg(value,{"TOURNAMENT_SYSTEM_MESSAGE", 30},COLOR_BLUE);
		--SendToClientMessageBox(L"토너먼트 신청이 취소되었습니다.",value);
	end
		
	-- N라운드
	--Serverlog("Tournament : ROUND ::" .. RoundCount);
	SetTournamentRound(RoundCount);
	SetTournamentPlayers(Matchingtable);
	
	SetFinalWinner(WinnerID);
	
	WaitTime(20);
	
	for key,value in pairs( Matchingtable ) do
	--	Serverlog("Matchingtable[" .. key .. "]=" .. value);
		FinalWinner = value;
		FinishGroup(value);
	end
		
	DoDestroy();
	
	return -- 디버그 코드
	
	--SetTournamentStatus(EMMATCHING_CONTENTS_CLOSED);
	--FinishGroup(WinnerID);
	--
end

function MatchingRealStart()


	SetTournamentStatus(EMMATCHING_CONTENTS_IN_PROGRESS);
	--SetTournamentStatus(EMMATCHING_CONTENTS_STANDBY_JOIN);
	
	--Serverlog("Matchingtable");
	
	
	----Serverlog("IndexOfMatchingStart[".. IndexOfMatchingStart .."]");
	----Serverlog("TournamentNodeCount[".. TournamentNodeCount .."]");
	
	-- 진입 여부 질의 하기 --
	for i = IndexOfMatchingStart,  TournamentNodeCount , 1 do
		if Matchingtable[i] ~= -1 then
			if IsValidGroup(Matchingtable[i]) == false then
				Matchingtable[i] = -1
			end
		end
	end
	
	local MapSize = 0;
	for i =  IndexOfMatchingStart , TournamentNodeCount , 2 do
	
		----Serverlog("Matchingtable[".. i .."]=" .. Matchingtable[i] );
		----Serverlog("Matchingtable[".. i+1 .."]=" .. Matchingtable[i+1] );
			
		if Matchingtable[i] ~= -1 and  Matchingtable[i+1] ~= -1 then
			MapSize = MapSize + 1;
		end
	end
	
	
	--Serverlog("MapRequestResualt  : MapSize ->" .. MapSize);
	
	
	MapRequestResualt =  MapMultyCreate(0,MapSize,MapID);
	
	--Serverlog("MapRequestResualt  : END");
	
	local MapList = {};
	for key in pairs( MapRequestResualt ) do
		for key,value in pairs( MapRequestResualt ) do
			
			MapList[value] = key;
		end
		
	end
	MapRequestResualt = {};
	MapRequestResualt =MapList;
	local Indexing = 0;
	local MapIndexing = 0;
	
	
	for i =  IndexOfMatchingStart , TournamentNodeCount , 2 do
		-- -1 이면 기권패 한놈임
		if Matchingtable[i] ~= -1 and  Matchingtable[i+1] ~= -1 then
			--Serverlog("MapRequestResualt  : ALLIN  ," .. i .. "/".. i+1 );
			StartMachedGame(i,i+1,TournamentNodeCount,MapRequestResualt[MapIndexing],Indexing);
			MapIndexing = MapIndexing +1
		else
			--Serverlog("MapRequestResualt  : SomeOneOut ," .. i .. "/".. i+1 );
			StartMachedGame(i,i+1,TournamentNodeCount,-1,Indexing);
			
		end
		
		Indexing = Indexing+1;
	end
	
	
	local PlusCount = TournamentNodeCount - IndexOfMatchingStart;
	
	IndexOfMatchingStart = TournamentNodeCount;
	
	IndexOfMatchingStart = IndexOfMatchingStart+ 1;
	
	TournamentNodeCount = TournamentNodeCount + math.floor( PlusCount/2 );
	
	----Serverlog("MatchingRealStartEnd")
	if ( MapSize  == 0) then
		readyforRestart = true;
	end
	
	--SetTournamentStatus(EMMATCHING_CONTENTS_IN_PROGRESS);
	
end


function shuffled(tab)
	local n, order, res = #tab, {}, {}

	for i=1,n do order[i] = { rnd = math.random(), idx = i } end
	table.sort(order, function(a,b) return a.rnd < b.rnd end)
	for i=1,n do res[i] = tab[order[i].idx] end
	return res
end 

-- 메칭 실패하면 자동으로 다시 큐에 넣어줌
function OnStartMatching( )

	isCanStart = false;
	----Serverlog("OnStartMatching");
	-- 맵 ID 초기화
	MapID = GetSNATIVEID(24,0);

	--Serverlog("OnStartMatching");
	
	if readyforRestart == true then
		----Serverlog("Tournament : OnStartReMatching");
		MatchingDashboardTable = {};
		
		TournamentSize = TournamentSize - 1;
		RoundCount = RoundCount + 1;
		
		readyforRestart = false;
		m_fElpaseTime = 0;
	
		--if(Matchingtable[TournamentNodeCount] == nil or Matchingtable[TournamentNodeCount+1] == nil )then
		

		
		--Serverlog("SetFinialWinner(Matchingtable[" .. IndexOfMatchingStart .. "]=" .. Matchingtable[IndexOfMatchingStart]);
		
		--Serverlog("Tournament : TournamentSize::" .. TournamentSize)
		if(TournamentSize ==  0)then
			local FinalWinner = 0
			for key,value in pairs( Matchingtable ) do
			--	Serverlog("F__Matchingtable[" .. key .. "]=" .. value);
				FinalWinner = value;
			end

			--Serverlog("Tournament : SetFinialWinner is " .. FinalWinner);
			SetFinialWinner(FinalWinner);
			return true;
		end
		
		
		
		--IndexOfMatchingStart = IndexOfMatchingStart+ 1;
		TournamentNodeCount = TournamentNodeCount + 1;
		--TournamentNodeCount = TournamentNodeCount +1;
		
		--Serverlog("IndexOfMatchingStart,TournamentNodeCount[".. IndexOfMatchingStart .. "/".. TournamentNodeCount .."]" );
		
		--if(IndexOfMatchingStart ==  TournamentNodeCount)then
		--	local FinalWinner = 0
		--	for key,value in pairs( Matchingtable ) do
		--		Serverlog("Matchingtable[" .. key .. "]=" .. value);
		--		FinalWinner = value;
		--	end
		--	----Serverlog("Tournament : SetFinialWinner" .. IndexOfMatchingStart);
		--	SetFinialWinner(FinalWinner);
		--	return true;
		--end
		
		
	else
		SetTournamentPlayersReset();
	
		-- Lua는 1부터 시작입니다.
		IndexOfMatchingStart = 1;
		TournamentTotalMatchingSize= 0;
		-- 지금 신청한 사람 숫자를 세어 보자
		local WaitingQueuSize = GetMatchingWaitingGroupCount();
		
		-- 총몇번의 토너먼트가 필요한지 알림
		TournamentSize = GetSizeofTournament(WaitingQueuSize);
		
		-- 내림으로 소수점 날려버림
		TournamentSize = math.floor(TournamentSize)+1
		
		-- 토너먼트 총 참여 팀수 (부전승 포함)
		TournamentNodeCount = GetSizeOfTotalNodeCount(WaitingQueuSize);
		
		Matchingtable = {}
		-- 출전자 목록. 
		for i = 1, TournamentNodeCount, 1 do
			local GroupID=	GetGroupToMatching();
			table.insert(Matchingtable,GroupID);
		end
		
		Matchingtable = shuffled(Matchingtable);
		
		--Matching Setting List --
		
		-- 이제 이미 시작해버렸으니 더이상 Start를 못하게 막는다.
		isCanStart = false;
		-- 토너먼트 크기
		SetTournamentSize(TournamentNodeCount);
		
		-- 시작은 항상 1라운드
		RoundCount = 1;
		
	end
	
	--Serverlog("Tournament : ROUND ::" .. RoundCount);
	SetTournamentRound(RoundCount);
--	SetTournamentRound(RoundCount);
	SetTournamentPlayers(Matchingtable);
	


	-- 상태를 입장 대기로 변경
	SetTournamentStatus(EMMATCHING_CONTENTS_STANDBY_JOIN);

	-- 진입 여부 질의 하기 --
	for i = IndexOfMatchingStart,  TournamentNodeCount , 1 do
		if Matchingtable[i] ~= -1 then
			if IsValidGroup(Matchingtable[i]) == false then
				Matchingtable[i] = -1
			else
				--Serverlog("SetGroupAskJoin = " .. i);
				SetGroupAskJoin(Matchingtable[i])
			end
			
		end
	end
	
	local AnswerClients = SendToGroupAskJoinAsyc(30);
	
	for Agroupid, isAnser in pairs( AnswerClients ) do
	end
	
	

	--대답 왔네 다시 체크!
	for i =  IndexOfMatchingStart , TournamentNodeCount do
		----Serverlog("Matchingtable[i]" .. i);
		if Matchingtable[i] ~= -1 then
			-- 중간에 나간놈 있으면 체크 (로비에서 자동으로 그룹없에므로 걍 지움)
			if IsValidGroup(Matchingtable[i]) == false then
				Matchingtable[i] = -1
			end
		end
	end
	MatchingRealStart();

end

function OnReadyGroupAnswer( GroupID, IsOK )


	if IsOK ~= true then
		SendToClientMessageXMLGROUP(GroupID, {"TOURNAMENT_MATCHING_SYSTEM_MESSAGE", 4}, COLOR_BLUE);
		for key,value in pairs( Matchingtable ) do
			if GroupID == value then
				Matchingtable[key] = -1;
			end
		end
		--FinishGroup(GroupID);
		--토너먼트 입장을 취소하여 기권패로처리됩니다
		--
		
		--Serverlog("ANSWER_CANSEL::" .. GroupID);
	else
		
		SendToClientMessageXMLGROUP(GroupID, {"TOURNAMENT_MATCHING_SYSTEM_MESSAGE", 3,ID2GAMEINTEXT("TOURNAMENT_PVP_CTF_MAPNAME")},COLOR_BLUE);
		--%s 경기장으로 이동합니다.
		--Serverlog("ANSWER_OK::" .. GroupID);
	end
end


function OnMapCreate(instanceMapID,bCreateComplete)

 numberiz = numberiz + 1;
 
----Serverlog("Tournament : numberiz=" .. numberiz);
	
end

function CreateMatchingTableNode(UserFactionStart,UserFactionEnd,TournamentEndIndex,InstanceID,Indexing)

	if( InstanceID == nil)then
		InstanceID = -1
	end
	
	local TempNode = 
	{ 
		faction = { } ,
		IsfinishedGame = false,
		MapInstanceID  = InstanceID,
		matchingCount = TournamentEndIndex + Indexing +1,
		LeagueCount = TournamentSize;
	}
	TempNode.faction[0] = Matchingtable[UserFactionStart];
	TempNode.faction[1] = Matchingtable[UserFactionEnd];
	----Serverlog("TempNode.matchingCount = ".. TempNode.matchingCount );
	----Serverlog(Matchingtable[UserFactionStart].." vs " .. Matchingtable[UserFactionEnd])
	return TempNode;
	
end

-- 본격적 리소스는 모두 요청 완료 했다.
-- 시작하자.
function StartMachedGame(UserFactionStart,UserFactionEnd,TournamentEndIndex , InstanceID,Indexing)
	
	if( InstanceID == nil)then
		--Serverlog("StartMachedGame : Nil");
	end
	

	local Node = CreateMatchingTableNode(UserFactionStart,UserFactionEnd,TournamentEndIndex,InstanceID,Indexing);
	table.insert(MatchingDashboardTable,Node);
	if( InstanceID == -1) then
		--적어도 한녀석이 기권했다.
		--아닌쪽을 이긴걸로 처리
		if Matchingtable[UserFactionStart] == -1 then
			WinTournamentGame(Matchingtable[UserFactionEnd] )
			return true;
		else -- 둘다 나간경우 걍 둘중 아무나.
			WinTournamentGame(Matchingtable[UserFactionStart] )
			return true;		
		end
	end
	
	----Serverlog("Tournament : StartMachedGame");

	if Node.faction[0] == nil then
		----Serverlog("Tournament : Node.faction[0] = nil");
	else 
		--Serverlog("Tournament : Node.faction[0] = " .. Node.faction[0]);
	end
	if Node.faction[1] == nil then
		--Serverlog("Tournament : Node.faction[1] = nil" );
	else 
		--Serverlog("Tournament : Node.faction[1] = " .. Node.faction[1]);
	end
-- 편가르고
	SendFaction(Node.faction[0], 0  , InstanceID);
	SendFaction(Node.faction[1], 1 , InstanceID );

	--Serverlog("Tournament : SendFaction");
--  방에 입장 시킨다.
	SendJoin(Node.faction[0], InstanceID);
	SendJoin(Node.faction[1], InstanceID);
	
	--Serverlog("Tournament : SendJoin");

		
	return true;
end

function WinTournamentGame(GroupID)

	local GroupTable = GetGroup(GroupID);
	for value in pairs( GroupTable ) do
	--	PrintChatMsg_Broad("GroupID=" .. GroupID .. "(" .. GetName(value) .. ")" ,COLOR_BLUE);
	end
	
	-- 메치를 찾는다. 그리곤 우승한 녀셕이 있어야 할곳으로 집어 넣는다.
	for matchingNode in pairs( MatchingDashboardTable ) do
		if MatchingDashboardTable[matchingNode].faction[0] == GroupID or MatchingDashboardTable[matchingNode].faction[1] == GroupID  then
			if MatchingDashboardTable[matchingNode].IsfinishedGame  == false then
				
		
				Matchingtable[MatchingDashboardTable[matchingNode].matchingCount] = GroupID;
				
				for key,value in pairs( Matchingtable ) do
				--	Serverlog("B__Matchingtable[" .. key .. "]=" .. value);
				end
				--Serverlog("MatchingDashboardTable[matchingNode].matchingCount = " .. MatchingDashboardTable[matchingNode].matchingCount );
				for key,value in pairs( Matchingtable ) do
				--	Serverlog("W__Matchingtable[" .. key .. "]=" .. value);
				end
				
				MatchingDashboardTable[matchingNode].IsfinishedGame = true;
				SendToClientMessageXMLGROUP(GroupID, {"TOURNAMENT_MATCHING_SYSTEM_MESSAGE", 1}, COLOR_BLUE);
				--Serverlog("WinTournamentGame ::".. GroupID  );
				--SendToClientMessageBoxGROUP(L"승리하셨습입니다.",GroupID);				
			end
		end
	end
				
end

function NoticeResualt(InstanceID,FactionID,_IsWIN)


	local GroupID;
	--Serverlog(string.format("%d InstanceID, %d Faction, %s _IsWIN",InstanceID,FactionID,tostring(_IsWIN)));
	if(_IsWIN == true) then 
		resualtCount  = resualtCount - 1;
		for matchingNode in pairs( MatchingDashboardTable ) do
			
			if MatchingDashboardTable[matchingNode].MapInstanceID == InstanceID then
				
				if MatchingDashboardTable[matchingNode].IsfinishedGame == true then
					return true;
				end
				
				
				
				for key,Fid in pairs( MatchingDashboardTable[matchingNode].faction ) do
					if key == FactionID then
						--GroupID = MatchingDashboardTable[matchingNode].faction[key];
						WinTournamentGame(Fid);
					else
						--local LoseID = MatchingDashboardTable[matchingNode].faction[key];
						SendToClientMessageXMLGROUP(Fid, {"TOURNAMENT_MATCHING_SYSTEM_MESSAGE", 2}, COLOR_BLUE);
						--for key,value in pairs( Matchingtable ) do
						--	if LoseID == value then
						--		Matchingtable[key] = -1;
						--	end
						--end
					end
				end
				
				return true; 
			end
		end
		
	else

		for matchingNode in pairs( MatchingDashboardTable ) do
			if MatchingDashboardTable[matchingNode].MapInstanceID == InstanceID then
			
				if MatchingDashboardTable[matchingNode].IsfinishedGame == true then
					return true;
				end
				
				GroupID = MatchingDashboardTable[matchingNode].faction[FactionID];
				
				SendToClientMessageXMLGROUP(GroupID, {"TOURNAMENT_MATCHING_SYSTEM_MESSAGE", 2}, COLOR_BLUE);
				for key,value in pairs( Matchingtable ) do
					if GroupID == value then
						Matchingtable[key] = -1;
					end
				end
				--FinishGroup(GroupID);
				return true; 
			end
		end
		
		
	end
	
	return true;
end

function IsMyMap(InstanceID)
	
	return true;
end

function ErrorCode(errorcode)
	return true;
end

function EndMessage()
	return true;
end

function RatingUp(nPlayerDbNum,_Rating)
	SetRating(nPlayerDbNum,_Rating);
end

function OnDestroy(InstanceID, isSucsess)
	
	local count = 0;
	local MaxCount = 0;
	
	-- 인던이 파괴 되었는데 결과가 없다? 둘다 나간경우다!
	for matchingNode in pairs( MatchingDashboardTable ) do
		
		if MatchingDashboardTable[matchingNode].MapInstanceID == InstanceID then
			----Serverlog("Tournament : matchingNode " .. MatchingDashboardTable[matchingNode].IsfinishedGame);
			if MatchingDashboardTable[matchingNode].IsfinishedGame == false then
				NoticeResualt(InstanceID,1,true);
			end
		end
	end
	
	-- 다끝이 났는지 체크하자.
	for matchingNode in pairs( MatchingDashboardTable ) do
		if MatchingDashboardTable[matchingNode].IsfinishedGame == false then
			count = count + 1;
		end
	end
	
	if count == 0 then
		readyforRestart = true;
	else
		----Serverlog("Tournament : Match NOT FINISHED");
	end
	
	return true;
end



function isReadyToStart()
	return isCanStart;
end