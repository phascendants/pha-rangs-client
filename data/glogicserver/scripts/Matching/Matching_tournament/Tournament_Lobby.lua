--
-- 주의) 
-- 1) 대/소 문자를 확실하게 구분해야 합니다.
-- 2) true, false 를 확실하게 구분해야 합니다.
-- 3) UCS-2 Little Endian 로 저장해야 합니다.
-- 제작자 Thkwon - GTPK
-- 제작한 사람이 제작함. 2014 1 10
-------------------------------------------------------------------------------
--------------------------------------------------------------------------------
-- ColorCode
	COLOR_WHITE				= {0,0,0};
	COLOR_BLACK				= {255,255,255};
	COLOR_RED				= {255,0,0};
	COLOR_GREEN				= {0,255,0};
	COLOR_BLUE				= {128,194,235};
--------------------------------------------------------------------------------
TournamentAnyTime = false;

-- CDM_ROOM.lua파일의 정보 참조 (실제 인던이 생성되는 시점);
ContentsDate =
{
--   날짜, 요일, 시간, 분 , Duration (진행시간)
	{ 0, 0, 20, 00, 32 }, -- 매일 9시30분에 3000분 동안 진행;
	{ 0, 0, 22, 00, 32 }, -- 매일 9시30분에 3000분 동안 진행;
}


-- 최소 입장 가능 레벨
Required_Level_Min = 0;		

-- 최대 입장 가능 레벨
Required_Level_Max = 999;		

-- 입장 가능한 파티 최소  크기
Required_Capacity_MIN = 1;

-- 입장 가능한 파티 최대  크기
Required_Capacity_MAX = 3;		

-- 토너먼트 시작하기위한 최소 크기
Required_Tournament_Capacity_Min = 10;

-- 토너먼트 시작하기 위한 최대 크기
Required_Tournament_Capacity_Max = 10;

-- 최소 입장 지연시간 (초)
Wait_NEXT_Match_Game_Second= 20;

-- 파티 조건에 따른 입장 옵션 ON
Use_Condition_Party_Status = false;

-- 클럽 상태에 대한 입장 옵션 ON
Use_Condition_Club_Status = false;

-- 토너먼트 사용료
GameMoney_Price = 0

-- 토너먼트 케쉬 사용료
GameCash_Price = 0

-- 랭킹 최소 제한 값.
Tournament_Rank_Point_MIN = 0

-- 랭킹 최대 제한 값.
Tournament_Rank_Point_MAX = 99999

-- 기여도 최소 제한값.
Tournament_Contribution_Point_MIN = 0

-- 기여도 최대 제한 값
Tournament_Contribution_Point_MAX = 99999

-----지긍은 사용중이지 않은 변수 ----

-- 학원별 입장 가능 인원수 제한 ON
Use_Condition_School_Count_Litmit = false;

-- 학원별 팀수제한. { 성문, 현암,  봉황 } 순으로 진행.
Condition_School_litmit = {10,10,10}

-- 특정학원 입장 옵션  ON
Use_Condition_SpacificSchool = false;

-- 학원별 최대입장 인원 제한
SpacificSchool = 1



Joinsize = 0;
TOTAL_TIME= 0 ;
-- 토너먼트 시작.
function reqTournamentReady(fElpaseTime)

	-- 진입 체크
	-- 현재는 인원수가 맞자마자 바로 시작하게 해놓음
	-- 추후 라이브 서버에 들어갈때는 이부분 수정 요망. (CDM이랑 비슷하게 수정하면될듯)
	if Joinsize >= Required_Tournament_Capacity_Min and  
	  Joinsize <=Required_Tournament_Capacity_Max  then
		--SendToClientMessageBoxALL(L"게임이 시작되었습니다.");
		PrintChatXmlMsg_Broad({"TOURNAMENT_SYSTEM_MESSAGE", 1},COLOR_BLUE);
		TournamentStart();
		Joinsize = 0;
		TOTAL_TIME = Wait_NEXT_Match_Game_Second;
		return true;
	else
		return false;
	end
	
--	end
	
	return false;
end

-- 토너먼트 입장 요청을 하였다! 확인해주자!
function SetMatchingQueue(nPlayerDbNum)
			
	local resualt = false;
	
	resualt = SendMatchingQueue(nPlayerDbNum)
	
	PrintChatXmlMsg(nPlayerDbNum,{"TOURNAMENT_SYSTEM_MESSAGE", 0},COLOR_BLUE); 
	--SendToClientMessageBox(L"토너먼트 참여하였습니다.",nPlayerDbNum)
	
	Joinsize = Joinsize + 1;
	
	return true;
end

-- 토너먼트 입장 요청을 하였다! 확인해주자!
function TournamentRequestJoin(nPlayerDbNum)
	
	--	if(IsJoinGame(nPlayerDbNum) == true) then
	
	if(IsJoinGame(nPlayerDbNum) == true) then
		PrintChatXmlMsg(nPlayerDbNum,{"TOURNAMENT_SYSTEM_MESSAGE", 2},COLOR_BLUE); 
		--SendToClientMessageBox(L"이미 신청중에 있습니다.",nPlayerDbNum)
		return false;
	end
	
	if(IsJoinGameofALL(nPlayerDbNum) == true) then

		PrintChatXmlMsg(nPlayerDbNum,{"TOURNAMENT_SYSTEM_MESSAGE", 3},COLOR_BLUE);
		--SendToClientMessageBox(L"토너먼트에 신청중에 있습니다.",nPlayerDbNum)
		return false;
	end
	
	--신청 시간 제한
--	if(reqTournamentReady(0) == false) then 
--		SendToClientMessageBox(L"아직 신청할수있는 시간이 아닙니다.",nPlayerDbNum)
--		return false
--	end
	-- 기본 캐릭터에 대한 조건 체크 
	
	-- 레벨 하행선 제한
	 if( GetLevel(nPlayerDbNum) < Required_Level_Min ) then 
		PrintChatXmlMsg(nPlayerDbNum,{"TOURNAMENT_SYSTEM_MESSAGE", 4},COLOR_BLUE);
		--신청자의 레벨이 낮습니다.
		return false
	 end
	 
	 -- 레벨 상행선 제한
	 if( GetLevel(nPlayerDbNum) > Required_Level_Max ) then 
		PrintChatXmlMsg(nPlayerDbNum,{"TOURNAMENT_SYSTEM_MESSAGE", 5},COLOR_BLUE);
		--신청자의 레벨이 높습니다.
		return false
	 end
	 
	 
	 -- 랭크 포인트 하행선 제한
	 if( GetRankPoint(nPlayerDbNum) < Tournament_Rank_Point_MIN ) then 
		PrintChatXmlMsg(nPlayerDbNum,{"TOURNAMENT_SYSTEM_MESSAGE", 6},COLOR_BLUE);
		--SendToClientMessageBox(L"신청자의 랭크 포인트가 낮습니다.",nPlayerDbNum)
		return false
	 end
	 
	 -- 랭크 포인트 상행선 제한
	 if( GetRankPoint(nPlayerDbNum) > Tournament_Rank_Point_MAX ) then 
		PrintChatXmlMsg(nPlayerDbNum,{"TOURNAMENT_SYSTEM_MESSAGE", 7},COLOR_BLUE);
		 SendToClientMessageBox(L"신청자의 랭크 포인트가 높습니다.",nPlayerDbNum)
		return false
	 end
	 
	 -- 기여도 하행선 제한
	 if( GetContribution(nPlayerDbNum) < Tournament_Contribution_Point_MIN ) then 
		PrintChatXmlMsg(nPlayerDbNum,{"TOURNAMENT_SYSTEM_MESSAGE", 8},COLOR_BLUE);
		--SendToClientMessageBox(L"신청자의 기여도가 낮습니다.",nPlayerDbNum)
		return false
	 end
	 
	 -- 기여도 상행선 제한
	 if( GetContribution(nPlayerDbNum) > Tournament_Contribution_Point_MAX ) then 
		PrintChatXmlMsg(nPlayerDbNum,{"TOURNAMENT_SYSTEM_MESSAGE", 9},COLOR_BLUE);
		-- SendToClientMessageBox(L"신청자의 기여도가 높습니다.",nPlayerDbNum)
		return false
	 end
	 
	 
	
	-- 신청자 학교 제한
	if(Use_Condition_SpacificSchool == true) then 
		if( GetSchool(nPlayerDbNum) ~= SpacificSchool) then 
			PrintChatXmlMsg(nPlayerDbNum,{"TOURNAMENT_SYSTEM_MESSAGE", 10},COLOR_BLUE);
			--SendToClientMessageBox(L"신청자의 학원이 조건과 맞지 않습니다.",nPlayerDbNum)
			return false
		end
	end
	
	
	-- 학교별 인원 제한
	if(TournamentAnyTime ~= true) then 
		if(Use_Condition_School_Count_Litmit == true) then 
			--참가자 인원수 가져온다.( 학원 번호 가져오고 ) == 학교 제한 수 가져오기 (학교 번호 가져오기)
			if(GetSchoolParticipation(GetSchool(nPlayerDbNum) )+ 1  > Condition_School_litmit[GetSchool(nPlayerDbNum)]) then 
				PrintChatXmlMsg(nPlayerDbNum,{"TOURNAMENT_SYSTEM_MESSAGE", 11},COLOR_BLUE);
				--SendToClientMessageBox(L"신청자의 학원이 많이 신청하여 제한에 걸렸습니다.",nPlayerDbNum)
				return false
			end
		end
	end
	
--	-- 토너먼트 게임비용 
--	if(GetGameMoney(nPlayerDbNum) < GameMoney_Price) then 
--		SendToClientMessageBox(L"토너먼트 참가비용(게임머니) 모자랍니다.",nPlayerDbNum)
--		return false
--	end
--	
--	-- 토너먼트 케쉬 게임비용 
--	if(GetCashMoney(nPlayerDbNum) < GameCash_Price) then 
--		SendToClientMessageBox(L"토너먼트 참가비용(란포인트) 모자랍니다.",nPlayerDbNum)
--		return false
--	end
	
	if(TournamentAnyTime ~= true) then 
		 -- 토너먼트 최대 인원 제한.
		 if( GetCountTournamentPeople() + 1 > Required_Tournament_Capacity_Max ) then 
			PrintChatXmlMsg(nPlayerDbNum,{"TOURNAMENT_SYSTEM_MESSAGE", 12},COLOR_BLUE);
			--토너먼트 참가인원 가득 차서 참여 하실수 없습니다.
			return false
		 end
	end
	

	if(Use_Condition_Party_Status == false) then 
		if( IsParty(nPlayerDbNum) == true) then  -- 파티가 있다면
			PrintChatXmlMsg(nPlayerDbNum,{"TOURNAMENT_SYSTEM_MESSAGE", 13},COLOR_BLUE);
			--SendToClientMessageBox(L"파티탈퇴후 참가 하셔야 합니다.",nPlayerDbNum)
			return false
		else
			SetMatchingQueue(nPlayerDbNum) -- 참가 가능
			EndTournamentQueue();		
			return true
		end
	end
	
	
	-- 이곳 밑에 부터는 무조건 파티일경우 오게 된다.
	
	
	 -- 파티 제한
	if(Use_Condition_Party_Status == true) then 
	
		-- 파티가 없다면.
		if IsParty(nPlayerDbNum) == false then  
			PrintChatXmlMsg(nPlayerDbNum,{"TOURNAMENT_SYSTEM_MESSAGE", 14},COLOR_BLUE);
			--SendToClientMessageBox(L"파티가 없으면 참여가 불가능합니다.",nPlayerDbNum)
			return false
		end
		
		-- 파티 장인지 체크
		if IsPartyMaster(nPlayerDbNum) == false then
			PrintChatXmlMsg(nPlayerDbNum,{"TOURNAMENT_SYSTEM_MESSAGE", 15},COLOR_BLUE);
			--SendToClientMessageBox(L"파티장만이 토너먼트 참가 신청이 가능합니다.",nPlayerDbNum)
			return false
		end
	end

	 -- 파티 인원 정보를 테이블로 가져 옵니다.
	 local partygroup = GetParty(nPlayerDbNum)
	 
	 local countingparty = 0;
	 
	 -- 파티 사이즈 알아온다.
	 for value in pairs( partygroup ) do
	    countingparty = countingparty + 1;
	 end
	 local size = countingparty;
	 
	 
	-- 파티 수 인원 제한. 
	if( size <= Required_Capacity_MIN ) then 
		PrintChatMsg(nPlayerDbNum,ID2GAMEINTEXT("TOURNAMENT_SYSTEM_MESSAGE",16) .. "(" .. size .. L")",COLOR_BLUE);
		--SendToClientMessageBox(L"파티 인원이 작습니다.(" .. size .. L")",nPlayerDbNum)
		return false
	elseif( size > Required_Capacity_MAX ) then 
		PrintChatMsg(nPlayerDbNum,ID2GAMEINTEXT("TOURNAMENT_SYSTEM_MESSAGE",17) .. "(" .. size .. L")",COLOR_BLUE);
		--SendToClientMessageBox(L"파티 인원이 많습니다.(" .. size .. L")",nPlayerDbNum)
		return false
	end
	
	 -- 파티원에 대한 모든 입장조건을 체크한다.
	 for value,key in pairs( partygroup ) do
	 	
		-- 랭크 포인트 하행선 제한
		
		 if( GetRankPoint(partygroup[value]) < Tournament_Rank_Point_MIN ) then 
			PrintChatXmlMsg(nPlayerDbNum,{"TOURNAMENT_SYSTEM_MESSAGE", 18},COLOR_BLUE);
			-- SendToClientMessageBox(L"파티원의  랭크 포인트가 낮습니다.",nPlayerDbNum)
			 partygroup = nil
			return false
		 end
		 
		 -- 랭크 포인트 상행선 제한
		 if( GetRankPoint(partygroup[value]) > Tournament_Rank_Point_MAX ) then 
			PrintChatXmlMsg(nPlayerDbNum,{"TOURNAMENT_SYSTEM_MESSAGE", 19},COLOR_BLUE);
			-- SendToClientMessageBox(L"파티원의 랭크 포인트가 높습니다.",nPlayerDbNum)
			 partygroup = nil
			return false
		 end
		 -- 기여도 하행선 제한
		 if( GetContribution(partygroup[value])< Tournament_Contribution_Point_MIN ) then 
			PrintChatXmlMsg(nPlayerDbNum,{"TOURNAMENT_SYSTEM_MESSAGE", 21},COLOR_BLUE);
			-- SendToClientMessageBox(L"파티원의 기여도가 낮습니다.",nPlayerDbNum)
			 partygroup = nil
			return false
		 end
		 
		 -- 기여도 상행선 제한
		 if( GetContribution(partygroup[value]) > Tournament_Contribution_Point_MAX ) then 
			PrintChatXmlMsg(nPlayerDbNum,{"TOURNAMENT_SYSTEM_MESSAGE", 22},COLOR_BLUE);
			-- SendToClientMessageBox(L"파티원의 기여도가 높습니다.",nPlayerDbNum)
			 partygroup = nil
			return false
		 end


		 -- 레벨 하행선 제한
		 if( GetLevel(value) < Required_Level_Min ) then 
			PrintChatXmlMsg(nPlayerDbNum,{"TOURNAMENT_SYSTEM_MESSAGE", 23},COLOR_BLUE);
			-- SendToClientMessageBox(L"파티원중 한명이 레벨이 낮습니다.",nPlayerDbNum)
			 partygroup = nil
			return false
		 end
		 
		 -- 레벨 상행선 제한
		 if( GetLevel(value) > Required_Level_Max ) then 
			PrintChatXmlMsg(nPlayerDbNum,{"TOURNAMENT_SYSTEM_MESSAGE", 24},COLOR_BLUE);
			--SendToClientMessageBox(L"파티원중 한명이 레벨이 높습니다.",nPlayerDbNum)
			return false
		 end
		 
		 -- 특정 학교 조건 확인
		 if(Use_Condition_SpacificSchool == true) then 
			if( GetSchool(value) ~= SpacificSchool) then 
				PrintChatXmlMsg(nPlayerDbNum,{"TOURNAMENT_SYSTEM_MESSAGE", 25},COLOR_BLUE);
				--SendToClientMessageBox(L"파티원의 학원이 조건과 맞지 않습니다.",nPlayerDbNum)
				partygroup = nil
				return false
			end
		end
		
		-- 토너먼트 게임비용 
		--if(GetGameMoney(partygroup[value]) < GameMoney_Price) then
		--	PrintChatXmlMsg(nPlayerDbNum,{"TOURNAMENT_SYSTEM_MESSAGE", 26},COLOR_BLUE);
		--	SendToClientMessageBox(L"파티원의 토너먼트 참가비용(게임머니) 모자랍니다.",nPlayerDbNum)
		--	partygroup = nil
		--	return false
		--end
		--
		---- 토너먼트 케쉬 게임비용 
		--if(GetCashMoney(partygroup[value]) < GameCash_Price) then 
		--	PrintChatXmlMsg(nPlayerDbNum,{"TOURNAMENT_SYSTEM_MESSAGE", 27},COLOR_BLUE);
		--	SendToClientMessageBox(L"파티원의 토너먼트 참가비용(란포인트) 모자랍니다.",nPlayerDbNum)
		--	partygroup = nil
		--	return false
		--end
		
		if(Use_Condition_School_Count_Litmit == true) then 
			if(GetSchool(nPlayerDbNum) ~=GetSchool(partygroup[value])) then 
				PrintChatXmlMsg(nPlayerDbNum,{"TOURNAMENT_SYSTEM_MESSAGE", 28},COLOR_BLUE);
				--SendToClientMessageBox(L"학원별 제한 설정으로 인하여 파티인원 모두가 학원이 같지 않으면 등록할수 없습니다.",nPlayerDbNum)
				partygroup = nil
				return false
			end
		end
		
		if(Use_Condition_Club_Status == true) then 
			if(GetClubNum(nPlayerDbNum) ~= GetClubNum(partygroup[value]) ) then 
				PrintChatXmlMsg(nPlayerDbNum,{"TOURNAMENT_SYSTEM_MESSAGE", 29},COLOR_BLUE);
				--SendToClientMessageBox(L"클럽이 같지 않다면 입장이 허용 되지 않습니다.",nPlayerDbNum)
				partygroup = nil;
				return false
			end
		end
	
	end
	
	
	-- Return 없이 이곳에 왔다면 신청이다
	
	for value in pairs( partygroup ) do
		SetMatchingQueue(value) -- 참가 
	end	
	EndTournamentQueue();

	
	
	reqTournamentReady(0);
end

--  메칭 실패 다시 대기 해야될때 이곳으로 넘어온다.
function TournamentRequestREJoinGroup(nGroupID)
	Joinsize = Joinsize + 1;
	return true;
end


function OnDropOutChar(ClietDBID)
	
	
	
	if(IsJoinGameofALL(ClietDBID) == true) then
		local GroupId = FindGroup(ClietDBID);
		
		
		if( GroupId == -1)then
			return false;
		end

		local GroupTable = GetGroup(GroupId);
		
		
		for value in pairs( GroupTable ) do
			PrintChatXmlMsg(value,{"TOURNAMENT_SYSTEM_MESSAGE", 30},COLOR_BLUE);
			--SendToClientMessageBox(L"토너먼트 신청이 취소되었습니다.",value);
		end
		
		Joinsize = Joinsize - GetGroupSize(GroupId);
		-- 그룹은 살리고 사람만 지운다.를 원한다면 아래의 주석 삭제
		--GroupOut(GroupId, ClietDBID);
		
		-- 그룹을 지운다.를 원한다면 아래의 주석 삭제
		DeleteGroup(GroupId); 
		
		
		
		--Joinsize = Joinsize - 1;
		
		return true;
	end
	return true;
end

function MatchingCansel()
	PrintChatXmlMsg(nPlayerDbNum,{"TOURNAMENT_SYSTEM_MESSAGE", 31},COLOR_BLUE);
	--SendToClientMessageBoxALL(L"메칭 취소 되었습니다.");
end

function MatchingJoinCansel(nPlayerDbNum)

	PrintChatXmlMsg(nPlayerDbNum,{"TOURNAMENT_SYSTEM_MESSAGE", 32},COLOR_BLUE);
	--SendToClientMessageBox(L"메칭 취소 되었습니다.",nPlayerDbNum);
	OnDropOutChar(nPlayerDbNum);

end










