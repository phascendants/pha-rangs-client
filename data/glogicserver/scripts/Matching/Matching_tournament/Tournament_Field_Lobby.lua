EMPOSTTYPE_WINNER = 0
EMPOSTTYPE_TOTO_WINNER = 1
EMPOSTTYPE_WINNER_POINT =2
EMPOSTTYPE_TOTO_WINNER_POINT=3

function TournamentRequestJoin( ClietDBID )

	-- 현재 인던시스템에서 채널이동이 어렵다 그래서 채널 0번으로 고정한다.
	-- 만약 채널을 바꾸고 싶으면 메칭스크립트중 Mapcreate를 고치시면 됩니다.
	-- In Now Enviroment, Instance System Can not Move Channel. So it Fixed by 0.
	-- If you want Changing Channel, then please do change "THE MATCHING SCRIPT in MAPCREATE".
	if GetServerChannel(ClietDBID) == 0 then
		return true;
	end
	
	PrintChatXMLMsg("TOURNAMENT_WARNING_JOIN_WRONG_CHANNEL",ClietDBID);
	return false;
end

function TournamentPostMessage(dwDbNum, dwKang, winMoney, emPostType)

	local TitleString ;
	
	if(emPostType == EMPOSTTYPE_WINNER) then 
		TitleString = string.format(ID2GAMEINTEXT("TOURNAMENT_POST_TITLE"),ID2GAMEINTEXT("TOURNAMENT_PVP_CTF_MAPNAME"));
	else if (emPostType == EMPOSTTYPE_TOTO_WINNER) then 

		TitleString = string.format(ID2GAMEINTEXT("TOURNAMENT_POST_TITLE_BETTING"),ID2GAMEINTEXT("TOURNAMENT_PVP_CTF_MAPNAME"));
		
	end
	
	
	local ContentString;
	if(emPostType == EMPOSTTYPE_WINNER) then 
	
		if(dwKang > 1) then
			ContentString = string.format(ID2GAMEINTEXT("TOURNAMENT_POST_CONTENT"),ID2GAMEINTEXT("TOURNAMENT_PVP_CTF_MAPNAME"),dwKang)
		else
			ContentString = string.format(ID2GAMEINTEXT("TOURNAMENT_POST_CONTENT_WINNER"),ID2GAMEINTEXT("TOURNAMENT_PVP_CTF_MAPNAME"))
		end		
	else if (emPostType == EMPOSTTYPE_TOTO_WINNER) then 

		ContentString = string.format(ID2GAMEINTEXT("TOURNAMENT_POST_CONTENT_BETTING_WINNER"),ID2GAMEINTEXT("TOURNAMENT_PVP_CTF_MAPNAME"))
		
	end
	
	SendSystemMail(dwDbNum,TitleString,ContentString,winMoney);
	
	local ChatString;
	ChatString = string.format(ID2GAMEINTEXT("TOURNAMENT_POST_CHAT_NOTY"),ID2GAMEINTEXT("TOURNAMENT_PVP_CTF_MAPNAME"))
	
	PrintChatMsg(dwDbNum, ChatString, COLOR_RED);
		
		
	
end