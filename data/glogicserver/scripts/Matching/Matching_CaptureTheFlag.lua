--
-- 주의) 
-- 1) 대/소 문자를 확실하게 구분해야 합니다.
-- 2) true, false 를 확실하게 구분해야 합니다.
-- 3) UCS-2 Little Endian 로 저장해야 합니다.
-- Matching_Content_Type는 는 해당 컨텐츠의 점수관리를 위한 컨텐츠의 고유이름입니다. 없을경우 해당컨텐츠는 전장 점수를 사용할수 없습니다.;
-------------------------------------------------------------------------------

Matching_Content_Type = "CaptureTheFlag";

MAX_Matching_SIZE = 10;

Room_Admin_Script =
{
	L"Matching_CaptureTheFlag\\CaptureTheFlag_Room.lua",
};

Lobby_Manager =	
{
	L"Matching_CaptureTheFlag\\CaptureTheFlag_Lobby.lua",
};

Matching_Script = 	
{
	L"Matching_CaptureTheFlag\\CaptureTheFlag_Matching.lua",
};
