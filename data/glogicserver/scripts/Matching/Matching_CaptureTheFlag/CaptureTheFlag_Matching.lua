----- 깃발전용 매칭 스크립트
----- 레이팅에 따라 인원들을 매칭시키고 매칭된 인원들을 인던생성후 입장시키는 역활;

----- 레이팅 테이블
RatingTable = {};
RatingTable[1] = 999;  -- 0    ~ 999   초보자;
RatingTable[2] = 1249; -- 1000 ~ 1249  하수;
RatingTable[3] = 1499; -- 1250 ~ 1499  중수;
RatingTable[4] = 1749; -- 1500 ~ 1749  고수;
RatingTable[5] = 1999; -- 1750 ~ 1999  초고수;
RatingTable[6] = 2249; -- 2000 ~ 2249  지존;
RatingTable[7] = 9999; -- 2250 ~ 9999  초월자;

RatingeGroupTable = {};
RatingeGroupTable[1] = {};
RatingeGroupTable[2] = {};
RatingeGroupTable[3] = {};
RatingeGroupTable[4] = {};
RatingeGroupTable[5] = {};
RatingeGroupTable[6] = {};
RatingeGroupTable[7] = {};

GroupTable = {};
GroupMemberTable = {};
bCanStart = true;
bCanDestroy = true;

function OnReset( )
	GroupTable = {};
	bCanStart = true;
	bCanDestroy = true;
end

bOk = false;
function OnStartMatching()
	if ( bOk == false ) then
		Serverlog("GetMatchGroup OK");
		local WaitingQueuSize = GetMatchingWaitingGroupCount();
		Serverlog("WaitingQueuSize = " .. WaitingQueuSize);
		for  i = 1, WaitingQueuSize , 1 do
			local GroupID = GetGroupToMatching();
			
			local Temp_AskJoinTable = GetGroup(GroupID);
			
			for UserDbNum in pairs( Temp_AskJoinTable ) do
				GroupMemberTable[UserDbNum] = GroupID;
				Serverlog("UserDbNum = " .. UserDbNum);
				local rating = GetRating(UserDbNum);
				Serverlog("Rating = " .. rating);
			end
			
			Serverlog("GroupID = " .. GroupID);
			table.insert(GroupTable, GroupID);
			
			Temp_AskJoinTable = nil;
			
			Serverlog("WaitingQueuSize OK");
			--SendToGroupAskJoin(GroupID,60,true);
			Serverlog("SendToGroupAskJoin OK");
			local mapid = GetSNATIVEID(27,0);
			RequestMapCreate(mapid);
		end
		
		bOk = true;
		return true;
	end
	return false;
end

function aa()
local RatingAverage = 0;
			local RatingMemberCount = 0;
			local Temp_AskJoinTable = GetGroup(GroupID);
			for UserDbNum in pairs( Temp_AskJoinTable ) do
				GroupMemberTable[UserDbNum] = GroupID;
				local Rating = GetCaptureTheFlagRating(UserDbNum);
				RatingAverage = RatingAverage + Rating;
				RatingMemberCount = RatingMemberCount +1;
			end
			RatingAverage = RatingAverage / RatingMemberCount;
			
			if ( RatingTable[1] >= RatingAverage ) then
				table.insert(RatingeGroupTable[1], GroupID);
			elseif ( RatingTable[2] >= RatingAverage ) then
				table.insert(RatingeGroupTable[2], GroupID);
			elseif ( RatingTable[3] >= RatingAverage ) then
				table.insert(RatingeGroupTable[3], GroupID);
			elseif ( RatingTable[4] >= RatingAverage ) then
				table.insert(RatingeGroupTable[4], GroupID);
			elseif ( RatingTable[5] >= RatingAverage ) then
				table.insert(RatingeGroupTable[5], GroupID);
			elseif ( RatingTable[6] >= RatingAverage ) then
				table.insert(RatingeGroupTable[6], GroupID);
			elseif ( RatingTable[7] >= RatingAverage ) then
				table.insert(RatingeGroupTable[7], GroupID);
			end
end

function OnMapCreate( InstanceID, isSucsess )
	Serverlog("OnMapCreate OK");
	if(isSucsess == false) then 
		Serverlog("MapRequestSize Fail3");
		bOk = false;
		return false;
	end
	StartMachedGame(InstanceID);
	return true;
end

function StartMachedGame(InstanceID)
	Serverlog("StartMachedGame OK");
	for key in pairs( GroupTable ) do
		if IsValidGroup(GroupTable[key])  == false then
			Serverlog("ERROR Group = " .. GroupTable[key]);
		else
			Serverlog("VALID Group = " .. GroupTable[key]);
		end
		
		SendToClientMessageBoxGROUP(L"깃발전을 시작합니다.", GroupTable[key]);
		SendJoin(GroupTable[key], InstanceID);
		FinishGroup(GroupTable[key]);
	end
end

function OnReadyAnswer( Message_Client , OKMessage)
	if ( OKMessage == true ) then
		local mapid = GetSNATIVEID(27,0);
		RequestMapCreate(mapid);
	--else 
		--FinishGroup(GroupMemberTable[Message_Client]);
	end
end

function NoticeResualt(InstanceID,FactionID,_Rating,_IsWinner)
	return true;
end

function isReadyToStart()
	return bCanStart;
end

function isReadyToDistroy()
	return bCanDestroy;
end