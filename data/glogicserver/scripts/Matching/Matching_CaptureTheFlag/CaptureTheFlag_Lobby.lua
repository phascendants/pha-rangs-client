--
-- 주의) 
-- 1) 대/소 문자를 확실하게 구분해야 합니다.
-- 2) true, false 를 확실하게 구분해야 합니다.
-- 3) UCS-2 Little Endian 로 저장해야 합니다.
-------------------------------------------------------------------------------
TournamentAnyTime = false;
bStartTournament = false;

numJoinTeam = 0;

-- 토너먼트 시작.
function reqTournamentStart()
	if ( numJoinTeam > 0 ) then
		TournamentStart();
	end
	return true; 
end

function reqTournamentReady(fElpaseTime)
	if ( bStartTournament == false ) then
		if ( numJoinTeam > 0 ) then
			--EndTournamentQueue();
			TournamentStart();
			bStartTournament = true;
		end
	end
	
	return false;
end

-- 토너먼트 입장 요청을 하였다! 확인해주자!
function SetMatchingQueue(nPlayerDbNum)
	local result = false;
	result = SendMatchingQueue(nPlayerDbNum)
	numJoinTeam = numJoinTeam + 1;
	return true;
end

-- 토너먼트 입장 요청을 하였다! 확인해주자!
function TournamentRequestJoin(nPlayerDbNum)
	SendToClientMessageBox(L"nPlayerDbNum = " .. nPlayerDbNum,nPlayerDbNum);
	if(IsJoinGame(nPlayerDbNum) == true) then
		SendToClientMessageBox(L"이미 신청중에 있습니다.",nPlayerDbNum);
		return false;
	end
	
	if(IsJoinGameofALL(nPlayerDbNum) == true) then
		SendToClientMessageBox(L"딴거에 이미 신청중에 있습니다.",nPlayerDbNum);
		return false;
	end
	
	SetMatchingQueue(nPlayerDbNum); -- 참가 
	EndTournamentQueue();
	
	SendToClientMessageBox(L"데스메치 입장 대기 중", nPlayerDbNum);
end













