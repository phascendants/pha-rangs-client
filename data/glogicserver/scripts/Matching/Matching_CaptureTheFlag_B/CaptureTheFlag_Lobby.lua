--
-- 주의) 
-- 1) 대/소 문자를 확실하게 구분해야 합니다.
-- 2) true, false 를 확실하게 구분해야 합니다.
-- 3) UCS-2 Little Endian 로 저장해야 합니다.
-------------------------------------------------------------------------------
bStartTournament = false;

winningStreakPoint =
{
    1.0, 1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8, 2.0,
}

losingStreakPoint =
{
    1.0, 1.0, 1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.8, 2.0,
}

WIN = 1;
LOSE = 0;

FACT_RED = 0;
FACT_BLUE = 1;

RunawayTable = {};

TodayDate = 0;
-- ColorCode
	COLOR_BLACK				= {0,0,0};
	COLOR_WHITE				= {255,255,255};
	COLOR_RED				= {255,0,0};
	COLOR_GREEN				= {0,255,0};
	COLOR_BLUE				= {128,194,235};
--------------------------------------------------------------------------------
-- CDM_ROOM.lua파일의 정보 참조 (실제 인던이 생성되는 시점);
ContentsDate =
{
--   요일, 시간, 분 , Duration (진행시간)			
	{ 0, 10, 00, {30,20,10,9,8,7,6,5,4,3,2,1} }, -- 매일 10시 0분에 깃발전 시작;		
	{ 0, 14, 00, {30,20,10,9,8,7,6,5,4,3,2,1} }, -- 매일 14시 0분에 깃발전 시작;
	{ 0, 18, 00, {30,20,10,9,8,7,6,5,4,3,2,1} }, -- 매일 18시 0분에 깃발전 시작;		
	{ 0, 22, 00, {30,20,10,9,8,7,6,5,4,3,2,1} }, -- 매일 22시 0분에 깃발전 시작;				
}
--------------------------------------------------------------------------------
OpenGameDateNum = nil;
ClubList = {};
--------------------------------------------------------------------------------

function reqTournamentReady(fElpaseTime)
	local _day = GetDay();
	local _weekDay = GetWeekDay();
	local _hour = GetHour();
	local _min = GetMinute();
	
	if ( _day ~= TodayDate ) then
        -- 탈주자들 명부 삭제;
        RunawayTable = {};
        
        TodayDate = _today;
    end
	
	-- 현재 진행중인 게임이 있는지 체크;
	if ( OpenGameDateNum ~= nil ) then
		local ContentOpenDate = ContentsDate[OpenGameDateNum];
		
		if ( ( ContentOpenDate[2] ~= _hour ) or ( ContentOpenDate[3] ~= _min ) )then
			OpenGameDateNum = nil;
		end
	else
		-- 현재 진행중인 게임이 없다면 열려야하는 게임이 있는지 체크;
		for dateKey, dateValue in pairs( ContentsDate ) do
			-- 해당 시간까지 남은 시간;
			local _leftHour = dateValue[2] - _hour;
			local _leftMinute = ( _leftHour * 60 ) + ( dateValue[3] - _min );
			-- 해당 요일까지 남은 일수;
			local _nDayInterval = 0;
			if ( dateValue[1] ~= 0 ) then
				_nDayInterval = dateValue[1] - _weekDay;
				if ( _nDayInterval < 0 ) then
					_nDayInterval = _nDayInterval + 7;
				elseif ( _nDayInterval == 0 ) and ( _leftMinute < 0 ) then
					_nDayInterval = 7;
				end
			elseif ( _leftMinute < 0 ) then
				_nDayInterval = 1;
			end
			_leftMinute = _leftMinute + ( _nDayInterval * 24 * 60 );
			
			-- 현재는 1일 이전에 한해서 알림;
			if ( _nDayInterval <= 1 ) then
				-- 알림이 있다면 알림 시간 체크;
				if ( dateValue[4] ~= nil ) then
					-- 최초 알림 초기화;
					if ( dateValue["currentNotify"] == nil ) then
						for NotifyKey, NotifyValue in pairs( dateValue[4] ) do
							dateValue["currentNotify"] = NotifyKey;
							if ( _leftMinute >= NotifyValue ) then
								break;
							end
						end
					end
					-- 알림 시간 체크;
					local notifyMinute = dateValue[4][dateValue["currentNotify"]];
					if ( notifyMinute ~= nil ) then
						if ( _leftMinute == notifyMinute ) then
							PrintSimpleXmlMsg_Broad(10, {"PVP_CTFLAG_SYSTEM_MESSAGE", 22, _leftMinute}, COLOR_WHITE);
							PrintChatXmlMsg_Broad({"PVP_CTFLAG_SYSTEM_MESSAGE", 22, _leftMinute}, COLOR_BLUE);
							dateValue["currentNotify"] = dateValue["currentNotify"] + 1;
							break;
						elseif ( _leftMinute < notifyMinute ) then
							dateValue["currentNotify"] = dateValue["currentNotify"] + 1;
						end
					end
				end
				-- 진행시간 체크;
				if ( _leftMinute == 0 ) then
					OpenGameDateNum = dateKey;
					dateValue["currentNotify"] = nil;
					
					-- CDM을 시작합니다.
					PrintSimpleXmlMsg_Broad(10, {"PVP_CTFLAG_SYSTEM_MESSAGE", 23},COLOR_WHITE);
					PrintChatXmlMsg_Broad({"PVP_CTFLAG_SYSTEM_MESSAGE", 23},COLOR_BLUE);
					return true;
				end
			end
		end
	end
	
	return false;
end

-- 토너먼트 입장 요청을 하였다! 확인해주자!
function TournamentRequestJoin(nPlayerDbNum)
    if ( IsParty(nPlayerDbNum) == true ) then -- 파티중에는 입장 불가;
        --PrintChatMsg(nPlayerDbNum, "impossible in Party",COLOR_RED);
        PrintChatXmlMsg(nPlayerDbNum, { "PVP_CTFLAG_SYSTEM_MESSAGE", 19 },COLOR_RED); -- (파티중에는 참가가 불가능합니다.);
        return false;
    end
    
	-- 이미 해당 게임에 신청한 상태인지 검사;
	if(IsJoinGame(nPlayerDbNum) == true) then	
		PrintChatXmlMsg(nPlayerDbNum, { "RNCDM_SYSTEM_MESSAGE", 12 },COLOR_RED); -- (이미 신청 중입니다)
		return false;
	end
	
	-- 다른 전장 컨텐츠를 신청,이용 중인지 검사;
	if ( (IsJoinGameofALL(nPlayerDbNum) == true) or (IsJoinOtherContents(nPlayerDbNum) == true) )then
		PrintChatXmlMsg(nPlayerDbNum, { "RNCDM_SYSTEM_MESSAGE", 13 },COLOR_RED); -- (다른 참가 신청이 있습니다.)
		return false;
	end
    
	-- 필요 레벨 검사
	local _nLevel = GetLevel(nPlayerDbNum);
	if ( _nLevel < 230) then
		PrintChatXmlMsg(requestPlayer, { "RNCDM_SYSTEM_MESSAGE", 21 },COLOR_RED); -- (입장 가능레벨을 확인하세요.) 신청자에게 보냄
		if ( requestPlayer ~= nPlayerDbNum ) then
			PrintChatXmlMsg(nPlayerDbNum, { "RNCDM_SYSTEM_MESSAGE", 21 },COLOR_RED); -- (입장 가능레벨을 확인하세요.) 해당 검사자에게 보냄;
		end
		return false;
	end
    --if ( RunawayTable[nPlayerDbNum] == true ) then	-- 이전경기 무단 이탈.
        --PrintChatMsg(nPlayerDbNum, "You are runaway",COLOR_RED);
        --PrintChatXmlMsg(nPlayerDbNum, { "PVP_CTFLAG_SYSTEM_MESSAGE", 20 },COLOR_RED); -- (이전 깃발전 무단 이탈로 인하여 참가가 제한되었습니다.)
        --return false
    --end
	
    SendMatchingQueue(nPlayerDbNum); --깃발전 참가신청을 하였습니다.;
    EndTournamentQueue();
    PrintChatXmlMsg(nPlayerDbNum, { "PVP_CTFLAG_SYSTEM_MESSAGE", 21 },COLOR_BLUE); -- 깃발전 참가신청을 하였습니다.;
    TournamentStart();
end

function TournamentRequestReJoin(nPlayerDbNum, InstanceMapID)
    return true;
end

function CaptureTheFlag_Result(RedTeamList, BlueTeamList, RedTeamRunawayList, BlueTeamRunawayList, WinnerTeam)
    --RedTeamList 레드팀 유저 테이블 true => 마지막에 참여중이였던 플레이어, false => 마지막에 참여중이지 않은 플레이어;
    --BlueTeamList 블루팀 유저 테이블;
    --WinnerTeam 승리팀 Red = 0; Blue = 1;
    
    -- 팀들의 평균 레이팅을 구한다;
    local redTeamAvrRating, blueTeamAvrRating = functionCalcTeamRatingAverage(RedTeamList, BlueTeamList, RedTeamRunawayList, BlueTeamRunawayList);
    
    if ( RedTeamList ~= nil ) then
        for key, value in pairs(RedTeamList) do                             -- 결과 순간까지 남아있던 레드팀의 레이팅 계산;
            local _rating = GetRating(value);
             _winRate = functionCalcWinRate(_rating, blueTeamAvrRating);    -- 승률을 계산한다;
            if ( WinnerTeam == FACT_RED ) then
                functionCalcRating(value, WIN, _winRate);                   -- 이겼을시 변동 레이팅을 계산;
            elseif ( WinnerTeam == FACT_BLUE ) then
                functionCalcRating(value, LOSE, _winRate);                  -- 졌을경우 변동 레이팅을 계산;
            end
        end
    end
    
    if ( BlueTeamList ~= nil ) then
        for key, value in pairs(BlueTeamList) do                            -- 결과 순간까지 남아있던 레드팀의 레이팅 계산;
            local _rating = GetRating(value);               
             _winRate = functionCalcWinRate(_rating, blueTeamAvrRating);    -- 승률을 계산한다;
            if ( WinnerTeam == FACT_RED ) then                              
                functionCalcRating(value, LOSE, _winRate);                  -- 졌을경우 변동 레이팅을 계산;
            elseif ( WinnerTeam == FACT_BLUE ) then                         
                functionCalcRating(value, WIN, _winRate);                   -- 이겼을시 변동 레이팅을 계산;
            end
        end
    end
    
    if ( RedTeamRunawayList ~= nil ) then                                   
        for key, value in pairs(RedTeamRunawayList) do                      -- 결과 순간에 없었던 레드팀 탈주자의 레이팅 계산;
            RunawayTable[value] = true;                                     -- 결과 순간까지 접속해있지 않았기 때문에 탈주자로 간주 (입장 패널티);
            local _rating = GetRating(value);
             _winRate = functionCalcWinRate(_rating, blueTeamAvrRating);
            if ( WinnerTeam == FACT_RED ) then
                --functionCalcRating(value, WIN, _winRate);                 -- 탈주자는 이겨도 보상없음;
            elseif ( WinnerTeam == FACT_BLUE ) then
                functionCalcRating(value, LOSE, _winRate);
            end
        end
    end
    
    if ( BlueTeamRunawayList ~= nil ) then                                  
        for key, value in pairs(BlueTeamRunawayList) do                     -- 결과 순간에 없었던 블루팀 탈주자의 레이팅 계산;
            RunawayTable[value] = true;                                     -- 결과 순간까지 접속해있지 않았기 때문에 탈주자로 간주 (입장 패널티);
            local _rating = GetRating(value);
             _winRate = functionCalcWinRate(_rating, blueTeamAvrRating);
            if ( WinnerTeam == FACT_RED ) then
                functionCalcRating(value, LOSE, _winRate);
            elseif ( WinnerTeam == FACT_BLUE ) then
                --functionCalcRating(value, WIN, _winRate);                 -- 탈주자는 이겨도 보상없음;
            end
        end
    end
    
end

function MatchingJoinCansel(nPlayerDbNum) -- 신청 취소시;
    local GroupId = FindGroup(nPlayerDbNum);
    DeleteGroup(GroupId);
end

function OnDropOutChar(ClietDBID)
    if ( GetUserState(ClietDBID) ~= 2 ) then
        MatchingJoinCansel(ClietDBID);
    end
end

function functionCalcTeamRatingAverage(RedTeamList, BlueTeamList, RedTeamRunawayList, BlueTeamRunawayList)
    local redRating = 0;
    local redTeamSize = 0;
    if ( RedTeamList ~= nil ) then
        for key, value in pairs(RedTeamList) do
            redRating = redRating + GetRating(value);
            redTeamSize = redTeamSize + 1;
        end
    end
    if ( RedTeamRunawayList ~= nil ) then
        for key, value in pairs(RedTeamRunawayList) do
            redRating = redRating + GetRating(value);
            redTeamSize = redTeamSize + 1;
        end
    end
    
    local blueRating = 0;
    local blueTeamSize = 0;
    if ( BlueTeamList ~= nil ) then
        for key, value in pairs(BlueTeamList) do
            blueRating = blueRating + GetRating(value);
            blueTeamSize = blueTeamSize + 1;
        end
    end
    if ( BlueTeamRunawayList ~= nil ) then
        for key, value in pairs(BlueTeamRunawayList) do
            blueRating = blueRating + GetRating(value);
            blueTeamSize = blueTeamSize + 1;
        end
    end
    
    if ( redTeamSize ~= 0 ) then
        redRating = redRating / redTeamSize;
    else
        redRating = 0;
    end
    
    if ( blueTeamSize ~= 0 ) then
        blueRating = blueRating / blueTeamSize;
    else
        blueRating = 0;
    end
    
    return redRating, blueRating;
end

function functionCalcWinRate(myRating, EnemyTeamRating)
    -- 승률 계산 공식
    -- 1 / 1 + 10의( 적팀평균 레이팅 - 나의 레이팅 )제곱 / 400;
    return 1 / ( 1 + 10^( EnemyTeamRating - myRating ) / 400 );
end

function functionCalcRating(userDbNum, winPoint, winRate)
    -- 레이팅 변동값 계산 공식
    -- 24 * ( 승리or패배 - 승률 ) * 연승,패에 따른 가감치;
    -- 24는 K 상수 값으로 점수별 차등을 둘 수도 있다.
    local K = 24;
    
    local winningStreak = GetWinningStreak(userDbNum);
    local streakPoint = 1.0;
    if ( winningStreak > 0) then    -- 연승 중;
        if ( winPoint == WIN ) then
            SetWinningStreak(userDbNum, winningStreak + 1); -- 승리시 연승 +1;
        elseif ( winPoint == LOSE ) then
            SetWinningStreak(userDbNum, -1); -- 패배시 연승 카운트 초기화;
        end
        
        if ( winningStreak > 10 ) then -- 10연승 이상은 2.0으로 고정
            streakPoint = 2.0;
        else
            streakPoint = winningStreakPoint[winningStreak]; -- 해당 연승에 해당하는 레이팅 가감치를 얻어옴;
        end
    elseif ( winningStreak < 0) then    -- 연패 중;
        if ( winPoint == WIN ) then
            SetWinningStreak(userDbNum, 1); -- 승리시 연패 카운트 초기화;
        elseif ( winPoint == LOSE ) then
            SetWinningStreak(userDbNum, winningStreak - 1); -- 패배시 연패 + 1;
        end
    
        if ( winningStreak < -10 ) then -- 10연패 이상은 2.0으로 고정;
            streakPoint = 2.0;
        else
            streakPoint = losingStreakPoint[math.abs(winningStreak)];  -- 해당 연패에 해당하는 레이팅 가감치를 얻어옴;
        end
    else -- 연패나 연승기록 없을시;
        if ( winPoint == WIN ) then
            SetWinningStreak(userDbNum, 1); -- 승리시 연승 카운트 1 로;
        elseif ( winPoint == LOSE ) then
            SetWinningStreak(userDbNum, -1); -- 패배시 연패 카운트 1로;
        end
    end
    winningStreak = GetWinningStreak(userDbNum);
    local _addRating = K * ( winPoint - winRate ) * streakPoint;
    --AddRating( userDbNum, _addRating);
    Log_CaptureTheFlag_Rating(userDbNum, GetRating(userDbNum), _addRating, winningStreak);  -- 레이팅로그 ( 유저DbNum, 레이팅, 변동 레이팅, 연승 );
end


function functionRatingDecrement(userDbNum)
    
end