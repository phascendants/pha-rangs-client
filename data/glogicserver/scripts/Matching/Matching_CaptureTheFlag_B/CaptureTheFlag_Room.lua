-- 트리 첫번째 이름
ContentsMainType = L"RNCOMPETITION_MAIN_TYPE_PVP";
-- 트리 두번째 이름
ContentsSubType = L"RNCOMPETITION_SUB_TYPE_CTFLAG";
-- 리스트 이름 (UI중앙)
ContentsName = L"TOURNAMENT_PVP_CTF_MAPNAME";
-- 맵목표
ContentsMissionInfo = L"TOURNAMENT_PVP_CTF_INFO";
-- 맵정보 
ContentsDetailInfo = L"TOURNAMENT_PVP_CTF_DETAIL";
-- 맵보상 
ContentsRewardInfo = L"TOURNAMENT_PVP_CTF_REWARD";

ContentsType = 2;
-- 유저에게 알릴 토너먼트 타입입니다. 
-- 0 => 정시
-- 1 => 상시
-- 2 => 임시
ContentsDateType = 0;
ContentsMapID = 190;

ContentsDate =
{
--   요일, 시간, 분 , Duration (진행시간)	
	{ 0, 10, 00, 10 }, -- 매일 10시00분에 10분 동안 신청가능;	
	{ 0, 14, 00, 10 }, -- 매일 14시00분에 10분 동안 신청가능;
	{ 0, 18, 00, 10 }, -- 매일 18시00분에 10분 동안 신청가능;	
	{ 0, 22, 00, 10 }, -- 매일 22시00분에 10분 동안 신청가능;		
}

ContentsUseInfoButton = false;
-------------------------------------------------------

function CreateComplete()
--	NotifyCreateComplete();
end

--function EventCustomMessage( ClientDbNum, CustomParam1, CustomParam2 )
--end