----- 깃발전용 매칭 스크립트
----- 레이팅에 따라 인원들을 매칭시키고 매칭된 인원들을 인던생성후 입장시키는 역활;

----- 레이팅 테이블
RatingTable = 
{
	999,		-- 0    ~ 999   초보자;
	1249,       -- 1000 ~ 1249  하수;
	1499,       -- 1250 ~ 1499  중수;
	1749,       -- 1500 ~ 1749  고수;
	1999,       -- 1750 ~ 1999  초고수;
	2249,       -- 2000 ~ 2249  지존;
	9999,       -- 2250 ~ 9999  초월자;
};

RatingGroupTable = 
{
	{},			-- 0    ~ 999   초보자 그룹 리스트;
	{},         -- 1000 ~ 1249  하수 그룹 리스트;
	{},         -- 1250 ~ 1499  중수 그룹 리스트;
	{},         -- 1500 ~ 1749  고수 그룹 리스트;
	{},         -- 1750 ~ 1999  초고수 그룹 리스트;
	{},         -- 2000 ~ 2249  지존 그룹 리스트;
	{},         -- 2250 ~ 9999  초월자 그룹 리스트;
};

bCanStart = true; 
bCanDestroy = true;

------ 깃발전 설정 변수 ---------
MinimumTeamMember = 10;  -- 한 게임당 깃발전 참가 인원;
MaxTeamMember = 5;      -- 한쪽 팀 최대 인원;

BattleMapID = {190, 0}; -- 깃발전의 맵 ID;

function OnReset( )
	GroupTable = {};
	bCanStart = true;
	bCanDestroy = true;
end

function OnStartMatching()
	Serverlog("OnStartMatching CTFlag");
	local WaitingQueuSize = GetMatchingWaitingGroupCount();
	for  i = 1, WaitingQueuSize , 1 do
		local GroupID = GetGroupToMatchingNotPlay();
		FunctionCalculateRating(GroupID);  -- 그룹의 레이팅 계산;
	end
	
	FunctionMatchingGame();	-- 적당한 레이팅 그룹끼리 매칭 후 게임 입장 시킴;
	return true;
end

function FunctionCalculateRating( GroupID )
	local RatingSum = 0;
	local RatingMemberCount = 0;
	local RatingAverage;
	
	local groupTable = GetGroup(GroupID);
	
	for UserDbNum in pairs( groupTable ) do
		local Rating = GetRating(UserDbNum);
		if ( Rating ~= nil ) then
			RatingSum = RatingSum + Rating;
			RatingMemberCount = RatingMemberCount +1;
		else
			Serverlog("Rating Nil");		
		end
	end
	
	RatingAverage = RatingSum / RatingMemberCount;	-- 팀 전체 레이팅 평균 계산;
	
	for i = 1, 7 , 1 do
		if ( RatingTable[i] >= RatingAverage ) then
			table.insert(RatingGroupTable[i], GroupID);
			break;
		end
	end
end

function FunctionMatchingGame()
	for RatingGroupKey, RatingGroup in pairs( RatingGroupTable ) do 		-- 레이팅 등급에 따른 그룹 리스트 중에서
        local _GroupCount = 0;
        local _tempTeam = {};
        for groupKey, groupValue in pairs( RatingGroup ) do 
            if ( IsValidGroup(groupValue) == true ) then
                _GroupCount = _GroupCount + 1;
                _tempTeam[groupKey] = groupValue;
            else
                RatingGroup[groupKey] = nil;
            end
            
            if ( _GroupCount == MinimumTeamMember ) then
                -------------- 깃발전 인던을 생성 한다.   --------------------
                local mapid = GetSNATIVEID(BattleMapID[1], BattleMapID[2]);
            
                local InstanceTable = MapMultyCreate(0, 1, mapid);
                local InstanceMapId = nil;
                local IsMapCreate = false;
                for inID in pairs( InstanceTable ) do
                    Serverlog("<MapCreate OK. mapGaeaID=" .. inID .. "/>");
                    IsMapCreate = true;
                    InstanceMapId = inID;
                end
                
                if IsMapCreate == false then
                    Serverlog("MapCreate Fail");
                    return true;
                end
                ------------------------------------------------------------
                -------------- 맵에 그룹을 입장 시킨다.   --------------------
                local redTeamCount = 0;
                local blueTeamCount = 0;
                for teamGroupKey, teamGroupValue in pairs( _tempTeam ) do
                    local randTeam = math.random();
                    Serverlog("randTeam"..randTeam);
                    if ( randTeam > 0.5 ) and ( redTeamCount < MaxTeamMember ) then
                        SendFaction(teamGroupValue, 0  , InstanceMapId);
                        Serverlog("SendFaction0");
                        redTeamCount = redTeamCount + 1;
                    elseif ( blueTeamCount < MaxTeamMember ) then
                        Serverlog("SendFaction1");
                        SendFaction(teamGroupValue, 1  , InstanceMapId);
                        blueTeamCount = blueTeamCount + 1;
                    else
                        Serverlog("SendFaction0");
                        SendFaction(teamGroupValue, 0  , InstanceMapId);
                    end
                    
                    --SendToClientMessageBoxGROUP("Capture The Flag Start!", teamGroupValue); -- 태혁씨 작업 후 xml로 교체.
                    --SendToClientMessageXMLGROUP(teamGroupValue, {"RNCDM_ENTER_TEXT", 0}, COLOR_BLUE);
                    SetGroupToPlay(teamGroupValue);
                    SendJoin(teamGroupValue, InstanceMapId);
                    --SamiFinishGroup(teamGroupValue);
                    RatingGroup[teamGroupKey] = nil;
                end
                ------------------------------------------------------------ 
                break;
            end           
            
        end
	end
end

function NoticeResualt(InstanceID,FactionID,_IsWIN)
    
end

function OnDestroy(InstanceID, isSucsess)	
	return false; -- 이맵에 있는 모든 녀석들 나가게 한다.
end

--function NoticeResualt(InstanceID,FactionID,_Rating,_IsWinner)
	--return true;
--end

--function isReadyToStart()
	--return bCanStart;
--end

--function isReadyToDistroy()
	--return bCanDestroy;
--end