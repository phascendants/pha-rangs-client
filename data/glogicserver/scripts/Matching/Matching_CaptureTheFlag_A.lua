--
-- 주의) 
-- 1) 대/소 문자를 확실하게 구분해야 합니다.
-- 2) true, false 를 확실하게 구분해야 합니다.
-- 3) UCS-2 Little Endian 로 저장해야 합니다.
-- Matching_Content_Type는 는 해당 컨텐츠의 점수관리를 위한 컨텐츠의 고유이름입니다. 없을경우 해당컨텐츠는 전장 점수를 사용할수 없습니다.;
-------------------------------------------------------------------------------

Matching_Content_Type = "CaptureTheFlag";

MAX_Matching_SIZE = 1;

Room_Admin_Script =
{
	L"Matching_CaptureTheFlag_A\\CaptureTheFlag_Room.lua",
};

Lobby_Manager =	
{
	L"Matching_CaptureTheFlag_A\\CaptureTheFlag_Lobby.lua",
};

Matching_Script = 	
{
	L"Matching_CaptureTheFlag_A\\CaptureTheFlag_Matching.lua",
};

-- << Matching Mode ID Lists >>
-- 0. 스크립트를 읽어 매칭을 하는 모드 ( 될 수 있으면 사용하지 말자 / 테스트 용도로만 사용하길 권한다 );
-- 1. 현재 매칭의 그룹정보를 파악하여 조건을 만족하면 계속해서 매칭하는 모드 ( CTF );
-- 2. 외부에서 매칭을 시작할 경우 바로 매칭이 진행되어 시작하는 모드 ( CDM );
Matching_Mode_ID = 1;