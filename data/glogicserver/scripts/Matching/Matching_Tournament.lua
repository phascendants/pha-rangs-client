--
-- 주의) 
-- 1) 대/소 문자를 확실하게 구분해야 합니다.
-- 2) true, false 를 확실하게 구분해야 합니다.
-- 3) UCS-2 Little Endian 로 저장해야 합니다.
-------------------------------------------------------------------------------

MAX_Matching_SIZE = 10;

Room_Admin_Script =
{
	L"Matching_PVP\\Tournament_No_1_RoomManager.lua",
};

Lobby_Manager =	
{
	L"Matching_PVP\\Tournament_No_1_LobbyManager.lua",
};

Matching_Script = 	
{
	L"Matching_PVP\\Tournament_No_1_MatchingManager.lua",
};
