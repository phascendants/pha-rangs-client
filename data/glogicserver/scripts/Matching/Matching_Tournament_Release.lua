--
-- 주의) 
-- 1) 대/소 문자를 확실하게 구분해야 합니다.
-- 2) true, false 를 확실하게 구분해야 합니다.
-- 3) UCS-2 Little Endian 로 저장해야 합니다.
-------------------------------------------------------------------------------

MAX_Matching_SIZE = 1;


Lobby_Manager =
{
	L"Matching_tournament\\Tournament_Lobby.lua",
};

Matching_Script = 
{
	L"Matching_tournament\\Tournament_Matching.lua",
};

Room_Admin_Script = 	
{
	L"Matching_tournament\\Tournament_Room.lua",
};

Betting_Script =
{
	L"Matching_tournament\\Tournament_Betting.lua",
};


Lobby_Field_Manager =
{
	L"Matching_tournament\\Tournament_Lobby.lua",
};