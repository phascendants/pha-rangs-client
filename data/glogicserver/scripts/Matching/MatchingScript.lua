--
-- 주의) 
-- 1) 대/소 문자를 확실하게 구분해야 합니다.
-- 2) true, false 를 확실하게 구분해야 합니다.
-- 3) UCS-2 Little Endian 로 저장해야 합니다.
-------------------------------------------------------------------------------


MatchingLuaList =
{
	--L"Matching_Tournament_Release.lua",
	L"Matching_CDM.lua",
	L"Matching_CaptureTheFlag_A.lua",
	L"Matching_CaptureTheFlag_B.lua",	
	L"Matching_DimensionLab.lua",	
};

Matching_Content_TypeList = 
{
    {"CaptureTheFlag", 0, 1200},
};
