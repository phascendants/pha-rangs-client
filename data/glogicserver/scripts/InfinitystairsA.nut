/**
	PVE 외벽대전
	서버 메인 스크립트       작성자 이익세
	
 */

// 맵 정보
root <- MapInfoTrigger();
root.SetMap(245, 0);			// 맵ID
root.SetGateID(2);			// 맵 시작 게이트 ID
root.SetDailyLimit(1);			// 하루 입장횟수 (라이브 적용 시 -> 1)
root.SetOwnTime(3600);			// 귀속 시간  1시간 (테스트 및 라이브 1시간)
root.SetPerson(2);			// 인원 제한 (테스트 및 라이브 2)
root.SetWaitEntrance(10);		// 입장 확인 대기 시간

// 보통
level0 <- LevelTrigger();
level0.SetFile("InfinitystairsALev0.nut");
level0.SetLevel(0);		
level0.SetUserLevel(210, 300);
level0.Load();					
root.InsertSibling(level0);