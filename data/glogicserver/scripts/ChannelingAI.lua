Type_Max_Count = 8

-- Idle State
State_Normal = {}

State_Normal[ "Begin" ] = function( me )
end

State_Normal[ "Update" ] = function( me, fElapsedTime )
	-- 자동 사냥
	me:AutoSearchTarget()
	
	-- 액션 시작;
	me:StartTargetAction()
end

State_Normal[ "End" ] = function( me )
end

State_Normal[ "Attack" ] = function( me )
end

State_Normal[ "Attacked" ] = function( me, attacker )
	return false
end

State_Normal[ "OwnerAttack" ] = function( me )
end

State_Normal[ "OwnerAttacked" ] = function( me, attacker )
	return false
end