Type_Max_Count = 2

GLAT_IDLE = 0
GLAT_MOVE = 1
GLAT_ATTACK = 2
GLAT_SKILL = 3
GLAT_SHOCK = 4
GLAT_PUSHPULL = 5
GLAT_GATHERING = 6
GLAT_TALK = 7
GLAT_CONFT_END = 8
GLAT_MOTION = 9
GLAT_AUTHED = 10
GLAT_FALLING = 11
GLAT_DIE = 12
GLAT_SKILL_WAIT = 13

-- Idle State
State_Normal = {}

State_Normal[ "Begin" ] = function( me )
	me:ApplyDelay(4)
end

State_Normal[ "Update" ] = function( me, fElapsedTime )	
	if ( me:GetDelayState() == false ) then
		me:SyncPositionTarget()
		me:AutoSearchTarget()
		if ( me:IsTarget() == false ) then
			me:DieAction()
		end
		me:StartTargetAction( 1 )
	else
		me:SyncPositionTarget()
		
		if ( me:IsTarget() == false ) then
			me:DieAction()
		end
	end
end

State_Normal[ "End" ] = function( me )
end

State_Normal[ "Attack" ] = function( me )
end

State_Normal[ "Attacked" ] = function( me, attacker )
	return false
end

State_Normal[ "OwnerAttack" ] = function( me )
end

State_Normal[ "OwnerAttacked" ] = function( me, attacker )
	return false
end