/**
	PVE 외벽대전
	서버 스크립트
	작성자 이익세 (수정하지말것)
 */
 

script1 <- ::import("data/glogicserver/scripts/infinitystairsA_0.nut", {});
script1.difficulty.SetMultipleHP(4);		// 전체몹 HP증가율
script1.difficulty.SetMultipleAttack(1.5);	// 전체몹 공격력 증가율
script1.difficulty.SetMultipleDefense(1);	// 전체몹 방어력 증가율
script1.difficulty.SetMultipleExp(2);		// 전체몹 설정된 경험치의 증가율 (1 = 현 설정값)
script1.difficulty.SetMultipleDrop(1);		// 전체몹 설정된 드랍율
script1.layer.SetLayer(0);			// 시작 레이어
script1.info.SetMessage("INFINITYSTAIRS_MESSAGE_31");	// 시작 출력 메시지
script1.info.SetLifeTime(4);				// 시작 출력 메시지 시간
script1.stage.SetLifeTime(190);				// 스테이지 클리어 시간
script1.start.InsertChild(script1.stage);
script1.start.InsertChild(script1.clear);
script1.clear.SetCount(20);				// 목표 수
script1.postinfo.SetMessage("INFINITYSTAIRS_MESSAGE_37");	// 클리어 시 출력 메시지
script1.postinfo.SetLifeTime(4);				// 클리어 시 출력 메시지 시간
script1.open.SetFromGateID(1);					// 오픈 할 게이트
script1.open.SetToMapID(245, 0);				// 다음 스테이지 맵 정보
script1.open.SetToGateID(3);					// 다음 이동 맵의 게이트 정보
script1.reward.SetMVPIncrease(2);				// 이 스테이지에서 게임종료 시 지급될 MVP 적용값
script1.reward.SetEXPIncrease(3);				// 이 스테이지에서 게임종료 시 지급될 EXP 적용값
stage1 <- script1.root;


script2 <- ::import("data/glogicserver/scripts/infinitystairsA_0.nut", {});
script2.difficulty.SetMultipleHP(6);
script2.difficulty.SetMultipleAttack(1);
script2.difficulty.SetMultipleDefense(1);
script2.difficulty.SetMultipleExp(1.7);
script2.difficulty.SetMultipleDrop(0.8);
script2.layer.SetLayer(1);
script2.info.SetMessage("INFINITYSTAIRS_MESSAGE_32");
script2.info.SetLifeTime(4);
script2.stage.SetLifeTime(240);
script2.start.InsertChild(script2.stage);
script2.start.InsertChild(script2.clear);
script2.clear.SetCount(120);
script2.postinfo.SetMessage("INFINITYSTAIRS_MESSAGE_36");
script2.postinfo.SetLifeTime(4);
script2.open.SetFromGateID(0);
script2.open.SetToMapID(245, 0);
script2.open.SetToGateID(2);
script2.reward.SetMVPIncrease(2);
script2.reward.SetEXPIncrease(3);
stage2 <- script2.root;


script3 <- ::import("data/glogicserver/scripts/infinitystairsA_0.nut", {});
script3.difficulty.SetMultipleHP(1);
script3.difficulty.SetMultipleAttack(1.2);
script3.difficulty.SetMultipleDefense(1);
script3.difficulty.SetMultipleExp(20);			// 설정된 경험치가 낮으므로 스크립트에서 보강됨
script3.difficulty.SetMultipleDrop(1);
script3.layer.SetLayer(2);
script3.info.SetMessage("INFINITYSTAIRS_MESSAGE_33");
script3.info.SetLifeTime(4);
script3.stage.SetLifeTime(300);
script3.start.InsertChild(script3.stage);
script3.start.InsertChild(script3.clear);
script3.clear.SetCount(1);
script3.postinfo.SetMessage("INFINITYSTAIRS_MESSAGE_37");
script3.postinfo.SetLifeTime(4);
script3.open.SetFromGateID(1);
script3.open.SetToMapID(245, 0);
script3.open.SetToGateID(3);
script3.reward.SetMVPIncrease(2);
script3.reward.SetEXPIncrease(3);
stage3 <- script3.root;

script4 <- ::import("data/glogicserver/scripts/infinitystairsA_0.nut", {});
script4.difficulty.SetMultipleHP(10);
script4.difficulty.SetMultipleAttack(1.2);
script4.difficulty.SetMultipleDefense(1);
script4.difficulty.SetMultipleExp(3.5);
script4.difficulty.SetMultipleDrop(1);
script4.layer.SetLayer(3);
script4.info.SetMessage("INFINITYSTAIRS_MESSAGE_34");
script4.info.SetLifeTime(4);
script4.stage.SetLifeTime(240);
script4.start.InsertChild(script4.stage);
script4.start.InsertChild(script4.clear);
script4.clear.SetCount(60);
script4.postinfo.SetMessage("INFINITYSTAIRS_MESSAGE_36");
script4.postinfo.SetLifeTime(4);
script4.open.SetFromGateID(0);
script4.open.SetToMapID(245, 0);
script4.open.SetToGateID(2);
script4.reward.SetMVPIncrease(2);
script4.reward.SetEXPIncrease(3);
stage4 <- script4.root;

script5 <- ::import("data/glogicserver/scripts/infinitystairsA_0.nut", {});
script5.difficulty.SetMultipleHP(1.2);
script5.difficulty.SetMultipleAttack(2);
script5.difficulty.SetMultipleDefense(1.3);
script5.difficulty.SetMultipleExp(3.2);
script5.difficulty.SetMultipleDrop(1);
script5.layer.SetLayer(4);
script5.info.SetMessage("INFINITYSTAIRS_MESSAGE_35");
script5.info.SetLifeTime(4);
script5.stage.SetLifeTime(360);
script5.start.InsertChild(script5.stage);
script5.start.InsertChild(script5.clear);
script5.clear.SetCount(1);
script5.postinfo.SetMessage("INFINITYSTAIRS_MESSAGE_38");
script5.postinfo.SetLifeTime(60);
script5.reward.SetMVPIncrease(2);
script5.reward.SetEXPIncrease(3);
script5.stage.SetLastStage();
stage5 <- script5.root;