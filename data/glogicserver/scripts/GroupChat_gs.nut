
print("------->  GroupChat script starts.  <-------")
print(" ")

//
//Note: 옵션의 마지막 글자가 H,M,S 등일 경우, 각각 의미하는 바가 다음과 같다.
//
// S: Seconds	(초단위 값)
// M: Minutes	(분단위 값)
// H: Hours		(시간단위 값)
// Freq: Frequency (주기값: 단위는 각각의 설정마다 다를 수 있다. 그러므로 단위는 각 항목의 주석에 명시)
//

//---------------------------- Configurations ----------------------------------

//
// Common Configuration
//
Config <-
{
	MaxOpenChat			= 10	//최대 참여 가능 채팅 수
	MaxTO				= 50	//각 방당 최대 참여 인원
	RequiredLevel		= 0		//그룹채팅 가능 최소 캐릭터 레벨
}

