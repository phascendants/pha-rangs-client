
print("------->  CaptureTheField script starts.  <-------")
print(" ")
print(" ")

//
//Note: 옵션의 마지막 글자가 H,M,S 등일 경우, 각각 의미하는 바가 다음과 같다.
//
// S: Seconds	(초단위 값)
// M: Minutes	(분단위 값)
// H: Hours		(시간단위 값)
// Freq: Frequency (주기값: 단위는 각각의 설정마다 다를 수 있다. 그러므로 단위는 각 항목의 주석에 명시)
//


//
// Configuration Names
//
Config <-
{	
	PlayTimeM				= 30	//한 경기의 플레이 시간
	
	RequiredLevel			= 200	//참가를 위한 캐릭터의 최소 Level
	
	ConsecutivelyCapturel	= true	// 연속 점령학원 참여가능/불가능
	ConsecutivelyNumber     = 2     // 2회부터
	StartCycleH				= 3		//시작 주기 (몇 시간에 한번씩 열릴것인가?)
	
	ReadyDurationM			= 10		//신청 시간 (시작 전에 몇 분 동안 신청을 받을 것인가?)
	RewardTimeM				= 1		//보상 시간 (종료 후에 몇 분 동안 보상시간을 가질 것인가? 이 시간 동안 CTF 통계결과를 유저들에게 전달한다.)
	
	MaxNumOfJoiners			= 70	//한 학원 당 신청 가능 최대 인원 (최대 몇 명까지 신청가능한가?)
	MaxNumOfPlayers			= 50	//한 학원 당 참가 가능 최대 인원 (신청 인원 중에서 최대 몇명까지 참여 가능한가?)
						//    MaxNumOfPlayers은 최대 60을 초과할 수 없다.(CTF_PLAYER_NUM_LIMIT) <-- 이 값보다 큰 값으로 설정하고자 할 경우네는 프로그래머에게 문의
                                                	//    한국:진행인원 20명   해외:진행인원 50명
							//    참여인원 조정 시 MobNPC 51/29, 51/30, 51/31    writed by islee 150921
							//    총 3개의 인증기 HP, 회복율을 각 국가 진행 인원수에 맞게 수정
							//    : 10명 HP 1184000 회복율 0.003,  20명 HP 2368000 회복율 0.0015,  30명 HP 3615200 회복율 0.001
							//    : 40명 HP 4344000 회복율 0.0008, 50명 HP 5136000 회복율 0.0007, 100명 HP 8217600 회복율 0.0004
							//참여인원 조정 시 MaxRebirthTimeS(부활에 소요되는 최대 시간)도 참여인원과 동일하도록 수정 하여야 함
	Channel					= 0		//몇 번 채널에서 개최할 것인가? 해당 채널 외의 채널에서는 CTF가 개최되는 동안  PvE/PvP 맵으로 진입할 수 없다.
	
	PvEMapMID				= 221	//PvE맵의 MID
	PvEMapSID				= 0		//PvE맵의 SID
	PvPMapMID				= 222	//PvP맵의 MID
	PvPMapSID				= 0		//PvP맵의 SID
	
	GateStart				= 0		//기본 시작 Gate (타Map에서 Gate를 타고 진입했을 경우, 이 지점에서 시작될 것이다.)
	GateExit1				= 1		//출구 Gate#1
	GateExit2				= 2		//출구 Gate#2
	
	GateRevivalSM			= 3		//성문학원 시작(부활) 지점
	GateRevivalHA			= 4		//현암학원 시작(부활) 지점
	GateRevivalBH			= 5		//봉황학원 시작(부활) 지점
	
	KickOutMapMIDSM		= 2		//성문학원 캐릭터, CTF시스템에 의해 강제로 Recall되거나, CTF에서 패배했을 경우 Recall될 맵의 MID (default: 2 )
	KickOutMapSIDSM		= 0		//성문학원 캐릭터, CTF시스템에 의해 강제로 Recall되거나, CTF에서 패배했을 경우 Recall될 맵의 SID (default: 0  )
	KickOutGateIDSM			= 6		//성문학원 캐릭터, CTF시스템에 의해 강제로 Recall되거나, CTF에서 패배했을 경우 Recall될 맵의 Gate (default: 6 )
	
	KickOutMapMIDHA		= 5		//현암학원 캐릭터, CTF시스템에 의해 강제로 Recall되거나, CTF에서 패배했을 경우 Recall될 맵의 MID (default: 5 )
	KickOutMapSIDHA		= 0		//현암학원 캐릭터, CTF시스템에 의해 강제로 Recall되거나, CTF에서 패배했을 경우 Recall될 맵의 SID (default: 0 )
	KickOutGateIDHA			= 6		//현암학원 캐릭터, CTF시스템에 의해 강제로 Recall되거나, CTF에서 패배했을 경우 Recall될 맵의 Gate (default: 6  )
	
	KickOutMapMIDBH		= 8		//봉황학원 캐릭터, CTF시스템에 의해 강제로 Recall되거나, CTF에서 패배했을 경우 Recall될 맵의 MID (default: 8 )
	KickOutMapSIDBH		= 0		//봉황학원 캐릭터, CTF시스템에 의해 강제로 Recall되거나, CTF에서 패배했을 경우 Recall될 맵의 SID (default: 0  )
	KickOutGateIDBH			= 6		//봉황학원 캐릭터, CTF시스템에 의해 강제로 Recall되거나, CTF에서 패배했을 경우 Recall될 맵의 Gate (default: 06 )
	
	UpdatePlayersFreq		= 5000	//참가자 중에서 도중에 나간 인원이 발생하거나 참여 인원이 미달일 경우, 해당 주기로 체크해서 신청자들을 CTF에 참여시킨다. (단위: milliseconds)
	UpdateMinimapFreq		= 1.0	//참가자들의 미니맵 위치 갱신 주기. 값이 작아질 수도록 서버의 load는 증가한다. (단위: sec) - 이 값은 부동소수임에 주의
	
	MaxRebirthTimeS			= 50	//부활에 소요되는 최대 시간
	
	Certification1MID		= 51	//인증기#1의 MID
	Certification1SID		= 29	//인증기#1의 SID
	Certification2MID		= 51	//인증기#2의 MID
	Certification2SID		= 30	//인증기#2의 SID
	Certification3MID		= 51	//인증기#3의 MID
	Certification3SID		= 31	//인증기#3의 SID
	
	ContributionPoint4Winner= 30	//승리학원에 속한 참가자들에게 일괄 지급할 기여도
	ContributionPoint4Loser	= 0		//패배학원에 속한 참가자들에게 일괄 지급할 기여도 (무승부일 경우, 모든 학원은 패배로 간주한다.)
	ContributionPointMax	= 50	//CTF 한판에서 획득할 수 있는 최대 기여도
	
	Damage					= 10000  // 승리학원생 기여도 지급 조건 피해량
    Recovery				= 25000    // 승리학원생 기여도 지급 조건 회복량
}

