--
-- 주의) 
-- 1) 대/소 문자를 확실하게 구분해야 합니다.
-- 2) true, false 를 확실하게 구분해야 합니다.
-- 3) UCS-2 Little Endian 로 저장해야 합니다.
-------------------------------------------------------------------------------
-- 버전 표기는 반드시 있어야 하며, 로직단과 반드시 일치해야 한다;
SCRIPT_VERSION = 
{
    "AlphaServer",     -- main version
    0,    -- sub version
};

-- 초기화시 저절로 기입되는 변수로 절대 선언 되면 안됩니다.;
--keyMapID = 선언금지 ( 초기화 이후 저절로 세팅 됨 );
--instanceMapID = 선언금지 ( 초기화 이후 저절로 세팅 됨 );

-- ActorType
	ACTOR_PC				= 0;
	ACTOR_NPC				= 1;
	ACTOR_MOB				= 2;
	ACTOR_SUMMON			= 6;
	ACTOR_NULL				= -1;
--------------------------------------------------------------------------------
-- FactionType
	FACTION_TEAM = 5
	FACTION_SAFE = 6
	FACTION_BASIC = 7
--------------------------------------------------------------------------------
-- RelationType
	RELATION_ENEMY				= 0;
	RELATION_ALLY				= 1;
	RELATION_NEUTRAL_ENEMY		= 2;
	RELATION_NEUTRAL_ALLY		= 3;
--------------------------------------------------------------------------------
-- TriggerType
	TRIGGER_PC				= 1;
	TRIGGER_MOB				= 2;
	TRIGGER_SUMMON			= 4;
	
	TRIGGER_PC_MOB		    = 3;	
	TRIGGER_PC_SUMMON       = 5;
--------------------------------------------------------------------------------
	PARAM3_MSGBOX_OK		= 0;
	PARAM3_MSGBOX_CANCEL	= 1;
	PARAM3_MSGBOX_TIMEOVER	= 2;
--------------------------------------------------------------------------------
-- ColorCode
	COLOR_BLACK				= {0,0,0};
	COLOR_WHITE				= {255,255,255};
	COLOR_RED				= {255,0,0};
	COLOR_GREEN				= {0,255,0};
	COLOR_BLUE				= {128,194,235};
	COLOR_MAGENTA			= {240,100,170}; 
	COLOR_YELLOW			= {255,200,0};
--------------------------------------------------------------------------------
-- CustomMessage Sender Type
    MSG_TO_USER             = 0;    -- 특정 유저;   SenderID : CharDbNum;
    MSG_TO_MY_INSTANCE      = 1;    -- 현재 자신이 속한 필드서버 인던 객체 (LevelField)  SenderID : 의미없음;
    MSG_TO_FIELD_BASE       = 2;    -- 필드서버의 특정 인던 컨텐츠 (PublicField);  SenderID : Instance KeyMap ID;
    MSG_TO_FIELD_INSTANCE   = 3;    -- 필드서버의 특정 인던 객체 (LevelField);  SenderID : Instance Map ID;
    MSG_TO_AGENT_BASE       = 4;    -- 에이전트서버의 특정 인던 컨텐츠 (PublicAgent);  SenderID : Instance KeyMap ID;
    MSG_TO_AGENT_INSTANCE   = 5;    -- 에이전트서버의 특정 인던 객체 (LevelAgent);  SenderID : Instance Map ID;
--------------------------------------------------------------------------------
-- Timer Event Code
EVENT_REWARD                = 1;  -- 보상 시간;
EVENT_DESTROY               = 2;  -- 인던 파괴 ( 게임종료 -> 보상 종료 후 )
EVENT_SAFE                  = 3;  -- 부활후 무적 시간;
EVENT_GAME_START            = 4;  -- 게임 시작;
EVENT_GAME_END              = 5;  -- 게임 종료;
EVENT_GAME_ABUSE            = 6;  -- 최소진행시간 이전에 게임이 끝나서 무효가 될 때;
EVENT_FLAG_RECALL           = 7;  -- 드랍된 깃발이 시간이 지나 회수가 될 때;
EVENT_NOTIFY_GAMESTATE      = 10;  -- 게임진행을 위한 도움말 공지;
EVENT_NOTIFY_GAMEEND        = 11;  -- 게임종료시간 공지;

EVENT_UI_START_1            = 15;  -- "깃발전투가 시작되었습니다."
EVENT_UI_START_2            = 16;  -- "적군의 깃발을 탈취한 후 아군 깃발지역으로 수송하세요."
EVENT_UI_START_3            = 17;  -- "깃발 수송에 성공한 경우 1점을 획득합니다."
EVENT_FLAG_GET				= 18;	-- 깃발취득시점 기준 n초후에 금지스킬을 한번 더 삭제
--------------------------------------------------------------------------------
-- CutsomParameter list
PARAM1_EXIT_BUTTON          = 1; -- 전장결과 UI의 나가기 버튼 클릭시;
PARAM1_JOIN_UI              = 2;
--------------------------------------------------------------------------------
-- 깃발전 UI 정보 타입
INDICATOR_SHOW              = 0;    -- 깃발전 진행 UI 켬,끔  ( true => 켬 ,false => 끔 );
INDICATOR_TIME              = 1;    -- 깃발전 남은 시간 ( 숫자 Second단위 );
INDICATOR_LEFT_NAME         = 2;    -- 왼쪽 상단 텍스트 박스 (팀 이름) ( 문자 (#xml로 변경 필요) );
INDICATOR_RIGHT_NAME        = 3;    -- 오른쪽 상단 텍스트 박스 (팀 이름) ( 문자 (#xml로 변경 필요) );
INDICATOR_LEFT_ICON         = 4;    -- 왼쪽 상단 앞 이미지 (레드팀 아이콘) ( 문자 XML keyword );
INDICATOR_RIGHT_ICON        = 5;    -- 오른쪽 상단 앞 이미지 (블루팀 아이콘) ( 문자 XML keyword );
INDICATOR_LEFT_SCORE        = 6;    -- 왼쪽 하단 텍스트 박스 (레드팀 점수) ( 숫자 );
INDICATOR_RIGHT_SCORE       = 7;    -- 오른쪽 하단 텍스트 박스 (블루팀 점수) ( 숫자 );
INDICATOR_LEFT_IMAGE        = 8;    -- 왼쪽 큰 이미지 (레드 깃발 현황 이미지) ( 문자 XML keyword ) ( + true => 이미지 깜빡임, false => 이미지 고정 ); 
INDICATOR_RIGHT_IMAGE       = 9;    -- 오른쪽쪽 큰 이미지 (블루 깃발 현황 이미지) ( 문자 XML keyword ) ( + true => 이미지 깜빡임, false => 이미지 고정 );
INDICATOR_OBJECT            = 10;   -- 왼쪽 최상단 텍스트 박스 깃발전 목표 ( 문자 (#xml로 변경 필요) );
------ 깃발전 설정 변수 ---------
BATTLE_MAP_ID = {189, 0};

-- 삭제하고자 하는 스킬 효과의 인덱스를 아래에 등록한다.
REMOVE_SKILLEFFECT = {

	16,		-- 은신
	36,		-- 데지미 흡수(무시)
	
}

--[[
-- 스킬 금지 리스트
FLAG_CLICK_FORBID_SKILL = { { 29, 1 },		-- 은신
							{ 29, 7 },
							{ 29, 8 },
							{ 29, 9 },
							{ 29, 15 },
							{ 34, 25 },
							{ 44, 12 },
							{ 45, 4 },
							{ 45, 12 },
							{ 55, 5 },
							{ 60, 3 },
							{ 68, 5 },		-- 변술부 백호 변장 (데미지 흡수 (무시))
							{ 68, 8 },		-- 변술부 거북이 변장 (데미지 흡수 (무시))
						};
FLAG_CLICI_FORBID_SKILL_NUM = 13;		-- 금지 스킬 갯수
]]--

						
GROUP_ID = {};

WINNER_REWARD1 = {320, 1, 1};			-- 승리 보상아이템 (훈장 320/1 1개)
WINNER_REWARD2 = {320, 1, 2};			-- 승리 보상아이템 (훈장 320/1 2개)
WINNER_REWARD3 = {320, 1, 3};			-- 승리 보상아이템 (훈장 320/1 3개)

LOSER_REWARD1 = {320, 1, 0};			-- 패배 보상아이템 (훈장 320/1 0개)
LOSER_REWARD2 = {320, 1, 1};			-- 패배 보상아이템 (훈장 320/1 1개)
LOSER_REWARD3 = {320, 1, 2};			-- 패배 보상아이템 (훈장 320/1 2개)

WINNER_REWARD_VIEW = 				-- 결과UI 승리 보상 출력용.
{
	{
		{320, 1, 1},
	},
	{
		{320, 1, 2},
	},
	{
		{320, 1, 3},
	},
	{
		{320, 1, 4},
	},
};
WIN_POINT   = 3;                    -- 승리 요구(목표) 점수;

FACT_RED    = 0;	                -- Red팀의  FactionID
FACT_BLUE   = 1;                    -- Blue팀의  FactionID
FACT_NONE   = 2;                    -- 무적팀의  FactionID

RESURRECT_TIME = 5;                 -- 강제 부활 대기 시간;

REWARD_TIME = 30;                   -- 자동 나가기 시간;
SAFE_TIME = 7;                      -- 부활 후 무적 시간;
INIT_TIME = 10;                      -- 게임 시작전 대기 시간;
AUTO_FLAG_RECALL_TIME = 15;          -- 자동 깃발 회수 시간;
PLAY_TIME = 1200;                    -- 깃발전 진행 시간;
ABUSE_TIME = 315;                    -- 어뷰징 판정 시간(핵,탈주를 악용한것으로 확실히 판명되는 시간); 변경할때 15초 정도 보정해줘야한다 (예: 5분 > 5분15초로 설정)
NOTIFY_GAMESTATE_TIME = 25;          -- 게임진행 도움말 공지 주기;
NOTIFY_GAME_END_TIME = {10,5,4,3,2,1};   -- 게임 종료 알림 주기;

CertifyMotion = {10, 41};			-- 인증 모션 ID
nChar_ViewRange = 550;				-- 깃발전 내 시야거리 값.

RED_START_POS = {630, 20}; --{31,25};      -- 레드팀의 입장시 최초 위치;
BLUE_START_POS = {-630, 20}; --{31,25};     -- 블루팀의 입장시 최초 위치

RED_FLAG_ZONE_POS = {739, 20};             -- 레드팀의 깃발지역 위치;
BLUE_FLAG_ZONE_POS = {-739, 20};            -- 블루팀의 깃발지역 위치;

FLAG_ZONE_RANGE_X = 20;             -- 깃발 지역의 판정 범위;
FLAG_ZONE_RANGE_Y = 20;             -- 깃발 지역의 판정 범위;

RED_FLAG_ID     = {100, 1};         -- 레드팀의 깃발 CrowData의 MID,SID
RED_ZONE_ID     = {100, 3};         -- 레드팀의 깃발지역 CrowData의 MID,SID
BLUE_FLAG_ID    = {100, 2};         -- 블루팀의 깃발 CrowData의 MID,SID
BLUE_ZONE_ID    = {100, 4};         -- 블루팀의 깃발지역 CrowData의 MID,SID

RED_FLAG_SKILL_ID = {54,3};         -- 레드팀의 깃발 획득시 스킬버프 MID,SID;
BLUE_FLAG_SKILL_ID = {54,4};        -- 블루팀의 깃발 획득시 스킬버프 MID,SID;

RED_TEAM_BUFF_ID = {54, 6};        	-- 레드팀의 입장 시 팀 구분버프 MID,SID;
BLUE_TEAM_BUFF_ID = {54, 7};        -- 블루팀의 입장 시 팀 구분버프 MID,SID;

RedFlag = nil;                      -- 레드팀의 깃발 Crow ID를 담는 변수;
RedDropFlag = nil;                  -- 레드팀의 드랍된 깃발 Crow ID를 담는 변수;
RedDropFlag_Timer = nil;            -- 레드팀의 드랍된 깃발의 자동 회수 타이머 변수;
RedFlagPlayer = nil;                -- 레드팀의 깃발을 탈취중인 Player DbNum을 담는 변수;

BlueFlag = nil;                     -- 블루팀의 깃발 Crow ID를 담는 변수;
BlueDropFlag = nil;                 -- 블루팀의 드랍된 깃발 Crow ID를 담는 변수;
BlueDropFlag_Timer = nil;           -- 블루팀의 드랍된 깃발의 자동 회수 타이머 변수;
BlueFlagPlayer = nil;               -- 블루팀의 깃발을 탈취중인 Player DbNum을 담는 변수;

nRedParty = nil;					-- 레드팀;
nRedPartyMasterID = nil;			-- 레드팀 파티장;
nBlueParty = nil;					-- 블루팀;
nBluePartyMasterID = nil;			-- 블루팀 파티장;

RedZone = nil;                      -- 레드팀의 깃발 Zone의 ActorID를 담는 변수;
BlueZone = nil;                     -- 블루팀의 깃발 Zone의 ActorID를 담는 변수;

RedTeamList = {}                    -- 레드팀의 팀원 명단 ( 재입장 처리에 사용 );
BlueTeamList = {}                   -- 블루팀의 팀원 명단 ( 재입장 처리에 사용 );

RedTeamPlayerList = {};             -- 레드팀 팀원이 플레이 상태 여부;
BlueTeamPlayerList = {};            -- 블루팀 팀원이 플레이 상태 여부;

Is_Alive = {};						-- 플레이어별 사망여부
PlayerScore = {};                   -- 플레이어들의 깃발 득점수를 담는 테이블;
PlayerFlagGain = {};				-- 플레이어들이 깃발 탈취(획득)수를 담는 테이블;
PlayerFlagReturn = {};				-- 플레이어들의 깃발 회수수를 담는 테이블;
PlayerKill = {};                    -- 플레이어들의 사살 수를 담는 테이블;
cPlayerKill = {};                   -- 전투기여도 계산에 사용되는 사살수 담는 테이블;
PlayerDeath = {};                   -- 플레이어들의 사망 수를 담는 테이블;
cPlayerDeath = {};                   -- 전투기여도 계산에 사용되는 사망수 담는 테이블;
PlayerDamage = {};					-- 플레이어들의 가한 데미지량을 담는 테이블;
PlayerHeal = {};					-- 플레이어들의 힐량을 담는 테이블;

PlayerContribute = {};				-- 플레이어들의 전투 참여도 계산하여 넣는 테이블;
PlayerProgressTimer = {}; 			-- 플레이어별 진행시간 테이블

RedTeamPoint = 0;                   -- 레드팀의 포인트 현황 변수;
BlueTeamPoint = 0;                  -- 레드팀의 포인트 현황 변수;

g_bRedTeam_Blink = false		-- 레드팀의 이미지 점멸
g_bBlueTeam_Blink = false		-- 블루팀의 이미지 점멸

GameStart = false;                  -- 게임 시작 여부;
GamePlaying = false;                -- 게임 진행 여부;

GUIDE_NONE = 0;
GUIDE_ZONE = 1;
GUIDE_DROP = 2;
GUIDE_PLAYER = 3;

RedTeamGuidRed = GUIDE_NONE;               -- 레드팀의 레드깃발 가이드;
RedTeamGuidBlue = GUIDE_NONE;              -- 레드팀의 블루깃발 가이드;
BlueTeamGuidRed = GUIDE_NONE;              -- 블루팀의 레드깃발 가이드;
BlueTeamGuidBlue = GUIDE_NONE;             -- 블루팀의 블루깃발 가이드;

Notify_Timer = nil;                 -- 게임진행을 위한 도움말 타이머 핸들값을 담는 변수;

GamePlayTimer = nil;

bAbuse = true;                      -- 최소 진행시간을 만족하는지에 대한 변수 (핵,탈주를 이용한 어뷰징으로 판정);
---------------------------------
function EventInitialize()	
	--RED_START_POS[1], RED_START_POS[2] = GetPosFromMapPos(BATTLE_MAP_ID[1],BATTLE_MAP_ID[2],32,18);		-- 맵(인게임)의 좌표를 통해 월드좌표를 가져옴(정확한 위치값 힘듬).
	--RED_START_POS[1] = 630;
	--RED_START_POS[2] = 20;
	--BLUE_START_POS[1], BLUE_START_POS[2] = GetPosFromMapPos(BATTLE_MAP_ID[1],BATTLE_MAP_ID[2],3,18);	-- 맵(인게임)의 좌표를 통해 월드좌표를 가져옴(정확한 위치값 힘듬).
	--BLUE_START_POS[1] = -630;
	--BLUE_START_POS[2] = 20;
	
    --RED_FLAG_ZONE_POS[1] = RED_START_POS[1] + 50;	 
	--RED_FLAG_ZONE_POS[2] = RED_START_POS[2];		
	--RED_FLAG_ZONE_POS[1] = 738;
	--RED_FLAG_ZONE_POS[2] = 20;
    
    --BLUE_FLAG_ZONE_POS[1] = -738;
	--BLUE_FLAG_ZONE_POS[2] = 20;
	--BLUE_FLAG_ZONE_POS[1] = BLUE_START_POS[1] + 50;
    --BLUE_FLAG_ZONE_POS[2] = BLUE_START_POS[2];
    
	RedFlag = DropCrow(BATTLE_MAP_ID[1], BATTLE_MAP_ID[2], RED_FLAG_ID[1], RED_FLAG_ID[2], RED_FLAG_ZONE_POS[1], RED_FLAG_ZONE_POS[2]);		-- 레드 깃발몹을 위치시킴
	RedZone = DropCrow(BATTLE_MAP_ID[1], BATTLE_MAP_ID[2], RED_ZONE_ID[1], RED_ZONE_ID[2], RED_FLAG_ZONE_POS[1], RED_FLAG_ZONE_POS[2]);		-- 레드 zone몹을 위치시킴
	
	BlueFlag = DropCrow(BATTLE_MAP_ID[1], BATTLE_MAP_ID[2], BLUE_FLAG_ID[1], BLUE_FLAG_ID[2], BLUE_FLAG_ZONE_POS[1], BLUE_FLAG_ZONE_POS[2]);	-- 블루 깃발몹을 위치시킴
	BlueZone = DropCrow(BATTLE_MAP_ID[1], BATTLE_MAP_ID[2], BLUE_ZONE_ID[1], BLUE_ZONE_ID[2], BLUE_FLAG_ZONE_POS[1], BLUE_FLAG_ZONE_POS[2]);	-- 블루 zone몹을 위치시킴
	
	RegistActorZoneTrigger(ACTOR_NPC, RedZone, TRIGGER_PC, FLAG_ZONE_RANGE_X, FLAG_ZONE_RANGE_Y);	-- Red Zone trigger 등록
	RegistActorZoneTrigger(ACTOR_NPC, BlueZone, TRIGGER_PC, FLAG_ZONE_RANGE_X, FLAG_ZONE_RANGE_Y);	-- Blue Zone trigger 등록
    
	SetHpVisible(true);
	
    RegistTimer( INIT_TIME, EVENT_GAME_START);		-- 게임시작타이머 등록
    RegistTimer( ABUSE_TIME, EVENT_GAME_ABUSE);		-- 어뷰징타이머 등록
	return true;
end

function EventRequestJoin(actorID)

	-- 진영이 설정되어 있는지 체크한다;
	-- 이곳에 체크되는 경우는 최초입장이라는 말이다;
	local FactionType, FactionID = GetFaction( ACTOR_PC, actorID )
	if ( FactionID == FACT_RED ) or ( FactionID == FACT_BLUE ) then
	
		return true
		
	end
	
	-- 최초입장이 아닌경우 아래를 체크한다;
	
	-- Red Team 인지 체크한다;
	for nKey, nActorID in pairs( RedTeamList ) do
	
		if ( nActorID == actorID ) then
			
			SetFaction( ACTOR_PC, actorID, FACTION_TEAM, FACT_RED )
			return true
			
		end
		
	end
	
	-- Blue Team 인지 체크한다;
	for nKey, nActorID in pairs( BlueTeamList ) do
	
		if ( nActorID == actorID ) then
			
			SetFaction( ACTOR_PC, actorID, FACTION_TEAM, FACT_BLUE )
			return true
			
		end
		
	end
	
	-- 팀에 속해있지 않은 유저는 입장을 거부한다;
	return false
	
end

function EventJoin(actorID)
	local FactionType, FactionID = GetFaction(ACTOR_PC, actorID);	--입장한 사람의 진영Type과 ID를 가져옴.
    Log_CaptureTheFlag_In(actorID, FactionID);  -- 입장 로그 ( 유저DBNum, 팀번호);
    
    if ( PlayerScore[actorID] == nil ) then
        PlayerScore[actorID] = 0;
    end
	if ( PlayerFlagGain[actorID] == nil ) then
        PlayerFlagGain[actorID] = 0;
    end
	if ( PlayerFlagReturn[actorID] == nil ) then
        PlayerFlagReturn[actorID] = 0;
    end
	if ( PlayerKill[actorID] == nil ) then
        PlayerKill[actorID] = 0;
    end
	if ( cPlayerKill[actorID] == nil ) then
        cPlayerKill[actorID] = 0;
    end
    if ( PlayerDeath[actorID] == nil ) then
        PlayerDeath[actorID] = 0;
    end
	if ( cPlayerDeath[actorID] == nil ) then
        cPlayerDeath[actorID] = 0;
    end
	if ( PlayerDamage[actorID] == nil ) then
        PlayerDamage[actorID] = 0;
    end
	if ( PlayerHeal[actorID] == nil ) then
        PlayerHeal[actorID] = 0;
    end
    if ( PlayerContribute[actorID] == nil ) then
        PlayerContribute[actorID] = 0;
    end
	if ( PlayerProgressTimer[actorID] == nil ) then		
        PlayerProgressTimer[actorID] = PLAY_TIME;
    end
	
	Is_Alive[actorID] = true;
	ServerLog( "IS_ALIVE : "..tostring( actorID ).."/"..tostring(Is_Alive[actorID]));
	-- 레드팀이라면 처리한다;
	if ( FactionID == FACT_RED ) then
	
		-- 파티가 없으면 파티를 생성한다;
		if ( ( nRedParty == nil ) and ( nRedPartyMasterID == nil ) ) then
			for nMemberID, bValid in pairs( RedTeamPlayerList ) do
				if ( bValid == true ) then
					-- 파티를 생성한다;
					JoinParty( nMemberID, actorID );
					
					-- 파티장 정보를 저장한다;
					nRedPartyMasterID = nMemberID;
					ServerLog( "RED Team | Create Party "..tostring( nMemberID ).." / "..tostring( actorID ) );
				end
			end
			
		-- 파티가 존재하는 상태이면 파티에 추가한다;
		elseif ( nRedParty ~= nil ) then
			JoinParty( nRedPartyMasterID, actorID );
			ServerLog( "RED Team | Join Party "..tostring( actorID ) );
		end
		
		-- 입장 인원 관리를 위해 추가한다;
		if ( RedTeamPlayerList[ actorID ] == nil ) then 	-- 최초 입장시 처리한다;
			table.insert( RedTeamList, actorID );
		end
		RedTeamPlayerList[ actorID ] = true;				-- 해당 참가자의 플레이 상태를 "가능"으로 변경한다;
		
		-- RED팀 스타트 포인트로 배치한다;
		return RED_START_POS[1], RED_START_POS[2];
	
	
	-- 블루팀이라면 처리한다;
	elseif ( FactionID == FACT_BLUE ) then
	
		-- 파티가 없으면 파티를 생성한다;
		if ( ( nBlueParty == nil ) and ( nBluePartyMasterID == nil ) ) then
			for nMemberID, bValid in pairs( BlueTeamPlayerList ) do
				if ( bValid == true ) then
					-- 파티를 생성한다;
					JoinParty( nMemberID, actorID );
					
					-- 파티장 정보를 저장한다;
					nBluePartyMasterID = nMemberID;
					ServerLog( "BLUE Team | Create Party "..tostring( nMemberID ).." / "..tostring( actorID ) );
				end
			end
			
		-- 파티가 존재하는 상태이면 파티에 추가한다;
		elseif ( nBlueParty ~= nil ) then
			JoinParty( nBluePartyMasterID, actorID );
			ServerLog( "BLUE Team | Join Party "..tostring( actorID ) );
		end
		
		-- 입장 인원 관리를 위해 추가한다;
		if ( BlueTeamPlayerList[ actorID ] == nil ) then 	-- 최초 입장시 처리한다;
			table.insert( BlueTeamList, actorID );
		end
		BlueTeamPlayerList[ actorID ] = true;				--해당 참가자의 플레이 상태를 "가능"으로 변경한다;
		
		-- BLUE팀 스타트 포인트로 배치한다;
		return BLUE_START_POS[1], BLUE_START_POS[2];
	end
	
end

function EventMapEnter(actorID)
	SetViewRange(actorID, nChar_ViewRange, PLAY_TIME);
	
	ActiveFactionChatting();  -- 진영 간 채팅 활성화
	
	-- 각 팀의 안전 진영과는 서로 아군이다;
	SetRelation( FACTION_SAFE, FACT_RED, FACTION_TEAM, FACT_RED, RELATION_ALLY );
	SetRelation( FACTION_TEAM, FACT_RED, FACTION_SAFE, FACT_RED, RELATION_ALLY );
	SetRelation( FACTION_SAFE, FACT_BLUE, FACTION_TEAM, FACT_BLUE, RELATION_ALLY );
	SetRelation( FACTION_TEAM, FACT_BLUE, FACTION_SAFE, FACT_BLUE, RELATION_ALLY );
	
	-- 각 팀은 서로 적이다;
	SetRelation( FACTION_TEAM, FACT_RED, FACTION_TEAM, FACT_BLUE, RELATION_ENEMY );		-- 팀 RED와 팀 BLUE는 적이다.
	SetRelation( FACTION_TEAM, FACT_BLUE, FACTION_TEAM, FACT_RED, RELATION_ENEMY );		-- 팀 BLUE와 팀 RED는 적이다.
	
					
	-- 다른 팀의 안전 진영과는 서로 중립적 아군이다;
	SetRelation( FACTION_TEAM, FACT_RED, FACTION_SAFE, FACT_BLUE, RELATION_NEUTRAL_ALLY );
	SetRelation( FACTION_SAFE, FACT_BLUE, FACTION_TEAM, FACT_RED, RELATION_NEUTRAL_ALLY );
	SetRelation( FACTION_TEAM, FACT_BLUE, FACTION_SAFE, FACT_RED, RELATION_NEUTRAL_ALLY );
	SetRelation( FACTION_SAFE, FACT_RED, FACTION_TEAM, FACT_BLUE, RELATION_NEUTRAL_ALLY );
					
	-- 안전 진영간에는 서로 중립적 아군이다;
	SetRelation( FACTION_SAFE, FACT_RED, FACTION_SAFE, FACT_BLUE, RELATION_NEUTRAL_ALLY );
	SetRelation( FACTION_SAFE, FACT_BLUE, FACTION_SAFE, FACT_RED, RELATION_NEUTRAL_ALLY );
	
	
	ServerLog("MapEnter"..tostring(actorID));
	
	if ( GameStart == false ) then		-- 게임 시작(타이머)상태가 아니면
		PrintSimpleXMLMsgAll(8, {"PVP_CTFLAG_SYSTEM_MESSAGE", 0, INIT_TIME}, COLOR_WHITE);	-- 화면중단 메시지 출력 PVP_CTFLAG_SYSTEM_MESSAGE, 0
		PrintChatXMLMsg(actorID, {"PVP_CTFLAG_SYSTEM_MESSAGE", 0, INIT_TIME}, COLOR_BLUE);	-- 채팅 메시지 출력 PVP_CTFLAG_SYSTEM_MESSAGE, 0
    	local FactionType, FactionID = GetFaction(ACTOR_PC, actorID);	--입장한 사람의 진영Type과 ID를 가져옴.
        if ( FactionID == FACT_RED ) or ( FactionID == FACT_BLUE ) then	-- RED팀 or Blue팀이면
	--	if ( FactionID == FACT_RED ) then	-- RED팀 or Blue팀이면
           SetMoveLock(ACTOR_PC, actorID, false);	--이동 불가 상태
			--PrintSimpleMsgFaction(FactionType, FactionID, 8, "You're RED TEAM");
	--		PrintSimpleXMLMsgFaction(FactionType, FactionID, 8,{"PVP_CTFLAG_SYSTEM_MESSAGE", 15}, COLOR_WHITE);	-- 화면중단 메시지 출력 PVP_CTFLAG_SYSTEM_MESSAGE, 15			
	--	elseif (FactionID == FACT_BLUE) then
	--		SetMoveLock(ACTOR_PC, actorID, false);	--이동 불가 상태
			--PrintSimpleMsgFaction(FactionType, FactionID, 8, "You're BLUE TEAM");
	--		PrintSimpleXMLMsgFaction(FactionType, FactionID, 8,{"PVP_CTFLAG_SYSTEM_MESSAGE", 16}, COLOR_WHITE);	-- 화면중단 메시지 출력 PVP_CTFLAG_SYSTEM_MESSAGE, 16
		ServerLog("MapSTART"..tostring(actorID));	
        end
    elseif ( GamePlaying == true ) then			--재입장
		--ServerLog("MapPlaying"..tostring(actorID));
		PlayerProgressTimer[actorID] = PlayerProgressTimer[actorID] + GetLeftTime(GamePlayTimer);		-- 재입장 시 진행시간에 남은시간을 플러스
		--ServerLog("ReJoin _ PlayTime: "..tostring(PlayerProgressTimer[actorID]).." Name: "..tostring(GetName(FactionType, actorID)));	
	    functionIndicator(actorID);
		
		UI_Navagation_On( actorID )
    end
end

function EventOut(actorID, toMapMID, toMapSID)
    local FactionType, FactionID = GetFaction(ACTOR_PC, actorID);	--입장한 사람의 진영Type과 ID를 가져옴.
    Log_CaptureTheFlag_Out(actorID, FactionID);  -- 퇴장 로그 ( 유저DBNum, 팀번호);
	
	if ( GamePlayTimer == nil ) then
		PlayerProgressTimer[actorID] = 0
	else
		PlayerProgressTimer[actorID] = PlayerProgressTimer[actorID] - GetLeftTime(GamePlayTimer);			-- 맵이탈 시 진행시간에서 남은시간을 마이너스
	end
	
	UI_Navagation_Off( actorID )
	
	--ServerLog("OUT _ PlayTime: "..tostring(PlayerProgressTimer[actorID]).." Name: "..tostring(GetName(FactionType, actorID)));	
	
	if ( FactionID == FACT_RED ) then
        --SetResurrectionForcedInstancePosition(nDieActorID, BATTLE_MAP_ID[1], BATTLE_MAP_ID[2], RED_START_POS[1], RED_START_POS[2], RESURRECT_TIME, 100);	--레드팀 부활
		--SetResurrectionForcedInstancePosition(nDieActorID, BATTLE_MAP_ID[1], BATTLE_MAP_ID[2], 630, 20, RESURRECT_TIME, 100);	--레드팀 부활
        --SetResurrectionForcedInstance(nDieActorID, BATTLE_MAP_ID[1], BATTLE_MAP_ID[2], 0, RESURRECT_TIME, 100);

        local haveSkill = IsHaveSkillFact(ACTOR_PC, actorID, BLUE_FLAG_SKILL_ID[1], BLUE_FLAG_SKILL_ID[2]);
        if ( haveSkill == true ) then
            RemoveSkillFact(ACTOR_PC, actorID, BLUE_FLAG_SKILL_ID[1], BLUE_FLAG_SKILL_ID[2]);
            local PosX,PosY,PosZ = GetPosition(ACTOR_PC, actorID);
            BlueDropFlag = DropCrow(BATTLE_MAP_ID[1], BATTLE_MAP_ID[2], BLUE_FLAG_ID[1], BLUE_FLAG_ID[2], PosX, PosZ);
            BlueDropFlag_Timer = RegistTimer( AUTO_FLAG_RECALL_TIME, EVENT_FLAG_RECALL, FACT_BLUE );
            
            if ( Notify_Timer ~= nil ) then
                RemoveTimer(Notify_Timer);
                Notify_Timer = RegistTimer(NOTIFY_GAMESTATE_TIME,EVENT_NOTIFY_GAMESTATE);
            end
            
            RedTeamGuidBlue = GUIDE_DROP;
            BlueTeamGuidBlue = GUIDE_DROP;
            functionGuideArrow();
        end
    
    elseif ( FactionID == FACT_BLUE ) then
        --SetResurrectionForcedInstancePosition(nDieActorID, BATTLE_MAP_ID[1], BATTLE_MAP_ID[2], BLUE_START_POS[1], BLUE_START_POS[2], RESURRECT_TIME, 100);	-- 부활위치
		--SetResurrectionForcedInstancePosition(nDieActorID, BATTLE_MAP_ID[1], BATTLE_MAP_ID[2], -630, 20, RESURRECT_TIME, 100);	-- 부활위치
        --SetResurrectionForcedInstance(nDieActorID, BATTLE_MAP_ID[1], BATTLE_MAP_ID[2], 0, RESURRECT_TIME, 100);

        local haveSkill = IsHaveSkillFact(ACTOR_PC, actorID, RED_FLAG_SKILL_ID[1], RED_FLAG_SKILL_ID[2]);
        if ( haveSkill == true ) then
            RemoveSkillFact(ACTOR_PC, actorID, RED_FLAG_SKILL_ID[1], RED_FLAG_SKILL_ID[2]);
            local PosX,PosY,PosZ = GetPosition(ACTOR_PC, actorID);
            RedDropFlag = DropCrow(BATTLE_MAP_ID[1], BATTLE_MAP_ID[2], RED_FLAG_ID[1], RED_FLAG_ID[2], PosX, PosZ);
            RedDropFlag_Timer = RegistTimer( AUTO_FLAG_RECALL_TIME, EVENT_FLAG_RECALL, FACT_RED );
            
            if ( Notify_Timer ~= nil ) then
                RemoveTimer(Notify_Timer);
                Notify_Timer = RegistTimer(NOTIFY_GAMESTATE_TIME,EVENT_NOTIFY_GAMESTATE);
            end
            
            RedTeamGuidRed = GUIDE_DROP;
            BlueTeamGuidRed = GUIDE_DROP;
            functionGuideArrow();
        end
	end
	
	ServerLog("MAP OUT"..tostring(actorID));
    OutParty(actorID);
	
    SetMoveLock(ACTOR_PC, actorID, true);
    UI_Indicator(actorID, INDICATOR_SHOW, false);			--현황UI를 제거.
	UI_FlagGuideArrow_Off_User( actorID )
	
    local _isTeamRed = false; -- Red팀인지 여부;
    local _isTeamBlue = false; -- Blue팀인지 여부;
    
	
    for key,value in pairs( RedTeamList ) do	-- 나간 유저가 레드팀인지 검사;
        if ( value == actorID ) then	-- 리스트에 값이 있으면
            _isTeamRed = true;	-- 레드팀에 속해있다.
            RedTeamPlayerList[actorID] = false;	-- 플레이 상태를 "불가능"으로 변경
			PrintChatXMLMsgFaction(FACTION_TEAM, FACT_RED, {"PVP_CTFLAG_SYSTEM_MESSAGE", 13, GetName(ACTOR_PC, actorID)}, COLOR_RED);	-- PVP_CTFLAG_SYSTEM_MESSAGE, 13
        end
    end
    if ( _isTeamRed == false ) then
        for key,value in pairs( BlueTeamList ) do	-- 나간 유저가 블루팀인지 검사;
            if ( value == actorID ) then	-- 리스트에 값이 있으면
                _isTeamBlue = true;	-- 블루팀에 속해있다.
                BlueTeamPlayerList[actorID] = false;	-- 플레이 상태를 "불가능"으로 변경
				PrintChatXMLMsgFaction(FACTION_TEAM, FACT_BLUE,  {"PVP_CTFLAG_SYSTEM_MESSAGE", 13, GetName(ACTOR_PC, actorID)}, COLOR_RED);	-- PVP_CTFLAG_SYSTEM_MESSAGE, 13
            end
        end
    end
    
    local RedTeamExist = 0;
    local BlueTeamExist = 0;    
    for key,value in pairs( RedTeamPlayerList ) do  -- Red팀원이 한명이라도 존재하는지를 검사;
        if ( value == true ) then
            RedTeamExist = RedTeamExist + 1;
        end
    end
    for key,value in pairs( BlueTeamPlayerList ) do  -- Blue팀원이 한명이라도 존재하는지를 검사;
        if ( value == true ) then
            BlueTeamExist = BlueTeamExist + 1;
        end
    end
    
    -- 팀원의 수가 1명만 남으면 파티가 해제된다;
	if ( RedTeamExist == 1 ) then
		nRedParty = nil;
		nRedPartyMasterID = nil;
		ServerLog( "RED Team | Destroy Party " );
	end
	
	if ( BlueTeamExist == 1 ) then
		nBlueParty = nil;
		nBluePartyMasterID = nil;
		ServerLog( "BLUE Team | Destroy Party " );
	end
    
    if ( RedTeamExist > 0 ) and ( BlueTeamExist == 0 ) then	-- 블루팀이 모두 나가면
        functionResult(FACT_RED);	-- 결과계산
    elseif ( RedTeamExist == 0 ) and ( BlueTeamExist > 0 ) then	-- 레드팀이 모두 나가면
        functionResult(FACT_BLUE);	-- 결과계산
    end
    
    if ( RedTeamExist == 0 ) and ( BlueTeamExist == 0 ) then	-- 레드/블루팀 모두 없다면
		InactiveFactionChatting();  -- 진영 간 채팅 비활성화
        DoDestroy();	-- 인던 파괴
    end
end

function EventTimer(nTimerHandle, nParam0, nParam1, nParam2, nParam3)  -- 타이머가 정한 시간이 되었을때 실행되는 부분;
    if ( nParam0 == EVENT_REWARD ) then	-- EVENT_REWARD 이벤트 호출
		InactiveFactionChatting();  -- 진영 간 채팅 비활성화
        functionReward(nParam1);	-- 보상진행
        RegistTimer(REWARD_TIME, EVENT_DESTROY); -- REWARD_TIME후 EventTimer호출 (30초 후 인던 파괴);
    elseif ( nParam0 == EVENT_DESTROY ) then	-- EVENT_DESTROY 이벤트 호출
        DoDestroy();	-- 인던 삭제
		
    elseif ( nParam0 == EVENT_SAFE ) then	-- EVENT_SAFE 이벤트 호출
		Is_Alive[nParam1] = true;
		ServerLog( "IS_ALIVE : "..tostring( nParam1 ).."/"..tostring(Is_Alive[nParam1]));
        SetFaction(ACTOR_PC, nParam1, FACTION_TEAM, nParam2);	-- 팩션을 (nParam1 = User ID, nParam2 = 팩션ID) SAFE에서 원래진영으로 변경, SAFE_TIME이 지난뒤에.
		functionGuideArrow();
		
		if ( nParam2 == FACT_RED) then	--죽은사람이 레드팀이면
			ApplySkill(ACTOR_PC, nParam1, RED_TEAM_BUFF_ID[1], RED_TEAM_BUFF_ID[2], 0, 9999 );		-- 캐릭터 RED팀버프 적용
		elseif (nParam2 == FACT_BLUE) then	--죽은사람이 블루팀이면
			ApplySkill(ACTOR_PC, nParam1, BLUE_TEAM_BUFF_ID[1], BLUE_TEAM_BUFF_ID[2], 0, 9999 );		-- 캐릭터 BLUE팀버프 적용
		end
		
       
    elseif ( nParam0 == EVENT_GAME_START ) then	-- ENENT_GAME_START 이벤트 호출
        for key,value in pairs( RedTeamList ) do	-- 레드팀 모두 MoveLock 해제
			SetMoveLock(ACTOR_PC, value, true);
			ApplySkill(ACTOR_PC, value, RED_TEAM_BUFF_ID[1], RED_TEAM_BUFF_ID[2], 0, 9999 );		-- 캐릭터 RED팀버프 적용
			--ApplySkill(nClickerType, nClickerID, BLUE_FLAG_SKILL_ID[1], BLUE_FLAG_SKILL_ID[2], 2, 9999);
			
			UI_Navagation_On( value )
		end
        for key,value in pairs( BlueTeamList ) do	-- 블루팀 모두 MoveLock 해제
			SetMoveLock(ACTOR_PC, value, true);
			ApplySkill(ACTOR_PC, value, BLUE_TEAM_BUFF_ID[1], BLUE_TEAM_BUFF_ID[2], 0, 9999 );		-- 캐릭터 BLUE팀버프 적용
			
			UI_Navagation_On( value )
		end
        GamePlayTimer = RegistTimer(PLAY_TIME, EVENT_GAME_END);	-- 플레이 타이머 등록.
        GameStart = true;
        GamePlaying = true;
        
        -- 깃발 유도 화살표 표시;
        RedTeamGuidRed = GUIDE_ZONE;
        RedTeamGuidBlue = GUIDE_ZONE;
        BlueTeamGuidRed = GUIDE_ZONE;
        BlueTeamGuidBlue = GUIDE_ZONE;
        functionGuideArrow();
        
        -- 게임 진행 도움말 공지 타이머 등록;
        Notify_Timer = RegistTimer(NOTIFY_GAMESTATE_TIME, EVENT_NOTIFY_GAMESTATE);
        
        UI_CDM_BattleStart();
        -- 게임 스타트 알림 메시지;
        RegistTimer(7, EVENT_UI_START_1);
        
        -- 게임 종료 알림 메시지 등록;
        for key,value in pairs( NOTIFY_GAME_END_TIME ) do
            RegistTimer(PLAY_TIME - (value*60), EVENT_NOTIFY_GAMEEND, value);
        end
        
        --bAbuse = false;
        --functionResult(FACT_RED);
       
    elseif ( nParam0 == EVENT_GAME_END ) then	-- EVENT_GAME_END 이벤트 호출
		if( bAbuse == false ) then
			if( RedTeamPoint > BlueTeamPoint ) then
				functionResult(FACT_RED);	-- 게임이 종료되고 레드팀 점수가 더 높으면 레드팀 승리
			elseif ( BlueTeamPoint > RedTeamPoint ) then 
				functionResult(FACT_BLUE);	-- 게임이 종료되고 블루팀 점수가 더 높으면 블루팀 승리
			elseif ( BlueTeamPoint == RedTeamPoint ) then 
				functionResult(FACT_NONE);	-- 게임이 종료되면 결과창 
			end
		else
			functionResult(FACT_NONE);	-- 게임이 종료되면 결과창 
		end
			
		
        
    elseif ( nParam0 == EVENT_GAME_ABUSE) then
        bAbuse = false;
        
    elseif ( nParam0 == EVENT_FLAG_RECALL ) then  -- 깃발 자동 회수;
        if ( nParam1 == FACT_RED ) then
            if ( RedDropFlag ~= nil ) then                
                SetHP(ACTOR_NPC, RedDropFlag, 0);	-- 깃발제거
                local x,y,z = GetPosition(ACTOR_NPC, RedDropFlag);
                DoEffectPositionAll("sum_human.egp", BATTLE_MAP_ID[1], BATTLE_MAP_ID[2], x, y, z);
                RedDropFlag = nil;
                
                RedFlag = DropCrow(BATTLE_MAP_ID[1], BATTLE_MAP_ID[2], RED_FLAG_ID[1], RED_FLAG_ID[2], RED_FLAG_ZONE_POS[1], RED_FLAG_ZONE_POS[2]);
                RedTeamGuidRed = GUIDE_ZONE;
                BlueTeamGuidRed = GUIDE_ZONE;
                functionGuideArrow();
                
				g_bRedTeam_Blink = false
                UI_Indicator_All(INDICATOR_LEFT_IMAGE, "CTF_INDICATOR_RED_FLAG", g_bRedTeam_Blink);
            end
            
        elseif ( nParam1 == FACT_BLUE ) then
            if ( BlueDropFlag ~= nil ) then
                SetHP(ACTOR_NPC, BlueDropFlag, 0);
                local x,y,z = GetPosition(ACTOR_NPC, BlueDropFlag);
                DoEffectPositionAll("sum_human.egp", BATTLE_MAP_ID[1], BATTLE_MAP_ID[2], x, y, z);
                BlueDropFlag = nil;
                
                BlueFlag = DropCrow(BATTLE_MAP_ID[1], BATTLE_MAP_ID[2], BLUE_FLAG_ID[1], BLUE_FLAG_ID[2], BLUE_FLAG_ZONE_POS[1], BLUE_FLAG_ZONE_POS[2]);
                RedTeamGuidBlue = GUIDE_ZONE;
                BlueTeamGuidBlue = GUIDE_ZONE;
                functionGuideArrow();
                
				g_bBlueTeam_Blink = false
                UI_Indicator_All(INDICATOR_RIGHT_IMAGE, "CTF_INDICATOR_BLUE_FLAG", g_bBlueTeam_Blink);
            end
            
        end
    elseif ( nParam0 == EVENT_NOTIFY_GAMESTATE ) then
        local RedExist = ((RedFlag ~= nil) or (RedDropFlag ~= nil)) ;  -- 깃발존이나 드랍된 깃발이 있다면;
        local BlueExist = ((BlueFlag ~= nil) or (BlueDropFlag ~= nil)) ;  -- 깃발존이나 드랍된 깃발이 있다면;
        
        if ( (RedExist == true) and (BlueExist == true) ) then  -- 깃발이 전부 진영에 있거나 드랍되어 있을 때;
            PrintSimpleXMLMsgAll(3,{"PVP_CTFLAG_SYSTEM_MESSAGE", 4}, COLOR_WHITE);	-- PVP_CTFLAG_SYSTEM_MESSAGE, 4
            
        elseif ( (RedExist == false) and (BlueExist == false) ) then  -- 깃발이 둘 다 탈취당하여 있을 때;
            PrintSimpleXMLMsgAll(3,{"PVP_CTFLAG_SYSTEM_MESSAGE", 5}, COLOR_WHITE);	-- PVP_CTFLAG_SYSTEM_MESSAGE, 5
        else  --
            if (RedExist == false) then -- 레드팀만 탈취 당하였을때;
                --PrintSimpleXMLMsgFaction(FACT_RED, FACTION_TEAM, 3, {"PVP_CTFLAG_SYSTEM_MESSAGE", 6}, COLOR_WHITE);	-- PVP_CTFLAG_SYSTEM_MESSAGE, 6
				--PrintSimpleXMLMsgFaction(FACT_BLUE, FACTION_TEAM, 3, {"PVP_CTFLAG_SYSTEM_MESSAGE", 7}, COLOR_WHITE); -- PVP_CTFLAG_SYSTEM_MESSAGE, 7
				PrintSimpleXMLMsgFaction(FACTION_TEAM, FACT_RED, 3, {"PVP_CTFLAG_SYSTEM_MESSAGE", 6}, COLOR_RED);	-- PVP_CTFLAG_SYSTEM_MESSAGE, 6
				PrintChatXMLMsgFaction(FACTION_TEAM, FACT_RED, {"PVP_CTFLAG_SYSTEM_MESSAGE", 6}, COLOR_RED);	-- PVP_CTFLAG_SYSTEM_MESSAGE, 6
                PrintSimpleXMLMsgFaction(FACTION_TEAM, FACT_BLUE, 3, {"PVP_CTFLAG_SYSTEM_MESSAGE", 7}, COLOR_WHITE); -- PVP_CTFLAG_SYSTEM_MESSAGE, 7
				PrintChatXMLMsgFaction(FACTION_TEAM, FACT_BLUE, {"PVP_CTFLAG_SYSTEM_MESSAGE", 7}, COLOR_WHITE);	-- PVP_CTFLAG_SYSTEM_MESSAGE, 6
                
            else  -- 블루팀만 탈취 당하였을때;
                --PrintSimpleXMLMsgFaction(FACT_RED, FACTION_TEAM, 3, {"PVP_CTFLAG_SYSTEM_MESSAGE", 7}, COLOR_WHITE);	-- PVP_CTFLAG_SYSTEM_MESSAGE, 7
                --PrintSimpleXMLMsgFaction(FACT_BLUE, FACTION_TEAM, 3, {"PVP_CTFLAG_SYSTEM_MESSAGE", 6}, COLOR_WHITE);	-- PVP_CTFLAG_SYSTEM_MESSAGE, 6
				PrintSimpleXMLMsgFaction(FACTION_TEAM, FACT_RED, 3, {"PVP_CTFLAG_SYSTEM_MESSAGE", 7}, COLOR_WHITE);	-- PVP_CTFLAG_SYSTEM_MESSAGE, 6
				PrintChatXMLMsgFaction(FACTION_TEAM, FACT_RED, {"PVP_CTFLAG_SYSTEM_MESSAGE", 7}, COLOR_WHITE);	-- PVP_CTFLAG_SYSTEM_MESSAGE, 6
                PrintSimpleXMLMsgFaction(FACTION_TEAM, FACT_BLUE, 3, {"PVP_CTFLAG_SYSTEM_MESSAGE", 6}, COLOR_RED); -- PVP_CTFLAG_SYSTEM_MESSAGE, 7
				PrintChatXMLMsgFaction(FACTION_TEAM, FACT_BLUE, {"PVP_CTFLAG_SYSTEM_MESSAGE", 6}, COLOR_RED);	-- PVP_CTFLAG_SYSTEM_MESSAGE, 6
                
            end
        end
        
        Notify_Timer = RegistTimer(NOTIFY_GAMESTATE_TIME,EVENT_NOTIFY_GAMESTATE);
        
    elseif (nParam0 == EVENT_NOTIFY_GAMEEND ) then
        PrintSimpleXMLMsgAll(3,{"PVP_CTFLAG_SYSTEM_MESSAGE", 8, nParam1}, COLOR_WHITE);	-- PVP_CTFLAG_SYSTEM_MESSAGE, 8
        
    elseif ( nParam0 == EVENT_UI_START_1 ) then
        UI_Indicator_All(INDICATOR_TIME         , GetLeftTime(GamePlayTimer));
		for key,value in pairs( RedTeamList ) do	-- 레드팀은 현황UI의 팀표시를 다르게.
			UI_Indicator(value, INDICATOR_LEFT_NAME    , ID2SERVERTEXT("PVP_CTFLAG_UI_TEXT", 1), COLOR_GREEN);
			UI_Indicator(value, INDICATOR_RIGHT_NAME   , ID2SERVERTEXT("PVP_CTFLAG_UI_TEXT", 2));
		end
        for key,value in pairs( BlueTeamList ) do	-- 블루팀은 현황UI의 팀표시를 다르게.
			UI_Indicator(value, INDICATOR_LEFT_NAME    , ID2SERVERTEXT("PVP_CTFLAG_UI_TEXT", 1));
			UI_Indicator(value, INDICATOR_RIGHT_NAME   , ID2SERVERTEXT("PVP_CTFLAG_UI_TEXT", 2), COLOR_GREEN);
		end
		UI_Indicator_All(INDICATOR_LEFT_ICON    , "CTF_INDICATOR_RED_ICON");
        UI_Indicator_All(INDICATOR_RIGHT_ICON   , "CTF_INDICATOR_BLUE_ICON");
        UI_Indicator_All(INDICATOR_LEFT_SCORE   , RedTeamPoint);
        UI_Indicator_All(INDICATOR_RIGHT_SCORE  , BlueTeamPoint);
        UI_Indicator_All(INDICATOR_LEFT_IMAGE   , "CTF_INDICATOR_RED_FLAG", g_bRedTeam_Blink);
        UI_Indicator_All(INDICATOR_RIGHT_IMAGE  , "CTF_INDICATOR_BLUE_FLAG", g_bBlueTeam_Blink);
        UI_Indicator_All(INDICATOR_OBJECT       , TableToString{"PVP_CTFLAG_UI_TEXT", 0, WIN_POINT});
        UI_Indicator_All(INDICATOR_SHOW         , true);
        
        --PrintSimpleMsgAll(4,"GamStart");	-- PVP_CTFLAG_SYSTEM_MESSAGE, 1
		PrintSimpleXMLMsgAll(3,{"PVP_CTFLAG_SYSTEM_MESSAGE", 1}, COLOR_WHITE);
        RegistTimer(3,EVENT_UI_START_2);
    elseif ( nParam0 == EVENT_UI_START_2 ) then
        --PrintSimpleMsgAll(4,"Attack Plz");	-- PVP_CTFLAG_SYSTEM_MESSAGE, 2
		PrintSimpleXMLMsgAll(5,{"PVP_CTFLAG_SYSTEM_MESSAGE", 2}, COLOR_WHITE);
        RegistTimer(5,EVENT_UI_START_3);
    elseif ( nParam0 == EVENT_UI_START_3 ) then
		PrintSimpleXMLMsgAll(5,{"PVP_CTFLAG_SYSTEM_MESSAGE", 3}, COLOR_WHITE);
        --PrintSimpleMsgAll(4,"Goal 3 Point");	-- PVP_CTFLAG_SYSTEM_MESSAGE, 3
        
	elseif ( nParam0 == EVENT_FLAG_GET ) then    
		
		
		for key, value in pairs ( REMOVE_SKILLEFFECT ) do
			
			-- 삭제 스킬 효과 인덱스에 해당하는 버프를 지운다.
			RemoveSkillFact_SkillEffect ( nParam1, nParam2, REMOVE_SKILLEFFECT[key] )
			
		end
		
    end
    
end

function EventDie(nDieActorType, nDieActorID, nKillActorType, nKillActorID)  -- 어떠한 PC,MOB이 죽었을 때 실행되는 부분;
    local factionType, factionID = GetFaction(nDieActorType, nDieActorID);
	
	Is_Alive[nDieActorID] = false;
	ServerLog( "IS_ALIVE : "..tostring( nDieActorID ).."/"..tostring(Is_Alive[nDieActorID]));
    if ( factionID == FACT_RED ) then
        SetResurrectionForcedInstancePosition(nDieActorID, BATTLE_MAP_ID[1], BATTLE_MAP_ID[2], RED_START_POS[1], RED_START_POS[2], RESURRECT_TIME, 100);	--레드팀 부활
		--SetResurrectionForcedInstancePosition(nDieActorID, BATTLE_MAP_ID[1], BATTLE_MAP_ID[2], 630, 20, RESURRECT_TIME, 100);	--레드팀 부활
        --SetResurrectionForcedInstance(nDieActorID, BATTLE_MAP_ID[1], BATTLE_MAP_ID[2], 0, RESURRECT_TIME, 100);
			
        local haveSkill = IsHaveSkillFact(nDieActorType, nDieActorID, BLUE_FLAG_SKILL_ID[1], BLUE_FLAG_SKILL_ID[2]);
        if ( haveSkill == true ) then
            RemoveSkillFact(nDieActorType, nDieActorID, BLUE_FLAG_SKILL_ID[1], BLUE_FLAG_SKILL_ID[2]);
            local PosX,PosY,PosZ = GetPosition(nDieActorType, nDieActorID);
            BlueDropFlag = DropCrow(BATTLE_MAP_ID[1], BATTLE_MAP_ID[2], BLUE_FLAG_ID[1], BLUE_FLAG_ID[2], PosX, PosZ);
            BlueDropFlag_Timer = RegistTimer( AUTO_FLAG_RECALL_TIME, EVENT_FLAG_RECALL, FACT_BLUE );
            
            if ( Notify_Timer ~= nil ) then
                RemoveTimer(Notify_Timer);
                Notify_Timer = RegistTimer(NOTIFY_GAMESTATE_TIME,EVENT_NOTIFY_GAMESTATE);
            end
            
            RedTeamGuidBlue = GUIDE_DROP;
            BlueTeamGuidBlue = GUIDE_DROP;
            functionGuideArrow();
        end
    
    elseif ( factionID == FACT_BLUE ) then
        SetResurrectionForcedInstancePosition(nDieActorID, BATTLE_MAP_ID[1], BATTLE_MAP_ID[2], BLUE_START_POS[1], BLUE_START_POS[2], RESURRECT_TIME, 100);	-- 부활위치
		--SetResurrectionForcedInstancePosition(nDieActorID, BATTLE_MAP_ID[1], BATTLE_MAP_ID[2], -630, 20, RESURRECT_TIME, 100);	-- 부활위치
        --SetResurrectionForcedInstance(nDieActorID, BATTLE_MAP_ID[1], BATTLE_MAP_ID[2], 0, RESURRECT_TIME, 100);
			
        local haveSkill = IsHaveSkillFact(nDieActorType, nDieActorID, RED_FLAG_SKILL_ID[1], RED_FLAG_SKILL_ID[2]);
        if ( haveSkill == true ) then
            RemoveSkillFact(nDieActorType, nDieActorID, RED_FLAG_SKILL_ID[1], RED_FLAG_SKILL_ID[2]);
            local PosX,PosY,PosZ = GetPosition(nDieActorType, nDieActorID);
            RedDropFlag = DropCrow(BATTLE_MAP_ID[1], BATTLE_MAP_ID[2], RED_FLAG_ID[1], RED_FLAG_ID[2], PosX, PosZ);
            RedDropFlag_Timer = RegistTimer( AUTO_FLAG_RECALL_TIME, EVENT_FLAG_RECALL, FACT_RED );
            
            if ( Notify_Timer ~= nil ) then
                RemoveTimer(Notify_Timer);
                Notify_Timer = RegistTimer(NOTIFY_GAMESTATE_TIME,EVENT_NOTIFY_GAMESTATE);
            end
            
            RedTeamGuidRed = GUIDE_DROP;
            BlueTeamGuidRed = GUIDE_DROP;
            functionGuideArrow();
        end
        
    end
    
    if ( nDieActorType == ACTOR_PC ) and ( nKillActorType == ACTOR_PC ) then
        PlayerKill[nKillActorID] = PlayerKill[nKillActorID] + 1;        
        PlayerDeath[nDieActorID] = PlayerDeath[nDieActorID] + 1;
        
    end
        
	return true;
end

function EventResurrect(nResurrectType, nResurretActorType, nResurrectActorID, nKillActorType, nKillActorID)
    local factionType, factionID = GetFaction(nResurretActorType, nResurrectActorID);
    RegistTimer(SAFE_TIME, EVENT_SAFE, nResurrectActorID, factionID);    -- SAFE_TIME초 뒤에 nResurrectActorID를 factionID팀으로 복귀를 예약;
    SetFaction( ACTOR_PC, nResurrectActorID, FACTION_SAFE, factionID );	-- 사망자의 진영을 SAFE로 변경.
	return true;
end

function EventActorTriggerIn(nTriggerType, nTriggerID, nInnerType, nInnerID)
    local factionType, factionID = GetFaction(nInnerType, nInnerID);
    if ( factionID == FACT_RED ) and ( nTriggerID == RedZone ) then  -- 레드팀이 레드팀 깃발 골인지역으로 진입했을 때;
        local haveSkill = IsHaveSkillFact(nInnerType, nInnerID, BLUE_FLAG_SKILL_ID[1], BLUE_FLAG_SKILL_ID[2]);  
        if ( haveSkill == true ) and ( RedFlag ~= nil ) then  -- 블루팀의 깃발을 소유하고 있으면서 레드팀의 깃발이 깃발지역에 있다면;
			--local x,y,z = GetPosition(ACTOR_PC, nInnerID);
			DoEffectPositionAll("cwz_gate_red1.egp", BATTLE_MAP_ID[1], BATTLE_MAP_ID[2], RED_FLAG_ZONE_POS[1], 0, RED_FLAG_ZONE_POS[2]);
		
            RedTeamPoint = RedTeamPoint + 1;
            
            --if ( PlayerScore[nInnerID] == nil ) then
            --    PlayerScore[nInnerID] = 1;
            --else
                PlayerScore[nInnerID] = PlayerScore[nInnerID] + 1;
            --end
            
            PrintSimpleXMLMsgAll(3, {"PVP_CTFLAG_SYSTEM_MESSAGE", 9}, COLOR_GREEN);	-- PVP_CTFLAG_SYSTEM_MESSAGE, 9
            RemoveSkillFact(nInnerType, nInnerID, BLUE_FLAG_SKILL_ID[1], BLUE_FLAG_SKILL_ID[2]);
            BlueFlag = DropCrow(BATTLE_MAP_ID[1], BATTLE_MAP_ID[2], BLUE_FLAG_ID[1], BLUE_FLAG_ID[2], BLUE_FLAG_ZONE_POS[1], BLUE_FLAG_ZONE_POS[2]);
            
            if ( Notify_Timer ~= nil ) then
                RemoveTimer(Notify_Timer);
                Notify_Timer = RegistTimer(NOTIFY_GAMESTATE_TIME,EVENT_NOTIFY_GAMESTATE);
            end
            
            RedTeamGuidBlue = GUIDE_ZONE;
            BlueTeamGuidBlue = GUIDE_ZONE;
            functionGuideArrow();
            
			--ServerLog("RedTeamPoint"..tostring(RedTeamPoint));
            UI_Indicator_All(INDICATOR_LEFT_SCORE   , RedTeamPoint);
			
			g_bBlueTeam_Blink = false
            UI_Indicator_All(INDICATOR_RIGHT_IMAGE, "CTF_INDICATOR_BLUE_FLAG", g_bBlueTeam_Blink);
			
		elseif ( haveSkill == true ) and ( RedFlag == nil ) then	-- 블루팀의 깃발을 소유하고 있지만 레드팀의 깃발이 깃발지역에 없으면...
			PrintSimpleXMLMsgFaction(FACTION_TEAM, FACT_RED, 3, {"PVP_CTFLAG_SYSTEM_MESSAGE", 11}, COLOR_RED);	-- PVP_CTFLAG_SYSTEM_MESSAGE, 11
			--PrintSimpleMsgFaction(FACTION_TEAM, FACT_RED, 3, "Return the RedFlag");	-- PVP_CTFLAG_SYSTEM_MESSAGE, 
        end    
        
    elseif ( factionID == FACT_BLUE ) and ( nTriggerID == BlueZone ) then   -- 블루팀이 블루팀 깃발 골인지역으로 진입했을 때;
        local haveSkill = IsHaveSkillFact(nInnerType, nInnerID, RED_FLAG_SKILL_ID[1], RED_FLAG_SKILL_ID[2]);
        if ( haveSkill == true ) and ( BlueFlag ~= nil ) then  -- 레드팀의 깃발을 소유하고 있으면서 블루팀의 깃발이 깃발지역에 있다면;
			--local x,y,z = GetPosition(ACTOR_PC, nInnerID);
			DoEffectPositionAll("cwz_gate_blue1.egp", BATTLE_MAP_ID[1], BATTLE_MAP_ID[2], BLUE_FLAG_ZONE_POS[1], 0, BLUE_FLAG_ZONE_POS[2]);
            BlueTeamPoint = BlueTeamPoint + 1;
            
            --if ( PlayerScore[nInnerID] == nil ) then	-- 플레이어 획득점수가 0이면 (플레이어별 탈취수 계산)
            --    PlayerScore[nInnerID] = 1;	-- 1득점
            --else										-- 플레이어 획득점수가 0이 아니면
                PlayerScore[nInnerID] = PlayerScore[nInnerID] + 1;	
            --end
            
            --PrintSimpleMsg(nInnerID, 3, "Blue Team Get Point! : "..BlueTeamPoint);   -- -- PVP_CTFLAG_SYSTEM_MESSAGE, 10
			PrintSimpleXMLMsgAll(3, {"PVP_CTFLAG_SYSTEM_MESSAGE", 10}, COLOR_GREEN);	-- PVP_CTFLAG_SYSTEM_MESSAGE, 10
            RemoveSkillFact(nInnerType, nInnerID, RED_FLAG_SKILL_ID[1], RED_FLAG_SKILL_ID[2]);
            RedFlag = DropCrow(BATTLE_MAP_ID[1], BATTLE_MAP_ID[2], RED_FLAG_ID[1], RED_FLAG_ID[2], RED_FLAG_ZONE_POS[1], RED_FLAG_ZONE_POS[2]);
            
            if ( Notify_Timer ~= nil ) then
                RemoveTimer(Notify_Timer);
                Notify_Timer = RegistTimer(NOTIFY_GAMESTATE_TIME,EVENT_NOTIFY_GAMESTATE);
            end
            
            RedTeamGuidRed = GUIDE_ZONE;
            BlueTeamGuidRed = GUIDE_ZONE;
            functionGuideArrow();
            
			--ServerLog("BlueTeamPoint"..tostring(BlueTeamPoint));
            UI_Indicator_All(INDICATOR_RIGHT_SCORE, BlueTeamPoint);
			
			g_bRedTeam_Blink = false
            UI_Indicator_All(INDICATOR_LEFT_IMAGE, "CTF_INDICATOR_RED_FLAG", g_bRedTeam_Blink);
			
		elseif ( haveSkill == true ) and ( BlueFlag == nil ) then	--레드팀의 깃발을 소유하고 있지만 블루팀의 깃발이 깃발지역에 없으면...
			PrintSimpleXMLMsgFaction(FACTION_TEAM, FACT_BLUE, 3, {"PVP_CTFLAG_SYSTEM_MESSAGE", 11}, COLOR_RED);	-- PVP_CTFLAG_SYSTEM_MESSAGE, 11
			--PrintSimpleMsgFaction(FACTION_TEAM, FACT_BLUE, 3, "Return the BlueFlag");	-- PVP_CTFLAG_SYSTEM_MESSAGE, 
        end
    end
	
    -- 결과 및 점수 체크;
	if ( RedTeamPoint >= WIN_POINT) then  -- 레드팀 승리로 게임 종료;
		functionResult(FACT_RED);
	elseif ( BlueTeamPoint >= WIN_POINT) then  -- 블루팀 승리로 게임 종료;
		functionResult(FACT_BLUE);
	end
end

--function EventActorTriggerOut(nTriggerType, nTriggerID, nOuterType, nOuterID)
--end

function EventClickTrigger(nTriggerType, nTriggerID, nClickerType, nClickerID)
    local FactionType,FactionID = GetFaction(nClickerType, nClickerID);
    local ClickerName = GetName(nClickerType, nClickerID);
	local ClickerHP = GetHP(nClickerType, nClickerID);
	--ServerLog("1 : "..ClickerHP);
	SkillDeleteCount = 0;					-- 스킬 삭제 횟수 담는 변수.
	
	if( Is_Alive[nClickerID] ) then
	ServerLog( "IS_ALIVE : "..tostring( nClickerID ).."/"..tostring(Is_Alive[nClickerID]));
		if ( FactionID == FACT_RED ) and (ClickerHP > 0) then
	
			if ( nTriggerID == RedDropFlag ) then
				-- 레드팀이 드랍되어있는 레드팀 깃발을 회수한다면 (레드팀 깃발이 깃발지역으로 돌아감);
				DoEffectActorAll("_StandBy.egp", nClickerType, nClickerID);
				RedDropFlag = nil;
				RemoveTimer(RedDropFlag_Timer);
				SetHP(nTriggerType, nTriggerID, 0);
				RedFlag = DropCrow(BATTLE_MAP_ID[1], BATTLE_MAP_ID[2], RED_FLAG_ID[1], RED_FLAG_ID[2], RED_FLAG_ZONE_POS[1], RED_FLAG_ZONE_POS[2]);
				SetMotion(nClickerType, nClickerID, CertifyMotion[1], CertifyMotion[2], 0.1); -- 0.5초 동안 인증모션 취함, 스킬끊기용
				
				for key, value in pairs ( REMOVE_SKILLEFFECT ) do
					
					-- 삭제 스킬 효과 인덱스에 해당하는 버프를 지운다.
					RemoveSkillFact_SkillEffect ( nClickerType, nClickerID, REMOVE_SKILLEFFECT[key] )
					
				end
				
				local x,y,z = GetPosition(ACTOR_NPC, RedDropFlag);
				DoEffectPositionAll("sum_human.egp", BATTLE_MAP_ID[1], BATTLE_MAP_ID[2], x, y, z);

				RedTeamGuidRed = GUIDE_ZONE;
				BlueTeamGuidRed = GUIDE_ZONE;
				functionGuideArrow();
				
				PrintSimpleXMLMsgFaction(FACTION_TEAM, FACT_RED, 3, {"PVP_CTFLAG_SYSTEM_MESSAGE", 12, ClickerName}, COLOR_WHITE);	-- PVP_CTFLAG_SYSTEM_MESSAGE, 12
				
				g_bRedTeam_Blink = false
				UI_Indicator_All(INDICATOR_LEFT_IMAGE, "CTF_INDICATOR_RED_FLAG", g_bRedTeam_Blink);
	
				--if ( PlayerFlagReturn[nClickerID] == nil ) then
				--    PlayerFlagReturn[nClickerID] = 1;
				--	ServerLog("Return : "..(PlayerFlagReturn[nClickerID]));
				--else
					PlayerFlagReturn[nClickerID] = PlayerFlagReturn[nClickerID] + 1;
					--ServerLog("Return : "..(PlayerFlagReturn[nClickerID]));
				--end
				
			elseif ( nTriggerID == BlueDropFlag ) then
				-- 레드팀이 드랍되어있는 블루팀 깃발을 회수하였을 때 (블루팀 깃발의 이어받는다.);
				BlueDropFlag = nil;
				RemoveTimer(BlueDropFlag_Timer);
				SetHP(nTriggerType, nTriggerID, 0);
				ApplySkill(nClickerType, nClickerID, BLUE_FLAG_SKILL_ID[1], BLUE_FLAG_SKILL_ID[2], 1, 9999);
				SetMotion(nClickerType, nClickerID, CertifyMotion[1], CertifyMotion[2], 0.1); -- 0.5초 동안 인증모션 취함, 스킬끊기용
				
				for key, value in pairs ( REMOVE_SKILLEFFECT ) do
					
					-- 삭제 스킬 효과 인덱스에 해당하는 버프를 지운다.
					RemoveSkillFact_SkillEffect ( nClickerType, nClickerID, REMOVE_SKILLEFFECT[key] )
					
				end

				DoEffectActorAll("_StandBy.egp", nClickerType, nClickerID);
	
				RegistTimer(1, EVENT_FLAG_GET, nClickerType, nClickerID);	-- 깃발획득 시점 기준 1초후 금지스킬을 한번더 제거하기 위함.
				RegistTimer(2, EVENT_FLAG_GET, nClickerType, nClickerID);	-- 깃발획득 시점 기준 2초후 금지스킬을 한번더 제거하기 위함.
				RegistTimer(3, EVENT_FLAG_GET, nClickerType, nClickerID);	-- 깃발획득 시점 기준 3초후 금지스킬을 한번더 제거하기 위함.
				RegistTimer(5, EVENT_FLAG_GET, nClickerType, nClickerID);	-- 깃발획득 시점 기준 5초후 금지스킬을 한번더 제거하기 위함.
				RegistTimer(7, EVENT_FLAG_GET, nClickerType, nClickerID);	-- 깃발획득 시점 기준 7초후 금지스킬을 한번더 제거하기 위함.
				RegistTimer(10, EVENT_FLAG_GET, nClickerType, nClickerID);	-- 깃발획득 시점 기준 10초후 금지스킬을 한번더 제거하기 위함.
			
				BlueFlagPlayer = nClickerID;
				RedTeamGuidBlue = GUIDE_PLAYER;
				BlueTeamGuidBlue = GUIDE_NONE;
				functionGuideArrow();
				PrintSimpleXMLMsgFaction(FACTION_TEAM, FACT_RED, 3, {"PVP_CTFLAG_SYSTEM_MESSAGE", 7}, COLOR_WHITE);	-- PVP_CTFLAG_SYSTEM_MESSAGE, 7
				PrintSimpleXMLMsgFaction(FACTION_TEAM, FACT_BLUE, 3, {"PVP_CTFLAG_SYSTEM_MESSAGE", 6}, COLOR_RED);	-- PVP_CTFLAG_SYSTEM_MESSAGE, 6
				
				--if ( PlayerFlagGain[nClickerID] == nil ) then
				--    PlayerFlagGain[nClickerID] = 1;
				--	ServerLog("Return : "..(PlayerFlagGain[nClickerID]));
				--else
					PlayerFlagGain[nClickerID] = PlayerFlagGain[nClickerID] + 1;
					--ServerLog("Return : "..(PlayerFlagGain[nClickerID]));
				--end
				
			elseif ( nTriggerID == BlueFlag ) then
				-- 레드팀이 블루팀 깃발지역에 도달하여 깃발을 탈취하였을 때;
				BlueFlag = nil;
				SetHP(nTriggerType, nTriggerID, 0);
				ApplySkill(nClickerType, nClickerID, BLUE_FLAG_SKILL_ID[1], BLUE_FLAG_SKILL_ID[2], 1, 9999);
				SetMotion(nClickerType, nClickerID, CertifyMotion[1], CertifyMotion[2], 0.1); -- 0.5초 동안 인증모션 취함, 스킬끊기용
				--RemoveSkillFact(nClickerType, nClickerID, 44, 12);	--44/12 보유스킬:은신 삭제
				
				for key, value in pairs ( REMOVE_SKILLEFFECT ) do
					
					-- 삭제 스킬 효과 인덱스에 해당하는 버프를 지운다.
					RemoveSkillFact_SkillEffect ( nClickerType, nClickerID, REMOVE_SKILLEFFECT[key] )
					
				end
				
				DoEffectActorAll("_StandBy.egp", nClickerType, nClickerID);
				
				RegistTimer(1, EVENT_FLAG_GET, nClickerType, nClickerID);	-- 깃발획득 시점 기준 1초후 금지스킬을 한번더 제거하기 위함.
				RegistTimer(2, EVENT_FLAG_GET, nClickerType, nClickerID);	-- 깃발획득 시점 기준 2초후 금지스킬을 한번더 제거하기 위함.
				RegistTimer(3, EVENT_FLAG_GET, nClickerType, nClickerID);	-- 깃발획득 시점 기준 3초후 금지스킬을 한번더 제거하기 위함.
				RegistTimer(5, EVENT_FLAG_GET, nClickerType, nClickerID);	-- 깃발획득 시점 기준 5초후 금지스킬을 한번더 제거하기 위함.
				RegistTimer(7, EVENT_FLAG_GET, nClickerType, nClickerID);	-- 깃발획득 시점 기준 7초후 금지스킬을 한번더 제거하기 위함.
				RegistTimer(10, EVENT_FLAG_GET, nClickerType, nClickerID);	-- 깃발획득 시점 기준 10초후 금지스킬을 한번더 제거하기 위함.				
				
				if ( Notify_Timer ~= nil ) then
					RemoveTimer(Notify_Timer);
					Notify_Timer = RegistTimer(NOTIFY_GAMESTATE_TIME,EVENT_NOTIFY_GAMESTATE);
				end
				
				BlueFlagPlayer = nClickerID;
				RedTeamGuidBlue = GUIDE_PLAYER;
				BlueTeamGuidBlue = GUIDE_NONE;
				functionGuideArrow();
				
				PrintSimpleXMLMsgFaction(FACTION_TEAM, FACT_RED, 3, {"PVP_CTFLAG_SYSTEM_MESSAGE", 7}, COLOR_WHITE);	-- PVP_CTFLAG_SYSTEM_MESSAGE, 7
				PrintSimpleXMLMsgFaction(FACTION_TEAM, FACT_BLUE, 3, {"PVP_CTFLAG_SYSTEM_MESSAGE", 6}, COLOR_RED);	-- PVP_CTFLAG_SYSTEM_MESSAGE, 6
				
				g_bBlueTeam_Blink = true
				UI_Indicator_All(INDICATOR_RIGHT_IMAGE, "CTF_INDICATOR_BLUE_FLAG", g_bBlueTeam_Blink);
				
				--if ( PlayerFlagGain[nClickerID] == nil ) then
				--    PlayerFlagGain[nClickerID] = 1;
				--	ServerLog("Return : "..(PlayerFlagGain[nClickerID]));
				--else
					PlayerFlagGain[nClickerID] = PlayerFlagGain[nClickerID] + 1;
					--ServerLog("Return : "..(PlayerFlagGain[nClickerID]));
				--end
		
			end
			
		elseif ( FactionID == FACT_BLUE ) and (ClickerHP > 0) then
			if ( nTriggerID == BlueDropFlag ) then
				-- 블루팀이 드랍되어있는 블루팀 깃발을 회수한다면 (블루팀 깃발이 깃발지역으로 돌아감);
				DoEffectActorAll("_StandBy.egp", nClickerType, nClickerID);
				BlueDropFlag = nil;
				RemoveTimer(BlueDropFlag_Timer);
				SetHP(nTriggerType, nTriggerID, 0);
				BlueFlag = DropCrow(BATTLE_MAP_ID[1], BATTLE_MAP_ID[2], BLUE_FLAG_ID[1], BLUE_FLAG_ID[2], BLUE_FLAG_ZONE_POS[1], BLUE_FLAG_ZONE_POS[2]);
				SetMotion(nClickerType, nClickerID, CertifyMotion[1], CertifyMotion[2], 0.1); -- 0.5초 동안 인증모션 취함, 스킬끊기용
				
				for key, value in pairs ( REMOVE_SKILLEFFECT ) do
					
					-- 삭제 스킬 효과 인덱스에 해당하는 버프를 지운다.
					RemoveSkillFact_SkillEffect ( nClickerType, nClickerID, REMOVE_SKILLEFFECT[key] )
					
				end
				
				local x,y,z = GetPosition(ACTOR_NPC, RedDropFlag);
				DoEffectPositionAll("sum_human.egp", BATTLE_MAP_ID[1], BATTLE_MAP_ID[2], x, y, z);

		
				RedTeamGuidBlue = GUIDE_ZONE;
				BlueTeamGuidBlue = GUIDE_ZONE;
				functionGuideArrow();
				
				PrintSimpleXMLMsgFaction(FACTION_TEAM, FACT_BLUE, 3, {"PVP_CTFLAG_SYSTEM_MESSAGE", 12, ClickerName}, COLOR_WHITE);	-- PVP_CTFLAG_SYSTEM_MESSAGE, 12
				
				g_bBlueTeam_Blink = false
				UI_Indicator_All(INDICATOR_RIGHT_IMAGE, "CTF_INDICATOR_BLUE_FLAG", g_bBlueTeam_Blink);
				
				--if ( PlayerFlagReturn[nClickerID] == nil ) then
				--    PlayerFlagReturn[nClickerID] = 1;
				--	ServerLog("Return : "..(PlayerFlagReturn[nClickerID]));
				--else
					PlayerFlagReturn[nClickerID] = PlayerFlagReturn[nClickerID] + 1;
					--ServerLog("Return : "..(PlayerFlagReturn[nClickerID]));
				--end
				
			elseif ( nTriggerID == RedDropFlag ) then
				-- 블루팀이 드랍되어있는 레드팀 깃발을 회수하였을 때 (레드팀 깃발의 이어받는다.);
				RedDropFlag = nil;
				RemoveTimer(RedDropFlag_Timer);
				SetHP(nTriggerType, nTriggerID, 0);
				ApplySkill(nClickerType, nClickerID, RED_FLAG_SKILL_ID[1], RED_FLAG_SKILL_ID[2], 1, 9999);
				SetMotion(nClickerType, nClickerID, CertifyMotion[1], CertifyMotion[2], 0.1); -- 0.5초 동안 인증모션 취함, 스킬끊기용
				
				for key, value in pairs ( REMOVE_SKILLEFFECT ) do
					
					-- 삭제 스킬 효과 인덱스에 해당하는 버프를 지운다.
					RemoveSkillFact_SkillEffect ( nClickerType, nClickerID, REMOVE_SKILLEFFECT[key] )
					
				end
	
				DoEffectActorAll("_StandBy.egp", nClickerType, nClickerID);
				
				RegistTimer(1, EVENT_FLAG_GET, nClickerType, nClickerID);	-- 깃발획득 시점 기준 1초후 금지스킬을 한번더 제거하기 위함.
				RegistTimer(2, EVENT_FLAG_GET, nClickerType, nClickerID);	-- 깃발획득 시점 기준 2초후 금지스킬을 한번더 제거하기 위함.
				RegistTimer(3, EVENT_FLAG_GET, nClickerType, nClickerID);	-- 깃발획득 시점 기준 3초후 금지스킬을 한번더 제거하기 위함.
				RegistTimer(5, EVENT_FLAG_GET, nClickerType, nClickerID);	-- 깃발획득 시점 기준 5초후 금지스킬을 한번더 제거하기 위함.
				RegistTimer(7, EVENT_FLAG_GET, nClickerType, nClickerID);	-- 깃발획득 시점 기준 7초후 금지스킬을 한번더 제거하기 위함.
				RegistTimer(10, EVENT_FLAG_GET, nClickerType, nClickerID);	-- 깃발획득 시점 기준 10초후 금지스킬을 한번더 제거하기 위함.
				
				RedFlagPlayer = nClickerID;
				BlueTeamGuidRed = GUIDE_PLAYER;
				RedTeamGuidRed = GUIDE_NONE;
				functionGuideArrow();
				PrintSimpleXMLMsgFaction(FACTION_TEAM, FACT_BLUE, 3, {"PVP_CTFLAG_SYSTEM_MESSAGE", 7}, COLOR_WHITE);	-- PVP_CTFLAG_SYSTEM_MESSAGE, 7
				PrintSimpleXMLMsgFaction(FACTION_TEAM, FACT_RED, 3, {"PVP_CTFLAG_SYSTEM_MESSAGE", 6}, COLOR_RED);	-- PVP_CTFLAG_SYSTEM_MESSAGE, 6
				
				--if ( PlayerFlagGain[nClickerID] == nil ) then
				--   PlayerFlagGain[nClickerID] = 1;
				--	ServerLog("Return : "..(PlayerFlagGain[nClickerID]));
				--else
					PlayerFlagGain[nClickerID] = PlayerFlagGain[nClickerID] + 1;
					--ServerLog("Return : "..(PlayerFlagGain[nClickerID]));
				--end
				
			elseif ( nTriggerID == RedFlag ) then
				-- 블루팀이 레드팀 깃발지역에 도달하여 깃발을 탈취하였을 때;
				RedFlag = nil;
				SetHP(nTriggerType, nTriggerID, 0);
				ApplySkill(nClickerType, nClickerID, RED_FLAG_SKILL_ID[1], RED_FLAG_SKILL_ID[2], 1, 9999);
				SetMotion(nClickerType, nClickerID, CertifyMotion[1], CertifyMotion[2], 0.1); -- 0.5초 동안 인증모션 취함, 스킬끊기용
				--RemoveSkillFact(nClickerType, nClickerID, 44, 12);	--44/12 보유스킬:은신 삭제
				
				for key, value in pairs ( REMOVE_SKILLEFFECT ) do
					
					-- 삭제 스킬 효과 인덱스에 해당하는 버프를 지운다.
					RemoveSkillFact_SkillEffect ( nClickerType, nClickerID, REMOVE_SKILLEFFECT[key] )
					
				end
				
				DoEffectActorAll("_StandBy.egp", nClickerType, nClickerID);
				
				RegistTimer(1, EVENT_FLAG_GET, nClickerType, nClickerID);	-- 깃발획득 시점 기준 1초후 금지스킬을 한번더 제거하기 위함.
				RegistTimer(2, EVENT_FLAG_GET, nClickerType, nClickerID);	-- 깃발획득 시점 기준 2초후 금지스킬을 한번더 제거하기 위함.
				RegistTimer(3, EVENT_FLAG_GET, nClickerType, nClickerID);	-- 깃발획득 시점 기준 3초후 금지스킬을 한번더 제거하기 위함.
				RegistTimer(5, EVENT_FLAG_GET, nClickerType, nClickerID);	-- 깃발획득 시점 기준 5초후 금지스킬을 한번더 제거하기 위함.
				RegistTimer(7, EVENT_FLAG_GET, nClickerType, nClickerID);	-- 깃발획득 시점 기준 7초후 금지스킬을 한번더 제거하기 위함.
				RegistTimer(10, EVENT_FLAG_GET, nClickerType, nClickerID);	-- 깃발획득 시점 기준 10초후 금지스킬을 한번더 제거하기 위함.
				
				if ( Notify_Timer ~= nil ) then
					RemoveTimer(Notify_Timer);
					Notify_Timer = RegistTimer(NOTIFY_GAMESTATE_TIME,EVENT_NOTIFY_GAMESTATE);
				end
				
				RedFlagPlayer = nClickerID;
				BlueTeamGuidRed = GUIDE_PLAYER;
				RedTeamGuidRed = GUIDE_NONE;
				functionGuideArrow();
				
				PrintSimpleXMLMsgFaction(FACTION_TEAM, FACT_BLUE, 3, {"PVP_CTFLAG_SYSTEM_MESSAGE", 7}, COLOR_WHITE);	-- PVP_CTFLAG_SYSTEM_MESSAGE, 7
				PrintSimpleXMLMsgFaction(FACTION_TEAM, FACT_RED, 3, {"PVP_CTFLAG_SYSTEM_MESSAGE", 6}, COLOR_RED);	-- PVP_CTFLAG_SYSTEM_MESSAGE, 6
				
				g_bRedTeam_Blink = true
				UI_Indicator_All(INDICATOR_LEFT_IMAGE, "CTF_INDICATOR_RED_FLAG", g_bRedTeam_Blink);
				
				--if ( PlayerFlagGain[nClickerID] == nil ) then
				--    PlayerFlagGain[nClickerID] = 1;
				--	ServerLog("Return : "..(PlayerFlagGain[nClickerID]));
				--else
					PlayerFlagGain[nClickerID] = PlayerFlagGain[nClickerID] + 1;
					--ServerLog("Return : "..(PlayerFlagGain[nClickerID]));
				--end
				
			end       
		end
	end
	ServerLog( "IS_ALIVE : "..tostring( nClickerID ).."/"..tostring(Is_Alive[nClickerID]));
end

function EventPartyJoin(nPartyID, bExpedition, nJoinerID, bInSameInstance)

	local nTempMasterID = GetPartyMaster( nPartyID );
	
	if ( ( nRedPartyMasterID == nTempMasterID ) and ( nRedParty == nil ) ) then	
		-- 파티 정보를 저장한다;
		nRedParty = nPartyID;
		
		-- 같은 팀인데 같은 파티가 아닌 유저들을 가입시킨다;
		for nMemberID, bValid in pairs( RedTeamPlayerList ) do
			if ( bValid == true ) then
				local nTempPartyID = GetPartyID( nMemberID );
				if ( nTempPartyID ~= nRedParty ) then
					JoinParty( nRedPartyMasterID, nMemberID );
					ServerLog( "RED Team | Callback Join Party "..tostring( nMemberID ) );
				end
            end
		end
	elseif ( ( nBluePartyMasterID == nTempMasterID ) and ( nBlueParty == nil ) ) then
		-- 파티 정보를 저장한다;
		nBlueParty = nPartyID;
		
		-- 같은 팀인데 같은 파티가 아닌 유저들을 가입시킨다;
		for nMemberID, bValid in pairs( BlueTeamPlayerList ) do
			if ( bValid == true ) then
				local nTempPartyID = GetPartyID( nMemberID );
				if ( nTempPartyID ~= nBlueParty ) then
					JoinParty( nBluePartyMasterID, nMemberID );
					ServerLog( "BLUE Team | Callback Join Party "..tostring( nMemberID ) );
				end
            end
		end
	end
	
	-- 같은 인던에 있지 않은 유저일 경우 체크한다;
	if ( bInSameInstance == false ) then
		-- 참가 자격이 있는 유저인지를 검사;
		local master = GetPartyMaster(nPartyID);
		local FactionType, FactionID = GetFaction(ACTOR_PC, master);
        
		if(FactionID == 0) then
			for key,value in pairs( RedTeamList ) do
				if(value == nJoinerID) then
					UI_XmlTimerMessageBox(nJoinerID, { "RNCDM_REJOIN_TEXT", 0 },{ "RNCDM_REJOIN_TEXT", 1 },{ "RNCDM_REJOIN_TEXT", 2 }, 30, PARAM1_JOIN_UI, 0); --gameserver
				end
			end
		else
			for key,value in pairs( BlueTeamList ) do
				if(value == nJoinerID) then
					UI_XmlTimerMessageBox(nJoinerID, { "RNCDM_REJOIN_TEXT", 0 },{ "RNCDM_REJOIN_TEXT", 1 },{ "RNCDM_REJOIN_TEXT", 2 }, 30, PARAM1_JOIN_UI, 0); --gameserver
				end
			end
		end
	end
end

function EventPartyChangeMaster ( nPartyID, bExpedition, nNewMasterID, bInMapNewMaster, nOldMaster, bInMapOldMaster )

	-- 파티의 마스터가 변경되면 변경세팅한다;
	if ( nRedParty == nPartyID ) then
		nRedPartyMasterID = nNewMasterID;
		ServerLog( "RED Team | Change Party Master "..tostring( nOldMaster ).." -> "..tostring( nNewMasterID ) );
	elseif( nBlueParty == nPartyID ) then
		nBluePartyMasterID = nNewMasterID;
		ServerLog( "BLUE Team | Change Party Master "..tostring( nOldMaster ).." -> "..tostring( nNewMasterID ) );
	end
	
end

function EventCustomMessage(nSenderType, nSenderID, nParam1, nParam2, nParam3, nParam4)
	if ( nParam1 == PARAM1_EXIT_BUTTON ) then -- 나가기 버튼 클릭;
		-- CDM 결과 UI에서 나가기 버튼 클릭시 해당 유저를 인던에서 나가게 함;
		DoOut(nSenderID);
        
    elseif ( nParam1 == PARAM1_JOIN_UI ) then -- 입장요청 UI;
		if ( nParam3 == PARAM3_MSGBOX_OK ) then -- OK 버튼 클릭;
			-- CDM 입장 의사를 묻는 UI에서 Yes를 선택했을 때 해당 유저를 입장시킴;
			DoJoin(nSenderID);
		end
        
	end
end

--  결과 계산 함수;
function functionResult(WinnerFaction)
    if ( GamePlaying == false ) then  -- 결과가 한번이라도 정해진 경우 두번 호출되는것을 방지;
        return;
    end
    
    GamePlaying = false;
    
    UI_CDM_BattleEnd();
    
    UI_FlagGuideArrow_Off(); -- 깃발전 가이드 이펙트 삭제;
    
    SetHP(ACTOR_NPC, RedFlag, 0);  -- 깃발 삭제;
    SetHP(ACTOR_NPC, RedDropFlag, 0);
    SetHP(ACTOR_NPC, BlueFlag, 0);
    SetHP(ACTOR_NPC, BlueDropFlag, 0);
    
    RemoveActorTrigger(ACTOR_NPC, RedZone);  -- 깃발존 삭제;
    RemoveActorTrigger(ACTOR_NPC, BlueZone);
    
    SetRelation( FACTION_TEAM, FACT_RED, FACTION_TEAM, FACT_BLUE, RELATION_ALLY );  -- faction 통일;
    SetRelation( FACTION_TEAM, FACT_BLUE, FACTION_TEAM, FACT_RED, RELATION_ALLY );
    
    UI_Indicator_All(INDICATOR_TIME, 0);
    	
    RegistTimer(5, EVENT_REWARD, WinnerFaction);  -- 보상 준비;
    
    Report_CaptureTheFlag_Result(RedTeamPlayerList, BlueTeamPlayerList, WinnerFaction); -- 깃발전 전용 결과 전송 함수;
    
	for key, value in pairs(RedTeamList) do
		PlayerProgressTimer[value] = PlayerProgressTimer[value] - GetLeftTime(GamePlayTimer);
	end
	for key, value in pairs(BlueTeamList) do
		PlayerProgressTimer[value] = PlayerProgressTimer[value] - GetLeftTime(GamePlayTimer);
	end
    RemoveTimer(GamePlayTimer); -- 게임 진행 타이머를 삭제;
end

function functionReward(winnerFaction)	-- 보상함수
    if ( bAbuse == true ) then			-- bAbuse의 기본값은 true 
        winnerFaction = FACT_NONE;		-- 진영의 팩션을 무적으로 변경
    end

	
	local ADD_Score = 10;			-- 득점 1당 가산치
	local ADD_FlagGain = 5;			-- 깃발탈취 1당 가산치
	local ADD_FlagReturn = 5;		-- 깃발회수 1당 가산치
	local ADD_Kill = 2;				-- 1킬당 가산치
	local ADD_Death = 1;			-- 1데스당 가산치
	local ADD_Damage = 0.00002;		-- 데미지(량) 1당 가산치
	local ADD_Heal = 0.00002;		-- 힐(량) 1당 가산치
	
	local FlagGain_MAX = 10;		-- 최대 가산가능 탈취 수
	local FlagReturn_MAX = 10;		-- 최대 가산가능 회수 수
	local Kill_MAX = 50;			-- 최대 가산가능 킬 수
	local Death_MAX = 5;			-- 최대 가산가능 데스 수
	local Damage_MAX = 1000000;		-- 최대 가산가능 데미지량 (100만)
	local Heal_MAX = 250000;		-- 최대 가산가능 힐량 (50만)
	
	local Progress_Time_Range = { 60, 300, 600, 900, 1200};	-- 게임참여(진행)시간 별 가산치 구간
	local ADD_Time = { 0.1, 0.8, 1, 1.2, 1.5};				-- 게임참여(진행)시간 별 가산치
	
	-- 레드팀 전투참여도 계산
	for key, value in pairs(RedTeamList) do
		if ( RedTeamPlayerList[value] == true ) then
			if(PlayerScore[value] > WIN_POINT) then 	-- 최대획득가능 득점수를 넘으면
				PlayerScore[value] = WIN_POINT;
			end
			if(PlayerFlagGain[value] > FlagGain_MAX) then 	-- 최대획득가능 탈취수를 넘으면
				PlayerFlagGain[value] = FlagGain_MAX;
			end
			if(PlayerFlagReturn[value] > FlagReturn_MAX) then 	-- 최대획득가능 회수 수를 넘으면
				PlayerFlagReturn[value] = FlagReturn_MAX;
			end
			if(PlayerKill[value] > Kill_MAX) then 	-- 최대획득가능 킬 수를 넘으면
				cPlayerKill[value] = Kill_MAX;
			else
				cPlayerKill[value] = PlayerKill[value];
			end
			if(PlayerDeath[value] > Death_MAX) then 	-- 최대획득가능 데스 수를 넘으면
				cPlayerDeath[value] = Death_MAX;
			else
				cPlayerDeath[value] = PlayerDeath[value];
			end
			if(PlayerDamage[value] > Damage_MAX) then 	-- 최대획득가능 데미지량을 넘으면
				PlayerDamage[value] = Damage_MAX;
			end
			if(PlayerHeal[value] > Heal_MAX) then 	-- 최대획득가능 힐량를 넘으면
				PlayerHeal[value] = Heal_MAX;
			end
			
			PlayerContribute[value] = PlayerContribute[value] + (PlayerScore[value] * ADD_Score);
			PlayerContribute[value] = PlayerContribute[value] + (PlayerFlagGain[value] * ADD_FlagGain);
			PlayerContribute[value] = PlayerContribute[value] + (PlayerFlagReturn[value] * ADD_FlagReturn);
			PlayerContribute[value] = PlayerContribute[value] + (cPlayerKill[value] * ADD_Kill);
			PlayerContribute[value] = PlayerContribute[value] + (cPlayerDeath[value] * ADD_Death);
			PlayerContribute[value] = PlayerContribute[value] + math.floor(PlayerDamage[value] * ADD_Damage);
			PlayerContribute[value] = PlayerContribute[value] + math.floor(PlayerHeal[value] * ADD_Heal);
			
			--ServerLog("Contribute : "..tostring(PlayerContribute[value]).."  Name:"..tostring(GetName(ACTOR_PC, value)));	--필드서버 로그(확인용)
			
			if (PlayerProgressTimer[value] < Progress_Time_Range[1]) then
				PlayerContribute[value] = PlayerContribute[value] * ADD_Time[1]; 
			elseif (PlayerProgressTimer[value] < Progress_Time_Range[2]) then
				PlayerContribute[value] = PlayerContribute[value] * ADD_Time[2]; 
			elseif (PlayerProgressTimer[value] < Progress_Time_Range[3]) then
				PlayerContribute[value] = PlayerContribute[value] * ADD_Time[3]; 
			elseif (PlayerProgressTimer[value] < Progress_Time_Range[4]) then
				PlayerContribute[value] = PlayerContribute[value] * ADD_Time[4]; 
			else
				PlayerContribute[value] = PlayerContribute[value] * ADD_Time[5]; 
			end
			ServerLog("------------------");
			ServerLog("Contribute: "..tostring(PlayerContribute[value]).."  Name: "..tostring(GetName(ACTOR_PC, value)));	--필드서버 로그(확인용)
			ServerLog("PlayTime: "..tostring(PlayerProgressTimer[value]));
			ServerLog("Score: "..tostring(PlayerScore[value]).."  Gain: "..tostring(PlayerFlagGain[value]).."  Return: "..tostring(PlayerFlagReturn[value]));
			ServerLog("Kill: "..tostring(PlayerKill[value]).."  Death: "..tostring(PlayerDeath[value]).."  Dmg: "..tostring(PlayerDamage[value]).."  Heal: "..tostring(PlayerHeal[value]));
		end
	end
	-- 블루팀 전투참여도 계산
	for key, value in pairs(BlueTeamList) do
		if ( BlueTeamPlayerList[value] == true ) then
			if(PlayerScore[value] > WIN_POINT) then 	-- 최대획득가능 득점수를 넘으면
				PlayerScore[value] = WIN_POINT;
			end
			if(PlayerFlagGain[value] > FlagGain_MAX) then 	-- 최대획득가능 탈취수를 넘으면
				PlayerFlagGain[value] = FlagGain_MAX;
			end
			if(PlayerFlagReturn[value] > FlagReturn_MAX) then 	-- 최대획득가능 회수 수를 넘으면
				PlayerFlagReturn[value] = FlagReturn_MAX;
			end
			if(PlayerKill[value] > Kill_MAX) then 	-- 최대획득가능 킬 수를 넘으면
				cPlayerKill[value] = Kill_MAX;
			else
				cPlayerKill[value] = PlayerKill[value];
			end
			if(PlayerDeath[value] > Death_MAX) then 	-- 최대획득가능 데스 수를 넘으면
				cPlayerDeath[value] = Death_MAX;
			else
				cPlayerDeath[value] = PlayerDeath[value];
			end
			if(PlayerDamage[value] > Damage_MAX) then 	-- 최대획득가능 데미지량을 넘으면
				PlayerDamage[value] = Damage_MAX;
			end
			if(PlayerHeal[value] > Heal_MAX) then 	-- 최대획득가능 힐량를 넘으면
				PlayerHeal[value] = Heal_MAX;
			end
			
			PlayerContribute[value] = PlayerContribute[value] + (PlayerScore[value] * ADD_Score);
			PlayerContribute[value] = PlayerContribute[value] + (PlayerFlagGain[value] * ADD_FlagGain);
			PlayerContribute[value] = PlayerContribute[value] + (PlayerFlagReturn[value] * ADD_FlagReturn);
			PlayerContribute[value] = PlayerContribute[value] + (cPlayerKill[value] * ADD_Kill);
			PlayerContribute[value] = PlayerContribute[value] + (cPlayerDeath[value] * ADD_Death);
			PlayerContribute[value] = PlayerContribute[value] + math.floor(PlayerDamage[value] * ADD_Damage);
			PlayerContribute[value] = PlayerContribute[value] + math.floor(PlayerHeal[value] * ADD_Heal);
		
			--ServerLog("Contribute : "..tostring(PlayerContribute[value]).."  Name:"..tostring(GetName(ACTOR_PC, value)));	--필드서버 로그(확인용)
			
			if (PlayerProgressTimer[value] < Progress_Time_Range[1]) then
				PlayerContribute[value] = PlayerContribute[value] * ADD_Time[1]; 
			elseif (PlayerProgressTimer[value] < Progress_Time_Range[2]) then
				PlayerContribute[value] = PlayerContribute[value] * ADD_Time[2]; 
			elseif (PlayerProgressTimer[value] < Progress_Time_Range[3]) then
				PlayerContribute[value] = PlayerContribute[value] * ADD_Time[3]; 
			elseif (PlayerProgressTimer[value] < Progress_Time_Range[4]) then
				PlayerContribute[value] = PlayerContribute[value] * ADD_Time[4]; 
			else
				PlayerContribute[value] = PlayerContribute[value] * ADD_Time[5]; 
			end
			
			ServerLog("------------------");
			ServerLog("Contribute: "..tostring(PlayerContribute[value]).."  Name: "..tostring(GetName(ACTOR_PC, value)));	--필드서버 로그(확인용)
			ServerLog("PlayTime: "..tostring(PlayerProgressTimer[value]));
			ServerLog("Score: "..tostring(PlayerScore[value]).."  Gain: "..tostring(PlayerFlagGain[value]).."  Return: "..tostring(PlayerFlagReturn[value]));
			ServerLog("Kill: "..tostring(PlayerKill[value]).."  Death: "..tostring(PlayerDeath[value]).."  Dmg: "..tostring(PlayerDamage[value]).."  Heal: "..tostring(PlayerHeal[value]));
		end
	end
	ServerLog("------------------");
	
    local _redKill = 0;
    local _redDeath = 0;
    local _blueKill = 0;
    local _blueDeath = 0;
    
    UI_CompetitionResult_CaptureTheFlag_Begin();	-- 결과 UI 출력
    if ( winnerFaction == FACT_RED ) then -- 레드팀 승리;
        for key,value in pairs( RedTeamList ) do	-- 레드팀 모두에게 결과 UI에 들어갈 정보를 날리고 보상을 지급.
			--PrintChatXMLMsgAll( { "PVP_CTFLAG_SYSTEM_MESSAGE", 24, GetName(ACTOR_PC, value), PlayerContribute[value]}, COLOR_YELLOW);
            if ( RedTeamPlayerList[value] == true ) then
				if ( PlayerContribute[value] < 10 ) then		-- 전투참여도가 10미만이면 보상없음
					UI_CompetitionResult_CaptureTheFlag_Info(value, 0, GetSchool(value), GetClass(value), GetName(ACTOR_PC, value), PlayerScore[value], PlayerKill[value], PlayerDeath[value], tostring(0));
				elseif ( PlayerContribute[value] < 50 ) then	-- 전투참여도가 10 ~ 49이면 훈장1개
					UI_CompetitionResult_CaptureTheFlag_Info(value, 0, GetSchool(value), GetClass(value), GetName(ACTOR_PC, value), PlayerScore[value], PlayerKill[value], PlayerDeath[value], WINNER_REWARD_VIEW[1]);
					SendSystemMail( value, { "SYSTEM_POST_SENDER_NAME", 0 }, { "SYSTEM_POST_SENDER_CONTENTS", 1 }, 0, WINNER_REWARD1 ); -- 보상을 메일로 보냄;
					Log_CaptureTheFlag_Reward(value, 11); -- 보상 로그; (유저DBNum, 보상금 액수);
				elseif ( PlayerContribute[value] < 100 ) then	-- 전투참여도가 50 ~ 99이면 훈장2개
					UI_CompetitionResult_CaptureTheFlag_Info(value, 0, GetSchool(value), GetClass(value), GetName(ACTOR_PC, value), PlayerScore[value], PlayerKill[value], PlayerDeath[value], WINNER_REWARD_VIEW[2]);
					SendSystemMail( value, { "SYSTEM_POST_SENDER_NAME", 0 }, { "SYSTEM_POST_SENDER_CONTENTS", 1 }, 0, WINNER_REWARD2 ); -- 보상을 메일로 보냄;
					Log_CaptureTheFlag_Reward(value, 12); -- 보상 로그; (유저DBNum, 보상금 액수);
				else											-- 전투참여도가 100이상이면 훈장3개
					UI_CompetitionResult_CaptureTheFlag_Info(value, 0, GetSchool(value), GetClass(value), GetName(ACTOR_PC, value), PlayerScore[value], PlayerKill[value], PlayerDeath[value], WINNER_REWARD_VIEW[3]);
					SendSystemMail( value, { "SYSTEM_POST_SENDER_NAME", 0 }, { "SYSTEM_POST_SENDER_CONTENTS", 1 }, 0, WINNER_REWARD3 ); -- 보상을 메일로 보냄;
					Log_CaptureTheFlag_Reward(value, 13); -- 보상 로그; (유저DBNum, 보상금 액수);
				end
            end
            _redKill = _redKill + PlayerKill[value];
            _redDeath = _redDeath + PlayerDeath[value];
        end
        for key,value in pairs( BlueTeamList ) do	-- 블루팀 패배
			--PrintChatXMLMsgAll( { "PVP_CTFLAG_SYSTEM_MESSAGE", 24, GetName(ACTOR_PC, value), PlayerContribute[value]}, COLOR_YELLOW);
            if ( BlueTeamPlayerList[value] == true ) then
                if ( PlayerContribute[value] < 10 ) then		-- 전투참여도가 50미만이면 보상없음
					UI_CompetitionResult_CaptureTheFlag_Info(value, 1, GetSchool(value), GetClass(value), GetName(ACTOR_PC, value), PlayerScore[value], PlayerKill[value], PlayerDeath[value], tostring(0));

				elseif ( PlayerContribute[value] < 50 ) then	-- 전투참여도가 10 ~ 49이면 훈장0개
					UI_CompetitionResult_CaptureTheFlag_Info(value, 1, GetSchool(value), GetClass(value), GetName(ACTOR_PC, value), PlayerScore[value], PlayerKill[value], PlayerDeath[value], tostring(0));
					--SendSystemMail( value, { "SYSTEM_POST_SENDER_NAME", 0 }, { "SYSTEM_POST_SENDER_CONTENTS", 6 }, 0, LOSER_REWARD1 ); -- 보상을 메일로 보냄;
					--Log_CaptureTheFlag_Reward(value, 01); -- 보상 로그; (유저DBNum, 보상금 액수);
				elseif ( PlayerContribute[value] < 100 ) then	-- 전투참여도가 50 ~ 99이면 훈장1개
					UI_CompetitionResult_CaptureTheFlag_Info(value, 1, GetSchool(value), GetClass(value), GetName(ACTOR_PC, value), PlayerScore[value], PlayerKill[value], PlayerDeath[value], WINNER_REWARD_VIEW[1]);
					SendSystemMail( value, { "SYSTEM_POST_SENDER_NAME", 0 }, { "SYSTEM_POST_SENDER_CONTENTS", 6 }, 0, LOSER_REWARD2 ); -- 보상을 메일로 보냄;
					Log_CaptureTheFlag_Reward(value, 01); -- 보상 로그; (유저DBNum, 보상금 액수);
				else											-- 전투참여도가 100이상이면 훈장2개
					UI_CompetitionResult_CaptureTheFlag_Info(value, 1, GetSchool(value), GetClass(value), GetName(ACTOR_PC, value), PlayerScore[value], PlayerKill[value], PlayerDeath[value], WINNER_REWARD_VIEW[2]);
					SendSystemMail( value, { "SYSTEM_POST_SENDER_NAME", 0 }, { "SYSTEM_POST_SENDER_CONTENTS", 6 }, 0, LOSER_REWARD3 ); -- 보상을 메일로 보냄;
					Log_CaptureTheFlag_Reward(value, 02); -- 보상 로그; (유저DBNum, 보상금 액수);
				end
            end
            _blueKill = _blueKill + PlayerKill[value];
            _blueDeath = _blueDeath + PlayerDeath[value];
        end
        Log_CaptureTheFlag_Result(FACT_RED, true, RedTeamPoint, _redKill, _redDeath);   -- 결과 로그 ( 팀번호, 승리여부, 팀 점수, 팀 킬 수, 팀 데스 수 );
        Log_CaptureTheFlag_Result(FACT_BLUE, false, BlueTeamPoint, _blueKill, _blueDeath);
        
    elseif ( winnerFaction == FACT_BLUE ) then -- 블루팀 승리;
        for key,value in pairs( BlueTeamList ) do
			--PrintChatXMLMsgAll( { "PVP_CTFLAG_SYSTEM_MESSAGE", 24, GetName(ACTOR_PC, value), PlayerContribute[value]}, COLOR_YELLOW);
            if ( BlueTeamPlayerList[value] == true ) then
                if ( PlayerContribute[value] < 10 ) then		-- 전투참여도가 10미만이면 보상없음
					UI_CompetitionResult_CaptureTheFlag_Info(value, 0, GetSchool(value), GetClass(value), GetName(ACTOR_PC, value), PlayerScore[value], PlayerKill[value], PlayerDeath[value], tostring(0));
				elseif ( PlayerContribute[value] < 50 ) then	-- 전투참여도가 10 ~ 49이면 훈장1개
					UI_CompetitionResult_CaptureTheFlag_Info(value, 0, GetSchool(value), GetClass(value), GetName(ACTOR_PC, value), PlayerScore[value], PlayerKill[value], PlayerDeath[value], WINNER_REWARD_VIEW[1]);
					SendSystemMail( value, { "SYSTEM_POST_SENDER_NAME", 0 }, { "SYSTEM_POST_SENDER_CONTENTS", 1 }, 0, WINNER_REWARD1 ); -- 보상을 메일로 보냄;
					Log_CaptureTheFlag_Reward(value, 11); -- 보상 로그; (유저DBNum, 보상금 액수);
				elseif ( PlayerContribute[value] < 100 ) then	-- 전투참여도가 50 ~ 99이면 훈장2개
					UI_CompetitionResult_CaptureTheFlag_Info(value, 0, GetSchool(value), GetClass(value), GetName(ACTOR_PC, value), PlayerScore[value], PlayerKill[value], PlayerDeath[value], WINNER_REWARD_VIEW[2]);
					SendSystemMail( value, { "SYSTEM_POST_SENDER_NAME", 0 }, { "SYSTEM_POST_SENDER_CONTENTS", 1 }, 0, WINNER_REWARD2 ); -- 보상을 메일로 보냄;
					Log_CaptureTheFlag_Reward(value, 12); -- 보상 로그; (유저DBNum, 보상금 액수);
				else											-- 전투참여도가 100이상이면 훈장3개
					UI_CompetitionResult_CaptureTheFlag_Info(value, 0, GetSchool(value), GetClass(value), GetName(ACTOR_PC, value), PlayerScore[value], PlayerKill[value], PlayerDeath[value], WINNER_REWARD_VIEW[3]);
					SendSystemMail( value, { "SYSTEM_POST_SENDER_NAME", 0 }, { "SYSTEM_POST_SENDER_CONTENTS", 1 }, 0, WINNER_REWARD3 ); -- 보상을 메일로 보냄;
					Log_CaptureTheFlag_Reward(value, 13); -- 보상 로그; (유저DBNum, 보상금 액수);
				end
            end
            _blueKill = _blueKill + PlayerKill[value];
            _blueDeath = _blueDeath + PlayerDeath[value];
        end
        for key,value in pairs( RedTeamList ) do
			--PrintChatXMLMsgAll( { "PVP_CTFLAG_SYSTEM_MESSAGE", 24, GetName(ACTOR_PC, value), PlayerContribute[value]}, COLOR_YELLOW);
            if ( RedTeamPlayerList[value] == true ) then
                if ( PlayerContribute[value] < 10 ) then		-- 전투참여도가 50미만이면 보상없음
					UI_CompetitionResult_CaptureTheFlag_Info(value, 1, GetSchool(value), GetClass(value), GetName(ACTOR_PC, value), PlayerScore[value], PlayerKill[value], PlayerDeath[value], tostring(0));
				elseif ( PlayerContribute[value] < 50 ) then	-- 전투참여도가 10 ~ 49이면 훈장0개
					UI_CompetitionResult_CaptureTheFlag_Info(value, 1, GetSchool(value), GetClass(value), GetName(ACTOR_PC, value), PlayerScore[value], PlayerKill[value], PlayerDeath[value], tostring(0));
					--SendSystemMail( value, { "SYSTEM_POST_SENDER_NAME", 0 }, { "SYSTEM_POST_SENDER_CONTENTS", 6 }, 0, LOSER_REWARD1 ); -- 보상을 메일로 보냄;
					--Log_CaptureTheFlag_Reward(value, 01); -- 보상 로그; (유저DBNum, 보상금 액수);
				elseif ( PlayerContribute[value] < 100 ) then	-- 전투참여도가 50 ~ 99이면 훈장1개
					UI_CompetitionResult_CaptureTheFlag_Info(value, 1, GetSchool(value), GetClass(value), GetName(ACTOR_PC, value), PlayerScore[value], PlayerKill[value], PlayerDeath[value], WINNER_REWARD_VIEW[1]);
					SendSystemMail( value, { "SYSTEM_POST_SENDER_NAME", 0 }, { "SYSTEM_POST_SENDER_CONTENTS", 6 }, 0, LOSER_REWARD2 ); -- 보상을 메일로 보냄;
					Log_CaptureTheFlag_Reward(value, 01); -- 보상 로그; (유저DBNum, 보상금 액수);
				else											-- 전투참여도가 100이상이면 훈장2개
					UI_CompetitionResult_CaptureTheFlag_Info(value, 1, GetSchool(value), GetClass(value), GetName(ACTOR_PC, value), PlayerScore[value], PlayerKill[value], PlayerDeath[value], WINNER_REWARD_VIEW[2]);
					SendSystemMail( value, { "SYSTEM_POST_SENDER_NAME", 0 }, { "SYSTEM_POST_SENDER_CONTENTS", 6 }, 0, LOSER_REWARD3 ); -- 보상을 메일로 보냄;
					Log_CaptureTheFlag_Reward(value, 02); -- 보상 로그; (유저DBNum, 보상금 액수);
				end
            end
			_redKill = _redKill + PlayerKill[value];
            _redDeath = _redDeath + PlayerDeath[value];
        end
        Log_CaptureTheFlag_Result(FACT_RED, false, RedTeamPoint, _redKill, _redDeath);   -- 결과 로그 ( 팀번호, 승리여부, 팀 점수, 팀 킬 수, 팀 데스 수 );
        Log_CaptureTheFlag_Result(FACT_BLUE, true, BlueTeamPoint, _blueKill, _blueDeath);
        
    elseif ( winnerFaction == FACT_NONE ) then		-- 어뷰징
        for key,value in pairs( BlueTeamList ) do
			--PrintChatXMLMsgAll( { "PVP_CTFLAG_SYSTEM_MESSAGE", 24, GetName(ACTOR_PC, value), PlayerContribute[value]}, COLOR_YELLOW);
            if ( BlueTeamPlayerList[value] == true ) then
                UI_CompetitionResult_CaptureTheFlag_Info(value, 1, GetSchool(value), GetClass(value), GetName(ACTOR_PC, value), PlayerScore[value], PlayerKill[value], PlayerDeath[value], tostring(0));
            end
			_blueKill = _blueKill + PlayerKill[value];
            _blueDeath = _blueDeath + PlayerDeath[value];            
        end
        for key,value in pairs( RedTeamList ) do
			--PrintChatXMLMsgAll( { "PVP_CTFLAG_SYSTEM_MESSAGE", 24, GetName(ACTOR_PC, value), PlayerContribute[value]}, COLOR_YELLOW);
            if ( RedTeamPlayerList[value] == true ) then
                UI_CompetitionResult_CaptureTheFlag_Info(value, 1, GetSchool(value), GetClass(value), GetName(ACTOR_PC, value), PlayerScore[value], PlayerKill[value], PlayerDeath[value], tostring(0));
            end
			_redKill = _redKill + PlayerKill[value];
            _redDeath = _redDeath + PlayerDeath[value];
        end
        Log_CaptureTheFlag_Result(FACT_RED, false, RedTeamPoint, _redKill, _redDeath);   -- 결과 로그 ( 팀번호, 승리여부, 팀 점수, 팀 킬 수, 팀 데스 수 );
        Log_CaptureTheFlag_Result(FACT_BLUE, false, BlueTeamPoint, _blueKill, _blueDeath);
        
    end
    UI_CompetitionResult_CaptureTheFlag_End(REWARD_TIME);	-- 자동 나가기 시간.
end
function functionGuideArrow()
--function functionGuideArrow(Red_Red, Red_Blue, Blue_Red, Blue_Blue)
    --RedTeamGuidRed = Red_Red;
    --RedTeamGuidBlue = Red_Blue;
    --BlueTeamGuidRed = Blue_Red;
    --BlueTeamGuidBlue = Blue_Blue;

	-- 레드팀의 레드화살표
    if ( RedTeamGuidRed == GUIDE_NONE ) then	-- NONE상태면
        UI_RedFlagGuideArrow_Off( FACTION_TEAM, FACT_RED );
    elseif ( RedTeamGuidRed == GUIDE_ZONE ) then	-- ZONE상태면
        UI_RedFlagGuideArrow_Off( FACTION_TEAM, FACT_RED );		-- 자신진영의 깃발이 시작위치에 있으면 보여주지 않는다.
		--UI_RedFlagGuideArrow_Target( FACTION_TEAM, FACT_RED, ACTOR_NPC, RedFlag );
    elseif ( RedTeamGuidRed == GUIDE_DROP ) then	-- DROP상태면
        UI_RedFlagGuideArrow_Target( FACTION_TEAM, FACT_RED, ACTOR_NPC, RedDropFlag, true );
    elseif ( RedTeamGuidRed == GUIDE_PLAYER ) then	-- PLAYER상태면
        UI_RedFlagGuideArrow_Target( FACTION_TEAM, FACT_RED, ACTOR_PC, RedFlagPlayer );
    end

	--레드팀의 블루화살표    
    if ( RedTeamGuidBlue == GUIDE_NONE ) then
        UI_BlueFlagGuideArrow_Off( FACTION_TEAM, FACT_RED );
    elseif ( RedTeamGuidBlue == GUIDE_ZONE ) then		-- 적군진영의 깃발이 시작위치에 있으면 보여준다.
		--UI_RedFlagGuideArrow_Off( FACTION_TEAM, FACT_BLUE );
        UI_BlueFlagGuideArrow_Target( FACTION_TEAM, FACT_RED, ACTOR_NPC, BlueFlag );
    elseif ( RedTeamGuidBlue == GUIDE_DROP ) then
        UI_BlueFlagGuideArrow_Target( FACTION_TEAM, FACT_RED, ACTOR_NPC, BlueDropFlag, true );
    elseif ( RedTeamGuidBlue == GUIDE_PLAYER ) then
        UI_BlueFlagGuideArrow_Target( FACTION_TEAM, FACT_RED, ACTOR_PC, BlueFlagPlayer );
    end
	
 	-- 블루팀의 레드화살표   
    if ( BlueTeamGuidRed == GUIDE_NONE ) then
        UI_RedFlagGuideArrow_Off( FACTION_TEAM, FACT_BLUE );
    elseif ( BlueTeamGuidRed == GUIDE_ZONE ) then	-- 적군진영의 깃발이 시작위치에 있으면 보여준다.
		--UI_BlueFlagGuideArrow_Off( FACTION_TEAM, FACT_RED );
        UI_RedFlagGuideArrow_Target( FACTION_TEAM, FACT_BLUE, ACTOR_NPC, RedFlag );
    elseif ( BlueTeamGuidRed == GUIDE_DROP ) then
        UI_RedFlagGuideArrow_Target( FACTION_TEAM, FACT_BLUE, ACTOR_NPC, RedDropFlag, true );
    elseif ( BlueTeamGuidRed == GUIDE_PLAYER ) then
        UI_RedFlagGuideArrow_Target( FACTION_TEAM, FACT_BLUE, ACTOR_PC, RedFlagPlayer );
    end
	
  	-- 블루팀의 블루화살표  
    if ( BlueTeamGuidBlue == GUIDE_NONE ) then
        UI_BlueFlagGuideArrow_Off( FACTION_TEAM, FACT_BLUE );
    elseif ( BlueTeamGuidBlue == GUIDE_ZONE ) then
		UI_BlueFlagGuideArrow_Off( FACTION_TEAM, FACT_BLUE );		-- 자신진영의 깃발이 시작위치에 있으면 보여주지 않는다.
        --UI_BlueFlagGuideArrow_Target( FACTION_TEAM, FACT_BLUE, ACTOR_NPC, BlueFlag );
    elseif ( BlueTeamGuidBlue == GUIDE_DROP ) then
        UI_BlueFlagGuideArrow_Target( FACTION_TEAM, FACT_BLUE, ACTOR_NPC, BlueDropFlag, true );
    elseif ( BlueTeamGuidBlue == GUIDE_PLAYER ) then
        UI_BlueFlagGuideArrow_Target( FACTION_TEAM, FACT_BLUE, ACTOR_PC, BlueFlagPlayer );
    end
end

function functionIndicator(CharDbNum)	-- 재입장 시 UI 그려주는 부분 + 팀버프
		local FactionType, FactionID = GetFaction(ACTOR_PC, CharDbNum);
		
		--ServerLog("Rejoin_Indicator_start "..tostring(CharDbNum));
		UI_Indicator(CharDbNum, INDICATOR_TIME          , GetLeftTime(GamePlayTimer));	-- 남은 시간
		if(FactionID == FACT_RED) then
			--ServerLog("Rejoin_RED_Indicator "..tostring(CharDbNum));
			ApplySkill(ACTOR_PC, CharDbNum, RED_TEAM_BUFF_ID[1], RED_TEAM_BUFF_ID[2], 0, 9999 );		-- 재입장 캐릭터 RED팀버프 적용
			UI_Indicator(CharDbNum, INDICATOR_LEFT_NAME     , ID2SERVERTEXT("PVP_CTFLAG_UI_TEXT", 1), COLOR_GREEN);					-- 팀이름 1
			UI_Indicator(CharDbNum, INDICATOR_RIGHT_NAME    , ID2SERVERTEXT("PVP_CTFLAG_UI_TEXT", 2));					-- 팀이름 2
		elseif(FactionID == FACT_BLUE) then
			--ServerLog("Rejoin_BLUE_Indicator "..tostring(CharDbNum));
			ApplySkill(ACTOR_PC, CharDbNum, BLUE_TEAM_BUFF_ID[1], BLUE_TEAM_BUFF_ID[2], 0, 9999 );		-- 재입장 캐릭터 BLUE팀버프 적용
			UI_Indicator(CharDbNum, INDICATOR_LEFT_NAME     , ID2SERVERTEXT("PVP_CTFLAG_UI_TEXT", 1));					-- 팀이름 1
			UI_Indicator(CharDbNum, INDICATOR_RIGHT_NAME    , ID2SERVERTEXT("PVP_CTFLAG_UI_TEXT", 2), COLOR_GREEN);					-- 팀이름 2
		end
		UI_Indicator(CharDbNum, INDICATOR_LEFT_ICON     , "CTF_INDICATOR_RED_ICON");	-- 팀 아이콘 1
		UI_Indicator(CharDbNum, INDICATOR_RIGHT_ICON    , "CTF_INDICATOR_BLUE_ICON");	-- 팀 아이콘 2
		UI_Indicator(CharDbNum, INDICATOR_LEFT_SCORE    , RedTeamPoint);				-- 획득포인트 1
		UI_Indicator(CharDbNum, INDICATOR_RIGHT_SCORE   , BlueTeamPoint);				-- 획득포인트 2
		UI_Indicator(CharDbNum, INDICATOR_LEFT_IMAGE    , "CTF_INDICATOR_RED_FLAG", g_bRedTeam_Blink);		-- 깃발아이콘1(점멸)
		UI_Indicator(CharDbNum, INDICATOR_RIGHT_IMAGE   , "CTF_INDICATOR_BLUE_FLAG", g_bBlueTeam_Blink);	-- 깃발아이콘2(점멸)
		UI_Indicator(CharDbNum, INDICATOR_OBJECT        , TableToString{"PVP_CTFLAG_UI_TEXT", 0, WIN_POINT});			-- 목표라운드 TEXT
		UI_Indicator(CharDbNum, INDICATOR_SHOW          , true);
		
		functionGuideArrow();	-- 화살표UI 갱신
end

--[[        
function EventRequestMoveGate(nActorID, nMapMainID, nMapSubID, nGateID, nMainMapIDTarget, nSubMapIDTarget, nGateIDTarget)
	return true;
end

]]--
function EventReceiveDamage(nDamagedActorType, nDamagedActorID, nAttackActorType, nAttackActorID, nDamage, nDamageFlag)  -- 어떠한 PC,MOB이 데미지를 받으면 실행되는 부분;
	if ( nil == PlayerDamage[nAttackActorID] ) then
		ServerLog( "[ Capture the flag ] [ EventReceiveDamage, Attacker ( "..nAttackActorType..", "..nAttackActorID.." ]" )
	else
		PlayerDamage[nAttackActorID] = PlayerDamage[nAttackActorID] + nDamage;
		--ServerLog("Damage =  "..tostring(PlayerDamage[nAttackActorID]));--.." "..tostring(GetName(nAttackActorType, nAttackActorID)));
	end
	
	return true,nDamage;
end
 
function EventReceiveHeal(nReceiveActorType, nReceiveActorID, nHealActorType, nHealActorID, nHeal, nHealFlag)  -- 어떠한 PC,MOB이 회복를 받으면 실행되는 부분;
	if ( nil == PlayerHeal[nHealActorID] ) then
		ServerLog( "[ Capture the flag ] [ EventReceiveHeal, Healer ( "..nHealActorType..", "..nHealActorID.." ]" )
	else
		PlayerHeal[nHealActorID] = PlayerHeal[nHealActorID] + nHeal;
		--ServerLog("Heal = "..tostring(PlayerHeal[nHealActorID]));
	end
	
	return true,nHeal; 
end
--[[ 
function EventUseItem(nActorID, nItemID, nParam0, nParam1) -- 유저가 어떠한 Item을 사용하였을때 실행되는 부분;
	return true;
end

function EventPartyOut(nPartyID, bExpedition, nOuterID, bInMapOuter)
end
]]--