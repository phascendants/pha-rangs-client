--
-- 주의) 
-- 1) 대/소 문자를 확실하게 구분해야 합니다.
-- 2) true, false 를 확실하게 구분해야 합니다.
-- 3) UCS-2 Little Endian 로 저장해야 합니다.
-------------------------------------------------------------------------------
-- 버전 표기는 반드시 있어야 하며, 로직단과 반드시 일치해야 한다;
SCRIPT_VERSION = 
{
    "AlphaServer",     -- main version
    0,    -- sub version
};

-- 초기화시 저절로 기입되는 변수로 절대 선언 되면 안됩니다.;
-- keyMapID = 선언금지 ( 초기화 이후 저절로 세팅 됨 );
-- instanceMapID = 선언금지 ( 초기화 이후 저절로 세팅 됨 );

--------------------------------------------------------------------------------
-- ActorType
ACTOR_PC								= 0
ACTOR_NPC								= 1
ACTOR_MOB								= 2
ACTOR_SUMMON						= 6
ACTOR_NULL							= -1


--------------------------------------------------------------------------------
-- FactionType
FACTION_TEAM						= 0
FACTION_PARTY						= 1
FACTION_CLUB							= 2
FACTION_EXPEDITION 				= 3


--------------------------------------------------------------------------------
-- RelationType
RELATION_ENEMY					= 0
RELATION_ALLY						= 1
RELATION_NEUTRAL_ENEMY		= 2
RELATION_NEUTRAL_ALLY		= 3


--------------------------------------------------------------------------------
-- TriggerType
TRIGGER_PC								= 1
TRIGGER_MOB							= 2
TRIGGER_PC_MOB					= 3
TRIGGER_SUMMON					= 4
TRIGGER_PC_SUMMON 			= 5


--------------------------------------------------------------------------------
-- TimerMessageBox Parameter
PARAM3_MSGBOX_OK				= 0
PARAM3_MSGBOX_CANCEL		= 1
PARAM3_MSGBOX_TIMEOVER	= 2


--------------------------------------------------------------------------------
-- ColorCode
COLOR_BLACK							= { 0, 0, 0 }
COLOR_WHITE							= { 255, 255, 255 }
COLOR_RED								= { 255, 0, 0 }
COLOR_GREEN							= { 0, 255, 0 }
COLOR_BLUE							= { 128, 194, 235 }
COLOR_MAGENTA						= { 240, 100, 170 } 
COLOR_YELLOW						= { 255, 200, 0 }


--------------------------------------------------------------------------------
-- CustomMessage Sender Type
MSG_TO_USER							= 0    					-- 특정 유저;   SenderID : CharDbNum;
MSG_TO_MY_INSTANCE			= 1    					-- 현재 자신이 속한 필드서버 인던 객체 (LevelField)  SenderID : 의미없음;
MSG_TO_FIELD_BASE				= 2    					-- 필드서버의 특정 인던 컨텐츠 (PublicField);  SenderID : Instance KeyMap ID;
MSG_TO_FIELD_INSTANCE		= 3    					-- 필드서버의 특정 인던 객체 (LevelField);  SenderID : Instance Map ID;
MSG_TO_AGENT_BASE				= 4    					-- 에이전트서버의 특정 인던 컨텐츠 (PublicAgent);  SenderID : Instance KeyMap ID;
MSG_TO_AGENT_INSTANCE		= 5    					-- 에이전트서버의 특정 인던 객체 (LevelAgent);  SenderID : Instance Map ID;


--------------------------------------------------------------------------------
-- Retry Result Flag
RETRY_OK								= 0
RETRY_FAIL								= 1
RETRY_TIMEOVER						= 2
RETRY_INVALID_MONEY			= 3


--------------------------------------------------------------------------------
-- DimensionLab 설정 상수

BATTLE_MAP_ID						= { 261, 0 }			-- Map ID;

REWARD_TIME								= 300			--보상 대기 시간
DESTROY_TIME							= 60						-- 인던 파괴전의 대기 시간;
WAIT_MOVE_PHASE_TIME			= 5						-- 페이즈 이동 대기 시간;
INIT_TIME									= 10						-- 게임 시작전 대기 시간;
PLAY_TIME								= 3600					-- 게임 진행 시간(초);
NOTIFY_GAMESTATE_TIME		= 25						-- 게임 진행 도움말 공지 주기;
AUTH_TIME								= 10						-- 인증 진행 시간;

-- 게임 종료 알림 주기;
NOTIFY_GAME_END_TIME		 	= { 55, 50, 45, 40, 35, 30, 25, 20, 15, 10, 5, 4, 3, 2, 1 }

-- 입장 시 시작위치; math.random(250, 280), math.random(-940, -960)
PARTY_START_POS 					= { 265, -943 }

-- 인증기 정보;
CERTIFICATION_ID 					= { 105, 8 }			-- 인증기 NPC MID/SID;
CERTIFICATION_POS 				= { -137, 71 }			-- 인증기 드랍위치;

-- Timer Event Code
EVENT_DESTROY						= 2						-- 인던 파괴 ( 게임종료 -> 보상 종료 후 )
EVENT_GAME_START				= 4						-- 게임 시작;
EVENT_GAME_END					= 5						-- 게임 종료;
EVENT_NOTIFY_GAMESTATE		= 10						-- 게임진행을 위한 도움말 공지;
EVENT_NOTIFY_GAMEEND		= 11						-- 게임종료시간 공지;

EVENT_UI_START_1					= 15						-- 게임 시작 알림 메시지;
EVENT_UI_START_2					= 16						-- 목표 알림 메시지;
EVENT_PHASE_SUMMONBOSS	= 18
EVENT_PHASE_1_START			= 19
EVENT_PHASE_1_END				= 20
EVENT_PHASE_2_START			= 21
EVENT_PHASE_2_LEVEL1			= 22
EVENT_PHASE_2_LEVEL2			= 23
EVENT_PHASE_2_END				= 24
EVENT_PHASE_3_START			= 25
EVENT_CHOI_INFO					= 27
EVENT_CHOI_INFO_1					= 28
EVENT_CHOI_INFO_2					= 29
EVENT_CHOI_INFO_3					= 30
EVENT_CHOI_INFO_4					= 31

-- CutsomParameter list
PARAM1_EXIT_BUTTON          	= 1						-- Result UI 의 나가기 버튼 클릭 시의 Parameter 값;

-- Mob 정보;
TABLE_MOB_ID = {
			{ 105, 9 },				-- 1보스 (최태규) NPC MID/SID;
			{ 105, 10 },			-- 2강화보스 (최태규) ID
			{ 105, 11 },			-- 3돌격대 대장
			{ 105, 12 },			-- 4경호대 대장
			{ 105, 13 },			-- 5연구대 대장
			{ 105, 14 },			-- 6선봉대 대장
			{ 105, 15 },			-- 7선봉대 와일드암
			{ 105, 16 },			-- 8선봉대 머신암
			{ 105, 17 },			-- 9연구대 슬라이서
			{ 105, 18 },			-- 10연구대 리버서
			{ 105, 19 },			-- 11경호대 서전트
			{ 105, 20 },			-- 12경호대 닌자
			{ 105, 21 },			-- 13돌격대 실험체
			{ 105, 22 },			-- 14돌격대 토트
			{ 105, 23 }, 			-- 15오비나인
			{ 105, 1 },				-- 16경비대 소총병
			{ 105, 2 },				-- 17경비대 봉술병
			{ 105, 3 },				-- 18경비대 기갑정찰병
			{ 105, 4 },				-- 19경비대 정찰견
			{ 105, 5 },				-- 20경비대 캐논병
			{ 105, 6 },				-- 21경비대 폭탄병
			{ 105, 7 },				-- 22경비대 검도부장
		}
			
TABLE_MOB_POS = { 
			{ -137, 71 },			-- 보스, 강화보스 좌표
			{ 101, 34 },			-- 돌격대 대장
			{ 143, -119 },			-- 경호대 대자
			{ -10, -220 },			-- 연구대 대장
			{ -121, -140 },		-- 선봉대 대장
		}

-- 재도전 정보;
TABLE_RETRY_MONEY = {
			2000000, 				-- 재도전 금액 1~10회차, 10회차 이상은 10회 비용으로
			2200000,				-- 2
			2420000,				-- 3
			2662000,				-- 4
			2928200,				-- 5
			3221020,				-- 6
			3543122,				-- 7
			3897434,				-- 8
			4287177,				-- 9
			4715895					-- 10
		}
				
TABLE_RETRY_LEVEL_RATE = {
			0.5,						-- 1, 250 파티 평균 레벨에 따른 재도전 비용적용 비율
			0.55,						-- 2, 251
			0.6,						-- 3, 252
			0.65,						-- 4, 253
			0.7,						-- 5, 254
			0.75,						-- 6, 255
			0.8,						-- 7, 256
			0.85,						-- 8, 257
			0.9,						-- 9, 258
			0.95,						-- 10, 259
			1.0,						-- 11, 260
			1.05,						-- 12, 261 (2017.02.15)
			1.1,						-- 13, 262
			1.15,						-- 14, 263
			1.2,						-- 15, 264
			1.25,						-- 16, 265
			1.3,						-- 17, 266
			1.35,						-- 18, 267
			1.4,						-- 19, 268
			1.45,						-- 20, 269
			1.5,						-- 21, 270
			1.55,						-- 22, 271
			1.6,						-- 23, 272
			1.65,						-- 24, 273
			1.7,						-- 25, 274
			1.75,						-- 26, 275
			1.8,						-- 27, 276
			1.85,						-- 28, 277
			1.9,						-- 29, 278
			1.95,						-- 30, 279
			2.0,						-- 31, 280				
		}

-- 삭제하고자 하는 스킬 효과의 인덱스를 아래에 등록한다.
REMOVE_SKILLEFFECT = {

	16,		-- 은신
	21,		-- 물리 데미지 흡수
	22,		-- 마법 데미지 흡수
	36,		-- 데미지 흡수(무시)
	53,		-- 면역
	
}

--[[
-- 인증 시도시 삭제 할 버프 정보;						
TABLE_REMOVE_BUFF_ID = {
			{ 29, 1 },		-- 은신
			{ 29, 7 },
			{ 29, 8 },
			{ 29, 9 },
			{ 29, 15 },
			{ 34, 25 },
			{ 44, 12 },
			{ 45, 4 },
			{ 45, 12 },
			{ 55, 5 },
			{ 60, 3 },
			{ 3, 14 },		-- 면역
			{ 7, 12 },
			{ 7, 13 },
			{ 29, 2 },
			{ 29, 46 },
			{ 29, 47 },
			{ 29, 48 },
			{ 29, 59 },
			{ 29, 60 },
			{ 34, 7 },
			{ 34, 8 },
			{ 34, 38 },
			{ 34, 66 },
			{ 39, 8 },
			{ 43, 12 },
			{ 55, 1 },
			{ 55, 2 },
			{ 60, 5 },
			{ 3, 9 },		-- 물리 데미지 흡수
			{ 7, 6 },
			{ 29, 38 },
			{ 29, 44 },
			{ 33, 10 },
			{ 42, 5 },
			{ 42, 16 },
			{ 59, 6 },
			{ 7, 7 },		-- 마법 데미지 흡수
			{ 34, 16 },
			{ 42, 7 },
			{ 42, 18 },
			{ 59, 4 },
			{ 14, 11 },		-- 물리/마법 데미지 흡수
			{ 29, 58 },
			{ 34, 20 },
			{ 60, 9 },
			{ 14, 16 },		-- 데미지 흡수 (무시)
			{ 14, 18 },
			{ 29, 41 },
			{ 41, 2 },
			{ 41, 28 },
			{ 41, 29 },
			{ 60, 13 },
			{ 68, 5 },		-- 변술부 백호 변장 (데미지 흡수 (무시))
			{ 68, 8 },		-- 변술부 거북이 변장 (데미지 흡수 (무시))
		}
]]--
		
-- 스킬 정보;
SKILL_INVINCIBLE 					= { 19, 23 }			-- 무적(면역, 반격)상태 스킬 MID/SID

-- 인증 모션 ID
MOTION_CERTIFY 					= { 10, 56 }

-- 각 페이즈 상수
PHASE_WAIT								= 0
PHASE_START							= 1
PHASE_SUMMONBOSS				= 2
PHASE_1_START						= 3
PHASE_1_END							= 4
PHASE_2_START						= 5
PHASE_2_LV1_START					= 6
PHASE_2_LV2_START					= 7
PHASE_2_END							= 8
PHASE_3_START						= 9
PHASE_CLEAR							= 10
PHASE_END							= 10000



-------------------------------------------------------------------------------------------------------
-- DimensionLab 전역 변수
				
-- Mob 관리 정보;
g_nBossCrowID 						= {}						-- 보스 ID <- DropCrowID;
g_nEnhancedBossCrowID 			= {}						-- 강화보스 ID <- DropCrowID;
g_nMiddleBossCrowID 				= {}						-- 1phase 사대장 ID <- DropCrowID;
g_nMiddleBoss2CrowID 				= {}						-- 2phase 사대장 ID <- DropCrowID;
g_nNormalCrowID 						= {}						-- 일반 몹 ID <- DropCrowID;
g_nBoss_MaxHP 						= nil						-- 보스 몹 HP 총량;
g_nEnhancedBoss_MaxHP 			= nil						-- 강화보스 몹HP 총량;

g_n4thCrowDeathNum 				= 0						-- 1phase 사대장 사망 수;
g_n4th2CrowDeathNum 				= 0						-- 2phase 사대장 사망 수;
g_nNormalCrowDieNum 				= 0						-- 75 ~ 25%에 나오는 노말 몹 사망수;

-- 인증기 관리 정보;
g_nCertificationID 						= nil						-- 인증기 DropCrowID 담는 변수;
g_nCertifyUser_CurrentMachine 	= {}						-- 유저의 인증 시도 중인 인증기;
g_bCertify 									= false					-- 인증 여부;
g_bCertifying 								= false					-- 인증 중 여부;

-- 전멸 후 재도전 횟수;
g_nRetryCount 							= 1

-- 현재 유효한 유저수;
g_nPartyCount							= 0

-- 인던의 Party Master ID;
g_nPartyMasterID 						= nil

-- 인던 인원 관리 Table ( 인원 관리도 개선 할 필요가 있다 );
g_tablePartyMember 					= {}						-- 입장 파티 리스트;
g_tablePartyMemberStatus 			= {}						-- 파티원의 플레이 상태 여부;

-- Game 상태 관리 ( 하나의 변수로 개선 할 필요가 있다 );
-- 페이즈 관리도 개선되어야 할 필요가 있다, 코드가 분산되어 관리가 힘들다;
g_bGameStart 							= false                	-- 게임 시작 여부;
g_bGameEnd 							= false					-- 인던 종료(보스 처치 기준) 여부;
g_bPhase1 								= false					-- 페이즈1 진행여부;
g_bPhase2 								= false					-- 페이즈2 진행여부;
g_bPhase3 								= false					-- 페이즈3 진행여부;
g_bFail										= false					-- 공략 실패 여부

g_nStage									= PHASE_WAIT		-- 현재 스테이지;

-- Timer 목록;
g_timerGameStartWait 				= nil						-- 게임 시작 전 대기 Timer;
g_timerGamePlay 						= nil						-- 게임 플레이 Timer;
g_timerNotifyHelp 						= nil						-- 게임 진행을 위한 도움말 Timer;
g_timerPhase1 							= nil						-- 페이즈1 타이머;
g_timerPhase2 							= nil						-- 페이즈2 타이머; 
g_timerPhase3 							= nil						-- 페이즈3 타이머;
g_timerPhase4 							= nil						-- 페이즈4 타이머;


-------------------------------------------------------------------------------------------------------
-- 인던이 생성되면서 호출되는 함수 ( 이곳에서 인던 관련 초기사항을 세팅해준다 );
function EventInitialize ( )	
	
	-- 인증기;
	g_nCertificationID = DropCrow(
		BATTLE_MAP_ID[1],
		BATTLE_MAP_ID[2],
		CERTIFICATION_ID[1],
		CERTIFICATION_ID[2],
		CERTIFICATION_POS[1],
		CERTIFICATION_POS[2],
		315 )
	
	-- 게임 시작 Timer 등록;
	g_timerGameStartWait = RegistTimer( INIT_TIME, EVENT_GAME_START )
	
	return true
	
end


-------------------------------------------------------------------------------------------------------
-- 유저가 인던에 입장 요청시 호출되는 함수;
function EventRequestJoin ( _nActorID )

	-- 게임이 진행 중일 경우 파티가 있다면 입장을 받지 않는다;
	if ( g_bGameStart ) then
	
		local nPartyID = GetPartyID( _nActorID )
		if ( nil ~= nPartyID ) then
			return false
		end
		
	end
	
	-- 인던이 종료되었으면 더이상 입장을 받지 않는다;
	if ( g_bGameEnd == true ) then	
	
		return false
		
	end
	
	return true
	
end


-------------------------------------------------------------------------------------------------------
-- 유저가 인던에 입장시 호출되는 함수;
function EventJoin ( _nActorID )

	-- 최초 입장이라면 g_tablePartyMember 에 넣어준다;
	if ( g_tablePartyMemberStatus[_nActorID] == nil ) then
	
		-- Party 를 알아보고 Party Master ID를 등록한다;
		local nPartyID = GetPartyID( _nActorID )
		if ( nil == nPartyID ) then
			
			-- Party 가 아니면 쫓아낸다;
			DoOut( _nActorID )
			
		end
			
		local nPartyMasterID = GetPartyMaster( nPartyID )
		if ( nil == nPartyMasterID ) then
		
			-- Party Master 가 없으면 쫓아낸다 ( 있을 수 없는 경우이다 );
			DoOut( _nActorID )
			
		end
		
		g_nPartyMasterID = nPartyMasterID;
		
		-- 인원 관리 테이블에 등록;
		table.insert( g_tablePartyMember, _nActorID )
		
		-- 플레이 상태를 활성화 한다;
		g_tablePartyMemberStatus[_nActorID] = true
		g_nPartyCount = g_nPartyCount + 1
		
		ServerLog( "[ DimensionLab Log ] [ First Join "..tostring( _nActorID ).." / "..tostring( g_tablePartyMemberStatus[_nActorID] ).." ]" )
		
		Log_EnterUser( "DimensionLab", BATTLE_MAP_ID[1], _nActorID, g_nStage, #g_tablePartyMember, g_nPartyCount, g_nRetryCount-1 )
		
	-- 재입장이라면 파티에 가입시킨다;
	elseif ( g_tablePartyMemberStatus[_nActorID] == false ) then
	
		-- 파티 가입;
		JoinParty( g_nPartyMasterID, _nActorID )
		
		-- 진영 등록;
		local nFactionType, nFactionID = GetFaction( ACTOR_PC, g_nPartyMasterID )
		SetFaction( ACTOR_PC, _nActorID, nFactionType, nFactionID )
		
		-- 플레이 상태를 활성화 한다;
		g_tablePartyMemberStatus[_nActorID] = true
		g_nPartyCount = g_nPartyCount + 1
		
		ServerLog( "[ DimensionLab Log ] [ Rejoin "..tostring( _nActorID ).." / "..tostring( g_tablePartyMemberStatus[_nActorID] ).." ]" )
		
		Log_ReenterUser( "DimensionLab", BATTLE_MAP_ID[1], _nActorID, g_nStage, #g_tablePartyMember, g_nPartyCount, g_nRetryCount-1 )
		
	end
	
	
	-- 스타팅 포인트에 배치한다 ( 260, -943 );
	return PARTY_START_POS[1], PARTY_START_POS[2]
	
end


-------------------------------------------------------------------------------------------------------
-- 유저가 인던에서 나갈 때 호출되는 함수;
function EventOut ( _nActorID, _nToMapMID, _nToMapSID )

	ServerLog( "[ DimensionLab Log ] [ Out "..tostring( _nActorID ).." / "..tostring( g_tablePartyMemberStatus[_nActorID] ).." ]" )
	
	-- "OOO님이 이탈하였다" 메시지를 출력한다;
	PrintChatXMLMsg( _nActorID, { "PVP_CTFLAG_SYSTEM_MESSAGE", 13, GetName( ACTOR_PC, _nActorID ) }, COLOR_RED )
	
	-- 파티에서 탈퇴시킨다;
    --OutParty( _nActorID )
	
	-- 이동 불가 상태 해제;
	SetMoveLock( ACTOR_PC, _nActorID, true )
	
	-- 인던 UI 해제;
	UI_Timer( _nActorID, 0, false )
	
	-- 플레이 상태를 비활성화 한다;
	g_tablePartyMemberStatus[_nActorID] = false
	g_nPartyCount = g_nPartyCount - 1
	
	Log_OutUser( "DimensionLab", BATTLE_MAP_ID[1], _nActorID, g_nStage, #g_tablePartyMember, g_nPartyCount, g_nRetryCount-1 )
	
	-- 1명이하가 되면 인던을 파괴한다;
	if ( g_nPartyCount == 1 ) then
	
		g_bFail = true
		LuaGlobalFunction_END_GAME(DESTROY_TIME)
	
	-- 인던에 아무도 없으면 인던을 즉시 파괴한다;
	elseif ( g_nPartyCount <= 0  ) and ( g_bGameEnd ) then
		
		LuaGlobalFunction_DESTROY_GAME()
		
	end
	
end


-------------------------------------------------------------------------------------------------------
-- 유저가 맵에 입장할 때 호출되는 함수;
function EventMapEnter ( _nActorID )

	ServerLog( "[ DimensionLab Log ] [ Enter Map "..tostring( _nActorID ).." ]" )
	
	-- 아직 게임 시작 상태가 아니라면 처리한다;
	if ( g_bGameStart == false ) then
	
		-- 10초 대기 메시지를 출력한다 (화면 및 채팅창 );
		PrintSimpleXMLMsgAll( 8, { "PVE_DIMENSIONLAB_SYSTEM_MESSAGE", 0, INIT_TIME }, COLOR_YELLOW )
		PrintChatXMLMsg( _nActorID, { "PVE_DIMENSIONLAB_SYSTEM_MESSAGE", 0, INIT_TIME }, COLOR_BLUE )
		
		-- 이동 불가 상태;
		SetMoveLock( ACTOR_PC, _nActorID, false )
		ServerLog( "[ DimensionLab Log ] [ Start MoveLock "..tostring( _nActorID ).." ]" )
		
		-- 입장 상태 UI 를 출력한다;
		local nFactionType, nFactionID = GetFaction( ACTOR_PC, _nActorID );
		UI_EntranceState( nFactionType, nFactionID, g_nPartyCount, GetPartyMemberNum( GetPartyID( _nActorID ) ), GetLeftTime( g_timerGameStartWait ), true );
	
	-- 게임이 진행중인데 입장할 경우 처리한다;
	else
	
		UI_Timer( _nActorID, GetLeftTime( g_timerGamePlay ), true )
	
    end
	
end


-------------------------------------------------------------------------------------------------------
-- 파티 참가가 이루어질 경우 호출되는 함수;
--[[function EventPartyJoin ( nPartyID, bExpedition, nJoinerID, bInSameInstance )

	if ( bInSameInstance == false ) then
		-- 참가 자격이 있는 유저인지를 검사;
		local master = GetPartyMaster(nPartyID);
        
		for key,value in pairs( g_tablePartyMember ) do
			if(value == nJoinerID) then	--파티리스트에 있는 참가자이면 
				UI_XmlTimerMessageBox(nJoinerID, { "RNCDM_REJOIN_TEXT", 0 },{ "RNCDM_REJOIN_TEXT", 1 },{ "RNCDM_REJOIN_TEXT", 2 }, 30, PARAM1_JOIN_UI, 0); --gameserver	재입장 UI출력
			end
		end
	end
	
end]]--


-------------------------------------------------------------------------------------------------------
-- 파티 마스터가 변경될 경우 호출되는 함수;
function EventPartyChangeMaster ( _nPartyID, _bExpedition, _nNewMasterID, _bInMapNewMaster, _nOldMaster, _bInMapOldMaster )

	g_nPartyMasterID = _nNewMasterID
	
end


-------------------------------------------------------------------------------------------------------
--재도전 처리가 이루어질 경우 호출되는 함수;
function EventRetry ( _nActorID, _bAccept, _emResultFlag )
	local nFactionType, nFactionID = GetFaction( ACTOR_PC, _nActorID );

	if ( 0 == _nActorID ) then -- 타임오버
		
		ServerLog( "[ DimensionLab Log ] [ Don't accept Retry, because It was all of the time limit. ]" )
		PrintChatXMLMsgFaction(nFactionType, nFactionID, {"PVE_RESULT_CODE_CANCEL_TEXT", 3}, COLOR_RED)		-- 재도전 타임아웃
		
	--elseif ( true == _bAccept ) then
	elseif ( _emResultFlag == 0 ) then	-- 수락
		
			ServerLog( "[ DimensionLab Log ] [ Accept Retry "..tostring( _nActorID ).." ]" )
			PrintChatXMLMsgFaction(nFactionType, nFactionID, {"PVE_RESULT_CODE_CANCEL_TEXT", 2, GetName( ACTOR_PC, _nActorID )}, COLOR_BLUE)	-- 재도전 수락
				

	elseif ( _emResultFlag == 1) then		-- 거절
			
			ServerLog( "[ DimensionLab Log ] [ Don't accept Retry "..tostring( _nActorID ).." ]" )
			PrintChatXMLMsgFaction(nFactionType, nFactionID, {"PVE_RESULT_CODE_CANCEL_TEXT", 0, GetName( ACTOR_PC, _nActorID )}, COLOR_RED)		-- 재도전 거절
		
	elseif ( _emResultFlag == 3) then		-- 소지금 부족
			
			ServerLog( "[ DimensionLab Log ] [ Don't accept Retry "..tostring( _nActorID ).." ]" )
			PrintChatXMLMsgFaction(nFactionType, nFactionID, {"PVE_RESULT_CODE_CANCEL_TEXT", 1, GetName( ACTOR_PC, _nActorID )}, COLOR_RED)		-- 수락 > 소지금 부족 
		--end
	end
	
end

function EventCompleteRetry ( _nFactionType, _nFactionID, _bSuccess, _emResultFlag )
			
	if ( _bSuccess ) then
	
		-- 모두가 재도전을 했다면 처리한다
		local listFactionMember = GetFactionList( _nFactionType, _nFactionID );
		if ( listFactionMember ~= nil ) then
		
			for key, value in pairs ( listFactionMember ) do
				SetResurrectionForcedInstancePosition(
					value[2],
					BATTLE_MAP_ID[1],
					BATTLE_MAP_ID[2],
					PARTY_START_POS[1],
					PARTY_START_POS[2],
					2,
					100 )
			end
			
		end
		
		g_nRetryCount = g_nRetryCount + 1;
		if ( g_nRetryCount > 10 ) then		-- 등급이 10을 넘는경우 10으로 책졍.
			g_nRetryCount = 10
		end
		
		ServerLog( "[ DimensionLab Log ] [ Retry Game "..tostring( instanceMapID ).." ]" )
		
	else
		-- 한명이라도 하지 않았다면 처리한다
		g_bFail = true
		LuaGlobalFunction_END_GAME(DESTROY_TIME)
	end
	
end


-------------------------------------------------------------------------------------------------------
-- 플레이어가 모두 죽을 경우 호출되는 함수;
function EventAllDie ( _nFactionType, _nFactionID )

	local nSumLevel = 0
	local nAverageLevel = 0
	local nGrade = 0
	
	for nKey, nMemberID in pairs( g_tablePartyMember ) do
		if ( true == g_tablePartyMemberStatus[ nMemberID ] ) then
			nSumLevel = nSumLevel + GetLevel( ACTOR_PC, nMemberID )		-- 파티 총레벨 
		end
	end
	
	
	nAverageLevel = math.ceil( nSumLevel / g_nPartyCount )					-- 파티 평균레벨 산출
	if(nAverageLevel <= 280) then
		nGrade = nAverageLevel - 249											-- 평균레벨 기준으로 적용할 머니비율(등급)을 결정
	else
		nGrade = 31		-- 280이상은 280으로 설정된 값으로 계산
	end
	if ( ( g_nRetryCount <= #TABLE_RETRY_MONEY and g_nRetryCount > 0  ) and
		( nGrade <= #TABLE_RETRY_LEVEL_RATE and nGrade > 0 ) ) then
		
		local nMoney = TABLE_RETRY_MONEY[g_nRetryCount] * TABLE_RETRY_LEVEL_RATE[nGrade]
		
		ServerLog( "[ DimensionLab Log ] [ All Die "..tostring( instanceMapID ).." ]" )
		
		RetryFaction(

			_nFactionType,	-- Faction Type
			_nFactionID,		-- Faction ID
			60,					-- Time
			nMoney )			-- 돈(Retry머니[재도전횟수) * retry비율[등급])
			
		Log_Retry( "DimensionLab", BATTLE_MAP_ID[1], nMoney, g_nStage, #g_tablePartyMember, g_nPartyCount, g_nRetryCount-1 )
			
	else
	

		ServerLog( "[ DimensionLab Log ] [ Error!!! EventAllDie Function, Invalid RetryCount or Grade : "..tostring( g_nRetryCount-1 ).." / "..tostring( nGrade ).." ]" )
		
	end
	
end


-------------------------------------------------------------------------------------------------------
-- Actor 가 죽었을 때 호출되는 함수;
function EventDie ( _nDieActorType, _nDieActorID, _nKillActorType, _nKillActorID )

	-- 죽은 놈이 플레이어이면 처리한다;
	if ( _nDieActorType == ACTOR_PC ) then
	
		ServerLog( "[ DimensionLab Log ] [ Die "..tostring( _nDieActorID ).." ]" )
		
		--false 이면 스킬부활은 가능하다는 의미이다;
		SetResurrectionDisable( _nDieActorID, false );
		
	-- 죽은 놈이 몬스터이면 처리한다;
	elseif ( _nDieActorType == ACTOR_MOB ) then
	
		-- 1차 사대장의 누적 사망수를 계산한 후 다 죽으면 이벤트 진행
		if ( _nDieActorID == g_nMiddleBossCrowID[_nDieActorID] ) then
		
			g_n4thCrowDeathNum = g_n4thCrowDeathNum + 1
			if ( g_n4thCrowDeathNum >= 4 ) then
			
				-- 페이즈 1 종료;
				RegistTimer( WAIT_MOVE_PHASE_TIME, EVENT_PHASE_1_END )
				
			end
			
		end
		
		-- 2차 사대장의 누적 사망수를 계산한 후 다 죽으면 이벤트 진행
		if ( _nDieActorID == g_nMiddleBoss2CrowID[_nDieActorID] ) then
		
			g_n4th2CrowDeathNum = g_n4th2CrowDeathNum + 1
			if ( g_n4th2CrowDeathNum >= 4 ) then
			
				-- 페이즈 2 레벨 2 종료;
				RegistTimer( WAIT_MOVE_PHASE_TIME, EVENT_PHASE_2_END )
				
			end
			
		end
		
		-- 죽은 놈이 보스(최태규)이면 페이즈 2 시작;
		if ( _nDieActorID == g_nBossCrowID[_nDieActorID] ) then
		
			-- 페이즈 2 시작;
			RegistTimer( WAIT_MOVE_PHASE_TIME, EVENT_PHASE_2_START )
			
			-- 최태규 사망알림 및 앵그리 태규 출현 알림
			PrintSimpleXMLMsgAll( 4, { "PVE_DIMENSIONLAB_SYSTEM_MESSAGE", 9 }, COLOR_YELLOW )
			
		end
		
		-- 죽은놈이 일반몹이면 카운팅한다;
		if ( _nDieActorID == g_nNormalCrowID[_nDieActorID] ) then
		
			g_nNormalCrowDieNum = g_nNormalCrowDieNum + 1
			--ServerLog( "Normal Mob Count"..tostring(g_nNormalCrowDieNum) )
			
		end
		
		-- 죽은놈이 보스(앵그리 최태규)이면 최태규의 상태를 알리고 게임을 종료한다;
		if ( _nDieActorID == g_nEnhancedBossCrowID[_nDieActorID] ) then
		
			-- 최태규 사망알림 및 앵그리 태규 출현 알림
			PrintSimpleXMLMsgAll( 3, { "PVE_DIMENSIONLAB_SYSTEM_MESSAGE", 16 }, COLOR_WHITE )
			
			-- 최태규의 상태를 알린다;
			RegistTimer( WAIT_MOVE_PHASE_TIME, EVENT_CHOI_INFO_3 )
			
			-- 게임을 종료한다;
			LuaGlobalFunction_END_GAME(REWARD_TIME)
			
		end
		
	end
	
  	return true
	
end


-------------------------------------------------------------------------------------------------------
-- 부활이 이루어질 경우 호출되는 함수;
function EventResurrect ( _nResurrectType, _nResurretActorType, _nResurrectActorID, _nKillActorType, _nKillActorID )

	ServerLog( "[ DimensionLab Log ] [ Resurrect "..tostring( _nResurrectActorID ).." ]" )

	return true
	
end


-------------------------------------------------------------------------------------------------------
-- 트리거 클릭 시 호출되는 함수;
function EventClickTrigger ( _nTriggerType, _nTriggerID, _nClickerType, _nClickerID )
	
	-- 자신이 이미 인증중인 경우 처리하지 않는다;
	if ( g_nCertifyUser_CurrentMachine[_nClickerID] ~= nil ) then
        
        return
		
    end
	
	local nClickerHP = GetHP( _nClickerType, _nClickerID )
	local nPartyID = GetPartyID( _nClickerID )
	
	-- 트리거를 실행한 액터가 정상인지 확인한다;
	if ( g_tablePartyMemberStatus[_nClickerID] == true ) and ( nClickerHP > 0 ) then

		-- 인증 가능 여부 체크한다;
		if ( g_bCertify == false ) and ( g_bGameEnd == false ) then
			
			-- 인증 중으로 변경한다;
			g_bCertifying = true
			
			-- 인증을 시도한다;
			LuaGlobalFunction_TryCertification( nPartyID, _nTriggerID, _nClickerID )
			
		end
		
	end
   
end


-------------------------------------------------------------------------------------------------------
-- 모션 처리가 종료되면 호출되는 함수;
function EventMotionFinish ( _nActorType, _nActorID, _nMotionMid, _nMotionSid, _nMotionRemainSec )

	-- 인증 모션 남은시간이 0초 이하라면
	if ( _nMotionRemainSec <= 0 ) then
	
		g_bCertifying = false
		
		-- 인증을 성공처리한다;
		LuaGlobalFunction_SuccessCertification( _nActorID )
		
	else 
	
		g_bCertifying = true
		
    end
	
	UI_Auth( _nActorID, false )
	
	-- 유저가 인증 중인 인증기 없다로 전환;
	g_nCertifyUser_CurrentMachine[_nActorID] = nil
	
end


-------------------------------------------------------------------------------------------------------
-- Actor 가 대미지를 받을 경우 호출되는 함수;
function EventReceiveDamage ( _nDamagedActorType, _nDamagedActorID, _nAttackActorType, _nAttackActorID, _nDamage, _nDamageFlag )
	
	-- 미스가 났을 경우 그냥 미스를 낸다;
	if ( _nDamage <= 0 ) then
		return true, _nDamage
	end
			
	-- 현재 피격당한 액터가 인증중이라면 처리한다;
	if ( g_nCertifyUser_CurrentMachine[_nDamagedActorID] ~= nil ) then
		
		-- 피격당한다면 인증을 실패한다;
		if ( _nDamage > 0 ) and ( IsMotion( _nDamagedActorType, _nDamagedActorID, MOTION_CERTIFY[1], MOTION_CERTIFY[2] ) == true ) then
				
			ResetMotion( _nDamagedActorType, _nDamagedActorID )
			g_bCertifying = false
			
		end
	
	-- 최태규가 피격당했을 경우 처리한다;
	elseif ( _nDamagedActorID == g_nBossCrowID[_nDamagedActorID] ) then
	
		local nBossHP = GetHP( _nDamagedActorType, _nDamagedActorID )
		
		-- HP가 50% 이하이고 (test 90%), Phase 1(사대장소환) 시작하지 않았으면 처리한다;
		if ( nBossHP <= ( g_nBoss_MaxHP * 0.5 ) and ( g_bPhase1 == false ) ) then

			-- EVENT_PHASE_1 이 시작되기 전 최태규를 무적으로 만든다;
			
			-- 보스HP회복률을 0으로 만든다;
			SetHPRestorationRate( _nDamagedActorType, _nDamagedActorID, 0 )
			
			-- 최태규에 면역버프를 부여한다;
			ApplySkill(
				_nDamagedActorType,
				_nDamagedActorID,
				SKILL_INVINCIBLE[1],
				SKILL_INVINCIBLE[2],
				1,
				9999,
				5 )
			
			--EVENT_PHASE_1 이벤트 시작한다;
			if ( g_timerPhase1 == nil ) then
			
				g_timerPhase1 = RegistTimer( 1, EVENT_PHASE_1_START )
				
			end
			
		end
		
		-- HP 50% 이하이고, 4대장이 소환되고 죽기전까지 최태규에게 대미지가 들어가지 않는다;
		if ( nBossHP <= ( g_nBoss_MaxHP * 0.5 ) and ( g_n4thCrowDeathNum < 4 ) ) then
		
			return false, 0
			
		end
	
	-- 앵그리 최태규가 피격을 당했을 경우 처리한다;
	elseif ( _nDamagedActorID == g_nEnhancedBossCrowID[_nDamagedActorID] ) then
	
		local nBossHP = GetHP( _nDamagedActorType, _nDamagedActorID )
		
		-- HP가 75% 이하이고 (test 90%), Phase 2 시작하지 않았으면 60초마다 쫄몹 소환 30마리를 소환한다;
		if ( nBossHP <= ( g_nEnhancedBoss_MaxHP * 0.75 ) and ( g_bPhase2 == false ) ) then
		
			-- EVENT_PHASE_2_LEVEL1 이벤트 시작한다;
			if ( g_timerPhase2 == nil ) then
			
				g_timerPhase2 = RegistTimer( 1, EVENT_PHASE_2_LEVEL1 )
				
				g_nStage = PHASE_2_LV1_START
	
				Log_NextStage( "DimensionLab", BATTLE_MAP_ID[1], g_nStage, #g_tablePartyMember, g_nPartyCount, g_nRetryCount-1 )
				
			end
			
		end
		
		-- HP 50% 이하이고, Phase3 시작하지 않았으면 처리한다;
		if ( nBossHP <= ( g_nEnhancedBoss_MaxHP * 0.5 ) and ( g_bPhase3 == false ) ) then
		
			-- 보스HP회복률을 0으로 만든다;
			SetHPRestorationRate( _nDamagedActorType, g_nEnhancedBossCrowID[_nDamagedActorID], 0 )
			
			-- 앵그리 최태규에 면역버프를 부여한다;
			ApplySkill(
				_nDamagedActorType,
				_nDamagedActorID,
				SKILL_INVINCIBLE[1],
				SKILL_INVINCIBLE[2],
				1,
				9999,
				5 )
			
			-- EVENT_PHASE_2_LEVEL2 이벤트 시작한다;
			if ( g_timerPhase3 == nil ) then
			
				g_timerPhase3 = RegistTimer( 1, EVENT_PHASE_2_LEVEL2 )
				
			end
			
		end
		
		-- HP 50% 이하이고, 4대장이 소환되고 죽기전까지 최태규에게 대미지가 들어가지 않는다;
		if ( nBossHP <= ( g_nEnhancedBoss_MaxHP * 0.5 ) and ( g_n4th2CrowDeathNum < 4 ) ) then
		
			return false, 0
			
		end
		
		-- HP가 25% 이하가 되면 페이즈3를 시작한다 ( Phase3은 사대장+쫄몹 );
		if ( nBossHP <= ( g_nEnhancedBoss_MaxHP * 0.25 ) and ( g_bPhase3 == true ) ) then
		
			-- 75~25% 까지 쫄몹 소환 이벤트 삭제한다;
			RemoveTimer( g_timerPhase2 )
			
			-- EVENT_PHASE_3_START 이벤트 시작한다;
			if ( g_timerPhase4 == nil ) then
			
				g_timerPhase4 = RegistTimer( 1, EVENT_PHASE_3_START )
				
			end
			
		end
		
	end
	
	return true, _nDamage
	
end


-------------------------------------------------------------------------------------------------------
-- 타이머 실행될 때 호출되는 함수;
function EventTimer ( _nTimerHandle, _nParam0, nParam1, nParam2, nParam3 )

	-- 인던 파괴;
    if ( _nParam0 == EVENT_DESTROY ) then
		
		LuaGlobalFunction_DESTROY_GAME()
		
	-- 게임 시작;
    elseif ( _nParam0 == EVENT_GAME_START ) then
	
		LuaGlobalFunction_START_GAME()
	
	-- 레이어 교체 및 최태규 소환 ( 보스 소환 페이즈 시작 );
	elseif ( _nParam0 == EVENT_PHASE_SUMMONBOSS ) then
	
		LuaGlobalFunction_PHASE_SUMMONBOSS()
		
	-- 최태규 HP가 50%가 되면 사대장 소환 ( 페이즈1 시작 );
	elseif ( _nParam0 == EVENT_PHASE_1_START ) then
	
		LuaGlobalFunction_START_PHASE1()
	
	-- 사대장이 다 죽을 경우 ( 페이즈1 종료 );
	elseif ( _nParam0 == EVENT_PHASE_1_END ) then
	
		LuaGlobalFunction_END_PHASE1()
			
	-- 최태규가 죽을 경우 ( 페이즈2 시작 );
	elseif ( _nParam0 == EVENT_PHASE_2_START ) then
	
		LuaGlobalFunction_START_PHASE2()
	
	-- 강화 최태규 HP가 75% ~ 25%이면 최태규 주위에 일정 주기로 쫄 몬스터 소환 ( 페이즈2 레벨1 );
	elseif ( _nParam0 == EVENT_PHASE_2_LEVEL1 ) then
	
		LuaGlobalFunction_START_PHASE2_LV1()
		
	-- 강화 최태규 HP 50%이하가 되면 사대장 + 쫄몹 출현 ( 페이즈2 레벨2 );
	elseif ( _nParam0 == EVENT_PHASE_2_LEVEL2 ) then
	
		LuaGlobalFunction_START_PHASE2_LV2()
	
	-- 두번째 사대장 모두 죽으면 ( 페이즈2 종료 );
	elseif ( _nParam0 == EVENT_PHASE_2_END ) then
	
		LuaGlobalFunction_END_PHASE2()
			
	-- 앵그리 최태규 HP 25%가 되면 ( 페이즈3 시작 );
	elseif ( _nParam0 == EVENT_PHASE_3_START ) then
	
		LuaGlobalFunction_START_PHASE3()
		
	-- 게임 종료;
    elseif ( _nParam0 == EVENT_GAME_END ) then
		g_bFail = true
		LuaGlobalFunction_END_GAME(DESTROY_TIME)
	
	-- 게임 도움말 공지;
    elseif ( _nParam0 == EVENT_NOTIFY_GAMESTATE ) then
	
	-- 게임 종료 공지;
    elseif ( _nParam0 == EVENT_NOTIFY_GAMEEND ) then
		
		PrintSimpleXMLMsgAll( 3, { "PVE_DIMENSIONLAB_SYSTEM_MESSAGE", 18, nParam1 }, COLOR_YELLOW ) -- PVP_CTFLAG_SYSTEM_MESSAGE, 8
		
	-- 전투 시작 공지;
    elseif ( _nParam0 == EVENT_UI_START_1 ) then
	
        --PrintSimpleMsgAll(4,"GamStart");	-- PVP_CTFLAG_SYSTEM_MESSAGE, 1
		PrintSimpleXMLMsgAll( 3,{ "PVE_DIMENSIONLAB_SYSTEM_MESSAGE", 1 }, COLOR_YELLOW )
        RegistTimer( 3, EVENT_UI_START_2 )
	
	-- 목표 알림 공지;
    elseif ( _nParam0 == EVENT_UI_START_2 ) then
	
        --PrintSimpleMsgAll(4,"Attack Plz");	-- PVP_CTFLAG_SYSTEM_MESSAGE, 2
		PrintSimpleXMLMsgAll( 5, { "PVE_DIMENSIONLAB_SYSTEM_MESSAGE", 2 }, COLOR_YELLOW )
	
	-- 최태규 상태 공지;
	elseif ( _nParam0 == EVENT_CHOI_INFO ) then
	
		--ServerLog("Musuk");
		PrintSimpleXMLMsgAll( 10, { "PVE_DIMENSIONLAB_SYSTEM_MESSAGE", 7 }, COLOR_YELLOW )
	
	-- 앵그리 최태규 상태 공지 1;
	elseif ( _nParam0 == EVENT_CHOI_INFO_1 ) then
	
		PrintSimpleXMLMsgAll( 10, { "PVE_DIMENSIONLAB_SYSTEM_MESSAGE", 12 }, COLOR_YELLOW )
	
	-- 앵그리 최태규 상태 공지 2;
	elseif ( _nParam0 == EVENT_CHOI_INFO_2 ) then
	
		PrintSimpleXMLMsgAll( 10, { "PVE_DIMENSIONLAB_SYSTEM_MESSAGE", 15 }, COLOR_YELLOW )
	
	-- 앵그리 최태규 상태 공지 3;
	elseif ( _nParam0 == EVENT_CHOI_INFO_3 ) then
	
		PrintSimpleXMLMsgAll( 5, { "PVE_DIMENSIONLAB_SYSTEM_MESSAGE", 17 }, COLOR_YELLOW )
		RegistTimer( 5, EVENT_CHOI_INFO_4 )
   
	-- 앵그리 최태규 상태 공지 4;
	elseif ( _nParam0 == EVENT_CHOI_INFO_4 ) then
	
		PrintSimpleXMLMsgAll( 5, { "PVE_DIMENSIONLAB_SYSTEM_MESSAGE", 20 }, COLOR_YELLOW )

    end
	
end


-------------------------------------------------------------------------------------------------------
-- 커스텀 메시지가 들어올 경우 호출되는 함수;
function EventCustomMessage ( _nSenderType, _nSenderID, _nParam1, _nParam2, _nParam3, _nParam4 )
	
	-- 결과창에서 나가기 버튼을 클릭할 경우 들어온다;
	if ( _nParam1 == PARAM1_EXIT_BUTTON ) then
		
		DoOut( _nSenderID );
		
	end
	
end


-------------------------------------------------------------------------------------------------------
--[[ 
function EventRequestMoveGate(nActorID, nMapMainID, nMapSubID, nGateID, nMainMapIDTarget, nSubMapIDTarget, nGateIDTarget)
	return true;
end

function EventReceiveHeal(nReceiveActorType, nReceiveActorID, nHealActorType, nHealActorID, nHeal, nHealFlag)  -- 어떠한 PC,MOB이 회복를 받으면 실행되는 부분;
	return true,nHeal; 
end

function EventUseItem(nActorID, nItemID, nParam0, nParam1) -- 유저가 어떠한 Item을 사용하였을때 실행되는 부분;
	return true;
end

function EventPartyOut(nPartyID, bExpedition, nOuterID, bInMapOuter)
end
]]--
-------------------------------------------------------------------------------------------------------
-- 인증 시도;
function LuaGlobalFunction_TryCertification ( _nCertifierPartyID, _nMachineID, _nCertifierID )

	-- 자신이 인중 중임을 알린다;
    g_nCertifyUser_CurrentMachine[_nCertifierID] = _nMachineID

	-- 인증모션 취한다;
	SetMotion( nClickerType, _nCertifierID, MOTION_CERTIFY[1], MOTION_CERTIFY[2], AUTH_TIME )
	
	-- 모션이 진행 중이면 처리한다;
	if ( IsMotion( nClickerType, _nCertifierID, MOTION_CERTIFY[1], MOTION_CERTIFY[2] ) == true ) then
	
		g_bCertifying = true
		
		-- 인증 UI 를 출력한다
		UI_Auth( _nCertifierID, true, AUTH_TIME )
		
	else
	
		g_bCertifying = false
		
	end
	
	-- 인증을 하면 버프 삭제한다;
    for key, value in pairs ( REMOVE_SKILLEFFECT ) do
	
        -- 삭제 스킬 효과 인덱스에 해당하는 버프를 지운다.
		RemoveSkillFact_SkillEffect ( ACTOR_PC, _nCertifierID, REMOVE_SKILLEFFECT[key] )
		
    end
	
end


-------------------------------------------------------------------------------------------------------
-- 인증 성공 처리 함수;
function LuaGlobalFunction_SuccessCertification ( _nCertifierID )

    local nMachineID = g_nCertifyUser_CurrentMachine[_nCertifierID]	-- 인증기 ID;
    local nX, nY, nZ = GetPosition( ACTOR_NPC, nMachineID )			-- 인증기 좌표;
		
	-- 인증 성공 시 트리거 삭제 및 이펙트 출력
	if ( g_bCertifying == false ) then
	
		-- 트리거 위치에 성공 이펙트 출력
		DoEffectPositionAll( "success_eff110124.egp", BATTLE_MAP_ID[1], BATTLE_MAP_ID[2], nX, nY, nZ )
		
		-- 트리거 삭제
		SetHP( ACTOR_NPC, nMachineID, 0 )
		
		-- 레이어 1번 삭제;
		DelMobLayer( BATTLE_MAP_ID[1], BATTLE_MAP_ID[2], 1 )
		
	end
	
	g_bCertify = true;	-- 인증 상태 on
		
	-- 해당 인증기를 동시 인증 중이던 사람의 인증 시도를 취소한다;
    for key, value in pairs( g_nCertifyUser_CurrentMachine ) do			-- key : actorID, value : 인증기 ID
        if ( value == nMachineID ) then
		
			-- 현재 진행중이던 모션을 취소한다;
            ResetMotion( ACTOR_PC, key )
			
        end
    end
	
	-- 인증성공 및 최태규 출현 알림 메시지;
	PrintSimpleXMLMsgAll( 3, { "PVE_DIMENSIONLAB_SYSTEM_MESSAGE", 3 }, COLOR_YELLOW )
	PrintSimpleXMLMsgAll( 3, { "PVE_DIMENSIONLAB_SYSTEM_MESSAGE", 4 }, COLOR_YELLOW )
	
	-- 최태규를 출현시킨다;
	RegistTimer( WAIT_MOVE_PHASE_TIME, EVENT_PHASE_SUMMONBOSS )
	
end


-------------------------------------------------------------------------------------------------------
-- 게임 시작;
function LuaGlobalFunction_START_GAME ()

	ServerLog( "[ DimensionLab Log ] [ Start Game "..tostring( instanceMapID ).." ]" )
	
	g_nStage = PHASE_START
	
	Log_NextStage( "DimensionLab", BATTLE_MAP_ID[1], g_nStage, #g_tablePartyMember, g_nPartyCount, g_nRetryCount-1 )
	
	-- 1번 레이어 몹 소환;
	AddMobLayer( BATTLE_MAP_ID[1], BATTLE_MAP_ID[2], 1)
	
	-- 플레이 타이머 등록. (인던 진행시간 타이머)
	g_timerGamePlay = RegistTimer( PLAY_TIME, EVENT_GAME_END )
	
	-- 게임 시작 준비;
	for nKey, nMemberID in pairs( g_tablePartyMember ) do
	
		if ( true == g_tablePartyMemberStatus[ nMemberID ] ) then
		
			-- 이동제한 해제;
			SetMoveLock(ACTOR_PC, nMemberID, true)
		
			-- 대기 인원 UI off;
			local nFactionType, nFactionID = GetFaction( ACTOR_PC, nMemberID )
			UI_EntranceState( nFactionType, nFactionID, 0, 0, 0, false )
			
			-- 게임 진행 Timer UI;
			UI_Timer( nMemberID, GetLeftTime( g_timerGamePlay ), true )
			
		end
		
	end
	
	-- 게임 시작 상태로 변경;
	g_bGameStart = true
		
	-- 게임 진행 도움말 공지 타이머 등록;
	g_timerNotifyHelp = RegistTimer( NOTIFY_GAMESTATE_TIME, EVENT_NOTIFY_GAMESTATE )
	
	-- Battle Start UI;
	UI_CDM_BattleStart();
	
	-- 게임 스타트 알림 메시지;
	RegistTimer(7, EVENT_UI_START_1);
	
	-- 게임 종료 알림 메시지 등록;
	for key,value in pairs( NOTIFY_GAME_END_TIME ) do
		RegistTimer( PLAY_TIME - (value*60), EVENT_NOTIFY_GAMEEND, value );
	end
		
end


-------------------------------------------------------------------------------------------------------
-- 최초 인증 후 최태규 소환 페이즈 시작;
function LuaGlobalFunction_PHASE_SUMMONBOSS ()

	ServerLog( "[ DimensionLab Log ] [ Start PHASE_SUMMONBOSS "..tostring( instanceMapID ).." ]" )
	
	g_nStage = PHASE_SUMMONBOSS
	
	Log_NextStage( "DimensionLab", BATTLE_MAP_ID[1], g_nStage, #g_tablePartyMember, g_nPartyCount, g_nRetryCount-1 )
	
	-- 레이어 0번 제거, 기존에 있던 몬스터들을 제거한다;
	DelMobLayer( BATTLE_MAP_ID[1], BATTLE_MAP_ID[2], 0 )
	
	local nBossID;
	
	-- 보스 위치에 생성 이펙트 출력
	DoEffectPositionAll( "ganghwana_fire.egp", BATTLE_MAP_ID[1], BATTLE_MAP_ID[2], TABLE_MOB_POS[1][1], 0, TABLE_MOB_POS[1][2] )
	
	-- 몹 위치에 생성 이펙트 출력
	DoEffectPositionAll( "KCB111_T.egp", BATTLE_MAP_ID[1], BATTLE_MAP_ID[2], TABLE_MOB_POS[1][1], 0, TABLE_MOB_POS[1][2] )
	
	-- 보스 최태규를 배치한다;
	nBossID = DropCrow(
		BATTLE_MAP_ID[1],
		BATTLE_MAP_ID[2],
		TABLE_MOB_ID[1][1],
		TABLE_MOB_ID[1][2],
		TABLE_MOB_POS[1][1],
		TABLE_MOB_POS[1][2],
		315,
		1 )
	
	-- 최태규 멘트1;
	PrintSimpleXMLMsgAll( 3, { "PVE_DIMENSIONLAB_SYSTEM_MESSAGE", 5 }, COLOR_WHITE )
	
	g_nBossCrowID[nBossID] = nBossID;
	g_nBoss_MaxHP = GetHP( ACTOR_MOB, nBossID );
		
end


-------------------------------------------------------------------------------------------------------
-- 페이즈 1 시작;
function LuaGlobalFunction_START_PHASE1 ()

	ServerLog( "[ DimensionLab Log ] [ Start PHASE1 "..tostring( instanceMapID ).." ]" )
	
	g_nStage = PHASE_1_START
	
	Log_NextStage( "DimensionLab", BATTLE_MAP_ID[1], g_nStage, #g_tablePartyMember, g_nPartyCount, g_nRetryCount-1 )
	
	g_bPhase1 = true
	
	-- 최태규 멘트2;
	PrintSimpleXMLMsgAll( 3, { "PVE_DIMENSIONLAB_SYSTEM_MESSAGE", 6 }, COLOR_WHITE )
	
	-- 최태규 상태 알림;
	RegistTimer( 5, EVENT_CHOI_INFO )
	
	if ( g_bPhase1 == true ) then
	
		local nMiddleBossID;
		
		-- 돌격대 소환;
		DoEffectPositionAll( "fbb114.egp", BATTLE_MAP_ID[1], BATTLE_MAP_ID[2], TABLE_MOB_POS[2][1], 0, TABLE_MOB_POS[2][2] )				 
		DoEffectPositionAll( "KCB111_T.egp", BATTLE_MAP_ID[1], BATTLE_MAP_ID[2], TABLE_MOB_POS[2][1], 0, TABLE_MOB_POS[2][2] )
		nMiddleBossID = DropCrow(
			BATTLE_MAP_ID[1],
			BATTLE_MAP_ID[2],
			TABLE_MOB_ID[3][1],
			TABLE_MOB_ID[3][2],
			TABLE_MOB_POS[2][1],
			TABLE_MOB_POS[2][2],
			60,
			1 )
		g_nMiddleBossCrowID[nMiddleBossID] = nMiddleBossID
		
		-- 경호대 소환;
		DoEffectPositionAll( "fbb114.egp", BATTLE_MAP_ID[1], BATTLE_MAP_ID[2], TABLE_MOB_POS[3][1], 0, TABLE_MOB_POS[3][2] )
		DoEffectPositionAll( "KCB111_T.egp", BATTLE_MAP_ID[1], BATTLE_MAP_ID[2], TABLE_MOB_POS[3][1], 0, TABLE_MOB_POS[3][2] )
		nMiddleBossID = DropCrow(
			BATTLE_MAP_ID[1],
			BATTLE_MAP_ID[2],
			TABLE_MOB_ID[4][1],
			TABLE_MOB_ID[4][2],
			TABLE_MOB_POS[3][1],
			TABLE_MOB_POS[3][2],
			120,
			1 )
		g_nMiddleBossCrowID[nMiddleBossID] = nMiddleBossID
		
		-- 연구대 소환;
		DoEffectPositionAll( "fbb114.egp", BATTLE_MAP_ID[1], BATTLE_MAP_ID[2], TABLE_MOB_POS[4][1], 0, TABLE_MOB_POS[4][2] )
		DoEffectPositionAll( "KCB111_T.egp", BATTLE_MAP_ID[1], BATTLE_MAP_ID[2], TABLE_MOB_POS[4][1], 0, TABLE_MOB_POS[4][2] )
		nMiddleBossID = DropCrow(
			BATTLE_MAP_ID[1],
			BATTLE_MAP_ID[2],
			TABLE_MOB_ID[5][1],
			TABLE_MOB_ID[5][2],
			TABLE_MOB_POS[4][1],
			TABLE_MOB_POS[4][2],
			180,
			1 )
		g_nMiddleBossCrowID[nMiddleBossID] = nMiddleBossID
		
		-- 선봉대 소환;
		DoEffectPositionAll( "fbb114.egp", BATTLE_MAP_ID[1], BATTLE_MAP_ID[2], TABLE_MOB_POS[5][1], 0, TABLE_MOB_POS[5][2] )
		DoEffectPositionAll( "KCB111_T.egp", BATTLE_MAP_ID[1], BATTLE_MAP_ID[2], TABLE_MOB_POS[5][1], 0, TABLE_MOB_POS[5][2] )
		nMiddleBossID = DropCrow(
			BATTLE_MAP_ID[1],
			BATTLE_MAP_ID[2],
			TABLE_MOB_ID[6][1],
			TABLE_MOB_ID[6][2],
			TABLE_MOB_POS[5][1],
			TABLE_MOB_POS[5][2],
			240,
			1 )
		g_nMiddleBossCrowID[nMiddleBossID] = nMiddleBossID
		
	end

end


-------------------------------------------------------------------------------------------------------
-- 페이즈 1 종료;
function LuaGlobalFunction_END_PHASE1 ()

	ServerLog( "[ DimensionLab Log ] [ End PHASE1 "..tostring( instanceMapID ).." ]" )
	
	g_nStage = PHASE_1_END
	
	Log_NextStage( "DimensionLab", BATTLE_MAP_ID[1], g_nStage, #g_tablePartyMember, g_nPartyCount, g_nRetryCount-1 )
		
	for nKey, nBossID in pairs ( g_nBossCrowID ) do
		if ( nBossID ~= nil ) then
		
			-- 최태규 상태알림 2
			PrintSimpleXMLMsgAll( 3, { "PVE_DIMENSIONLAB_SYSTEM_MESSAGE", 8 }, COLOR_YELLOW )
			
			-- 면역상태 해제
			RemoveSkillFact( ACTOR_MOB, g_nBossCrowID[nBossID], SKILL_INVINCIBLE[1], SKILL_INVINCIBLE[2] )
			
			-- 보스HP회복률을 0.002으로 만듬;
			SetHPRestorationRate( ACTOR_MOB, g_nBossCrowID[nBossID], 0.003 )
			
		end
	end
		
end

-------------------------------------------------------------------------------------------------------
-- 페이즈 2 시작;
function LuaGlobalFunction_START_PHASE2 ()

	ServerLog( "[ DimensionLab Log ] [ Start PHASE2 "..tostring( instanceMapID ).." ]" )
	
	g_nStage = PHASE_2_START
	
	Log_NextStage( "DimensionLab", BATTLE_MAP_ID[1], g_nStage, #g_tablePartyMember, g_nPartyCount, g_nRetryCount-1 )
	
	-- 최태규 사망 후 만약 4대장이 남아있다면 제거;
	for nKey, nMobID in pairs ( g_nMiddleBossCrowID ) do
		SetHP( ACTOR_MOB, nMobID, 0 );
	end
	
	-- 앵그리 최태규 소환;
	local nBossID;
	DoEffectPositionAll( "ganghwana_fire.egp", BATTLE_MAP_ID[1], BATTLE_MAP_ID[2], TABLE_MOB_POS[1][1], 0, TABLE_MOB_POS[1][2] )
	DoEffectPositionAll( "KCB111_T.egp", BATTLE_MAP_ID[1], BATTLE_MAP_ID[2], TABLE_MOB_POS[1][1], 0, TABLE_MOB_POS[1][2] )
	nBossID = DropCrow(
		BATTLE_MAP_ID[1],
		BATTLE_MAP_ID[2],
		TABLE_MOB_ID[2][1],
		TABLE_MOB_ID[2][2],
		TABLE_MOB_POS[1][1],
		TABLE_MOB_POS[1][2],
		315,
		1 )
	
	-- 앵그리 태규 멘트1
	PrintSimpleXMLMsgAll( 3, { "PVE_DIMENSIONLAB_SYSTEM_MESSAGE", 10 }, COLOR_WHITE )
	
	-- 앵그리 태규 등록;
	g_nEnhancedBossCrowID[nBossID] = nBossID
	g_nEnhancedBoss_MaxHP = GetHP( ACTOR_MOB, nBossID )
		
end


-------------------------------------------------------------------------------------------------------
-- 페이즈 2 Lv1 시작;
function LuaGlobalFunction_START_PHASE2_LV1 ()

	ServerLog( "[ DimensionLab Log ] [ Start PHASE2_Lv1 "..tostring( instanceMapID ).." ]" )

	-- 사대장 소환 + 쫄짜들 같이 소환.
	local _crowID;
	local _1stStartPlaceXZ = {-159,-18};	-- 봉술병 1부대 시작위치
	local _2ndStartPlaceXZ = {-87, 50};		-- 봉술병 2부대 시작위치
	local _3rdStartPlaceXZ = {-175, 67};	-- 소총병 3부대 시작위치
	local _Gap = {4,4};		-- 몹간 간격.
	
	if (g_bPhase2 == false) then		-- 최초 시작할때 한번 뿌리고 g_bPhase2의 시작을 on으로 변경.
		for i=1, 10, 1 do		-- 20마리 배치 (4, 4)간격으로
			_crowID = DropCrow(BATTLE_MAP_ID[1], BATTLE_MAP_ID[2], TABLE_MOB_ID[17][1], TABLE_MOB_ID[17][2], _1stStartPlaceXZ[1] + (_Gap[1] * i), _1stStartPlaceXZ[2] + (_Gap[2] * i), 315, 1);	-- 봉술병 10마리
			g_nNormalCrowID[_crowID] = _crowID; 
			_crowID = DropCrow(BATTLE_MAP_ID[1], BATTLE_MAP_ID[2], TABLE_MOB_ID[17][1], TABLE_MOB_ID[17][2], _2ndStartPlaceXZ[1] + (_Gap[1] * i), _2ndStartPlaceXZ[2] + (_Gap[2] * i), 315, 1);	-- 봉술병 10마리
			g_nNormalCrowID[_crowID] = _crowID; 
			_crowID = DropCrow(BATTLE_MAP_ID[1], BATTLE_MAP_ID[2], TABLE_MOB_ID[16][1], TABLE_MOB_ID[16][2], _3rdStartPlaceXZ[1] + (_Gap[1] * i), _3rdStartPlaceXZ[2] + (_Gap[2] * i), 315, 1);	-- 소총병 10마리
			g_nNormalCrowID[_crowID] = _crowID; 
		end
		PrintSimpleXMLMsgAll(3, {"PVE_DIMENSIONLAB_SYSTEM_MESSAGE", 11}, COLOR_WHITE);	-- 앵그리 태규 멘트2
		g_bPhase2 = true;		-- Phase 2 했다.
		g_timerPhase2 = RegistTimer(60, EVENT_PHASE_2_LEVEL1);
		ServerLog( "[ DimensionLab Log ] [ Start PHASE2_Lv1 State 1 ]" )
	end
	
	if ( g_bGameEnd == false) then	-- 게임 종료 되지 않았고
		if (g_nNormalCrowDieNum >= 30) then		-- 소환된 몹이 다 죽으면 다시 소환
			for i=1, 10, 1 do		-- 20마리 배치 (4, 4)간격으로
				_crowID = DropCrow(BATTLE_MAP_ID[1], BATTLE_MAP_ID[2], TABLE_MOB_ID[17][1], TABLE_MOB_ID[17][2], _1stStartPlaceXZ[1] + (_Gap[1] * i), _1stStartPlaceXZ[2] + (_Gap[2] * i), 315, 1);	-- 봉술병 10마리
				g_nNormalCrowID[_crowID] = _crowID; 
				_crowID = DropCrow(BATTLE_MAP_ID[1], BATTLE_MAP_ID[2], TABLE_MOB_ID[17][1], TABLE_MOB_ID[17][2], _2ndStartPlaceXZ[1] + (_Gap[1] * i), _2ndStartPlaceXZ[2] + (_Gap[2] * i), 315, 1);	-- 봉술병 10마리
				g_nNormalCrowID[_crowID] = _crowID; 
				_crowID = DropCrow(BATTLE_MAP_ID[1], BATTLE_MAP_ID[2], TABLE_MOB_ID[16][1], TABLE_MOB_ID[16][2], _3rdStartPlaceXZ[1] + (_Gap[1] * i), _3rdStartPlaceXZ[2] + (_Gap[2] * i), 315, 1);	-- 소총병 10마리
				g_nNormalCrowID[_crowID] = _crowID; 
			end
			PrintSimpleXMLMsgAll(3, {"PVE_DIMENSIONLAB_SYSTEM_MESSAGE", 11}, COLOR_WHITE);	-- 앵그리 태규 멘트2
			g_timerPhase2 = RegistTimer(60, EVENT_PHASE_2_LEVEL1);
			ServerLog( "[ DimensionLab Log ] [ Start PHASE2_Lv1 State 2 ]" )
			g_nNormalCrowDieNum = 0;				-- 죽은 수 초기화
		else	-- 소환몹이 모두 죽지 않으면 타이머만 새로 등록
		--PrintSimpleXMLMsgAll(3, {"PVE_DIMENSIONLAB_SYSTEM_MESSAGE", 11}, COLOR_WHITE);	-- 앵그리 태규 멘트2
		g_timerPhase2 = RegistTimer(60, EVENT_PHASE_2_LEVEL1);
		ServerLog( "[ DimensionLab Log ] [ Start PHASE2_Lv1 State 3 ]" )
		end
		return;
	end
	
end


-------------------------------------------------------------------------------------------------------
-- 페이즈 2 Lv2 시작;
function LuaGlobalFunction_START_PHASE2_LV2 ()

	ServerLog( "[ DimensionLab Log ] [ Start PHASE2_Lv2 "..tostring( instanceMapID ).." ]" )
	
	g_nStage = PHASE_2_LV2_START
	
	Log_NextStage( "DimensionLab", BATTLE_MAP_ID[1], g_nStage, #g_tablePartyMember, g_nPartyCount, g_nRetryCount-1 )
	
	-- Phase3 시작
	g_bPhase3 = true
	
	-- 앵그리 태규 멘트3
	PrintSimpleXMLMsgAll( 3, { "PVE_DIMENSIONLAB_SYSTEM_MESSAGE", 6 }, COLOR_WHITE )
	
	-- 멘트 후 알림등록
	RegistTimer( 5, EVENT_CHOI_INFO_1 )
	
	local nBossID = nil
	
	-- 돌격대 소환;
	DoEffectPositionAll( "fbb114.egp", BATTLE_MAP_ID[1], BATTLE_MAP_ID[2], TABLE_MOB_POS[2][1], 0, TABLE_MOB_POS[2][2] )
	DoEffectPositionAll( "KCB111_T.egp", BATTLE_MAP_ID[1], BATTLE_MAP_ID[2], TABLE_MOB_POS[2][1], 0, TABLE_MOB_POS[2][2] )
	nBossID = DropCrow(
		BATTLE_MAP_ID[1],
		BATTLE_MAP_ID[2],
		TABLE_MOB_ID[3][1],
		TABLE_MOB_ID[3][2],
		TABLE_MOB_POS[2][1],
		TABLE_MOB_POS[2][2],
		60,
		1 )
	g_nMiddleBoss2CrowID[nBossID] = nBossID
	
	-- 경호대 소환;
	DoEffectPositionAll( "fbb114.egp", BATTLE_MAP_ID[1], BATTLE_MAP_ID[2], TABLE_MOB_POS[3][1], 0, TABLE_MOB_POS[3][2] )
	DoEffectPositionAll( "KCB111_T.egp", BATTLE_MAP_ID[1], BATTLE_MAP_ID[2], TABLE_MOB_POS[3][1], 0, TABLE_MOB_POS[3][2] )
	nBossID = DropCrow(
		BATTLE_MAP_ID[1],
		BATTLE_MAP_ID[2],
		TABLE_MOB_ID[4][1],
		TABLE_MOB_ID[4][2],
		TABLE_MOB_POS[3][1],
		TABLE_MOB_POS[3][2],
		120,
		1 )
	g_nMiddleBoss2CrowID[nBossID] = nBossID 
	
	-- 연구대 소환;
	DoEffectPositionAll( "fbb114.egp", BATTLE_MAP_ID[1], BATTLE_MAP_ID[2], TABLE_MOB_POS[4][1], 0, TABLE_MOB_POS[4][2] )
	DoEffectPositionAll( "KCB111_T.egp", BATTLE_MAP_ID[1], BATTLE_MAP_ID[2], TABLE_MOB_POS[4][1], 0, TABLE_MOB_POS[4][2] )
	nBossID = DropCrow(
		BATTLE_MAP_ID[1],
		BATTLE_MAP_ID[2],
		TABLE_MOB_ID[5][1],
		TABLE_MOB_ID[5][2],
		TABLE_MOB_POS[4][1],
		TABLE_MOB_POS[4][2],
		180,
		1 )
	g_nMiddleBoss2CrowID[nBossID] = nBossID 
	
	-- 선봉대 소환;
	DoEffectPositionAll( "fbb114.egp", BATTLE_MAP_ID[1], BATTLE_MAP_ID[2], TABLE_MOB_POS[5][1], 0, TABLE_MOB_POS[5][2] )
	DoEffectPositionAll( "KCB111_T.egp", BATTLE_MAP_ID[1], BATTLE_MAP_ID[2], TABLE_MOB_POS[5][1], 0, TABLE_MOB_POS[5][2] )
	nBossID = DropCrow(
		BATTLE_MAP_ID[1],
		BATTLE_MAP_ID[2],
		TABLE_MOB_ID[6][1],
		TABLE_MOB_ID[6][2],
		TABLE_MOB_POS[5][1],
		TABLE_MOB_POS[5][2],
		240,
		1 )
	g_nMiddleBoss2CrowID[nBossID] = nBossID
	
	-- 두번째 사대장은 공격력/방어력 증가( 30%, test후 변경 필요 )
	for nKey, nBossID in pairs ( g_nMiddleBoss2CrowID ) do
		SetMultipleAttack( ACTOR_MOB, nBossID, 2.0 )		-- 공격력 +100%
		SetMultipleDefense( ACTOR_MOB, nBossID, 2.0 )	-- 방어력 +100%
	end
	
	-- 쫄 소환(15마리씩)
	for i=0, 15, 1 do	
		DropCrow(BATTLE_MAP_ID[1], BATTLE_MAP_ID[2], TABLE_MOB_ID[13][1], TABLE_MOB_ID[13][2], math.random(TABLE_MOB_POS[2][1] - 40, TABLE_MOB_POS[2][1] - 20), math.random(TABLE_MOB_POS[2][2] - 40, TABLE_MOB_POS[2][2] + 30), 60, 1);	-- 돌격대 쫄몹
		DropCrow(BATTLE_MAP_ID[1], BATTLE_MAP_ID[2], TABLE_MOB_ID[14][1], TABLE_MOB_ID[14][2], math.random(TABLE_MOB_POS[2][1] + 20, TABLE_MOB_POS[2][1] + 40), math.random(TABLE_MOB_POS[2][2] - 40, TABLE_MOB_POS[2][2] + 30), 60, 1);	-- 돌격대 쫄몹
		DropCrow(BATTLE_MAP_ID[1], BATTLE_MAP_ID[2], TABLE_MOB_ID[11][1], TABLE_MOB_ID[11][2], math.random(TABLE_MOB_POS[3][1] - 30, TABLE_MOB_POS[3][1] - 10), math.random(TABLE_MOB_POS[3][2] - 40, TABLE_MOB_POS[3][2] + 30), 120, 1);	-- 경호대 쫄몹
		DropCrow(BATTLE_MAP_ID[1], BATTLE_MAP_ID[2], TABLE_MOB_ID[12][1], TABLE_MOB_ID[12][2], math.random(TABLE_MOB_POS[3][1] + 10, TABLE_MOB_POS[3][1] + 30), math.random(TABLE_MOB_POS[3][2] - 40, TABLE_MOB_POS[3][2] + 30), 120, 1);	-- 경호대 쫄몹
		DropCrow(BATTLE_MAP_ID[1], BATTLE_MAP_ID[2], TABLE_MOB_ID[9][1], TABLE_MOB_ID[9][2], math.random(TABLE_MOB_POS[4][1] - 30, TABLE_MOB_POS[4][1] - 10), math.random(TABLE_MOB_POS[4][2] - 40, TABLE_MOB_POS[4][2] + 30), 180, 1);	-- 연구대 쫄몹
		DropCrow(BATTLE_MAP_ID[1], BATTLE_MAP_ID[2], TABLE_MOB_ID[10][1], TABLE_MOB_ID[10][2], math.random(TABLE_MOB_POS[4][1] + 10, TABLE_MOB_POS[4][1] + 30), math.random(TABLE_MOB_POS[4][2] - 40, TABLE_MOB_POS[4][2] + 30), 180, 1);	-- 연구대 쫄몹
		DropCrow(BATTLE_MAP_ID[1], BATTLE_MAP_ID[2], TABLE_MOB_ID[7][1], TABLE_MOB_ID[7][2], math.random(TABLE_MOB_POS[5][1] - 10, TABLE_MOB_POS[5][1] + 30), math.random(TABLE_MOB_POS[5][2] + 10, TABLE_MOB_POS[5][2] + 40), 240, 1);	-- 선봉대 쫄몹-120, -140
		DropCrow(BATTLE_MAP_ID[1], BATTLE_MAP_ID[2], TABLE_MOB_ID[8][1], TABLE_MOB_ID[8][2], math.random(TABLE_MOB_POS[5][1] - 40, TABLE_MOB_POS[5][1] - 5), math.random(TABLE_MOB_POS[5][2] - 45, TABLE_MOB_POS[5][2] - 5), 240, 1);	-- 선봉대 쫄몹
	end
	
end


-------------------------------------------------------------------------------------------------------
-- 페이즈 2 종료;
function LuaGlobalFunction_END_PHASE2 ()

	ServerLog( "[ DimensionLab Log ] [ End PHASE2 "..tostring( instanceMapID ).." ]" )
	
	g_nStage = PHASE_2_END
	
	Log_NextStage( "DimensionLab", BATTLE_MAP_ID[1], g_nStage, #g_tablePartyMember, g_nPartyCount, g_nRetryCount-1 )
		
	for nKey, nBossID in pairs ( g_nEnhancedBossCrowID ) do
		if ( nBossID ~= nil ) then
		
			-- 앵그리 태규 상태알림 2
			PrintSimpleXMLMsgAll( 3, { "PVE_DIMENSIONLAB_SYSTEM_MESSAGE", 13 }, COLOR_YELLOW )

			-- 면역상태 해제;
			RemoveSkillFact(ACTOR_MOB, g_nEnhancedBossCrowID[nBossID], SKILL_INVINCIBLE[1], SKILL_INVINCIBLE[2])
			
			-- 보스HP회복률을 원래대로 돌림;
			SetHPRestorationRate(ACTOR_MOB, g_nEnhancedBossCrowID[nBossID], 0.003)
			
		end
	end
		
end


-------------------------------------------------------------------------------------------------------
-- 페이즈 3 시작;
function LuaGlobalFunction_START_PHASE3 ()

	ServerLog( "[ DimensionLab Log ] [ Start PHASE3 "..tostring( instanceMapID ).." ]" )
	
	g_nStage = PHASE_3_START
	
	Log_NextStage( "DimensionLab", BATTLE_MAP_ID[1], g_nStage, #g_tablePartyMember, g_nPartyCount, g_nRetryCount-1 )
	
	for nKey, nBossID in pairs ( g_nEnhancedBossCrowID ) do
		if ( nBossID ~= nil ) then
		
			-- 앵그리 태규 멘트4;
			PrintSimpleXMLMsgAll(3, {"PVE_DIMENSIONLAB_SYSTEM_MESSAGE", 14}, COLOR_WHITE)
			
			-- 멘트 후 상태 알림 등록;
			RegistTimer(5, EVENT_CHOI_INFO_2 )
			
			-- 공격력 +50% test 1.5 > 1.3;
			SetMultipleAttack( ACTOR_MOB, g_nEnhancedBossCrowID[nBossID], 1.3 )
			
			-- 방어력 +20%;
			SetMultipleDefense( ACTOR_MOB, g_nEnhancedBossCrowID[nBossID], 1.2 )
			
		end
	end
	
end


-------------------------------------------------------------------------------------------------------
-- 게임 종료;
function LuaGlobalFunction_END_GAME (s_time)

	if ( g_bGameEnd ) then
		return;
	end

	ServerLog( "[ DimensionLab Log ] [ End Game "..tostring( instanceMapID ).." ]" )
	
	g_bGameEnd = true
	
	-- 인증기 중이라면 모두 취소한다;
    for nActorID, nMachineID in pairs( g_nCertifyUser_CurrentMachine ) do
        if ( nMachineID ~= nil ) then
		
			-- 현재 진행중이던 모션을 취소한다;
            ResetMotion( ACTOR_PC, nActorID )
			
        end
    end
	
	if ( g_bFail == true ) then		-- 게임 종료 이면서 보상 시간이면	(공략 실패)
		--ServerLog( "DestroyTime ")
		PrintSimpleXMLMsgAll( 10, { "PVE_DIMENSIONLAB_SYSTEM_MESSAGE", 19 }, COLOR_YELLOW )
		DelMobLayer( BATTLE_MAP_ID[1], BATTLE_MAP_ID[2], 1)		-- 공략에 실패하면 레이어를 삭제 (남은 시간동안 부정행동 방지)
		
		g_nStage = PHASE_END
	
		Log_NextStage( "DimensionLab", BATTLE_MAP_ID[1], g_nStage, #g_tablePartyMember, g_nPartyCount, g_nRetryCount-1 )
		
		-- Result UI 호출;
		UI_Result( s_time, PARAM1_EXIT_BUTTON )
		
		-- DESTROY 타이머 등록;
		RegistTimer( s_time, EVENT_DESTROY )
		
	else								-- 게임종료이면서 보상시간 아니면 (공략성공)
		
		g_nStage = PHASE_CLEAR
	
		Log_NextStage( "DimensionLab", BATTLE_MAP_ID[1], g_nStage, #g_tablePartyMember, g_nPartyCount, g_nRetryCount-1 )
		--ServerLog( "RewardTime ")
		-- Result UI 호출;
		UI_Result( s_time, PARAM1_EXIT_BUTTON )
		
		-- DESTROY 타이머 등록;
		RegistTimer( s_time, EVENT_DESTROY )
	
	end
		
end


-------------------------------------------------------------------------------------------------------
-- 게임 종료;
function LuaGlobalFunction_DESTROY_GAME ()

	ServerLog( "[ DimensionLab Log ] [ Destroy "..tostring( instanceMapID ).." ]" )
		
	-- 인던 파괴;
	DoDestroy()
		
end


-------------------------------------------------------------------------------------------------------