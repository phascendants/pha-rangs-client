--
-- 주의) 
-- 1) 대/소 문자를 확실하게 구분해야 합니다.
-- 2) true, false 를 확실하게 구분해야 합니다.
-- 3) UCS-2 Little Endian 로 저장해야 합니다.
-------------------------------------------------------------------------------
-- 버전 표기는 반드시 있어야 하며, 로직단과 반드시 일치해야 한다;
SCRIPT_VERSION_AGENT = 100;

mapList = 
{
	2, 5, 15
}
currentMapID = -1;

function luaRequestCreate(nPlayerDbNum, bRequestByGate)
	return true;
end

function luaRequestMapID(nPlayerDbNum)	
	return 0;
end