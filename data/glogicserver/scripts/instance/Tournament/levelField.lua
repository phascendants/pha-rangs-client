--
-- 주의) 
-- 1) 대/소 문자를 확실하게 구분해야 합니다.
-- 2) true, false 를 확실하게 구분해야 합니다.
-- 3) UCS-2 Little Endian 로 저장해야 합니다.
-------------------------------------------------------------------------------
-- 버전 표기는 반드시 있어야 하며, 로직단과 반드시 일치해야 한다;
SCRIPT_VERSION_FIELD = 100;

-- ActorType
	ACTOR_PC				= 0;
	ACTOR_NPC				= 1;
	ACTOR_MOB				= 2;
	ACTOR_SUMMON			= 6;
	ACTOR_NULL				= -1;
--------------------------------------------------------------------------------
-- FactionType
	FACTION_TEAM			= 0;
	FACTION_PARTY			= 1;
	FACTION_CLUB			= 2;
	FACTION_EXPEDITION 		= 3;
--------------------------------------------------------------------------------
-- RelationType
	RELATION_ENEMY			= 0;
	RELATION_ALLY			= 1;
	RELATION_NEUTRAL		= 2;
--------------------------------------------------------------------------------
-- Item Code
	POTION_ITEM_CODE	    = {10,10};
--------------------------------------------------------------------------------
-- TIMERCODE
	KICK_SAFEZONE			= 1;
	RESTART_EVENT			= 40;
	MOVE_SAFE_TIME			= 2;
	SHOW_PROCESS			= 11;
	JOIN_SUSPEND			= 12;
	TEST_TIMER				= 20;
--------------------------------------------------------------------------------
-- TOURNAMENT PLAYER TYPE
    TOURNAMENT_PLAYER = 0;
	TOURNAMENT_OBSERVER = 1;
--------------------------------------------------------------------------------
-- MATCHING UI TYPE
	TOURNAMENT_UI_ONE_BY_ONE = 0;
	TOURNAMENT_UI_N_BY_N = 1;
	TOURNAMENT_UI_LEAGE_SCORE = 2;
--------------------------------------------------------------------------------

	
	
------ 데스메치 설정 변수 -------
nClub_Start_Take_Point = 80;	-- 최초 클럽에 부여되는 점수;
nChar_Start_Take_Point = 10;	-- 최초 플레이어에게 부여되는 개인 점수;
nChar_Death_Point = 3;			-- 플레이어가 죽었을 때 소속 클럽의 클럽점수 감소 포인트;
CDM_Timer = 5;				-- CMD 진행 시간;
---------------------------------

ClubPoint = {}  -- 참가 클럽의 Point;
ClubKill = {}  -- 참가 클럽의 Kill횟수;
ClubDeath = {}  -- 참가 클럽의 Death횟수;
ClubResurrect = {}  -- 참가 클럽의 SkillResurrect횟수;

ClubList = {}; -- 참가 클럽;
PartyID = {} -- 참가 파티;
PlayerList = {}; -- 참가 유저;

PlayerPoint = {}  -- 참가 유저의 개인 Point;


ObserverPlayerList = {}  -- 관전자 유저 리스트 입니당.

ClubPlayerList = {}  --- 참가 유저의 리스트;
ProgressTimer= 0;
RewardTime = 30;

GameViewAroundTimer = 30;

GameViewAroundSizeA = 0;
GameViewAroundSizeB = 0;

GameTimer = 0;
GameTimerRole = 160


SendOnce = false;

Canfight = false;

UserExsist = false;


Score = {}

function EventInitialize()
	--SetJoinSuspendInstance(true);
	--RegistTimer(0, JOIN_SUSPEND, 0, 0, 0);
	--SetFaction( ACTOR_NULL, 0, FACTION_TEAM, 0 );  -- 비 전투 인원 진영(무적 타임);
	
	SetRelation( FACTION_TEAM, 0, FACTION_TEAM, 1, RELATION_ENEMY );
	SetRelation( FACTION_TEAM, 1, FACTION_TEAM, 0, RELATION_ENEMY );
	
	bBattle = false;
	ProgressTimer = 0;
	bReward = false;
	RewardTime = 30;

	table.insert(Score,0);
	table.insert(Score,0);
	
	
	
	return true;
end

function EventRequestJoin()

	local _String = "ReqJoin"; 
	--PrintChatMsg(actorID, _String);	
	return true;
end

function EventRequestJoinGM(actorID)
	local _String = "GMJoin"; 
	--PrintChatMsg(actorID, _String);
	--Matching_Tournament_IN(actorID,TOURNAMENT_OBSERVER);
	return true;
end

function EventRequestJoinForced(actorID)
	
	for  key,value in pairs( ObserverPlayerList ) do
		if(actorID == value)then
			return false;
		end
	end
	
	local _String = "ReqFJoin"; 
	--PrintChatMsg(actorID, _String);	
	
	
	return true;
end

function EventRequestJoinObserver(actorID)

	--입장 한놈이면
	for  key,value in pairs( ObserverPlayerList ) do
		if(actorID == value)then
		-- 나가
			return true;
		end
	end
	
	
	-- 아니라면 관전자 목록에 등록을 합니다.
	table.insert(ObserverPlayerList,actorID);
	
	return true;
	
end

PlayerCount = 1;



function EventJoin(actorID, mapMainID, mapSubID) -- 인던 "입장 요청"시 실행되는 부분. ex)입장아이템 소모;	
	local _String = "10SEC SAFE MODE"; -- 소속 클럽이 없을 경우 에러 출력;
	--PrintChatMsg(actorID, _String);
	
	UserExsist = true;
	
	if(PlayerList == nil) then
		PlayerList = {}
	end
	
	-- 토너먼트 관전자 입장 세팅!
	for  key,value in pairs( ObserverPlayerList ) do
		if(actorID == value)then
			local _ObString = "Observer"; 
			--PrintChatMsg(value, _ObString);
			SetMatchingUserOn(TOURNAMENT_OBSERVER,value);
			SetVisible(ACTOR_PC,value,false);
			-- 넌 싸울수 없다.
			SetFaction(ACTOR_PC, value, FACTION_TEAM, 7);
			
			--SendClientsInstancePopUp(true,false,123,true,"A-YO");
			
			SetFcationUI(actorID,1,0,TOURNAMENT_UI_ONE_BY_ONE);
			SetPositionInstance(ACTOR_PC,actorID, 186, 0,  10,20);  -- 안전지역 맵에서 강제부활;
			return;
			--ObserverPlayerList[key] = nil
		end
	end

	if(BLUE_TEAMUSER == nil) then
		BLUE_TEAMUSER = actorID;
		SetPositionInstance(ACTOR_PC,BLUE_TEAMUSER, 186, 0,  10,20);  -- 안전지역 맵에서 강제부활;
		--SetFaction(ACTOR_PC,actorID,FACTION_TEAM,0);
	elseif(RED_TEAMUSER == nil) then
		RED_TEAMUSER = actorID;
		SetPositionInstance(ACTOR_PC,RED_TEAMUSER, 186, 0, 10,-140.0);  -- 안전지역 맵에서 강제부활;
		--SetFaction(ACTOR_PC,actorID,FACTION_TEAM,0);
	elseif(BLUE_TEAMUSER == actorID) then
		SetPositionInstance(ACTOR_PC,actorID, 186, 0,  10,20);  -- 안전지역 맵에서 강제부활;
		--SetFaction(ACTOR_PC,actorID,FACTION_TEAM,0);
	elseif(RED_TEAMUSER == actorID) then
		SetPositionInstance(ACTOR_PC,actorID, 186, 0, 10,-140.0);  -- 안전지역 맵에서 강제부활;
		--SetFaction(ACTOR_PC,actorID,FACTION_TEAM,1);
	end

	SetFcationUI(actorID,1,0,TOURNAMENT_UI_ONE_BY_ONE);
	
	PlayerList[PlayerCount] = actorID;
	PlayerCount = PlayerCount + 1;
	
	-- table.insert(PlayerList,actorID);
	
	local isJoined = false;
	
	for  key,value in pairs( PlayerList ) do
		if(actorID == value)then
		isJoined = true;
		end
	end
	
end

--EnterCount = 0;
function EventMapEnter(actorID, fromMapMainID, fromMapSubID, toMapMainID, toMapSubID)
	--local _String = "EnterMap"; -- 소속 클럽이 없을 경우 에러 출력;
	local _String = "EventJoin - >" .. actorID; -- 소속 클럽이 없을 경우 에러 출력;
	--PrintChatMsg(actorID, _String);
		
	
	SetRelation( FACTION_TEAM, 0, FACTION_TEAM, 1, RELATION_ENEMY );
	SetRelation( FACTION_TEAM, 1, FACTION_TEAM, 0, RELATION_ENEMY );
	
	-- 우승 조건 두번 이겨라
	-- 시간은 아직 정하지 않는다.
	Matching_Tournament_GamInfo_Role(2,0);
	Matching_Tournament_GameTimeLimit(0,false,true);
	
	--if ( bBattle == true ) then
	--	RegistTimer(1, SHOW_PROCESS, ACTOR_PC, actorID, 0);
	--end
	
--	EnterCount = EnterCount + 1;
	
	--if(EnterCount == 2) then
		RegistTimer(10, KICK_SAFEZONE, ACTOR_PC, actorID, PlayerList);
	--end

end

function EventOffline(actorID)
	if(isSendResualt == false) then
		ForceWin()
	end
end

function EventOut(actorID, toMapMID, toMapSID)
	--PlayerList[actorID] = nil
	for  key,value in pairs( ObserverPlayerList ) do
		if(actorID == value)then
			table.remove(ObserverPlayerList,actorID);
			SetVisible(ACTOR_PC,value,true);
			Matching_Tournament_OUT(actorID) -- 나간놈 처리
			return true;
		end
	end
	
	
	
	if(isSendResualt == false) then
		EventOffline(actorID)
	end
	
	
	
	
	
end

function EventRequestMoveGate(nActorID, nMapMainID, nMapSubID, nGateID)
	local _String = "RequestMoveGate"; -- 소속 클럽이 없을 경우 에러 출력;
	--PrintChatMsg(nActorID, _String);
	return false;
end

GameBeginTimer = 0;
GameBeginRole = 10;

GameDebugRole = 10;
GameDebugTimer = 0;
function EventUpdate(nElapsedTime)
	--if ( bBattle == false ) then
		--return;
	--end
	
	if UserExsist == false then
		return
	end
	
	CDM_Timer = CDM_Timer - nElapsedTime;
	ProgressTimer = ProgressTimer + nElapsedTime;
	
	GameViewAroundTimer = GameViewAroundTimer- nElapsedTime;

	
	--GetPosition
	
	--Matching_Tournament_GameTimeLimit(GameTimerRole,false,true);
	if(Canfight == true) then
		
		if(GameTimer ~= nElapsedTime + GameTimer)then
			GameTimer = nElapsedTime + GameTimer;
			if(GameTimerRole - GameTimer > 0) then
				local blank = false;
				if(GameTimerRole - GameTimer <= 3) then
					blank = true;
				end
				Matching_Tournament_GameTimeLimit(GameTimerRole - GameTimer,blank,true);
			else
				 ForceWin();
			end
			
		end
	end
	
	if(Canfight == false) then
		
		if(GameBeginTimer ~=  GameBeginTimer + nElapsedTime)then
			GameBeginTimer =  GameBeginTimer + nElapsedTime;
			if(GameBeginRole - GameBeginTimer > 0) then
				local blank = false;
				if(GameBeginRole - GameBeginTimer <= 3) then
					blank = true;
				end
				Matching_Tournament_GameTimeLimit(GameBeginRole - GameBeginTimer,blank,true);
			else
				Matching_Tournament_GameTimeLimit(0,true,true);
			end
		end
	else
		GameBeginTimer = 0;
	end
	
	
	if ( GameViewAroundTimer < 0 ) then
		GameViewAroundTimer = 30;
		-- 뷰어라운드 세팅!
		--local MapSize = Matching_GetLandSize(23,0);
		
		for key,value in pairs( PlayerList ) do
			
			SetViewRange(PlayerList[key],60,6000);
			
		end
		
		for key,value in pairs(ObserverPlayerList)do
			SetViewRange(ObserverPlayerList[key],60,6000);
			
		end
	end
end

function ForceWin(OutActorID)


	if(BLUE_TEAMUSER  ~= nil) then
		--SetPositionInstance(ACTOR_PC,BLUE_TEAMUSER, 186, 0,  10,20);  -- 안전지역 맵에서 강제부활;
	end
	if(RED_TEAMUSER ~= nil )then
		--SetPositionInstance(ACTOR_PC,RED_TEAMUSER, 186, 0, 10,-140.0);  -- 안전지역 맵에서 강제부활;
	end
	
	
	local winID;
	local LoseID;
	
	
	if(GetHP(ACTOR_PC,BLUE_TEAMUSER)  > GetHP(ACTOR_PC,RED_TEAMUSER) )then
		winID = BLUE_TEAMUSER;
		LoseID = RED_TEAMUSER;
	else
		LoseID = BLUE_TEAMUSER;
		winID = RED_TEAMUSER;
	end
	
	if OutActorID ~= nil then
		if OutActorID == BLUE_TEAMUSER then
			LoseID = BLUE_TEAMUSER;
			winID = RED_TEAMUSER;
		else
			winID = BLUE_TEAMUSER;
			LoseID = RED_TEAMUSER;
		end
		
	end
	
	
	local AttackerType ,AttackerFaction = GetFaction(ACTOR_PC,winID);
	local DamagedType, DamagedFaction = GetFaction(ACTOR_PC,LoseID);
		
	local _String = string.format("TIME OUT %d Faction Lose",AttackerFaction);
	--PrintChatMsg(LoseID, _String);
	local _AString = string.format("TIME OUT %d Faction Win",DamagedFaction);
	--PrintChatMsg(winID, _AString);
	
	
	if(Score[AttackerFaction] == nil)then 
		Score[AttackerFaction] = 0;
	end
	
	Score[AttackerFaction] = Score[AttackerFaction] + 1;
	
	if OutActorID ~= nil then
		Score[AttackerFaction] = 3;
	end
	
	if isSendResualt == false then
		isSendResualt =true
		

		Matching_Resualt_Send_Start();
		Matching_Faction_Resualt(true,AttackerFaction);
		Matching_Faction_Resualt(false,DamagedFaction);
		Matching_Resualt_Send_End();

		Matching_Tournament_OUT_ALL(10);

		Matching_Tournament_OUT(nKillActorID) -- 나간놈 처리
		Matching_Tournament_OUT(nDieActorID) -- 나간놈 처리



		DoDestroy();

	end

	
end


function EventTimer(nTimerHandle, nParam0, nParam1, nParam2, nParam3)  -- 타이머가 정한 시간이 되었을때 실행되는 부분;


	if nParam0 == TEST_TIMER then
		
		--Matching_Resualt_Send_Start();
		--Matching_Faction_Resualt(true,1);
		--Matching_Faction_Resualt(false,0);
		--Matching_Resualt_Send_End();
		----걍 끝내시오라.
		--DoDestroy();
		
	elseif ( nParam0 == KICK_SAFEZONE ) then  -- 안전지대 체류시간이 끝나서 강제로 전장 이동;
	
		
		--SetPositionInstance(ACTOR_PC, nParam2, 23, 0, 50, 50);  -- 전장 맵으로 이동;
		
		_String = "Game Start!";
		--PrintChatMsg(nParam2, _String);
		
		Canfight =true;
		
		if(BLUE_TEAMUSER == nil) then
			Score[0] =3; 
			Score[1] =3;
			ForceWin(RED_TEAMUSER);
			return;
		elseif(RED_TEAMUSER == nil) then
			Score[0] =3; 
			Score[1] =3;
			ForceWin(BLUE_TEAMUSER);
			return;
		end
		
		
		if(BLUE_TEAMUSER == nParam2) then
			SetPositionInstance(ACTOR_PC,BLUE_TEAMUSER, 186, 0,  10,20);  -- 안전지역 맵에서 강제부활;
		end
		if(RED_TEAMUSER == nParam2 )then
			SetPositionInstance(ACTOR_PC,RED_TEAMUSER, 186, 0, 10,-140.0);  -- 안전지역 맵에서 강제부활;
		end
		
		
		
		
		--SetPositionInstance(ACTOR_PC, nParam2, 23, 0, 50, 50);  -- 전장 맵으로 이동;
		
		SetRelation( FACTION_TEAM, 0, FACTION_TEAM, 1, RELATION_ENEMY );
		SetRelation( FACTION_TEAM, 1, FACTION_TEAM, 0, RELATION_ENEMY );
		
		--RELATION_ALLY
		--SetRelation( FACTION_TEAM, 0, FACTION_TEAM, 7, RELATION_ALLY );
		SetRelation( FACTION_TEAM, 7, FACTION_TEAM, 0, RELATION_ALLY );
		SetRelation( FACTION_TEAM, 7, FACTION_TEAM, 1, RELATION_ALLY );
		
		--if PlayerList[1] ~=nil then
		--
		--	if PlayerList[2] ~=nil then
		--	
		--		if SendOnce == false then
		--			SendOnce = true;
		--			Matching_Resualt_Send_Start();
		--			Matching_Faction_Resualt(true,0);
		--			Matching_Faction_Resualt(false,1);
		--			Matching_Resualt_Send_End();
		--			
		--			Matching_Tournament_OUT(PlayerList[1]) -- 나간놈 처리
		--			Matching_Tournament_OUT(PlayerList[2]) -- 나간놈 처리
		--		
		--			DoDestroy();
		--		end
		--		
		--	end
		--end
		
		-- 토너먼트 정보 던져 주기
		-- Matching_Tournament_GamInfo_Role(3,60);	
		
		
		for key,value in pairs( Score ) do
			--SetPositionField(Score[key],);
		end

		
		
	
	
		
	elseif ( nParam0 == RESTART_EVENT )then  -- 전투 지역입장후 무적시간 경과시;
		
		Canfight =true;
		
		--SetMoveLock(ACTOR_PC,BLUE_TEAMUSER,false);
		--SetMoveLock(ACTOR_PC,RED_TEAMUSER,false);		
		
		if(BLUE_TEAMUSER  ~= nil) then
			SetPositionInstance(ACTOR_PC,BLUE_TEAMUSER, 186, 0,  10,20);  -- 안전지역 맵에서 강제부활;
		end
		if(RED_TEAMUSER ~= nil )then
			SetPositionInstance(ACTOR_PC,RED_TEAMUSER, 186, 0, 10,-140.0);  -- 안전지역 맵에서 강제부활;
		end
		
		SetRelation( FACTION_TEAM, 0, FACTION_TEAM, 1, RELATION_ENEMY );
		SetRelation( FACTION_TEAM, 1, FACTION_TEAM, 0, RELATION_ENEMY );
		
	elseif ( nParam0 == JOIN_SUSPEND ) then
		SetJoinSuspendInstance(false);
		bBattle = true;		
	end
end


isSendResualt = false;

function EventDie(nDieActorType, nDieActorID, nKillActorType, nKillActorID)  -- 어떠한 PC,MOB이 죽었을 때 실행되는 부분;
	--local _String = "DieEvent"; -- 소속 클럽이 없을 경우 에러 출력;
	----PrintChatMsg(nDieActorID, _String);

	--SetResurrectionForcedInstance(nDieActorID, 2, 0, 3, 10, 50);  -- 안전지역 맵에서 강제부활;

	if(Score == nil)then
		Score = {};
	end
	--if ( nDieActorType == ACTOR_PC )then
		
	--end
	
	
	if ( ( nDieActorType == ACTOR_PC ) and ( nKillActorType == ACTOR_PC ) ) then -- 플레이어끼리의 PVP일 경우만;
	
		--local DieString = string.format("%s KILL --O-> %s", GetName(nKillActorID) , GetName(nDieActorID));
		----PrintChatMsg(nKillActorID, DieString);
		----PrintChatMsg(nDieActorID, DieString);
		
		local AttackerType ,AttackerFaction = GetFaction(nKillActorType,nKillActorID);
		local DamagedType, DamagedFaction = GetFaction(nDieActorType,nDieActorID);
		
		
		if(Score[AttackerFaction] == nil)then 
			Score[AttackerFaction] = 0;
		end
		
		Score[AttackerFaction] = Score[AttackerFaction] + 1;
		
		if(Score[AttackerFaction] >= 3) then
		
			
			if isSendResualt == false then
				isSendResualt =true
				local _String = string.format("%d Faction Lose",AttackerFaction);
				--PrintChatMsg(nDieActorID, _String);
				local _AString = string.format("%d Faction Win",DamagedFaction);
				--PrintChatMsg(nKillActorID, _AString);

				Matching_Resualt_Send_Start();
				Matching_Faction_Resualt(true,AttackerFaction);
				Matching_Faction_Resualt(false,DamagedFaction);
				Matching_Resualt_Send_End();

				Matching_Tournament_OUT_ALL(10);

				Matching_Tournament_OUT(nKillActorID) -- 나간놈 처리
				Matching_Tournament_OUT(nDieActorID) -- 나간놈 처리



				DoDestroy();

			end
		else
		
			if(BLUE_TEAMUSER == nDieActorID) then
				SetResurrectionForcedInstance(nDieActorID, 186, 0,0,3.0,100.0, false);
			end
			if(RED_TEAMUSER == nDieActorID) then
				SetResurrectionForcedInstance(nDieActorID, 186, 0,1,3.0,100.0, false);
			end
		
			
			-- 이겼으면 이겼다고 UI 띄워주기
			Matching_Tournament_GamInfo_Win(nKillActorID,TOURNAMENT_PLAYER)
			
			-- 졌으면 졌다고 UI 띄워주기
			Matching_Tournament_GamInfo_Lose(nDieActorID,TOURNAMENT_PLAYER)
						
			RegistTimer(10, RESTART_EVENT, 0, 0, 0);
			
			Canfight = false;
			GameTimer= 0
				
			for key,value in pairs( Score ) do
				
				SendGameTeamInfo(value,key);
			end

			
		end
	end
	
	return true;
end

function EventReceiveDamage(nDamagedActorType, nDamagedActorID, nAttackActorType, nAttackActorID, nDamage, nDamageFlag)  -- 어떠한 PC,MOB이 데미지를 받으면 실행되는 부분;

	for  key,value in pairs( ObserverPlayerList ) do
		if(actorID == nAttackActorID or actorID ==  nDamagedActorID)then
			return false,0;
		end
	end

	if( Canfight == false)then
		return false,0;
	end
	
	return true,nDamage;
end

function EventResurrect(nResurrectType, nResurretActorType, nResurrectActorID, nKillActorType, nKillActorID)
	return true;
end

function EventReceiveHeal(nReceiveActorType, nReceiveActorID, nHealActorType, nHealActorID, nHeal, nHealFlag)  -- 어떠한 PC,MOB이 회복를 받으면 실행되는 부분;
	--local _String = "ReceveHealEvent"; -- 소속 클럽이 없을 경우 에러 출력;
	----PrintChatMsg(nReceiveActorID, _String);
	
	for  key,value in pairs( ObserverPlayerList ) do
		if(actorID == nHealActorID)then
		--바로 입장시키고
			return false,0;
		end
	end
	
	
	return true,nHeal; 
end

function EventUseItem(nActorID, nItemID, nParam0, nParam1) -- 유저가 어떠한 Item을 사용하였을때 실행되는 부분;
	local _String = "UseItemEvent"; -- 소속 클럽이 없을 경우 에러 출력;
	--PrintChatMsg(nActorID, _String);
	-- 물약 사용 금지;
	if ( nItemID == POTION_ITEM_CODE ) then
		return false;
	end
	
	return true;
end