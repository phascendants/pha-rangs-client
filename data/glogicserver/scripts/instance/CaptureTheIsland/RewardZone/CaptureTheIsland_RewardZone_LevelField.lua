--
-- 주의) 
-- 1) 대/소 문자를 확실하게 구분해야 합니다.
-- 2) true, false 를 확실하게 구분해야 합니다.
-- 3) UCS-2 Little Endian 로 저장해야 합니다.
-------------------------------------------------------------------------------
-- 버전 표기는 반드시 있어야 하며, 로직단과 반드시 일치해야 한다;
SCRIPT_VERSION = 
{
    "AlphaServer",     -- main version
    0,    -- sub version
};

-- 초기화시 저절로 기입되는 변수로 절대 선언 되면 안됩니다.;
--keyMapID = 선언금지 ( 초기화 이후 저절로 세팅 됨 );
--instanceMapID = 선언금지 ( 초기화 이후 저절로 세팅 됨 );

-- ActorType
	ACTOR_PC				= 0;
	ACTOR_NPC				= 1;
	ACTOR_MOB				= 2;
	ACTOR_SUMMON			= 6;
	ACTOR_NULL				= -1;
--------------------------------------------------------------------------------
-- FactionType
	FACTION_TEAM			= 0;
	FACTION_PARTY			= 1;
	FACTION_CLUB			= 2;
	FACTION_EXPEDITION 	= 3;
	FACTION_CLUB_SAFE	= 4;
--------------------------------------------------------------------------------
-- RelationType
	RELATION_ENEMY			= 0;
	RELATION_ALLY			= 1;
	RELATION_NEUTRAL		= 2;
--------------------------------------------------------------------------------
-- TriggerType
	TRIGGER_PC				= 1;
	TRIGGER_MOB				= 2;
	TRIGGER_SUMMON			= 4;
	
	TRIGGER_PC_MOB		    = 3;	
	TRIGGER_PC_SUMMON       = 5;
--------------------------------------------------------------------------------
	PARAM3_MSGBOX_OK		= 0;
	PARAM3_MSGBOX_CANCEL	= 1;
	PARAM3_MSGBOX_TIMEOVER	= 2;
--------------------------------------------------------------------------------
-- ColorCode
	COLOR_BLACK				= {0,0,0};
	COLOR_WHITE				= {255,255,255};
	COLOR_RED				= {255,0,0};
	COLOR_GREEN				= {0,255,0};
	COLOR_BLUE				= {128,194,235};
	COLOR_MAGENTA			= {240,100,170}; 
	COLOR_YELLOW				= {255,200,0};
--------------------------------------------------------------------------------
-- CustomMessage Sender Type
    MSG_TO_USER             = 0;    -- 특정 유저;   SenderID : CharDbNum;
    MSG_TO_MY_INSTANCE      = 1;    -- 현재 자신이 속한 필드서버 인던 객체 (LevelField)  SenderID : 의미없음;
    MSG_TO_FIELD_BASE       = 2;    -- 필드서버의 특정 인던 컨텐츠 (PublicField);  SenderID : Instance KeyMap ID;
    MSG_TO_FIELD_INSTANCE   = 3;    -- 필드서버의 특정 인던 객체 (LevelField);  SenderID : Instance Map ID;
    MSG_TO_AGENT_BASE       = 4;    -- 에이전트서버의 특정 인던 컨텐츠 (PublicAgent);  SenderID : Instance KeyMap ID;
    MSG_TO_AGENT_INSTANCE   = 5;    -- 에이전트서버의 특정 인던 객체 (LevelAgent);  SenderID : Instance Map ID;
--------------------------------------------------------------------------------
-- Timer Event Code
	T_CertifyCheck = 0;		-- 인증 체크 시간
	T_CertifyHoldCheck = 1;		-- 인증 시도 유보 시간
	T_GameDuration = 2;		-- 게임 진행시간
	T_AcceleratorRecharge = 3;		-- 제어기 재활성화 시간
	T_GameStateStart = 4;		-- 게임 시작상태;
	T_GameDestroy = 5;		-- 인던 파괴 ( 결과 UI 출력 후)
	T_SaftyTime = 6;		-- 전장 입장 후 무적 시간 
	T_RemoveAcceleratorBuff = 7;		-- 가속기 파괴 시 추가 포인트 버프 삭제 시간
	
	T_Notify_GameStart_1 = 100;			-- 게임 시작 공지1;
	T_Notify_GameStart_2 = 101;			-- 게임 시작 공지2;
	T_Notify_GameStart_3 = 102;		-- 게임 시작 공지2;
	T_Notify_AcceleratorRecharge = 103;		-- 가속기 활성화 남은시간 알림
	T_TargetApproached = 104;			-- 1위 클럽의 목표점수 달성 임박 알림
	T_Notify_GameEnd = 105;		-- 게임 종료시간 공지;
	T_CheckTicket = 300;		-- 티켓 보유 체크	
--------------------------------------------------------------------------------
-- CutsomParameter list
PARAM1_EXIT_BUTTON          = 1;
PARAM1_EVENT_JOIN           = 2;
PARAM1_EVENT_OUT            = 3;
PARAM1_EVENT_CREATEMAP      = 4;
PARAM1_MEMBER_NUM           = 5;
PARAM1_EVENT_SET_GUIDANCE  = 6;
EVENT_SKILL_RESURRECT = 8;
EVENT_SYSTEM_RESURRECT = 16;
PARAM1_BATTLE_OPEN_NOTIFY   = 90;
PARAM1_BATTLE_OPEN          = 91;
PARAM1_BATTLE_CLOSE         = 92;
--------------------------------------------------------------------------------
------ 선도전 설정 변수 ---------

-- 타이머 핸들 모음;
H_GameStartTimer = nil;
H_CertifyCheckTimer = nil;
H_GameDurationTimer = nil;
H_AcceleratorRechargeTimer = nil;
H_AutoExitTimer = nil;
---------------------------------

-- ID 관련 --
MemberNum = 0;			-- 인던에 진입한 인원수
CheckTicketCycle = 10;		-- 티켓 유무 체크 주기
TicketID = { 456, 215 }		-- 보상 사냥터 입장 티켓 ID

MemberTable = {};		-- 인던 입장한 유저 수

-- 초기화 ( 인던 생성 직후 );
function EventInitialize()
	-- 티켓 체크 타이머 등록 (CheckTicketCycle 주기로 반복)
 	RegistTimer(CheckTicketCycle, T_CheckTicket, nil, nil, nil, 1);
	
	return true;
end

-- 입장 요청 발생시
function EventRequestJoin(actorID)
	return true;
end

-- 유저 입장시
function EventJoin(actorID)
	return true;
end

-- 실제 맵에 진입할 때 실행되는 부분
function EventMapEnter(actorID, mapMid, mapSid)
	functionMemberAdjust(actorID,1);			--인던안의 맴버수를 체크하기위한 임의함수(접속한 캐릭터의 Db Num 저장)(1이 증가, 2가 감소) **유저가 진입할경우 인원카운트를 1올린다.
	return true;
end

-- 유저가 오프라인 상태가 될 때 실행되는 부분
function EventOffline(actorID)
    EventOut(actorID);
end

-- 인던을 나갈 때 실행되는 부분
function EventOut(actorID)
    functionMemberAdjust(actorID, 2);
	if (MemberNum == 0) then -- 인던에 0명인 경우 인던 파괴
		DoDestroy();	-- 인던 삭제
	end
	return true;
end
	
-- 게이트를 통한 이동 시 실행되는 부분
function EventRequestMoveGate(nActorID, nMapMainID, nMapSubID, nGateID, nMainMapIDTarget, nSubMapIDTarget, nGateIDTarget)
	if ( nMainMapIDTarget == 22 ) then		-- 이동할 맵이 학원광장이라면 (학원광장은 c 코드에 명시되어 있으므로 변경 금지)
		SetPositionField(nActorID, nMainMapIDTarget, nSubMapIDTarget, 0, 0);		-- 학광으로 이동하는 게이트 사용 시 인던이 아닌 필드로 보냄
		return false;
	end
	return true;
end

-- 타이머가 정한 시간이 되었을때 실행되는 부분;
function EventTimer(nTimerHandle, nParam0, nParam1, nParam2, nParam3)
	if ( nParam0 == T_CheckTicket ) then
		-- 인던 내부에 있는 유저수만큼 인벤 내 티켓 유무 체크
		for key, value in pairs (MemberTable) do
			if ( MemberTable[key] ~= nil ) then
				functionCheckTicket(key);
			end
		end
	end
end

-- 어떠한 PC,MOB이 데미지를 받으면 실행되는 부분;
--function EventReceiveDamage(nDamagedActorType, nDamagedActorID, nAttackActorType, nAttackActorID, nDamage, nDamageFlag)
--end

-- 어떠한 PC,MOB이 죽었을 때 실행되는 부분;
--function EventDie(nDieActorType, nDieActorID, nKillActorType, nKillActorID)
 --end

-- 부활 이벤트가 발생할 때 실행되는 부분
--function EventResurrect(nResurrectType, nResurretActorType, nResurrectActorID, nKillActorType, nKillActorID)
--end

--function EventActorTriggerIn(nTriggerType, nTriggerID, nInnerType, nInnerID)
--end

--function EventActorTriggerOut(nTriggerType, nTriggerID, nOuterType, nOuterID)
--end

-- 클릭 트리거를 클릭했을 때 실행되는 부분
--function EventClickTrigger(nTriggerType, nTriggerID, nClickerType, nClickerID)
--end

-- 모션이 종료될 때 실행되는 부분
--function EventMotionFinish(nActorType, nActorID, nMotionMid, nMotionSid, nMotionRemainSec)
--end

--function EventPartyJoin(nPartyID, bExpedition, nJoinerID, bInSameInstance)
--end

--function EventCustomMessage(nSenderType, nSenderID, nParam1, nParam2, nParam3, nParam4)
--end

function EventUpdateVictoriousCountry ( ContentID, PreCountryID, CurrentCountryID )
	--ServerLog("ContentID : "..tostring(ContentID));
	--ServerLog("PreCountryID : "..tostring(PreCountryID));
	--ServerLog("CurrentCountryID : "..tostring(CurrentCountryID));
	if ( CurrentCountryID == nil ) or ( CurrentCountryID == 0 ) then
		DoDestroy();
	end
	if ( PreCountryID ~= CurrentCountryID ) then
		DoDestroy();
	end
end

--[[        
function EventRequestMoveGate(nActorID, nMapMainID, nMapSubID, nGateID, nMainMapIDTarget, nSubMapIDTarget, nGateIDTarget)
	return true;
end

function EventReceiveHeal(nReceiveActorType, nReceiveActorID, nHealActorType, nHealActorID, nHeal, nHealFlag)  -- 어떠한 PC,MOB이 회복를 받으면 실행되는 부분;
	return true,nHeal; 
end

function EventUseItem(nActorID, nItemID, nParam0, nParam1) -- 유저가 어떠한 Item을 사용하였을때 실행되는 부분;
	return true;
end

function EventPartyChangeMaster(nPartyID, bExpedition, nNewMasterID, bInMapNewMaster, nOldMaster, bInMapOldMaster)
end
]]--

function EventPartyOut(nPartyID, bExpedition, nOuterID, bInMapOuter)
	-- 파티를 탈퇴할 경우 탈퇴자를 내쫒는다.;
	DoOut(nOuterID); 
end

--인던안의 인원수 가감을 위한 임의함수(유저넘버,가감타입)
function functionMemberAdjust(MemberDbNum,AdjustType)
	if (AdjustType == 1) then
		MemberNum = MemberNum + 1;			--유저수 증가
		MemberTable[MemberDbNum] = true;	--유저 진입시 MemberTable 에 유저 DbNum 부여
	elseif (AdjustType == 2) then
		MemberNum = MemberNum - 1;			--유저수 감소
		MemberTable[MemberDbNum] = nil;		--유저 퇴장시 MemberTable 에 유저 DbNum 삭제(스크립트에서 nil은 테이블을 삭제한다.)
	end
end

function functionCheckTicket(actorID)
	local _isHaveTicket;
	-- 유저 인벤에 보유한 티켓이 있는지 검사
	if ( GetItemCount(actorID, TicketID[1], TicketID[2]) ~= 0 ) then
		--ServerLog("Item Checked"..tostring(i));
		_isHaveTicket = true;
	else
		_isHaveTicket = false;
	end
	
	if ( _isHaveTicket == false ) then
		-- 티켓이 없다면 퇴장 시킴
		PrintChatXMLMsg(actorID, {"PREMIUM_MAP_FORCED_EXIT", 0}, COLOR_RED);
		DoOut(actorID);
	end
end