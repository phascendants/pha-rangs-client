--
-- 주의) 
-- 1) 대/소 문자를 확실하게 구분해야 합니다.
-- 2) true, false 를 확실하게 구분해야 합니다.
-- 3) UCS-2 Little Endian 로 저장해야 합니다.
-------------------------------------------------------------------------------
-- 버전 표기는 반드시 있어야 하며, 로직단과 반드시 일치해야 한다;
SCRIPT_VERSION = 
{
    "AlphaServer",     -- main version
    0,    -- sub version
};

-- 초기화시 저절로 기입되는 변수로 절대 선언 되면 안됩니다.;
--keyMapID = 선언금지 ( 초기화 이후 저절로 세팅 됨 );

-- ActorType
	ACTOR_PC				= 0;
	ACTOR_NPC				= 1;
	ACTOR_MOB				= 2;
	ACTOR_SUMMON			= 6;
	ACTOR_NULL				= -1;
--------------------------------------------------------------------------------	
	-- ColorCode
	COLOR_BLACK				= {0,0,0};
	COLOR_WHITE				= {255,255,255};
	COLOR_RED				= {255,0,0};
	COLOR_GREEN				= {0,255,0};
	COLOR_BLUE				= {128,194,235};
	COLOR_MAGENTA			= {240,100,170}; 
	COLOR_YELLOW				= {255,200,0};
--------------------------------------------------------------------------------

RequireLevel = 190;		-- 보상 사냥터 입장 레벨 
TicketID = { 456, 215 }		-- 보상 사냥터 입장 티켓 ID


function luaFieldRequestJoin(nPlayerDbNum, bRequestByGate)
	local _countryID = GetCountry(nPlayerDbNum);
	--local _cTIVCountry = GetCTIVCountry();
	--ServerLog("_countryID : "..tostring(_countryID));
	--ServerLog("_cTIVCountry : "..tostring(_cTIVCountry));
	if ( _countryID == nil ) or (_countryID == 0 ) then		-- 선택한 국가가 없다면
		PrintChatXMLMsg(nPlayerDbNum, {"PVP_CTI_SYSTEM_MESSAGE", 18}, COLOR_RED);
		return false;
	end
	
    if ( GetLevel(ACTOR_PC, nPlayerDbNum) < RequireLevel ) then		-- 캐릭터의 레벨이 미달이라면
		PrintChatXMLMsg(nPlayerDbNum, {"PVP_CTI_SYSTEM_MESSAGE", 19}, COLOR_RED);
		return false;
    end
    
	if ( GetItemCount(CharDbNum, TicketID[1], TicketID[2]) ~= 0 ) then
		return true;
	end
	
	--[[
	if ( _countryID ~= _cTIVCountry) then
		PrintChatXMLMsg(nPlayerDbNum, {"PVP_CTI_SYSTEM_MESSAGE", 21}, COLOR_RED);		-- 점령국 소속이 아니라면
		return false;
	end
	]]--
	
    return false;
end
