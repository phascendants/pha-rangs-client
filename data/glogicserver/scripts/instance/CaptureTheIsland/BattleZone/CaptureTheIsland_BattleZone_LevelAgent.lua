
-------------------------------------------------------------------------------
-- 버전 표기는 반드시 있어야 하며, 로직단과 반드시 일치해야 한다;
SCRIPT_VERSION = 
{
    "AlphaServer",		-- main version
    0,					-- sub version
};

-- 초기화시 저절로 기입되는 변수로 절대 선언 되면 안됩니다;
-- keyMapID = 선언금지 ( 초기화 이후 저절로 세팅 됨 );
-- instanceMapID = 선언금지 ( 초기화 이후 저절로 세팅 됨 );
--------------------------------------------------------------------------------
-- ColorCode
COLOR_BLACK						= { 0, 0, 0 }
COLOR_WHITE						= { 255, 255, 255 }
COLOR_RED						= { 255, 0, 0 }
COLOR_GREEN						= { 0, 255, 0 }
COLOR_BLUE						= { 128, 194, 235 }

-------------------------------------------------------------------------------
-- CustomMessage Sender Type
MSG_TO_USER						= 0							-- 특정 유저;   SenderID : CharDbNum;
MSG_TO_MY_INSTANCE				= 1							-- 현재 자신이 속한 필드서버 인던 객체 (LevelField)  SenderID : 의미없음;
MSG_TO_FIELD_BASE				= 2							-- 필드서버의 특정 인던 컨텐츠 (PublicField);  SenderID : Instance KeyMap ID;
MSG_TO_FIELD_INSTANCE			= 3							-- 필드서버의 특정 인던 객체 (LevelField);  SenderID : Instance Map ID;
MSG_TO_AGENT_BASE				= 4							-- 에이전트서버의 특정 인던 컨텐츠 (PublicAgent);  SenderID : Instance KeyMap ID;
MSG_TO_AGENT_INSTANCE			= 5							-- 에이전트서버의 특정 인던 객체 (LevelAgent);  SenderID : Instance Map ID;

--------------------------------------------------------------------------------
-- CutsomParameter list
PARAM1_EVENT_OUT				= 3
--------------------------------------------------------------------------------

LimitMaxCountryMember			= 120;						-- 국가별 입장 가능한 인원 제한;

CountryMemberSize				= {};						-- 국가별 난사군도 전장 참여 인원 총합;

function luaAgentRequestEntry ( _nPlayerDbNum, _bRequestByGate )

	return true
	
end

function luaAgentRequestJoin ( CharDbNum, emAuthority )

	local _countryID = GetCountry( CharDbNum )
	
	if ( CountryMemberSize[_countryID] ~= nil ) then
	
		if ( CountryMemberSize[_countryID] >= LimitMaxCountryMember ) then
		
			-- 국가별 최대 인원을 넘어설 경우 입장 불가
			ServerLog("Request Join Over!! CountryMemberSize["..tostring(_countryID).."] : "..tostring(CountryMemberSize[_countryID]));
			PrintChatXmlMsg(CharDbNum, {"PVP_CTI_SYSTEM_MESSAGE", 27}, COLOR_RED);
			return false
			
		else
		
			return true
			
		end
		
	else
	
		return true
		
	end
	
end

function luaAgentEventJoin ( actorID, emAuthority )

	local _countryID = GetCountry(actorID);  -- 소속 국가를 확인;
	if ( _countryID == nil ) or ( _countryID == 0 ) then
        return false;
    end
	
	if ( CountryMemberSize[_countryID] == nil ) then
		-- 최초 입장시 소속국가 인원 정보를 초기화
		CountryMemberSize[_countryID] = 1;
	else
		-- 입장시 소속국가 인원 정보를 갱신
		CountryMemberSize[_countryID] = CountryMemberSize[_countryID] + 1;
	end
	
	local _partyMember = GetParty(actorID);		-- 입장 시 파티원의 국가가 다를 경우 파티 해산
	if ( _partyMember ~= nil ) then
		for key, value in pairs(_partyMember) do
			local _partyMemberCountry = GetCountry(value);
			if ( _partyMemberCountry ~= _countryID ) then
				ClearParty(GetPartyID(actorID));
				break;
			end
		end
	end
	
end

function luaAgentEventPartyJoin ( masterDbNum, membetDbNum )

	local _partyMasterCountryID = GetCountry( masterDbNum )
	local _partyMemberCountryID = GetCountry( membetDbNum )
	
	-- 파티 신청자와 파티장의 국가가 같을 경우에만 파티 승인
	if ( _partyMasterCountryID == _partyMemberCountryID) then
	
		return true
		
	else
	
		return false
		
	end
	
end

function luaAgentEventPartryOut ( playerDbNum )

	return true
	
end

function luaAgentEventPartryDissolution ( playerDbNum )

	return true
	
end

function luaAgentEventClubOut ( nClubDbNum, nPlayerDbNum, nKickActionActorDbNum )

	DoOut( nPlayerDbNum )
	
end

function luaAgentEventCustomMessage ( nSenderType, nSenderID, Param1, Param2, Param3, Param4 )

	if ( nSenderType == MSG_TO_USER ) then		-- 유저에게서 agent로 메시지를 보내는 일은 있을 수 없음
	
		return
		
	end

	if ( Param1 == PARAM1_EVENT_OUT ) then		-- 캐릭터가 전장에서 이탈 시 국가별 인원 정보를 갱신
	
		ServerLog( "Event out message from Field!! CountryMemberSize["..tostring( Param2 ).."] : "..tostring( CountryMemberSize[ Param2 ] ) )
		CountryMemberSize[ Param2 ] = Param3
		
	end

end