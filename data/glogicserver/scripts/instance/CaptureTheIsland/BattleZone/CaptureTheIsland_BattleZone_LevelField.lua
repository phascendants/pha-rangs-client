
--------------------------------------------------------------------------------
-- 버전 표기는 반드시 있어야 하며, 로직단과 반드시 일치해야 한다;

SCRIPT_VERSION = 
{
    "AlphaServer",		-- main version
    0,					-- sub version
};


--------------------------------------------------------------------------------
-- 초기화시 저절로 기입되는 변수로 절대 선언 되면 안됩니다;
--keyMapID = 선언금지 ( 초기화 이후 저절로 세팅 됨 );
--instanceMapID = 선언금지 ( 초기화 이후 저절로 세팅 됨 );


--------------------------------------------------------------------------------
-- 0 : None ( Script Only )
-- 1 : Certification Machine Dungeon Mode ( Club )
-- 2 : Certification Machine Dungeon Mode ( Country )

InstanceFieldMode = 2

-- Faction Type
EMFACTION_TEAM = 5
EMFACTION_SAFE_TEAM = 6
EMFACTION_BASIC = 7

-- Actor Type
ACTOR_PC = 0
ACTOR_NPC = 1
ACTOR_MOB = 2
ACTOR_SUMMON = 6
ACTOR_NULL = -1

--------------------------------------------------------------------------------
-- Certification Machine Mode 를 사용하려면 아래를 설정해야한다;

-- 시간 ( 초 )
TIME_ACCELERATOR_COOLTIME = 300
TIME_ACCELERATOR_NOTIFY_COOLTIME = 240
TIME_GAME_START_HELP_NOTIFY_TIME = 7
TIME_CERTIFY_POINT_TIME = 5
TIME_SAFE_TIME = 7
TIME_ACCELERATOR_BUFFTIME = 180
TIME_CERTIFY_WAITTIME = 3
TIME_CERTIFICATION_MACHINE_LOCKTIME = 30
TIME_CERTIFY_TIME = 30
TIME_GAME_DESTROY_WAITTIME = 30
TIME_RESURRECTION_WAITTIME = 10
TIME_JOIN_BUFFTIME = 9999
TIME_JOIN_DEBUFFTIME = 180
TIME_CONFIRM_CERTIFICATION_TIME = 10

-- 타겟 포인트
NUM_TARGET_POINT = 5000
NUM_APPROACH_POINT = 4000

-- 한번에 증가할 수 있는 최대포인트
NUM_ONCE_LIMIT_POINT = 20

-- 참여 보상을 위한 최소포인트
NUM_MINIMUM_REWARD_POINT = 50

-- 인던 이름
STR_INSTANCE_DUNGEON_NAME = "CaptureTheIsland"
NUM_PROGRESS_UI_INDEX = 0
NUM_MINIMAP_ICON_TYPE = 1

-- 로비 및 전장맵
MAP_LOBBY = { 197, 0 }
MAP_BATTLE = { 198, 0 }

-- 인증 모션
MOTION_CERTIFY = { 10, 56 }

-- 스킬 목록
SKILL_CERTIFY_ADD_POINT_BUFF = { 54, 16 }
SKILL_RESURRECTION_TIME_REDUCE_BUFF = { 54, 15 }
SKILL_CERTIFY_TIME_REDUCE_BUFF = { 54, 14 }

-- 인증기 해제 스킬효과 목록
VEC_REMOVE_SKILLEFFECT_BY_CERTIFYING = {

	16,		-- 은신
	21,		-- 물리 데미지 흡수
	22,		-- 마법 데미지 흡수
	36,		-- 데미지 흡수(무시)
	53,		-- 면역
	
}

--[[
-- 인증시 해제 스킬목록 (20160630 갱신)
VEC_REMOVE_BUFF_BY_CERTIFYING = {

	{ 29, 1 },		-- 은신
	{ 29, 7 },
	{ 29, 8 },
	{ 29, 9 },
	{ 29, 15 },
	{ 34, 25 },
	{ 44, 12 },
	{ 45, 4 },
	{ 45, 12 },
	{ 55, 5 },
	{ 60, 3 },
	{ 3, 14 },		-- 면역
	{ 7, 12 },
	{ 7, 13 },
	{ 29, 2 },
	{ 29, 46 },
	{ 29, 47 },
	{ 29, 48 },
	{ 29, 59 },
	{ 29, 60 },
	{ 34, 7 },
	{ 34, 8 },
	{ 34, 38 },
	{ 34, 66 },
	{ 39, 8 },
	{ 43, 12 },
	{ 55, 1 },
	{ 55, 2 },
	{ 60, 5 },
	{ 3, 9 },		-- 물리 데미지 흡수
	{ 7, 6 },
	{ 29, 38 },
	{ 29, 44 },
	{ 33, 10 },
	{ 42, 5 },
	{ 42, 16 },
	{ 59, 6 },
	{ 7, 7 },		-- 마법 데미지 흡수
	{ 34, 16 },
	{ 42, 7 },
	{ 42, 18 },
	{ 59, 4 },
	{ 14, 11 },		-- 물리/마법 데미지 흡수
	{ 29, 58 },
	{ 34, 20 },
	{ 60, 9 },
	{ 14, 16 },		-- 데미지 흡수 (무시)
	{ 14, 18 },
	{ 29, 41 },
	{ 41, 2 },
	{ 41, 28 },
	{ 41, 29 },
	{ 60, 13 },
	{ 68, 5 },		-- 변술부 백호 변장 (데미지 흡수 (무시))
	{ 68, 8 },		-- 변술부 거북이 변장 (데미지 흡수 (무시))
	
}
]]--

-- 가속기 버프
VEC_ACCELERATOR_BUFF = {

	SKILL_CERTIFY_TIME_REDUCE_BUFF,
	SKILL_RESURRECTION_TIME_REDUCE_BUFF,
	SKILL_CERTIFY_ADD_POINT_BUFF,
	
}

-- 입장 버프
VEC_JOIN_BUFF = {

	{ 54, 17 },
	
}

-- 입장 디버프
VEC_JOIN_DEBUFF = {

	{ 54, 18 },
	
}

-- 승리 보상 아이템
VEC_REWARD_WIN = {

	{ 456, 208, 1, 4 },
	
}

-- 참가 보상 아이템
VEC_REWARD_PARTICIPANT = {

	{ 456, 209, 1, 5, 50, 500 },
	{ 456, 210, 1, 5, 501, 1500 },
	{ 456, 211, 1, 5, 1501, 2500 },
	{ 456, 212, 1, 5, 2501, 3500 },
	{ 456, 213, 1, 5, 3501, 5000 },
	
}

-- 인증기의 정보
VEC_CERTIFICATION_INFO = {

	-- Machine 0
	{
		ID_CROW = { 102, 25 },
		ID_MAP = MAP_BATTLE,
		ID_CERTIFY_EFFECT_BUFF = { 54, 12 },
		POSITION = { -2450, 0, -734 },
		ROTATE = 90,
		NUM_POINT = 10,
		NUM_POINT_COUNT = 36,
		NUM_ADD_POINT_COUNT = 0,
		STR_CERTIFICATION_EFFECT = "success_eff110124.egp",
		STR_UNCERTIFICATION_EFFECT = "ganghwana_fire.egp",
	},
	
}

-- 가속기의 정보
VEC_ACCELERATOR_INFO = {

	-- Machine 0
	{
		ID_CROW = { 102, 26 },
		ID_EFFECT = { 54, 11 },
		ID_MAP = MAP_BATTLE,
		POSITION = { -3055, 0, 750 },
		ROTATE = 180,
		NUM_POINT = 50,
		STR_EFFECT1 = "youngsun.egp",
		STR_EFFECT2 = "fbb114.egp",
	},
	
	-- Machine 1
	{
		ID_CROW = { 102, 27 },
		ID_EFFECT = { 54, 11 },
		ID_MAP = MAP_BATTLE,
		POSITION = { -2325, 0, 120 },
		ROTATE = 180,
		NUM_POINT = 50,
		STR_EFFECT1 = "youngsun.egp",
		STR_EFFECT2 = "fbb114.egp",
	},
	
	-- Machine 2
	{
		ID_CROW = { 102, 28 },
		ID_EFFECT = { 54, 11 },
		ID_MAP = MAP_BATTLE,
		POSITION = { -3055, 0, 120 },
		ROTATE = 180,
		NUM_POINT = 50,
		STR_EFFECT1 = "youngsun.egp",
		STR_EFFECT2 = "fbb114.egp",
	},
	
	-- Machine 3
	{
		ID_CROW = { 102, 29 },
		ID_EFFECT = { 54, 11 },
		ID_MAP = MAP_BATTLE,
		POSITION = { -3880, 0, 120 },
		ROTATE = 180,
		NUM_POINT = 50,
		STR_EFFECT1 = "youngsun.egp",
		STR_EFFECT2 = "fbb114.egp",
	},
	
}


--------------------------------------------------------------------------------
-- 아래로 콜백함수를 추가해주면 Script 도 동작한다.
-- 사용하지 않는 콜백함수는 추가하지 말자, 추가하면 Script 를 무조건 들어오기 때문에 성능을 악화시킨다.

--------------------------------------------------------------------------------
-- ColorCode
COLOR_BLACK	= { 0, 0, 0 }
COLOR_WHITE = { 255, 255, 255 }
COLOR_RED = { 255, 0, 0 }
COLOR_GREEN = { 0, 255, 0 }
COLOR_BLUE = { 128, 194, 235 }
COLOR_MAGENTA = { 240, 100, 170 } 
COLOR_YELLOW = { 255, 200, 0 }

--------------------------------------------------------------------------------

-- 인증기모드 전용 Callback Function;

-- 게임 진행 메시지 출력
function EventCMMode_GameHelp1 ( )

	PrintSimpleXMLMsgAll( 3, { "PVP_CTI_SYSTEM_MESSAGE", 1 }, COLOR_BLUE )

end

-- 게임 진행 메시지 출력
function EventCMMode_GameHelp2 ( )

	PrintSimpleXMLMsgAll( 5, { "PVP_CTI_SYSTEM_MESSAGE", 2 }, COLOR_BLUE )

end

-- 게임 진행 메시지 출력
function EventCMMode_GameHelp3 ( )

	PrintSimpleXMLMsgAll( 5, { "PVP_CTI_SYSTEM_MESSAGE", 3 }, COLOR_BLUE )

end

-- 가속기 활성화 시
function EventCMMode_AcceleratorCreate ( )

	PrintSimpleXMLMsgAll( 3, { "PVP_CTI_SYSTEM_MESSAGE", 14 }, COLOR_YELLOW )
	PrintChatXMLMsgAll( { "PVP_CTI_SYSTEM_MESSAGE", 14 }, COLOR_YELLOW )

end

-- 가속기 파괴 시
function EventCMMode_AcceleratorCapture ( _nFactionID )

end

-- 가속기 파괴 시 추가 포인트
function EventCMMode_AcceleratorCaptureAddPoint ( _nFactionID, _nAcceleratorID, _nPoint )

	local _factionName = GetCountryName( _nFactionID )
	local _acceleratorName = GetName( ACTOR_NPC, _nAcceleratorID )

	PrintSimpleXMLMsgAll( 3, { "PVP_CTI_SYSTEM_MESSAGE", 15, _factionName, _acceleratorName, _nPoint }, COLOR_RED )
	
	PrintChatXMLMsgFaction( EMFACTION_TEAM, _nFactionID, { "PVP_CTI_SYSTEM_MESSAGE", 16 }, COLOR_YELLOW, false )
	PrintChatXMLMsgFaction( EMFACTION_SAFE_TEAM, _nFactionID, { "PVP_CTI_SYSTEM_MESSAGE", 16 }, COLOR_YELLOW, false )

end

-- 가속기 활성화 1분 전 알림
function EventCMMode_AcceleratorRecharge ( _nMinute )

	PrintSimpleXMLMsgAll( 3, { "PVP_CTI_SYSTEM_MESSAGE", 13, _nMinute }, COLOR_YELLOW )
	PrintChatXMLMsgAll( { "PVP_CTI_SYSTEM_MESSAGE", 13, _nMinute }, COLOR_YELLOW )

end

-- 인증 성공
function EventCMMode_CertifySuccess ( _nCharID, _nCertificationMachineID, _bAddPoint )

	local _charName = GetName( ACTOR_PC, _nCharID )
	local _factionID = GetCountry( _nCharID )
	local _factionName = GetCountryName( _factionID )
	local _machineName = GetName( ACTOR_NPC, _nCertificationMachineID )
			
	if ( _bAddPoint == 1 ) then
		
		PrintSimpleXMLMsgAll( 3, { "PVP_CTI_SYSTEM_MESSAGE", 5, _factionName, _machineName}, COLOR_YELLOW)
		
		PrintChatXMLMsgFaction( EMFACTION_TEAM, _factionID, { "PVP_CTI_SYSTEM_MESSAGE", 8 }, COLOR_YELLOW, false )
		PrintChatXMLMsgFaction( EMFACTION_SAFE_TEAM, _factionID, { "PVP_CTI_SYSTEM_MESSAGE", 8 }, COLOR_YELLOW, false )
		
	else
	
		PrintSimpleXMLMsgAll( 3, { "PVP_CTI_SYSTEM_MESSAGE", 5, _factionName, _machineName}, COLOR_YELLOW)
	
	end
	
	PrintChatXMLMsgFaction( EMFACTION_TEAM, _factionID, { "PVP_CTI_SYSTEM_MESSAGE", 6, _charName, _machineName }, COLOR_YELLOW, false )
	PrintChatXMLMsgFaction( EMFACTION_SAFE_TEAM, _factionID, { "PVP_CTI_SYSTEM_MESSAGE", 6, _charName, _machineName }, COLOR_YELLOW, false )

end

-- 인증 실패
function EventCMMode_CertifyFail ( _nCharID )

	PrintChatXMLMsg( _nCharID, { "PVP_CTI_SYSTEM_MESSAGE", 20 }, COLOR_RED )

end

-- 자기 클럽이 이미 인증한 인증기라면
function EventCMMode_CertifyCapturedFail ( _nCharID, _nFactionID )

	PrintChatXMLMsg( _nCharID, { "PVP_CTI_SYSTEM_MESSAGE", 20 }, COLOR_RED )

end

-- 동맹 클럽이 이미 인증한 인증기라면
function EventCMMode_CertifyDuplicateFail ( _nCharID )

end

-- 인증기 중립화  60초 전 알림 (60초, 30초)
function EventCMMode_CertificationMachineNeutralizationWait ( _nFactionID, _nCertificationMachineID, _nSecond )

	local _machineName = GetName( ACTOR_NPC, _nCertificationMachineID )
	
	PrintChatXMLMsgFaction( EMFACTION_TEAM, _nFactionID, { "PVP_CTI_SYSTEM_MESSAGE", 9, _machineName, _nSecond }, COLOR_RED, false )
	PrintChatXMLMsgFaction( EMFACTION_SAFE_TEAM, _nFactionID, { "PVP_CTI_SYSTEM_MESSAGE", 9, _machineName, _nSecond }, COLOR_RED, false )

end

-- 인증기 중립화
function EventCMMode_CertificationMachineNeutralization ( _nFactionID, _nCertificationMachineID )

	local _factionName = GetCountryName( _nFactionID );
	local _machineName = GetName( ACTOR_NPC, _nCertificationMachineID )
	
	PrintSimpleXMLMsgAll( 3, { "PVP_CTI_SYSTEM_MESSAGE", 10, _factionName, _machineName }, COLOR_RED )
	
	PrintChatXMLMsgFaction( EMFACTION_TEAM, _nFactionID, { "PVP_CTI_SYSTEM_MESSAGE", 11, _machineName }, COLOR_RED, false )
	PrintChatXMLMsgFaction( EMFACTION_SAFE_TEAM, _nFactionID, { "PVP_CTI_SYSTEM_MESSAGE", 11, _machineName }, COLOR_RED, false )

end

-- 목표 점수 도달
function EventCMMode_ApproachedGoal ( _nFactionID )

	local _factionName = GetCountryName( _nFactionID );
	
	PrintSimpleXMLMsgAll( 5, { "PVP_CTI_SYSTEM_MESSAGE", 4, _factionName }, COLOR_RED )
	PrintChatXMLMsgAll( { "PVP_CTI_SYSTEM_MESSAGE", 4, _factionName }, COLOR_RED )

end

-- 게임 종료 알림
function EventCMMode_ExitGame ( _nMinute )

	PrintSimpleXMLMsgAll( 3, { "PVP_CTI_SYSTEM_MESSAGE", 12, _nMinute }, COLOR_RED )
	PrintChatXMLMsgAll( { "PVP_CTI_SYSTEM_MESSAGE", 12, _nMinute }, COLOR_RED )

end

-- 인증 확인
function EventCMMode_ConfirmCertification ( _nCertificationMachineID, _nFactionID )

	local _factionName = GetCountryName( _nFactionID )

	if ( _nFactionID == -1 ) or ( _nFactionID == nil ) then
		
		PrintSimpleXMLMsgAll( 3, { "PVP_CTI_SYSTEM_MESSAGE", 22 }, COLOR_WHITE )
		
	else
	
		PrintSimpleXMLMsgAll( 3, { "PVP_CTI_SYSTEM_MESSAGE", 23, _factionName }, COLOR_WHITE )
	
	end
	
end

-- 인증 시도
function EventCMMode_TryCertify ( _nCharID, _nCertificationMachineID )
	
	local _factionID = GetCountry( _nCharID )
	local _factionName = GetCountryName( _factionID )
	local _charName = GetName( ACTOR_PC, _nCharID )
	
	PrintSimpleXMLMsgAll( 5, { "PVP_CTI_SYSTEM_MESSAGE", 24, _factionName, _charName }, COLOR_RED )
	PrintChatXMLMsgAll( { "PVP_CTI_SYSTEM_MESSAGE", 24, _factionName, _charName }, COLOR_RED )

end

--------------------------------------------------------------------------------
