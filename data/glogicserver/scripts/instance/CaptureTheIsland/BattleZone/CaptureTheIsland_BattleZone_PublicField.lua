
-------------------------------------------------------------------------------
-- 버전 표기는 반드시 있어야 하며, 로직단과 반드시 일치해야 한다;
SCRIPT_VERSION = 
{
    "AlphaServer",		-- main version
    0,					-- sub version
};

-- 초기화시 저절로 기입되는 변수로 절대 선언 되면 안됩니다.;
--keyMapID = 선언금지 ( 초기화 이후 저절로 세팅 됨 );
--------------------------------------------------------------------------------
-- ActorType
ACTOR_PC						= 0;
ACTOR_NPC						= 1;
ACTOR_MOB						= 2;
ACTOR_SUMMON					= 6;
ACTOR_NULL						= -1;


--------------------------------------------------------------------------------	
-- ColorCode
COLOR_BLACK						= { 0, 0, 0 }
COLOR_WHITE						= { 255, 255, 255 }
COLOR_RED						= { 255, 0, 0 }
COLOR_GREEN						= { 0, 255, 0 }
COLOR_BLUE						= { 128, 194, 235 }
COLOR_MAGENTA					= { 240, 100, 170 } 
COLOR_YELLOW					= { 255, 200, 0 }


--------------------------------------------------------------------------------
RequireLevel					= 210
RequireClubLevel				= 5
RequireClubMember				= 10


--------------------------------------------------------------------------------
function luaFieldRequestJoin ( nPlayerDbNum, bRequestByGate )

	local _countryID = GetCountry( nPlayerDbNum )
	
	-- 선택한 국가가 없다면 처리한다;
	if ( _countryID == nil ) or (_countryID == 0 ) then
	
		PrintChatXMLMsg( nPlayerDbNum, { "PVP_CTI_SYSTEM_MESSAGE", 18 }, COLOR_RED )
		
		return false
		
	end
	
	-- 캐릭터의 레벨이 미달이라면 처리한다;
    if ( GetLevel( ACTOR_PC, nPlayerDbNum ) < RequireLevel ) then
    
		PrintChatXMLMsg( nPlayerDbNum, { "PVP_CTI_SYSTEM_MESSAGE", 19 }, COLOR_RED )
		
		return false
		
    end
    
    return true
    
end


--------------------------------------------------------------------------------