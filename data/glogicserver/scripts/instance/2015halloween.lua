--
-- 주의) 
-- 1) 대/소 문자를 확실하게 구분해야 합니다.
-- 2) true, false 를 확실하게 구분해야 합니다.
-- 3) UCS-2 Little Endian 로 저장해야 합니다.
-------------------------------------------------------------------------------

name = L"2015halloween"; -- 절대 중복 되어선 안된다;
keyMapMainID = 308; -- 절대 중복 되어선 안된다(subID는 사용하지 않음);
keyMapSubID = 0;
maxPlayer = 100;
maxCreate = 200;
isVisible = true; -- 클라이언트에 보여줄건지;
isUse = true; -- 이 인던을 사용할건지;

publicAgentScript =
{
	L"2015halloween\\2015halloween_PublicAgent.lua",
};

publicFieldScript =
{
	L"2015halloween\\2015halloween_PublicField.lua",
};

levelAgentScript =
{
	L"2015halloween\\2015halloween_LevelAgent.lua",
};

levelFieldScript =
{
	L"2015halloween\\2015halloween_LevelField.lua",
};
