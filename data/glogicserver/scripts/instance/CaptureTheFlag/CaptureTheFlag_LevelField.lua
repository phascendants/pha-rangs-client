--
-- 주의) 
-- 1) 대/소 문자를 확실하게 구분해야 합니다.
-- 2) true, false 를 확실하게 구분해야 합니다.
-- 3) UCS-2 Little Endian 로 저장해야 합니다.
-------------------------------------------------------------------------------
-- 버전 표기는 반드시 있어야 하며, 로직단과 반드시 일치해야 한다;
SCRIPT_VERSION_FIELD = 100;

-- ActorType
	ACTOR_PC				= 0;
	ACTOR_NPC				= 1;
	ACTOR_MOB				= 2;
	ACTOR_SUMMON			= 6;
	ACTOR_NULL				= -1;
--------------------------------------------------------------------------------
-- FactionType
	FACTION_TEAM			= 0;
	FACTION_PARTY			= 1;
	FACTION_CLUB			= 2;
	FACTION_EXPEDITION 		= 3;
--------------------------------------------------------------------------------
-- RelationType
	RELATION_ENEMY			= 0;
	RELATION_ALLY			= 1;
	RELATION_NEUTRAL		= 2;
--------------------------------------------------------------------------------
-- TriggerType
	TRIGGER_PC				= 1;
	TRIGGER_MOB				= 2;
	TRIGGER_PC_MOB		    = 3;
	TRIGGER_PC_SUMMON       = 8;
--------------------------------------------------------------------------------
-- Timer Event Code
	TEVENT_BATTLE_READY_TIME	= 1;	-- 깃발전 시작 준비 시간( 깃발전 시작전 준비시간 경과시 이벤트 발생 )
	TEVENT_BATTLE_TIME			= 2;	-- 깃발전 진행 ( 깃발전 시간 종료시 이벤트 발생 )
	TEVENT_SAFETY_TIME			= 3;	-- 부활 후 무적시간 ( 무적시간 종료시 이벤트 발생 )
	TEVENT_REWARD_TIME			= 4;	-- 깃발전 보상진행 ( 보상시간 종료후 이벤트 발생 )
--------------------------------------------------------------------------------
-- FACTION Team Code
	TEAM_SAFE				= 0;
	TEAM_BLUE				= 1;
	TEAM_RED				= 2;
--------------------------------------------------------------------------------

------ 깃발전 설정 변수 -------
nGoalScore = 3;					-- 승리 깃발획득 점수;
nBattle_Ready_Time = 10;		-- 게임 준비 시간;
nBattle_Time = 30;				-- 게임 제한 시간;
nRedTeam_StartGate = 6;			-- A팀 시작 게이트;
nBlueTeam_StartGate = 2;		-- B팀 시작 게이트;
nBattleStartCount = 3;			-- 입장후 전투 시작까지의 카운트;

nResurrectTime = 10;			-- 강제 부활 시간;
nResurrectRate = 10;			-- 강제 부활시 체력회복률;
nResurrectSafetyTime = 5;		-- 강제 부활후 무적시간;

nReward_Time = 30;				-- 보상 진행 시간;
nGoalTriggerRange = 20;			-- 골인지점 반경;

nRedFlagSkillSID = 14; 			-- 레드팀 깃발 이펙트 SkillID(sid & mid);
nRedFlagSkillMID = 4;

nBlueFlagSkillSID = 14;			-- 블루팀 깃발 이펙트 SkillID(sid & mid);
nBlueFlagSkillMID = 5;

nRedFlagSID = 88;				-- 레드팀 깃발 데이터 ID(sid & mid);
nRedFlagMID = 43;
nBlueFlagSID = 88;				-- 블루팀 깃발 데이터 ID(sid & mid);
nBlueFlagMID = 43;

nRedFlagPositionX = 61;			-- 레드팀 깃발 기본 생성 위치;
nRedFlagPositionZ = 52;
nBlueFlagPositionX = 62;		-- 블루팀 깃발 기본 생성 위치;
nBlueFlagPositionZ = 52;

nRedGoalActorSID = 88;			-- 레드팀 골인지점 데이터 ID (sid & mid);
nRedGoalActorMID = 44;
nBlueGoalActorSID = 88;			-- 블루팀 골인지점 데이터 ID (sid & mid);
nBlueGoalActorMID = 44;

nRedGoalPositionX = 50;			-- 레드팀 골인지점 위치;
nRedGoalPositionZ = 50;
nBlueGoalPositionX = 50;		-- 블루팀 골인지점 위치;
nBlueGoalPositionZ = 40;

nRedFlagTriggerID = nil;		-- 레드팀 깃발 트리거 ID (gaeaID);
bRedFlagDrop = false;
nBlueFlagTriggerID = nil;		-- 블루팀 깃발 트리거 ID (gaeaID);
bBlueFlagDrop = false;

nRedGoalTriggerID = nil;		-- 레드팀 골인지점 트리거 ID (gaeaID);
nBlueGoalTriggerID = nil;		-- 블루팀 골인지점 트리거 ID (gaeaID);

nRedTeamScore = 0;				-- 레드팀 현재 깃발 점수;
nBlueTeamScore = 0;				-- 블루팀 현재 깃발 점수;

PlayerList = {};				-- 플레이어 리스트;
TeamList = {};					-- 팀 리스트;

nBattleTimerHandle = nil;		-- 깃발전 진행 타이머 핸들;

bLoosePlaying = false;
bLoosePlayingTimer = 30;		-- 깃발전 루즈플레이 타이머;
---------------------------------

function EventInitialize()	
	SetFaction( ACTOR_NULL, TEAM_SAFE, FACTION_TEAM, TEAM_SAFE );  -- 비 전투 인원 진영(무적 진영);
	SetFaction( ACTOR_NULL, TEAM_BLUE, FACTION_TEAM, TEAM_BLUE );  -- A팀 진영;
	SetFaction( ACTOR_NULL, TEAM_RED, FACTION_TEAM, TEAM_RED );  -- B팀 진영;
	
	SetRelation( FACTION_TEAM, TEAM_SAFE, FACTION_TEAM, TEAM_BLUE, RELATION_ALLY);
	SetRelation( FACTION_TEAM, TEAM_BLUE, FACTION_TEAM, TEAM_SAFE, RELATION_ALLY);
	SetRelation( FACTION_TEAM, TEAM_SAFE, FACTION_TEAM, TEAM_RED, RELATION_ALLY);
	SetRelation( FACTION_TEAM, TEAM_RED, FACTION_TEAM, TEAM_SAFE, RELATION_ALLY);
	
	SetRelation( FACTION_TEAM, TEAM_BLUE, FACTION_TEAM, TEAM_RED, RELATION_ENEMY);
	SetRelation( FACTION_TEAM, TEAM_RED, FACTION_TEAM, TEAM_BLUE, RELATION_ENEMY);	
	
	RegistTimer(nBattle_Ready_Time, TEVENT_BATTLE_READY_TIME, 0, 0, 0);
	
	return true;
end

function EventJoin(actorID, mapMainID, mapSubID) -- 인던 "입장 요청"시 실행되는 부분. ex)입장아이템 소모;	
	if ( PlayerList[actorID] == nil ) then
		--최초 입장 플레이어인 경우 초기화 작업;
		PlayerList[actorID] = true;
		local schoolNum = GetSchool(actorID);
		if ( schoolNum == 0 ) then
			SetFaction(ACTOR_PC, actorID, FACTION_TEAM, 1);
		else
			SetFaction(ACTOR_PC, actorID, FACTION_TEAM, 2);
		end
		
		local factionType, factionID = GetFaction(ACTOR_PC, actorID);
		TeamList[actorID] = factionID;
		--SetFaction(ACTOR_PC, actorID, FACTION_TEAM, 0);
	end
end

function EventOut(actorID, toMapMID, toMapSID)
	-- 유저 인던 이탈시. 해당 유저 재입장 금지;
	PlayerList[actorID] = false;
end

function EventTimer(nTimerHandle, nParam0, nParam1, nParam2, nParam3)  -- 타이머가 정한 시간이 되었을때 실행되는 부분;
	if ( nParam0 == TEVENT_BATTLE_READY_TIME ) then
		nRedFlagTriggerID = DropCrowMapPos(2, 0, nRedFlagSID, nRedFlagMID, nRedFlagPositionX, nRedFlagPositionZ);
		nBlueFlagTriggerID = DropCrowMapPos(2, 0, nBlueFlagSID, nBlueFlagMID, nBlueFlagPositionX, nBlueFlagPositionZ);
		nRedGoalTriggerID = DropCrowMapPos(2, 0, nRedGoalActorSID, nRedGoalActorMID, nRedGoalPositionX, nRedGoalPositionZ);
		nBlueGoalTriggerID = DropCrowMapPos(2, 0, nBlueGoalActorSID, nBlueGoalActorMID, nBlueGoalPositionX, nBlueGoalPositionZ);
		RegistActorTrigger(ACTOR_NPC, nRedGoalTriggerID, TRIGGER_PC, nGoalTriggerRange);
		RegistActorTrigger(ACTOR_NPC, nBlueGoalTriggerID, TRIGGER_PC, nGoalTriggerRange);
		nBattleTimerHandle = RegistTimer(nBattle_Time, TEVENT_BATTLE_TIME,0,0,0);	
		
		UI_RedFlagGuideArrow_Target(FACTION_TEAM, TEAM_SAFE, ACTOR_NPC, nRedFlagTriggerID);
		UI_BlueFlagGuideArrow_Target(FACTION_TEAM, TEAM_SAFE, ACTOR_NPC, nBlueFlagTriggerID);
		UI_RedFlagGuideArrow_Target(FACTION_TEAM, TEAM_BLUE, ACTOR_NPC, nRedFlagTriggerID);
		UI_BlueFlagGuideArrow_Target(FACTION_TEAM, TEAM_BLUE, ACTOR_NPC, nBlueFlagTriggerID);
		UI_RedFlagGuideArrow_Target(FACTION_TEAM, TEAM_RED, ACTOR_NPC, nRedFlagTriggerID);
		UI_BlueFlagGuideArrow_Target(FACTION_TEAM, TEAM_RED, ACTOR_NPC, nBlueFlagTriggerID);
		
		PrintSimpleMsgAll("StartCTF!", 5, 0, 12);
		
		UI_CompetitionResult_CaptureTheFlag();
		UI_CompetitionResultInfo();
		UI_CompetitionResultEndInfo();
		
		Log_CaptureTheFlag_Start(3);
		
	elseif ( nParam0 == TEVENT_BATTLE_TIME ) then
		Log_CaptureTheFlag_End(3);
		functionReward();
		RegistTimer(nReward_Time, TEVENT_REWARD_TIME, 0, 0, 0);
		
	elseif ( nParam0 == TEVENT_SAFETY_TIME ) then  -- 전투 지역입장후 무적시간 경과시;		
		SetFaction( ACTOR_PC, nParam1, FACTION_TEAM, TeamList[actorID] );
		
	elseif ( nParam0 == TEVENT_REWARD_TIME ) then
		DoDestroy();
	
	end
	
end

function EventDie(nDieActorType, nDieActorID, nKillActorType, nKillActorID)  -- 어떠한 PC,MOB이 죽었을 때 실행되는 부분;
	if ( nDieActorType == ACTOR_PC ) then
		-- PC 사망시 해당하는 진영부활지점에서 일정시간후 강제 부활;
		local nFactionID = TeamList[actorID];
		if ( nFactionID == 1 ) then
			SetResurrectionForcedInstance(nDieActorID, 2, 0, nRedTeam_StartGate, nResurrectTime, nResurrectRate);  -- 안전지역 맵에서 강제부활;	
		else
			SetResurrectionForcedInstance(nDieActorID, 2, 0, nBlueTeam_StartGate, nResurrectTime, nResurrectRate);  -- 안전지역 맵에서 강제부활;	
		end
		
		-- 만약 깃발 소유자가 죽을시 깃발 재배치;			
		if ( IsHaveSkillFact(nDieActorType, nDieActorID, nRedFlagSkillSID, nRedFlagSkillMID) == true ) then
			PrintChatMsgAll("flag user die!!");
			local posX,posY,posZ = GetPosition(nDieActorType, nDieActorID);
			if (posX ~= nil) then
				nRedFlagTriggerID = DropCrow(2, 0, nRedFlagSID, nRedFlagMID, posX, posZ);
				bRedFlagDrop = true;
				UI_RedFlagGuideArrow_Target(FACTION_TEAM, TEAM_RED, ACTOR_NPC, nRedFlagTriggerID);
				UI_RedFlagGuideArrow_Target(FACTION_TEAM, TEAM_BLUE, ACTOR_NPC, nRedFlagTriggerID);
			end
		
		elseif ( IsHaveSkillFact(nDieActorType, nDieActorID, nBlueFlagSkillSID, nBlueFlagSkillMID) == true ) then
			PrintChatMsgAll("flag user die!!");
			local posX,posY,posZ = GetPosition(nDieActorType, nDieActorID);
			if (posX ~= nil) then
				nBlueFlagTriggerID = DropCrow(2, 0, nBlueFlagSID, nBlueFlagMID, posX, posZ);
				bBlueFlagDrop = true;
				UI_BlueFlagGuideArrow_Target(FACTION_TEAM, TEAM_RED, ACTOR_NPC, nBlueFlagTriggerID);
				UI_BlueFlagGuideArrow_Target(FACTION_TEAM, TEAM_BLUE, ACTOR_NPC, nBlueFlagTriggerID);
			end
		end
	end
	return true;
end

function EventResurrect(nResurrectType, nResurretActorType, nResurrectActorID, nKillActorType, nKillActorID)
	if ( nDieActorType == ACTOR_PC ) then
		SetFaction(nResurretActorType, nResurrectActorID, FACTION_TEAM, 0);
		RegistTimer(nResurrectSafetyTime, TEVENT_SAFETY_TIME, nResurrectActorID, 0, 0);
	end
	return true;
end

function EventActorTriggerIn(nTriggerType, nTriggerID, nInnerType, nInnerID)
	if ( nTriggerID == nRedGoalTriggerID ) then
		local bSkillFact = IsHaveSkillFact(nInnerType, nInnerID, nBlueFlagSkillSID, nBlueFlagSkillMID);
		if ( bSkillFact == true ) then
			nBlueFlagTriggerID = DropCrowMapPos(2, 0, nBlueFlagSID, nBlueFlagMID, nBlueFlagPositionX, nBlueFlagPositionZ);
			RemoveSkillFact(nInnerType, nInnerID, nBlueFlagSkillSID, nBlueFlagSkillMID);
			nRedTeamScore = nRedTeamScore + 1;
			UI_BlueFlagGuideArrow_Target(FACTION_TEAM, TEAM_RED, ACTOR_NPC, nBlueFlagTriggerID);
			UI_BlueFlagGuideArrow_Target(FACTION_TEAM, TEAM_BLUE, ACTOR_NPC, nBlueFlagTriggerID);
			if ( nRedTeamScore > nGoalScore ) then
				RemoveTimer(nBattleTimerHandle);
				RegistTimer(0, TEVENT_BATTLE_TIME,0,0,0);
			end
		end
	
	elseif ( nTriggerID == nBlueGoalTriggerID ) then
		local bSkillFact = IsHaveSkillFact(nInnerType, nInnerID, nRedFlagSkillSID, nRedFlagSkillMID);	
		if ( bSkillFact == true ) then
			nRedFlagTriggerID = DropCrowMapPos(2, 0, nRedFlagSID, nRedFlagMID, nRedFlagPositionX, nRedFlagPositionZ);
			RemoveSkillFact(nInnerType, nInnerID, nRedFlagSkillSID, nRedFlagSkillMID);
			nBlueTeamScore = nBlueTeamScore + 1;
			UI_RedFlagGuideArrow_Target(FACTION_TEAM, TEAM_RED, ACTOR_NPC, nRedFlagTriggerID);
			UI_RedFlagGuideArrow_Target(FACTION_TEAM, TEAM_BLUE, ACTOR_NPC, nRedFlagTriggerID);
			if ( nBlueTeamScore > nGoalScore ) then
				RemoveTimer(nBattleTimerHandle);
				RegistTimer(0, TEVENT_BATTLE_TIME,0,0,0);
			end
		end
		
	end
end

--function EventActorTriggerOut(nTriggerType, nTriggerID, nOuterType, nOuterID)
--end


function EventClickTrigger(nTriggerType, nTriggerID, nClickerType, nClickerID)
	if ( nTriggerID == nRedFlagTriggerID ) then -- 레드팀 깃발을 클릭시 (탈취 시);
		if ( TeamList[nClickerID] == TEAM_BLUE ) then
			SetHP(nTriggerType, nTriggerID, 0);
			nRedFlagTriggerID = nil;
			ApplySkill(nClickerType, nClickerID, nRedFlagSkillSID, nRedFlagSkillMID, 1, 300, 0);
			UI_RedFlagGuideArrow_Target(FACTION_TEAM, TEAM_BLUE, nClickerType, nClickerID);
			UI_RedFlagGuideArrow_Off(FACTION_TEAM, TEAM_RED);
		elseif ( bRedFlagDrop == true ) then
			SetHP(nTriggerType, nTriggerID, 0);
			nRedFlagTriggerID = DropCrowMapPos(2, 0, nRedFlagSID, nRedFlagMID, nRedFlagPositionX, nRedFlagPositionZ);
			bRedFlagDrop = false;
			UI_RedFlagGuideArrow_Target(FACTION_TEAM, TEAM_BLUE, ACTOR_NPC, nRedFlagTriggerID);
			UI_RedFlagGuideArrow_Target(FACTION_TEAM, TEAM_BLUE, ACTOR_NPC, nRedFlagTriggerID);
		end
	
	elseif ( nTriggerID == nBlueFlagTriggerID ) then -- 블루팀 깃발을 클릭시 (탈취 시);
		if ( TeamList[nClickerID] == TEAM_RED ) then
			SetHP(nTriggerType, nTriggerID, 0);
			nBlueFlagTriggerID = nil;
			ApplySkill(nClickerType, nClickerID, nBlueFlagSkillSID, nBlueFlagSkillMID, 1, 300, 0);
			UI_BlueRedFlagGuideArrow_Target(FACTION_TEAM, TEAM_RED, nClickerType, nClickerID);
			UI_BlueFlagGuideArrow_Off(FACTION_TEAM, TEAM_BLUE);
		elseif ( bBlueFlagDrop == true ) then
			SetHP(nTriggerType, nTriggerID, 0);
			nBlueFlagTriggerID = DropCrowMapPos(2, 0, nBlueFlagSID, nBlueFlagMID, nBlueFlagPositionX, nBlueFlagPositionZ);
			bBlueFlagDrop = false;
			UI_BlueRedFlagGuideArrow_Target(FACTION_TEAM, TEAM_BLUE, ACTOR_NPC, nBlueFlagTriggerID);
			UI_BlueRedFlagGuideArrow_Target(FACTION_TEAM, TEAM_RED, ACTOR_NPC, nBlueFlagTriggerID);
		end
		
	end
end

function functionReward()
	PrintChatMsgAll("End Game");
	Log_CaptureTheFlag_Result(12312,1,1,3,214,324,3,0,0, "32/23");
	Log_CaptureTheFlag_Result(24517,0,0,1,10,0,0,3,2, "No Reward");
end






--[[        
function EventMapEnter(actorID, fromMapMainID, fromMapSubID, toMapMainID, toMapSubID)
end

function EventRequestMoveGate(nActorID, nMapMainID, nMapSubID, nGateID, nMainMapIDTarget, nSubMapIDTarget, nGateIDTarget)
	return true;
end

function EventReceiveDamage(nDamagedActorType, nDamagedActorID, nAttackActorType, nAttackActorID, nDamage, nDamageFlag)  -- 어떠한 PC,MOB이 데미지를 받으면 실행되는 부분;
	return true,nDamage;
end

function EventReceiveHeal(nReceiveActorType, nReceiveActorID, nHealActorType, nHealActorID, nHeal, nHealFlag)  -- 어떠한 PC,MOB이 회복를 받으면 실행되는 부분;
	return true,nHeal; 
end

function EventUseItem(nActorID, nItemID, nParam0, nParam1) -- 유저가 어떠한 Item을 사용하였을때 실행되는 부분;
	return true;
end

function EventPartyChangeMaster(nPartyID, bExpedition, nNewMasterID, bInMapNewMaster, nOldMaster, bInMapOldMaster)
end

function EventPartyJoin(nPartyID, bExpedition, nJoinerID, bInMapJoiner)
end

function EventPartyOut(nPartyID, bExpedition, nOuterID, bInMapOuter)
end
]]--