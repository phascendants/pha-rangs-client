--
-- 주의) 
-- 1) 대/소 문자를 확실하게 구분해야 합니다.
-- 2) true, false 를 확실하게 구분해야 합니다.
-- 3) UCS-2 Little Endian 로 저장해야 합니다.
---------------------
-- 버전 표기는 반드시 있어야 하며, 로직단과 반드시 일치해야 한다;
SCRIPT_VERSION = 
{
    "AlphaServer",     -- main version
    0,    -- sub version
};

-- 초기화시 저절로 기입되는 변수로 절대 선언 되면 안됩니다.;
--keyMapID = 선언금지 ( 초기화 이후 저절로 세팅 됨 );

----------------------------------------------------------
-- CustomMessage Sender Type
    MSG_TO_USER             = 0;    -- 특정 유저;   SenderID : CharDbNum;
    MSG_TO_MY_INSTANCE      = 1;    -- 현재 자신이 속한 필드서버 인던 객체 (LevelField)  SenderID : 의미없음;
    MSG_TO_FIELD_BASE       = 2;    -- 필드서버의 특정 인던 컨텐츠 (PublicField);  SenderID : Instance KeyMap ID;
    MSG_TO_FIELD_INSTANCE   = 3;    -- 필드서버의 특정 인던 객체 (LevelField);  SenderID : Instance Map ID;
    MSG_TO_AGENT_BASE       = 4;    -- 에이전트서버의 특정 인던 컨텐츠 (PublicAgent);  SenderID : Instance KeyMap ID;
    MSG_TO_AGENT_INSTANCE   = 5;    -- 에이전트서버의 특정 인던 객체 (LevelAgent);  SenderID : Instance Map ID;
--------------------------------------------------------------------------------
-- CutsomParameter list
	PARAM1_EXIT_BUTTON			= 1;
	PARAM1_JOIN_UI				= 2;
	PARAM1_REQ_JOIN				= 3;
	
	PARAM3_MSGBOX_OK			= 0;
	PARAM3_MSGBOX_CANCEL		= 1;
	PARAM3_MSGBOX_TIMEOVER		= 2;
--------------------------------------------------------------------------------

currentMapID = -1;

bOpen = false;

ReserveJoinUserTable = {};

function luaAgentRequestCreate(nPlayerDbNum, bRequestByGate)
	return true;
end

function luaAgentRequestDuplicateCreate()
	return false;
end

function luaAgentRequestMapID(nPlayerDbNum)
	return 180,0;
end

function luaAgentEventCustomMessage(nSenderType, nSenderID, nParam1, nParam2, nParam3, nParam4)
    ServerLog("EventCustomMessage Agent");
    ServerLog("nSenderType : "..tostring(nSenderType));
    ServerLog("nSenderID : "..tostring(nSenderID));
    ServerLog("nParam1 : "..tostring(nParam1));
    ServerLog("nParam2 : "..tostring(nParam2));
    ServerLog("nParam3 : "..tostring(nParam3));
    ServerLog("nParam4 : "..tostring(nParam4));
    ServerLog("------------------------");
    if ( nParam1 == PARAM1_REQ_JOIN ) then
		local _partyID = GetPartyID(nParam2);
		if ( _partyID ~= nil ) then
			local _master = GetPartyMaster(_partyID);
			local _ClubID = GetClubID(nParam2);
			if ( _ClubID ~= nil ) then
				local _masterClubID = GetClubID(_master);
				if ( _masterClubID == _ClubID ) then
					if ( functionCheckRequire(nParam2) == true ) then
                        --SendMessage(MSG_TO_FIELD_INSTANCE, nSenderID, PARAM1_REQ_JOIN, nParam2, 0, 0);
                        ReserveJoinUserTable[nParam2] = nSenderID;
                        UI_XmlTimerMessageBox(nParam2, { "RNCDM_REJOIN_TEXT", 0 },{ "RNCDM_REJOIN_TEXT", 1 },{ "RNCDM_REJOIN_TEXT", 2 }, 30, PARAM1_JOIN_UI, 0); --gameserver
					end
				else
					--PrintChatMsg(nParam2, "not in Same Club (plz Change XMlString)");
				end
			else
				--PrintChatMsg(nParam2, "not in club (plz Change XMlString)");
			end
		end
    elseif ( nParam1 == PARAM1_JOIN_UI ) then
        if ( ReserveJoinUserTable[nSenderID] ~= nil ) then
            if ( nParam3 == PARAM3_MSGBOX_OK ) then -- OK 버튼 클릭;
                SendMessage(MSG_TO_FIELD_INSTANCE, ReserveJoinUserTable[nSenderID], PARAM1_JOIN_UI, nSenderID, 0, 0);
            end
            ReserveJoinUserTable[nSenderID] = nil;
		end
	end
end

function functionCheckRequire(nPlayerDbNum)
	-- 전장 컨텐츠를 신청,이용 중인지 검사;
	if ( (IsJoinGameofALL(nPlayerDbNum) == true) or (IsJoinOtherContents(nPlayerDbNum) == true) )then
		--PrintChatXmlMsg(nPlayerDbNum, { "RNCDM_SYSTEM_MESSAGE", 13 },COLOR_RED); -- (다른 참가 신청이 있습니다.) 신청자에게 보냄
		return false;
	end

	-- 필요 레벨 검사
	local _nLevel = GetLevel(nPlayerDbNum);
	if ( _nLevel < 195) then
		--PrintChatXmlMsg(nPlayerDbNum, { "RNCDM_SYSTEM_MESSAGE", 21 },COLOR_RED); -- (입장 가능레벨을 확인하세요.) 신청자에게 보냄
		return false;
	end
	
	-- CDM 참가 권한이 있는가?
	local bAuthority = HaveCDM_Authority(nPlayerDbNum);
	if ( bAuthority == false) then
		--PrintChatXmlMsg(nPlayerDbNum, { "RNCDM_SYSTEM_MESSAGE", 22 },COLOR_RED); -- (클럽권한(클럽데스매치 참가자격)을 확인하세요.) 신청자에게 보냄
		return false;
	end
	
	return true;
end