--
-- 주의) 
-- 1) 대/소 문자를 확실하게 구분해야 합니다.
-- 2) true, false 를 확실하게 구분해야 합니다.
-- 3) UCS-2 Little Endian 로 저장해야 합니다.
-------------------------------------------------------------------------------
-- 버전 표기는 반드시 있어야 하며, 로직단과 반드시 일치해야 한다;
SCRIPT_VERSION = 
{
    "AlphaServer",     -- main version
    0,    -- sub version
};

-- 초기화시 저절로 기입되는 변수로 절대 선언 되면 안됩니다.;
--keyMapID = 선언금지 ( 초기화 이후 저절로 세팅 됨 );
--instanceMapID = 선언금지 ( 초기화 이후 저절로 세팅 됨 );

--------------------------------------------------------------------------------
-- ColorCode
	COLOR_BLACK				= {0,0,0};
	COLOR_WHITE				= {255,255,255};
	COLOR_RED				= {255,0,0};
	COLOR_GREEN				= {0,255,0};
	COLOR_BLUE				= {128,194,235};
--------------------------------------------------------------------------------

function luaAgentRequestEntry(nPlayerDbNum, bRequestByGate)
	return true;
end

function luaAgentRequestJoin(nPlayerDbNum, nAutority)
	local _nClubID = GetClubID(nPlayerDbNum);
	if ( _nClubID == nil ) then
		PrintChatXmlMsg(nPlayerDbNum, {"PVP_CDM_SYSTEM_MESSAGE", 0}, COLOR_RED);	-- 채팅 메시지 출력 
		return false;
	end

	local _partyID = GetPartyID(nPlayerDbNum);
	if ( _partyID == nil ) then
		PrintChatXmlMsg(nPlayerDbNum, {"PVP_CDM_SYSTEM_MESSAGE", 2}, COLOR_RED);	-- 채팅 메시지 출력 
		return false;
	end
	
	return true;
end

function luaAgentEventJoin(nPlayerDbNum, nAutority)
	return 0; -- 0번 게이트로 입장;
end

function luaAgentEventClubOut(nClubDbNum, nPlayerDbNum, nKickActionActorDbNum)
	DoOut(nPlayerDbNum);
end