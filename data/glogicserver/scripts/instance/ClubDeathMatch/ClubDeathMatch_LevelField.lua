--
-- 주의) 
-- 1) 대/소 문자를 확실하게 구분해야 합니다.
-- 2) true, false 를 확실하게 구분해야 합니다.
-- 3) UCS-2 Little Endian 로 저장해야 합니다.
-------------------------------------------------------------------------------
-- 버전 표기는 반드시 있어야 하며, 로직단과 반드시 일치해야 한다;
SCRIPT_VERSION = 
{
    "AlphaServer",     -- main version
    0,    -- sub version
};

-- 초기화시 저절로 기입되는 변수로 절대 선언 되면 안됩니다.;
--keyMapID = 선언금지 ( 초기화 이후 저절로 세팅 됨 );
--instanceMapID = 선언금지 ( 초기화 이후 저절로 세팅 됨 );

-- ActorType
	ACTOR_PC				= 0;
	ACTOR_NPC				= 1;
	ACTOR_MOB				= 2;
	ACTOR_SUMMON			= 6;
	ACTOR_NULL				= -1;
--------------------------------------------------------------------------------
-- FactionType
	FACTION_CLUB				= 0;
	FACTION_SAFE_CLUB	= 1;
--------------------------------------------------------------------------------
-- RelationType
	RELATION_ENEMY					= 0;
	RELATION_ALLY						= 1;
	RELATION_NEUTRAL_ENEMY		= 2;
	RELATION_NEUTRAL_ALLY		= 3;
--------------------------------------------------------------------------------
-- ColorCode
	COLOR_BLACK				= {0,0,0};
	COLOR_WHITE				= {255,255,255};
	COLOR_RED				= {255,0,0};
	COLOR_GREEN				= {0,255,0};
	COLOR_BLUE				= {128,194,235};
--------------------------------------------------------------------------------
-- CustomMessage Sender Type
    MSG_TO_USER             = 0;    -- 특정 유저;   SenderID : CharDbNum;
    MSG_TO_MY_INSTANCE      = 1;    -- 현재 자신이 속한 필드서버 인던 객체 (LevelField)  SenderID : 의미없음;
    MSG_TO_FIELD_BASE       = 2;    -- 필드서버의 특정 인던 컨텐츠 (PublicField);  SenderID : Instance KeyMap ID;
    MSG_TO_FIELD_INSTANCE   = 3;    -- 필드서버의 특정 인던 객체 (LevelField);  SenderID : Instance Map ID;
    MSG_TO_AGENT_BASE       = 4;    -- 에이전트서버의 특정 인던 컨텐츠 (PublicAgent);  SenderID : Instance KeyMap ID;
    MSG_TO_AGENT_INSTANCE   = 5;    -- 에이전트서버의 특정 인던 객체 (LevelAgent);  SenderID : Instance Map ID;
--------------------------------------------------------------------------------
-- Timer Event Code
	TEVENT_KICK_SAFEZONE		= 1;
	TEVENT_SAFETY_TIME			= 2;
	TEVENT_BATTLE_TIME			= 3;
	TEVENT_REWARD_TIME			= 4;
	TEVENT_SHOW_PROGRESS_INFO	= 5;
	TEVENT_DOUBLE_TIME			= 6;
	TEVENT_SKILL_RESURRECT		= 7;
--------------------------------------------------------------------------------
-- CustomParameter list
	PARAM1_EXIT_BUTTON			= 1;
	PARAM1_JOIN_UI				= 2;
	PARAM1_REQ_JOIN				= 3;
	
	PARAM3_MSGBOX_OK			= 0;
	PARAM3_MSGBOX_CANCEL		= 1;
	PARAM3_MSGBOX_TIMEOVER		= 2;
	
	EVENT_SKILL_RESURRECT 		= 8;		-- 스킬 부활
	EVENT_SYSTEM_RESURRECT 		= 16;		-- 시스템 부활
--------------------------------------------------------------------------------
-- ColorCode
	COLOR_BLACK				= {0,0,0};
	COLOR_WHITE				= {255,255,255};
	COLOR_RED				= {255,0,0};
	COLOR_GREEN				= {0,255,0};
	COLOR_BLUE				= {128,194,235};
--------------------------------------------------------------------------------

-- 색인용 검색 키워드
	-- CDM 초기화
	-- CDM 입장 제어
	-- CDM (파티) 입장 처리
	-- CDM 맵 이동 처리
	-- CDM 타이머 처리
	-- CDM 전투 진행 처리
	-- CDM 커스텀 메시지 처리
	-- CDM 스크립트 내부 함수
	-- CDM 로그
--------------------------------------------------------------------------------

------ 데스메치 설정 변수 -------
nClub_Start_Take_Point = 80;		-- 최초 클럽에 부여되는 점수;
nChar_Start_Take_Point = 10;		-- 최초 플레이어에게 부여되는 개인 점수;
nChar_Death_Point = 3;				-- 플레이어가 죽었을 때 소속 클럽의 클럽점수 감소 포인트;
nChar_ViewRange = 400;				-- CDM 전투 맵에서의 시야 거리;

	-- 시간 관련 변수 -- 
Battle_Time = 1800;					-- CDM 전투 진행 시간;
Reward_Time = 30;					-- 보상 진행 시간;
Safety_Time = 7;					-- 전투 지역 입장시 무적 시간;
SafeMap_Kick_Time = 60;				-- 안전 지역 체류시 강제 전투 진입 시간;
Progress_Show_Timer = 5;			-- 진행상태 정보 갱신 시간;

	-- 더블포인트 관련 --
DoubleState = 1;					-- 현재 더블타임 상황;
DoublePoint = { 1, 2, 1, 2, 1, 2};			-- 더블타임 포인트 배수;
DoubleTime = { 540, 60, 540, 60, 480, 120};		-- 더블타임 시간표 (초);
MaxDoubleTime = 6;

	-- 맵 ID 관련 --
LobbyMapID = { 180, 0 };			-- 로비 지역 MapID;
BattleMapID = { 181, 0 };			-- 내부 지역 MapID;

	-- HP버프 --
Buff_ID = {54, 13};					-- HP 증폭 버프 (1레벨 0.5, 2레벨, 0.8, 3레벨 1.0, 4레벨, 1.3, 5레벨 1.5, 6레벨, 1.8, 7레벨, 2.0, 8레벨 2.3, 9레벨 2.5)

	-- CDM 보상 관련 --
rewardList =
{
	{-- 1등 보상;
		{320,01,5}, -- 아이템 (MID,SID,겹침 수량); 1등 훈장5개;
	},
	{-- 2등 보상;
		{320,01,4}, -- 아이템 (MID,SID); 2등 훈장4개;
	},
	{-- 3등 보상;
		{320,01,3}, -- 아이템 (MID,SID); 3등 훈장3개;
	},
}
---------------------------------

ClubPoint = {}  -- 참가 클럽의 Point;
ClubKill = {}  -- 참가 클럽의 Kill횟수;
ClubDeath = {}  -- 참가 클럽의 Death횟수;
ClubResurrect = {}  -- 참가 클럽의 SkillResurrect횟수;

ClubList = {}; -- 참가 클럽;
ClubPlayerList = {}  --- 참가 유저의 리스트;
PartyID = {} -- 참가 파티;
BanPlayerList = {}; -- 참여 금지 유저 리스트;

PlayerPoint = {}  -- 참가 유저의 개인 Point;
PlayerKillCount = {}  -- 참가 유저의 개인 kill Count;
PlayerDeathCount = {}  -- 참가 유저의 개인 death Count;
PlayerDamageCount = {} -- 참가 유저의 개인 총 데미지 량;
PlayerHealCount = {} -- 참가 유저의 개인 총 회복 량;
PlayerResurrectCount = {} -- 참가 유저의 개인 부활 횟수;

PlayerSafeKickTimer = {} -- 유저의 타이머 핸들;
RankList = {} -- 순위가 저장되는 테이블;






---------------------------------------- CDM 초기화 ----------------------------------------
function EventInitialize()
	tStartTime = GetTime_TIME64();	-- CDM 이전 기록에 사용하기 위한 CDM 시작시간;
	
	SetHpVisible(true);				-- CDM에서는 적의 HP를 볼 수 있도록 한다.;
	
	CDM_TimerHandle = RegistTimer(Battle_Time, TEVENT_BATTLE_TIME,0,0,0);	-- CDM 전투시간 동안만 진행하기 위해 타이머 등록;
	RegistTimer(Progress_Show_Timer, TEVENT_SHOW_PROGRESS_INFO, 0, 0, 0, 1);	-- CDM 진행상황 갱신 주기를 타이머에 등록;
	RegistTimer(DoubleTime[DoubleState], TEVENT_DOUBLE_TIME, 0, 0, 0);		-- CDM 더블 타임 주기를 타이머에 등록;
	return true;
end
---------------------------------------------------------------------------------------------





---------------------------------------- CDM 입장 제어 ----------------------------------------
-- 입장제어를 할 때 actorID는 같은 Field에서 들어오는 경우에만 유효하다.
-- 그렇기 때문에 actorID를 이용하는 예외처리 코드는 EventJoin Callback에서 한번 더 처리해주어야 한다.
function EventRequestJoin(actorID)
	--local _nClubID = GetClubID(actorID);
	--if ( _nClubID == nil ) then
		--return false;
	--end

	--if ( BanPlayerList[actorID] == true ) then -- 해당 플레이어가 입장 금지 된 경우;
		--return false;
	--end
	
	--if ( ClubList[_nClubID] == false ) then -- 해당 클럽이 입장 금지된 경우 ( 클럽원 전부가 전장에 나간적이 있을때 );
		--return false;
	--end
	
	--local _partyID = GetPartyID(actorID);
	--if ( ClubList[_nClubID] ~= nil ) then -- 이미 입장해있는 클럽인 경우 파티는 1개만 가능하다;			
		--if ( PartyID[_nClubID] ~= _partyID ) then
			--return false;
		--end
	--end
	
	-- 현재 유저가 필드서버에 접속하기 전이라서 actorID의 정보가 없을 수 있다.;
	-- 따라서 Agent서버에서 위 사항을 체크한다. ( 현재는 Matching Lobby에서 체크 );	
	return true;
end

function EventRequestJoinGM(actorID)
	return true; -- GM 명령어로 들어온 경우 무조건 허용;
end

function EventRequestJoinForced(actorID)
	local _nClubID = GetClubID(actorID);

	local _partyID = GetPartyID(actorID);
	if ( ClubList[_nClubID] ~= nil ) then
		if ( PartyID[_nClubID] ~= _partyID ) then
			PrintChatXMLMsg(actorID, {"PVP_CDM_SYSTEM_MESSAGE", 1}, COLOR_RED);	-- 채팅 메시지 출력 
			return false;
		end
	end
	
	return true;
end
---------------------------------------------------------------------------------------------







--------------------------------------- CDM (파티) 입장 처리  -----------------------------------------
function EventJoin(actorID, mapMainID, mapSubID) -- 인던 "입장 요청"시 실행되는 부분. ex)입장아이템 소모;    
	local _nClubID = GetClubID(actorID);
	
	local _partyID = GetPartyID(actorID);
	if ( ClubList[_nClubID] ~= nil ) then
		if ( PartyID[_nClubID] ~= _partyID ) then
			PrintChatXMLMsg(actorID, {"PVP_CDM_SYSTEM_MESSAGE", 1}, COLOR_RED);	-- 채팅 메시지 출력 
			DoOut(actorID);
			return;
		end
	end
	
	-- CDM 로그 ( 입장 )
	Log_CDM_In(actorID, _nClubID);
end

function EventOffline(actorID)
	RemoveSkillFact(ACTOR_PC, actorID, Buff_ID[1], Buff_ID[2]);	-- offline인 경우  HP버프 삭제;
    EventOut(actorID);
end

function EventOut(actorID)
	local OuterClubID = GetClubID(actorID);
	
	-- CDM 로그 ( 퇴장 );
	Log_CDM_Out(actorID, OuterClubID);
	
	-- 유저 인던 이탈시. 해당 유저 재입장 금지, 보상 금지;
	BanPlayerList[actorID] = true;
	ClubPlayerList[OuterClubID][actorID] = false;
	RemoveSkillFact(ACTOR_PC, actorID, Buff_ID[1], Buff_ID[2]);	-- 맵에서 나간경우  HP버프 삭제;
	
	-- 클럽원 전원 퇴장인지 검사;
	local bExistClub = false;
	for key_PlayerNum, value_PlayerExist in pairs( ClubPlayerList[OuterClubID] ) do
		if ( value_PlayerExist == true ) then
			bExistClub = true;
			break;
		end
	end	
	
	if ( bExistClub == false ) then
		ClubList[OuterClubID] = false;
	end	
end

function EventPartyJoin(nPartyID, bExpedition, nJoinerID, bInSameInstance)
	if ( bInSameInstance == false ) then
		local _PartyMasterID = GetPartyMaster(nPartyID);
		local _PartyClubID = GetClubID(_PartyMasterID);
		if ( ClubList[_PartyClubID] == true ) then
			-- Agent에 메시지를 보내서 입장가능한지를 물어본다;
			SendMessage(MSG_TO_AGENT_BASE, keyMapID, PARAM1_REQ_JOIN, nJoinerID, 0, 0);
		end
	end
end

function EventPartyOut(nPartyID, bExpedition, nOuterID, bInSameInstance)
	local nPartyMemberNum = GetPartyMemberNum(nPartyID);
	local OuterClubID = GetClubID(nOuterID);
	if ( nPartyMemberNum < 2) then
		-- 파티 인원이 2인 이하인 경우 파티원을 전부 내쫒는다.;
		for i = 0, nPartyMemberNum-1, 1 do
			local _memberID = GetPartyMember(nPartyID,i);
			RemoveSkillFact(ACTOR_PC, _memberID, Buff_ID[1], Buff_ID[2]);	-- 2인이하 내쫓을 때 HP버프 삭제;
			DoOut(_memberID);
		end
	end
	-- 파티를 탈퇴할 경우 탈퇴자를 내쫒는다.;
	RemoveSkillFact(ACTOR_PC, nOuterID, Buff_ID[1], Buff_ID[2]);	-- 파티 탈퇴시 HP버프 삭제;
	DoOut(nOuterID);
end
---------------------------------------------------------------------------------------------






----------------------------------------- CDM 맵 이동 처리 --------------------------------------
function EventMapEnter(actorID, mapMainID, mapSubID)
	local _nClubID = GetClubID(actorID);
	if (  _nClubID == nil ) then
        return false;
    end
	
	-- 클럽 초기화;
	if ( ClubList[_nClubID] == nil ) then
		-- 최초 입장 클럽인 경우. 초기화 작업;
		ClubList[_nClubID] = true;
		PartyID[_nClubID] = GetPartyID(actorID);
		ClubPlayerList[_nClubID] = {};		
		
		ClubPoint[_nClubID] = nClub_Start_Take_Point;
		ClubKill[_nClubID] = 0;
		ClubDeath[_nClubID] = 0;
		ClubResurrect[_nClubID] = 0;
		
		-- 각 클럽의 안전 진영과는 서로 아군이다;
		SetRelation( FACTION_SAFE_CLUB, _nClubID, FACTION_CLUB, _nClubID, RELATION_ALLY );
		SetRelation( FACTION_CLUB, _nClubID, FACTION_SAFE_CLUB, _nClubID, RELATION_ALLY );
		
		for key_ClubDbNum, value_ClubExist in pairs( ClubList ) do
			if ( value_ClubExist == true ) then
				if ( key_ClubDbNum ~= _nClubID ) then
					-- 각 클럽은 서로 적이다;
					SetRelation( FACTION_CLUB, _nClubID, FACTION_CLUB, key_ClubDbNum, RELATION_ENEMY );
					SetRelation( FACTION_CLUB, key_ClubDbNum, FACTION_CLUB, _nClubID, RELATION_ENEMY );
					
					-- 다른 클럽의 안전 진영과는 서로 중립적 아군이다;
					SetRelation( FACTION_CLUB, _nClubID, FACTION_SAFE_CLUB, key_ClubDbNum, RELATION_NEUTRAL_ALLY );
					SetRelation( FACTION_SAFE_CLUB, key_ClubDbNum, FACTION_CLUB, _nClubID, RELATION_NEUTRAL_ALLY );
					SetRelation( FACTION_CLUB, key_ClubDbNum, FACTION_SAFE_CLUB, _nClubID, RELATION_NEUTRAL_ALLY );
					SetRelation( FACTION_SAFE_CLUB, _nClubID, FACTION_CLUB, key_ClubDbNum, RELATION_NEUTRAL_ALLY );
					
					-- 안전 진영간에는 서로 중립적 아군이다;
					SetRelation( FACTION_SAFE_CLUB, _nClubID, FACTION_SAFE_CLUB, key_ClubDbNum, RELATION_NEUTRAL_ALLY );
					SetRelation( FACTION_SAFE_CLUB, key_ClubDbNum, FACTION_SAFE_CLUB, _nClubID, RELATION_NEUTRAL_ALLY );
				end
			end
		end
	end
	
	-- 플레이어 초기화;
	if ( ClubPlayerList[_nClubID][actorID] == nil ) then
		-- 최초 입장 플레이어인 경우 초기화 작업;
		PlayerPoint[actorID] = nChar_Start_Take_Point;
		PlayerKillCount[actorID] = 0;
		PlayerDeathCount[actorID] = 0;
		PlayerDamageCount[actorID] = 0;
		PlayerHealCount[actorID] = 0;
		PlayerResurrectCount[actorID] = 0;
		
		ClubPlayerList[_nClubID][actorID] = true;
	elseif ( false == ClubPlayerList[_nClubID][actorID] ) then
		ClubPlayerList[_nClubID][actorID] = true;
	end
	
	-- 안전지대 체류시간이 있었다면 해제;
    if ( PlayerSafeKickTimer[actorID] ~= nil ) then
        RemoveTimer(PlayerSafeKickTimer[actorID]);
        PlayerSafeKickTimer[actorID] = nil;
    end
	
	if ( mapMainID == LobbyMapID[1] ) then
		-- 안전맵으로 이동시. 안전지역강퇴 타이머 등록, 비전투진영에 배치;		
		-- 보상진행중이 아닐 경우에만 안전지역강퇴 기능 동작;
		if ( bUI_CDM_ProgressOff ~= true ) then
			PlayerSafeKickTimer[actorID] = RegistTimer(SafeMap_Kick_Time, TEVENT_KICK_SAFEZONE, ACTOR_PC, actorID, 0);
		end
		
		SetFaction( ACTOR_PC, actorID, FACTION_SAFE_CLUB, _nClubID );
		
	elseif ( mapMainID == BattleMapID[1] ) then 		
		-- 전투 맵으로 이동시. 전투지역 최초입장 무적시간 타이머 등록;
		RegistTimer(Safety_Time, TEVENT_SAFETY_TIME, ACTOR_PC, actorID, 0);
		
		-- 전투 맵에서의 시야거리 조정;
		SetViewRange(actorID, nChar_ViewRange, Battle_Time);
	end

	-- 새로운 맵에 진입시. CDM진행정보 전송, 안전지역강퇴 타이머 삭제;
	if ( DoublePoint[DoubleState] ~= 1 ) then -- 더블포인트 진행시;
		UI_CDM_DoublePoint(actorID, true); -- 더블포인트 UI 표시;
	end		
	if ( bUI_CDM_ProgressOff == true ) then -- 전투 종료시;
		functionResultUI(); -- 전투 결과 UI표시;
	else
		UI_CDM_Progress(actorID, GetLeftTime(CDM_TimerHandle), ClubPoint); -- 전투 진행 UI표시;
	end		
end

--function EventRequestMoveGate(nActorID, nMapMainID, nMapSubID, nGateID, nMainMapIDTarget, nSubMapIDTarget, nGateIDTarget)
	--return true;
--end
-------------------------------------------------------------------------------------------------








----------------------------------------- CDM 타이머 처리 --------------------------------------
function EventTimer(nTimerHandle, nParam0, nParam1, nParam2, nParam3)  -- 타이머가 정한 시간이 되었을때 실행되는 부분;
	if ( nParam0 == TEVENT_KICK_SAFEZONE ) then  -- 안전지대 체류시간이 끝났을 때;
		-- 내부 지역으로 강제 이동;
		SetPositionInstance(ACTOR_PC, nParam2, BattleMapID[1], BattleMapID[2], math.random(-300,200), math.random(-1000,1000));  -- 전장 맵으로 이동;
		
	elseif ( nParam0 == TEVENT_SAFETY_TIME )then  -- 전투 지역입장후 무적시간 경과시;
		-- 해당 클럽 진영으로 배속;
		local _nClubID = GetClubID(nParam2);
		SetFaction( ACTOR_PC, nParam2, FACTION_CLUB, _nClubID );
		ApplySkill(ACTOR_PC, nParam2, Buff_ID[1], Buff_ID[2], 1, 9999 );	-- 전투지역 입장 시 HP버프 적용.
		
	elseif ( nParam0 == TEVENT_SKILL_RESURRECT  )then  -- 스킬 부활 후 n초 경과 시;
		ApplySkill(ACTOR_PC, nParam2, Buff_ID[1], Buff_ID[2], 1, 9999 );	-- 스킬부활 후 HP버프 적용.		

	elseif ( nParam0 == TEVENT_BATTLE_TIME ) then -- CDM 전투 진행이 끝났을 때;
		-- 결과 처리;
		RegistTimer(Reward_Time, TEVENT_REWARD_TIME, 0, 0, 0);
		UI_CDM_BattleEnd();
		UI_CDM_ProgressOffBroad();
		bUI_CDM_ProgressOff = true;
		functionEndBattle();		
	
	elseif ( nParam0 == TEVENT_SHOW_PROGRESS_INFO ) then -- CDM 진행 상황 발송 주기가 되었을 때;
		if ( bUI_CDM_ProgressOff ~= true ) then
			UI_CDM_ProgressBroad(GetLeftTime(CDM_TimerHandle), ClubPoint);
			--RegistTimer(Progress_Show_Timer, TEVENT_SHOW_PROGRESS_INFO, 0, 0, 0);
		end
		
	elseif ( nParam0 == TEVENT_REWARD_TIME ) then -- CDM 보상시간이 끝났을 때;
		-- 인던 파괴;
		DoDestroy();
		
	elseif ( nParam0 == TEVENT_DOUBLE_TIME ) then -- CDM 더블타임;
		if ( DoubleState < MaxDoubleTime) then
			DoubleState = DoubleState + 1;
			RegistTimer(DoubleTime[DoubleState], TEVENT_DOUBLE_TIME, 0, 0, 0);
			if ( DoublePoint[DoubleState] == 1 ) then
				UI_CDM_DoublePointBroad(false);
			else
				UI_CDM_DoublePointBroad(true);
			end
		else
			UI_CDM_DoublePointBroad(false);
		end
	end
end
-------------------------------------------------------------------------------------------------







----------------------------------------- CDM 전투 진행 처리 -------------------------------------------------
function EventDie(nDieActorType, nDieActorID, nKillActorType, nKillActorID)  -- 어떠한 PC,MOB이 죽었을 때 실행되는 부분;
	RemoveSkillFact(nDieActorType, nDieActorID, Buff_ID[1], Buff_ID[2]);					-- 사망 시 HP버프 삭제;
	SetResurrectionForcedInstance(nDieActorID, LobbyMapID[1], LobbyMapID[2], 0, 10, 95);	-- 안전지역 맵에서 강제부활;
	
	if ( ( nDieActorType == ACTOR_PC ) and ( nKillActorType == ACTOR_PC ) ) then -- 플레이어끼리의 PVP일 경우만;
		
		local KillClubID = GetClubID(nKillActorID);
		local DieClubID = GetClubID(nDieActorID);
		local KillActorName = GetName(nKillActorType, nKillActorID);
		local DieActorName = GetName(nDieActorType, nDieActorID);
		local DieClubName = GetClubName(DieClubID);
		local KillClubName = GetClubName(KillClubID);
		
		----------------------- 죽인 사람 처리 -------------------------------		
		ClubPoint[KillClubID] = ClubPoint[KillClubID] + ( PlayerPoint[nDieActorID] * DoublePoint[DoubleState] );
		ClubKill[KillClubID] = ClubKill[KillClubID] + 1;
		PlayerKillCount[nKillActorID] = PlayerKillCount[nKillActorID] + 1;
		if ( PlayerDeathCount[nKillActorID] > 1 ) then
			-- 연속 데스 초기화시;
			PlayerPoint[nKillActorID] = nChar_Start_Take_Point;
			PlayerDeathCount[nKillActorID] = 0;
			PrintChatXMLMsg(nDieActorID, { "RNCDM_SYSTEM_MESSAGE", 8 },COLOR_GREEN);
		else
			PlayerPoint[nKillActorID] = PlayerPoint[nKillActorID] + 1;
		end
		
		local KillFactionType,KillFactionID = GetFaction(nKillActorType, nKillActorID);
		local changePoint = ( PlayerPoint[nDieActorID] * DoublePoint[DoubleState] );
		PrintChatXMLMsgFaction(KillFactionType, KillFactionID, { "RNCDM_SYSTEM_MESSAGE", 1, KillActorName, DieClubName, DieActorName, ClubPoint[KillClubID], changePoint }, COLOR_GREEN);
		
		if ( PlayerKillCount[nKillActorID] > 9 ) then
			-- 출력 조건 : 자신이 연속 10킬 이상시:
			PrintChatXMLMsg(nKillActorID, { "RNCDM_SYSTEM_MESSAGE", 5, PlayerKillCount[nKillActorID] }, COLOR_GREEN);
		elseif ( PlayerKillCount[nKillActorID] > 4 ) then
			-- 출력 조건 : 자신이 연속 킬시;
			PrintChatXMLMsg(nKillActorID, { "RNCDM_SYSTEM_MESSAGE", 10, PlayerKillCount[nKillActorID] }, COLOR_GREEN);			
		elseif ( PlayerKillCount[nKillActorID] > 1 ) then
			-- 출력 조건 : 자신이 연속 킬시;
			PrintChatXMLMsg(nKillActorID, { "RNCDM_SYSTEM_MESSAGE", 3, PlayerKillCount[nKillActorID] }, COLOR_GREEN);
		end
		----------------------------------------------------------------------
		
		----------------------- 죽은 사람 처리 -------------------------------	
		local _nDieClubID = GetClubID(nDieActorID);
		ClubPoint[_nDieClubID] = ClubPoint[_nDieClubID] - nChar_Death_Point;
		ClubDeath[_nDieClubID] = ClubDeath[_nDieClubID] + 1;
		PlayerDeathCount[nDieActorID] = PlayerDeathCount[nDieActorID] + 1;
		if ( ClubPoint[_nDieClubID] < 0 ) then
			ClubPoint[_nDieClubID] = 0;
		end
		
		if ( PlayerKillCount[nDieActorID] > 1 ) then
			-- 연속킬 초기화시;
			PlayerPoint[nDieActorID] = nChar_Start_Take_Point;
			PlayerKillCount[nDieActorID] = 0;
			PrintChatXMLMsg(nDieActorID, { "RNCDM_SYSTEM_MESSAGE", 7 }, COLOR_RED);
		elseif ( PlayerPoint[nDieActorID] > 1 ) then
			PlayerPoint[nDieActorID] = PlayerPoint[nDieActorID] - 1;
		end
		
		local DieFactionType,DieFactionID = GetFaction(nDieActorType, nDieActorID);
		PrintChatXMLMsgFaction(DieFactionType, DieFactionID, { "RNCDM_SYSTEM_MESSAGE", 2, KillClubName, KillActorName, DieActorName, ClubPoint[DieClubID], nChar_Death_Point }, COLOR_RED);
		
		if ( PlayerDeathCount[nDieActorID] > 9 ) then
			-- 출력 조건 : 자신이 연속 10데스 이상시:
			PrintChatXMLMsg(nDieActorID, { "RNCDM_SYSTEM_MESSAGE", 6, PlayerDeathCount[nDieActorID] }, COLOR_RED);
		elseif ( PlayerDeathCount[nDieActorID] > 4 ) then
			-- 출력 조건 : 자신이 연속 데스시;
			PrintChatXMLMsg(nDieActorID, { "RNCDM_SYSTEM_MESSAGE", 9, PlayerDeathCount[nDieActorID] }, COLOR_RED);
		elseif ( PlayerDeathCount[nDieActorID] > 1 ) then
			-- 출력 조건 : 자신이 연속 데스시;
			PrintChatXMLMsg(nDieActorID, { "RNCDM_SYSTEM_MESSAGE", 4, PlayerDeathCount[nDieActorID] }, COLOR_RED);
		end
		----------------------------------------------------------------------
	end
	
	return true;
end

--function EventReceiveDamage(nDamagedActorType, nDamagedActorID, nAttackActorType, nAttackActorID, nDamage, nDamageFlag)  -- 어떠한 PC,MOB이 데미지를 받으면 실행되는 부분;
	--if ( nAttackActorType == ACTOR_PC ) then
		--PlayerDamageCount[nAttackActorID] = PlayerDamageCount[nAttackActorID] + nDamage; -- 데미지량 누적;
	--end
	--UI_CDM_Progress(ClubPoint, CDM_Timer);
	--return true,nDamage;
--end

--function EventReceiveHeal(nReceiveActorType, nReceiveActorID, nHealActorType, nHealActorID, nHeal, nHealFlag)  -- 어떠한 PC,MOB이 회복를 받으면 실행되는 부분;
	--if ( nHealActorType == ACTOR_PC ) then
		--PlayerHealCount[nHealActorID] = PlayerHealCount[nHealActorID] + nHeal; -- 회복량 누적;
	--end
	--return true,nHeal; 
--end

function EventResurrect(nResurrectType, nResurretActorType, nResurrectActorID, nKillActorType, nKillActorID)
	local ResurrectClubID = GetClubID(nResurrectActorID);
	ClubResurrect[ResurrectClubID] = ClubResurrect[ResurrectClubID] + 1;
	--PlayerResurrectCount[nResurrectActorID] = PlayerResurrectCount[nResurrectActorID] + 1; -- 부활 수 누적;
	if ( nResurrectType == EVENT_SYSTEM_RESURRECT ) then	--시스템 부활이면
		return true;
	elseif ( nResurrectType == EVENT_SKILL_RESURRECT ) then		--스킬 부활이면
		RegistTimer(3, TEVENT_SKILL_RESURRECT, nResurretActorType, nResurrectActorID, 0);	-- 부활 후 3초지나면 동작하는 타이머
		return true;
	end
	return true;
end
-------------------------------------------------------------------------------------------------










----------------------------------------- CDM 커스텀 메시지 처리 -------------------------------------------
function EventCustomMessage(nSenderType, nSenderID, nParam1, nParam2, nParam3, nParam4)
    ServerLog("EventCustomMessage field");
    ServerLog("nSenderType : "..tostring(nSenderType));
    ServerLog("nSenderID : "..tostring(nSenderID));
    ServerLog("nParam1 : "..tostring(nParam1));
    ServerLog("nParam2 : "..tostring(nParam2));
    ServerLog("nParam3 : "..tostring(nParam3));
    ServerLog("nParam4 : "..tostring(nParam4));
    ServerLog("------------------------");
	if ( nParam1 == PARAM1_EXIT_BUTTON ) then -- 나가기 버튼 클릭;
		-- CDM 결과 UI에서 나가기 버튼 클릭시 해당 유저를 인던에서 나가게 함;
		RemoveSkillFact(nSenderType, nSenderID, Buff_ID[1], Buff_ID[2]);	-- 나가기 버튼 클릭 시 HP버프 삭제;
		DoOut(nSenderID);
        
    elseif ( nParam1 == PARAM1_JOIN_UI ) then -- 입장요청 UI;
        if ( nSenderType == MSG_TO_AGENT_BASE ) then
            -- CDM 입장 의사를 묻는 UI에서 Yes를 선택했을 때 해당 유저를 입장시킴;
            DoJoin(nParam2);
        end
        
    elseif ( nParam1 == PARAM1_REQ_JOIN ) then
		-- 파티에 가입시켜서 CDM 입장 의사를 묻는 UI 출력;
		UI_XmlTimerMessageBox(nParam2, { "RNCDM_REJOIN_TEXT", 0 },{ "RNCDM_REJOIN_TEXT", 1 },{ "RNCDM_REJOIN_TEXT", 2 }, 30, PARAM1_JOIN_UI, 0); --gameserver
	end
end
-------------------------------------------------------------------------------------------------
















----------------------------------------  CDM 스크립트 내부 함수 -----------------------------------------
function functionEndBattle()
	-- 순위 계산하는 부분;
	functionCalculateRank();
	
	-- 결과 UI 전송;
	functionResultUI();
	
	-- 보상 지급;
	functionReward();
	
	-- CDM 이전기록 저장;
	Save_CDM_History(tStartTime, GetTime_TIME64(), RankList);
end

function functionCalculateRank()
	for key_ClubNum, value_ClubExist in pairs( ClubList ) do
		local rank = 1;
		if ( value_ClubExist == true ) then -- 해당하는 클럽이 있을경우에만 순위 체크;
			local tempPoint = ClubPoint[key_ClubNum];
			
			if ( tempPoint == nil ) then
				ServerLog("ClubList or ClubPoint TableError"); -- 클럽포인트가 없는경우는 있을 수 없다.;
			end
		
			for key_CompareClubNum, value_CompareClubExist in pairs( ClubList ) do
				if ( key_ClubNum ~= key_CompareClubNum ) and ( value_CompareClubExist == true ) then -- 해당하는 클럽이 있을경우에만 순위 비교;
				
					local tempComparePoint = ClubPoint[key_CompareClubNum];
					if ( tempComparePoint == nil ) then
						ServerLog("CompareClubList or CompareClubPoint TableError");  -- 클럽포인트가 없는경우는 있을 수 없다.;
					end
					
					if ( tempPoint < tempComparePoint ) then 	-- 점수 1차 비교;
						rank = rank + 1;
					elseif ( tempPoint == tempComparePoint ) then	-- 만약 점수가 같을 경우 2차로 킬포인트로 순위를 책정;
						local tempKillPoint = ClubKill[key_ClubNum];
						local tempCompareKillPoint = ClubKill[key_CompareClubNum];
					
						if ( tempKillPoint == nil ) or ( tempCompareKillPoint == nil ) then
							ServerLog("KillPoint TableError");  -- 킬포인트가 없는경우는 있을 수 없다.;
						else
							if ( tempKillPoint <= tempCompareKillPoint ) then
								rank = rank + 1;
							end
						end
					end
				end
			end
			RankList[key_ClubNum] = rank;
			
			-- CDM 로그 ( 결과 );
			Log_CDM_Result(key_ClubNum, rank, ClubPoint[key_ClubNum], ClubKill[key_ClubNum], ClubDeath[key_ClubNum]);
		end
	end
end

function functionResultUI()
	UI_CompetitionResult_CDM_Begin();
	for key_ClubNum,value_ClubExist in pairs( ClubList ) do
		if ( value_ClubExist == true ) then
			UI_CompetitionResult_CDM_Info( key_ClubNum, RankList[key_ClubNum], key_ClubNum,
			GetClubName(key_ClubNum), GetClubMasterName(key_ClubNum), ClubPoint[key_ClubNum],
			ClubKill[key_ClubNum], ClubDeath[key_ClubNum], ClubResurrect[key_ClubNum], rewardList[RankList[key_ClubNum]] );
		end
	end
	UI_CompetitionResult_CDM_End(Reward_Time);
end

function functionReward()
	for key_ClubNum, value_ClubExist in pairs( ClubList ) do
		if ( value_ClubExist == true ) then
			for key_PlayerDbNUm, value_PlayerExist in pairs( ClubPlayerList[key_ClubNum] ) do -- 참가 클럽 플레이어 만큼 순회
				if ( value_PlayerExist == true ) then -- 접속중인 플레이어 인지?
					SetFaction(ACTOR_PC, key_PlayerDbNUm, FACTION_SAFE_CLUB, key_ClubNum);  -- 접속중인 플레이어는 모두 동일한 편으로 만들어서 싸우지못하게 함;
				
					local ClubRank = RankList[key_ClubNum];
					if ( ClubRank == nil ) then
						ServerLog("RankList TableError"); -- 랭크 
					else
						ServerLog("CDM Reward Club : "..key_ClubNum..", User : "..key_PlayerDbNUm..", UserRank : "..ClubRank);
						
						local reward = rewardList[ClubRank];
						if ( reward ~= nil ) then
							for rewardKey, reward_value in pairs( rewardList[ClubRank] ) do -- 보상 갯수만큼 순회;
								SendSystemMail( key_PlayerDbNUm, { "SYSTEM_POST_SENDER_NAME", 0 }, { "SYSTEM_POST_SENDER_CONTENTS", 0, RankList[key_ClubNum] }, 0,  reward_value); -- 보상을 메일로 보냄;
							end
						end
					end
				end
			end
		end
	end
end
-------------------------------------------------------------------------------------------------