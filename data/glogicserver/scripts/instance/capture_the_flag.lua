--
-- 주의) 
-- 1) 대/소 문자를 확실하게 구분해야 합니다.
-- 2) true, false 를 확실하게 구분해야 합니다.
-- 3) UCS-2 Little Endian 로 저장해야 합니다.
-------------------------------------------------------------------------------

name = L"CaptureTheFlag"; -- 절대 중복 되어선 안된다;
keyMapMainID = 27; -- 절대 중복 되어선 안된다(subID는 사용하지 않음);
keyMapSubID = 0;
maxPlayer = 100;
maxCreate = 100;
isVisible = true; -- 클라이언트에 보여줄건지;
isUse = true; -- 이 인던을 사용할건지;

publicAgentScript =
{
	L"CaptureTheFlag\\CaptureTheFlag_PublicAgent.lua",
};

publicFieldScript =
{
	L"CaptureTheFlag\\CaptureTheFlag_PublicField.lua",
};

levelAgentScript =
{
	L"CaptureTheFlag\\CaptureTheFlag_LevelAgent.lua",
};

levelFieldScript =
{
	L"CaptureTheFlag\\CaptureTheFlag_LevelField.lua",
};
