--
-- 주의) 
-- 1) 대/소 문자를 확실하게 구분해야 합니다.
-- 2) true, false 를 확실하게 구분해야 합니다.
-- 3) UCS-2 Little Endian 로 저장해야 합니다.
-------------------------------------------------------------------------------
-- 버전 표기는 반드시 있어야 하며, 로직단과 반드시 일치해야 한다;
SCRIPT_VERSION_AGENT = 100;

name = L"ClubDeathMatch"; -- 절대 중복 되어선 안된다;
keyMapMainID = 24 -- 절대 중복 되어선 안된다(subID는 사용하지 않음);
keyMapSubID = 0;
maxPlayer = 100;
maxCreate = 100;
isVisible = true; -- 클라이언트에 보여줄건지;
isUse = true; -- 이 인던을 사용할건지;

publicAgentScript =
{
	L"Tournament\\publicAgent.lua",
};

publicFieldScript =
{
	L"Tournament\\publicField.lua",
};

levelAgentScript =
{
	L"Tournament\\levelAgent.lua",
};

levelFieldScript =
{
	L"Tournament\\levelField.lua",
};
