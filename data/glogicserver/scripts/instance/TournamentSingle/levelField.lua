--
-- 주의) 
-- 1) 대/소 문자를 확실하게 구분해야 합니다.
-- 2) true, false 를 확실하게 구분해야 합니다.
-- 3) UCS-2 Little Endian 로 저장해야 합니다.
-------------------------------------------------------------------------------
-- 버전 표기는 반드시 있어야 하며, 로직단과 반드시 일치해야 한다;
SCRIPT_VERSION = 1.3;

-- 초기화시 저절로 기입되는 변수로 절대 선언 되면 안됩니다.;
--keyMapID = 선언금지 ( 초기화 이후 저절로 세팅 됨 );
--instanceMapID = 선언금지 ( 초기화 이후 저절로 세팅 됨 );

-- ActorType
	ACTOR_PC				= 0;
	ACTOR_NPC				= 1;
	ACTOR_MOB				= 2;
	ACTOR_SUMMON			= 6;
	ACTOR_NULL				= -1;
--------------------------------------------------------------------------------
-- FactionType
	FACTION_TEAM			= 0;
	FACTION_PARTY			= 1;
	FACTION_CLUB			= 2;
	FACTION_EXPEDITION 		= 3;
--------------------------------------------------------------------------------
-- RelationType
	RELATION_ENEMY			= 0;
	RELATION_ALLY			= 1;
	RELATION_NEUTRAL		= 2;
--------------------------------------------------------------------------------
-- CustomMessage Sender Type
    MSG_TO_USER             = 0;    -- 특정 유저;   SenderID : CharDbNum;
    MSG_TO_MY_INSTANCE      = 1;    -- 현재 자신이 속한 필드서버 인던 객체 (LevelField)  SenderID : 의미없음;
    MSG_TO_FIELD_BASE       = 2;    -- 필드서버의 특정 인던 컨텐츠 (PublicField);  SenderID : Instance KeyMap ID;
    MSG_TO_FIELD_INSTANCE   = 3;    -- 필드서버의 특정 인던 객체 (LevelField);  SenderID : Instance Map ID;
    MSG_TO_AGENT_BASE       = 4;    -- 에이전트서버의 특정 인던 컨텐츠 (PublicAgent);  SenderID : Instance KeyMap ID;
    MSG_TO_AGENT_INSTANCE   = 5;    -- 에이전트서버의 특정 인던 객체 (LevelAgent);  SenderID : Instance Map ID;
--------------------------------------------------------------------------------
-- ColorCode
	COLOR_BLACK				= {0,0,0};
	COLOR_WHITE				= {255,255,255};
	COLOR_RED				= {255,0,0};
	COLOR_GREEN				= {0,255,0};
	COLOR_BLUE				= {128,194,235};
--------------------------------------------------------------------------------
-- TIMERCODE
	KICK_SAFEZONE			= 1;
	RESTART_EVENT			= 40;
	MOVE_SAFE_TIME			= 2;
	SHOW_PROCESS			= 11;
	JOIN_SUSPEND			= 12;
	TEST_TIMER				= 20;
	GAME_OVER				= 40;
	
	RESURRECT_TIMER			= 30;
	TEVENT_REWARD_TIME   = 90
	DO_DESTROY_MAP			=100;
--------------------------------------------------------------------------------
-- TOURNAMENT PLAYER TYPE
    TOURNAMENT_PLAYER = 0;
	TOURNAMENT_OBSERVER = 1;
--------------------------------------------------------------------------------
-- MATCHING UI TYPE
	TOURNAMENT_UI_ONE_BY_ONE = 0;
	TOURNAMENT_UI_N_BY_N = 1;
	TOURNAMENT_UI_LEAGE_SCORE = 2;
--------------------------------------------------------------------------------
 	ERESULT_SUCCEED = 0 --       //@ 성공
	ERESULT_FAILED  = 1 --      //@ 실패
	ERESULT_WAITAFTERDEATH =2 -- //@ 죽음 대기
--------------------------------------------------------------------------------
-- CutsomParameter list
	PARAM1_EXIT_BUTTON			= 1;
	PARAM1_JOIN_UI				= 2;
	PARAM1_REQ_JOIN				= 3;
	
	PARAM3_MSGBOX_OK			= 0;
	PARAM3_MSGBOX_CANCEL		= 1;
	PARAM3_MSGBOX_TIMEOVER		= 2;
--------------------------------------------------------------------------------

    EMSKILLFACT_TYPE_NONE					= -1
    EMSKILLFACT_TYPE_NORMAL				= 0
	EMSKILLFACT_TYPE_ADDITIONAL			= 1
    EMSKILLFACT_TYPE_LUNCHBOX			= 2
    EMSKILLFACT_TYPE_SYSTEM				= 3
	EMSKILLFACT_TYPE_EXP_LUNCHBOX	= 4
--------------------------------------------------------------------------------


-- 간편하게 조작 가능한 변수들 --------------------------------------------------
WinCounting = 3;        -- 최종 승리를 위해 필요한 요구 승리 횟수;
GameTimerRole = 180;    -- 1판당 게임 제한 시간;
Reward_Time = 30;       -- 최종 결과 후 강제 나가기 시간;
--------------------------------------------------------------------------------


-- 내부변수로 사용중이거나 변경시 주의가 필요한 변수들로 특수한 상황을 제외하고는 조작을 하지 말 변수들 ------------
TounamentUI = TOURNAMENT_UI_ONE_BY_ONE; -- UI 타입 지정 ( 조작 금지 );
GameTimer = 0; -- 내부변수용 ( 조작 금지 );
GameViewAroundTimer = 30; -- 시야반경을 조작하는데 걸리는 시간 ( 조작 금지 );
Battle_Time = GameTimerRole * ((WinCounting * 2) -1)    -- 최대 가능한 게임 진행시간 ( 조작 금지 );
Canfight = false;  -- 싸울수있는지 없는지 체크하는 플레그 ( 조작 금지 );
UserExsist = false;  -- 유저가 있는지 체크 ( 조작 금지 );
Score = {}  -- 점수 ( 조작 금지 );
DamageTable = {}    -- 데미지 량 ( 조작 금지 );
HealTable = {}  -- 회복량 ( 조작 금지 );
PlayerCount = 1;    -- 플렝이어 숫자 +1 한 숫자 ( 조작 금지 );
IsResultTime = false;   -- 결과시간인지 체크 ( 조작 금지 );
-- 블루팀 인원 ( 조작 금지 );
BLUE_TEAMUSER = {}
BLUE_TEAM_FLAGNUM = 0;
-- 레드팀 인원 ( 조작 금지 );
RED_TEAMUSER = {}
RED_TEAM_FLAGNUM = 1;
-----------------------------------------------------------------------------------------------------------------------

function REFILLFULL(chardbid)
	SetHP(ACTOR_PC,chardbid,GetMaxHP(ACTOR_PC,chardbid))
	SetSP(ACTOR_PC,chardbid,GetMaxSP(ACTOR_PC,chardbid))
	SetMP(ACTOR_PC,chardbid,GetMaxMP(ACTOR_PC,chardbid))		
	ApplySkill(ACTOR_PC,chardbid,54,2,2,180,EMSKILLFACT_TYPE_NORMAL);
end

function ReFillAllTeam()
	for key,value in pairs( BLUE_TEAMUSER ) do
		local position = key;
		--SetGatePosition(value,186,0,1);
		REFILLFULL(value);
	end
	
	for key,value in pairs( RED_TEAMUSER ) do
		local position = key;
		--SetGatePosition(value,186,0,0);
		REFILLFULL(value);	
	end
end

function EventInitialize()
	SetRelation( FACTION_TEAM, 0, FACTION_TEAM, 1, RELATION_ENEMY );
	SetRelation( FACTION_TEAM, 1, FACTION_TEAM, 0, RELATION_ENEMY );
	table.insert(Score,0);
	table.insert(Score,0);
    SetHpVisible(true);
	return true;
end

function EventJoin(actorID, mapMainID, mapSubID) -- 인던 "입장 요청"시 실행되는 부분.
	UserExsist = true;
	
	if PlayerList == nil then
		PlayerList = {}
	end
    
	local EventJoinFactionType ,EventJoinFaction = GetFaction(ACTOR_PC,actorID);
	if(EventJoinFaction == BLUE_TEAM_FLAGNUM) then
		table.insert(BLUE_TEAMUSER,actorID);
		local position = #BLUE_TEAMUSER;
		SetPositionInstance(ACTOR_PC,actorID, 186, 0,  -120,148 ); 
	elseif(EventJoinFaction == RED_TEAM_FLAGNUM) then
		table.insert(RED_TEAMUSER,actorID);
		local position = #RED_TEAMUSER;
		SetPositionInstance(ACTOR_PC,actorID, 186, 0,  120,-148 ); 
	end
	
    -- HP,MP,SP를 100%로 회복시킴;
	SetHP(ACTOR_PC,actorID,GetMaxHP(ACTOR_PC,actorID));
	SetSP(ACTOR_PC,actorID,GetMaxSP(ACTOR_PC,actorID));
	SetMP(ACTOR_PC,actorID,GetMaxMP(ACTOR_PC,actorID));	
end

--EnterCount = 0;
KickSafeAlreadStart = false;
function EventMapEnter(actorID, fromMapMainID, fromMapSubID, toMapMainID, toMapSubID)
	
	SetRelation( FACTION_TEAM, 0, FACTION_TEAM, 1, RELATION_ENEMY );
	SetRelation( FACTION_TEAM, 1, FACTION_TEAM, 0, RELATION_ENEMY );
	
	-- 우승 조건 두번 이겨라
	-- 시간은 아직 정하지 않는다.
	Matching_Tournament_GamInfo_Role(3,0);
	Matching_Tournament_GameTimeLimit(0,false,true);	
	
-- 전투 맵에서의 시야거리 조정;
	SetViewRange(actorID, 800, Battle_Time);
	
	SetMatchingUserOn(TOURNAMENT_PLAYER,actorID);
	ApplySkill(ACTOR_PC,actorID,54,2,2,180*5,EMSKILLFACT_TYPE_NORMAL);
	
	-- 미니 파티유아이를 활용한 대련 UI 사용한다.
	-- SetFcationUI(actorID,1,0,TounamentUI);
	SetFcationUI(actorID,1,0,TounamentUI);
	-- 플레이어 저장.
	PlayerList[PlayerCount] = actorID;
	-- 플레이어 전체 인원저장.
	PlayerCount = PlayerCount + 1;
	
	

end

function EventOffline(actorID)
	-- 토너먼트에서도 끄지만 벡업 플렌으로 인던에서도 끈다.
	Matching_Tournament_OUT(actorID)
	
	if IsResultTime == true then
		return;
	end
	
	if(isSendResualt == true) then
		return;
	end
	
	-- 블루팀 나간사람을 테이블에서 뺀다
	for  key,value in pairs( BLUE_TEAMUSER ) do
		if(actorID == value)then
			table.remove(BLUE_TEAMUSER,key);
			PlayerCount = PlayerCount - 1;
			break;
		end
	end
	-- 레드팀 나간사람을 테이블에서 뺀다
	for  key,value in pairs( RED_TEAMUSER ) do
		if(actorID == value)then
			table.remove(RED_TEAMUSER,key);
			PlayerCount = PlayerCount - 1;
			break;
		end
	end
	
	if #RED_TEAMUSER ~= 0 and #BLUE_TEAMUSER ~= 0 then
		-- 둘다 인원수가 0이 아니라면 계속 게임을 할수가있다.
		return;
	end
	

	
	if #RED_TEAMUSER == 0 then
		ForceFinish( BLUE_TEAM_FLAGNUM )
		return;
	else
		ForceFinish( RED_TEAM_FLAGNUM )
		return;
	end
	
end

function EventOut(actorID)
	-- 토너먼트에서도 끄지만 벡업 플렌으로 인던에서도 끈다.
	Matching_Tournament_OUT(actorID) 
	
	-- 이미 결과를 보낸상태라면 딱히 다른움직임은 없다.
	if(isSendResualt == false) then
		EventOffline(actorID)
	end
end

GameBeginTimer = 0;
GameBeginRole = 10;

GameDebugRole = 10;
GameDebugTimer = 0;

GameWaitJoinRole = 30;
GameWaitJoinTimer = 0;

WaitUser = true;
function EventUpdate(nElapsedTime)
	-- 유저가 없으면 업데이트 의미가 없어서 안쓴다
	if UserExsist == false then
		return
	end
	-- 뷰어라운드 ( 멀리있는 유저를 찾을수있어서 정보값이 정확해진다 )
	GameViewAroundTimer = GameViewAroundTimer- nElapsedTime;
	if ( GameViewAroundTimer < 0 ) then
		GameViewAroundTimer = 30;
		for key,value in pairs( PlayerList ) do
			SetViewRange(PlayerList[key],800,6000);
		end
	end
	
	if IsResultTime == true then
		return;
	end
	
	-- 싸울수있는 시간인지 체크
	if(Canfight == true) then
		if(GameTimer ~= nElapsedTime + GameTimer)then
			GameTimer = nElapsedTime + GameTimer;
			if(GameTimerRole - GameTimer > 0) then
				local blank = false;
				if(GameTimerRole - GameTimer <= 3) then
					blank = true;
				end
				Matching_Tournament_GameTimeLimit(GameTimerRole - GameTimer,blank,true);
			else
				ForceWin();
			end
		end
	end
	
	 
	
	-- 싸울수있는 시간인지 체크
	if(Canfight == false) then
	
		if WaitUser == true then
			if PlayerCount < 2 + 1 then
				GameWaitJoinTimer =  GameWaitJoinTimer + nElapsedTime;
				if GameWaitJoinTimer < GameWaitJoinRole then
					return;
				else
					WaitUser = false;
					--10초동안 안전지대
					if KickSafeAlreadStart == false then
						--KickSafeAlreadStart = true;
						RegistTimer(1, KICK_SAFEZONE, 0, 0, 0);
					end
				end
			else
				WaitUser = false;
				--10초동안 안전지대
				if KickSafeAlreadStart == false then
					--KickSafeAlreadStart = true;
					RegistTimer(1, KICK_SAFEZONE, 0, 0, 0);
				end
			end
		else
			if(GameBeginTimer ~=  GameBeginTimer + nElapsedTime)then
				GameBeginTimer =  GameBeginTimer + nElapsedTime;
				
				for key,value in pairs( PlayerList ) do
					SetViewRange(PlayerList[key],800,6000);
				end
				
				for key,value in pairs( BLUE_TEAMUSER ) do
					ApplySkill(ACTOR_PC,value,54,2,2,180,EMSKILLFACT_TYPE_NORMAL);
				end
				for key,value in pairs( RED_TEAMUSER ) do
					ApplySkill(ACTOR_PC,value,54,2,2,180,EMSKILLFACT_TYPE_NORMAL);
				end
					
				if(GameBeginRole - GameBeginTimer > 0) then
					local blank = false;
					if(GameBeginRole - GameBeginTimer <= 3) then
						blank = true;
					end
					ShowCountingUI(GameBeginRole - GameBeginTimer);
					if GameBeginRole - GameBeginTimer == 0 then
						Matching_Tournament_GameTimeLimit(GameTimerRole-1,false,true);
					else
						Matching_Tournament_GameTimeLimit(GameTimerRole,true,true);
					end
				else
					--ShowCountingUI(0);
					Canfight =true;
					Matching_Tournament_GameTimeLimit(GameTimerRole-1,false,true);
					isSetScore = false
				end
				
			end
		end
	else
		GameBeginTimer = 0;
	end
end

function ForceWin(OutActorID)

	local ConditionFind = false;
	-- 강제 우승
	local winID;
	local LoseID;
	
	local BlueHP = 0;
	local RedHP = 0;
	
	for key,value in pairs( BLUE_TEAMUSER ) do
		BlueHP = BlueHP +  GetHP(ACTOR_PC,value) / GetMaxHP(ACTOR_PC,value);
	end
	BlueHP = BlueHP / #BLUE_TEAMUSER;

	for key,value in pairs( RED_TEAMUSER ) do
		RedHP = RedHP +  GetHP(ACTOR_PC,value) / GetMaxHP(ACTOR_PC,value);	
	end
	RedHP = RedHP / #RED_TEAMUSER;
	
	
	if( BlueHP > RedHP )then
		winID = BLUE_TEAMUSER;
		LoseID = RED_TEAMUSER;
	else
		LoseID = BLUE_TEAMUSER;
		winID = RED_TEAMUSER;
	end
	
	if OutActorID ~= nil then
		for key,value in pairs( BLUE_TEAMUSER ) do
			if value == OutActorID then
				LoseID = BLUE_TEAMUSER;
				winID = RED_TEAMUSER;
				break;
			end
		end

		for key,value in pairs( RED_TEAMUSER ) do
			if value == OutActorID then
				winID = BLUE_TEAMUSER;
				LoseID = RED_TEAMUSER;
				break;
			end
		end		
	end
	
	for key,value in pairs( winID ) do
		if value == OutActorID then
			LoseID = BLUE_TEAMUSER;
			winID = RED_TEAMUSER;
			break;
		end
	end
	
	for key,value in pairs( winID ) do
		if value == OutActorID then
			LoseID = BLUE_TEAMUSER;
			winID = RED_TEAMUSER;
			break;
		end
	end
	local AttackerType;
	local AttackerFaction;
	local DamagedType;
	local DamagedFaction;
		
	for key,value in pairs( winID ) do
		AttackerType ,AttackerFaction = GetFaction(ACTOR_PC,value)
	end
	
	for key,value in pairs( LoseID ) do
		DamagedType, DamagedFaction = GetFaction(ACTOR_PC,value)
	end
	
		
	--local _String = string.format("TIME OUT %d Faction Lose",AttackerFaction);
	--PrintChatMsg(LoseID, _String);
	--local _AString = string.format("TIME OUT %d Faction Win",DamagedFaction);
	--PrintChatMsg(winID, _AString);
	
	if( DamagedFaction == nil) then
		Score[AttackerFaction] = 3;
	end
	
	
	if(Score[AttackerFaction] == nil)then 
		Score[AttackerFaction] = 0;
	end
	
	Score[AttackerFaction] = Score[AttackerFaction] + 1;
	
	if OutActorID ~= nil then
		--Score[AttackerFaction] = Score[AttackerFaction] + 1;
	end
	
	--ForceFinish(AttackerFaction);
	local loseFactionID = AttackerFaction;
	local WinerFactionID = AttackerFaction;
	if AttackerFaction == RED_TEAM_FLAGNUM then
		loseFactionID = BLUE_TEAM_FLAGNUM;
	else
		loseFactionID = RED_TEAM_FLAGNUM;
	end
	forcewinner(loseFactionID,WinerFactionID)
end

function functionResultUI( WinTeamFlag )

	IsResultTime = true;
	Canfight = false;
	UI_CompetitionResult_Tour_Begin();
	local rewardList = {}
	
	if BLUE_TEAM_FLAGNUM == WinTeamFlag then
		local Item = 1;
		for key,value in pairs( BLUE_TEAMUSER ) do
			UI_CompetitionResult_Tour_Info( value, Item, GetSchool(value), GetClass(value), GetLevel(ACTOR_PC,value), GetName(ACTOR_PC,value), Score[BLUE_TEAM_FLAGNUM], DamageTable[value], HealTable[key] );
		end

		Item = Item + 1;
		for key,value in pairs( RED_TEAMUSER ) do
			UI_CompetitionResult_Tour_Info( value, Item, GetSchool(value), GetClass(value), GetLevel(ACTOR_PC,value), GetName(ACTOR_PC,value), Score[RED_TEAM_FLAGNUM], DamageTable[value], HealTable[key] );
		end
	else
		local Item = 1;
		for key,value in pairs( RED_TEAMUSER ) do
			UI_CompetitionResult_Tour_Info( value, Item, GetSchool(value), GetClass(value), GetLevel(ACTOR_PC,value), GetName(ACTOR_PC,value), Score[RED_TEAM_FLAGNUM], DamageTable[value], HealTable[key] );
		end

		Item = Item + 1;
		for key,value in pairs( BLUE_TEAMUSER ) do
			UI_CompetitionResult_Tour_Info( value, Item, GetSchool(value), GetClass(value), GetLevel(ACTOR_PC,value), GetName(ACTOR_PC,value), Score[BLUE_TEAM_FLAGNUM], DamageTable[value], HealTable[key] );
		end
	end
	
	RegistTimer(Reward_Time, TEVENT_REWARD_TIME, WinTeamFlag, 0, 0);
	
	UI_CompetitionResult_Tour_End(Reward_Time);
end


function ForceFinish( FactionID )
	if isSendResualt == false then
		isSendResualt =true
		
		
		functionResultUI(FactionID);
		
		local loseFactionID = FactionID;
		local WinerFactionID = FactionID;
		if WinerFactionID == RED_TEAM_FLAGNUM then
			loseFactionID = BLUE_TEAM_FLAGNUM;
		else
			loseFactionID = RED_TEAM_FLAGNUM;
		end
		
		Matching_Resualt_Send_Start();
		Matching_Faction_Resualt(true,WinerFactionID);
		Matching_Faction_Resualt(false,loseFactionID);
		Matching_Resualt_Send_End();
		
		ReFillAllTeam();
		
	
	end
end

function AllTeamPosisionSetting()

	for key,value in pairs( BLUE_TEAMUSER ) do
		local position = key;
		SetPositionInstance(ACTOR_PC,value, 186, 0,  -120,148 ); 
		--SetGatePosition(value,186,0,1);
	end

	for key,value in pairs( RED_TEAMUSER ) do
		local position = key;
		SetPositionInstance(ACTOR_PC,value, 186, 0,  120,-148 );
		--SetGatePosition(value,186,0,0);
	end

	ReFillAllTeam();
end

function EventTimer(nTimerHandle, nParam0, nParam1, nParam2, nParam3)  
-- 타이머가 정한 시간이 되었을때 실행되는 부분;
	if ( nParam0 == KICK_SAFEZONE ) then  -- 안전지대 체류시간이 끝나서 강제로 전장 이동;
		-- 싸울수있다.
		--Canfight =true;
		-- 한팀이 다 나갔으면 다른 한팀이 승리
		if #BLUE_TEAMUSER == 0 then
			ForceFinish(RED_TEAM_FLAGNUM);
			return;
		elseif #RED_TEAMUSER == 0 then
			ForceFinish(BLUE_TEAM_FLAGNUM);
			return;
		end
		
		--AllTeamPosisionSetting();
		
		SetRelation( FACTION_TEAM, 0, FACTION_TEAM, 1, RELATION_ENEMY );
		SetRelation( FACTION_TEAM, 1, FACTION_TEAM, 0, RELATION_ENEMY );
		
		SetRelation( FACTION_TEAM, 7, FACTION_TEAM, 0, RELATION_ALLY );
		SetRelation( FACTION_TEAM, 7, FACTION_TEAM, 1, RELATION_ALLY );
		
	elseif ( nParam0 == RESTART_EVENT )then  -- 전투 지역입장후 무적시간 경과시;
		--Canfight =true;
		--AllTeamPosisionSetting();
	elseif ( nParam0 == TEVENT_REWARD_TIME ) then
		
		ReFillAllTeam();
		
		for key,value in pairs( BLUE_TEAMUSER ) do
			RemoveSkillFact(ACTOR_PC,value,54,2);
			DoOut(value)			
		end
		
		for key,value in pairs( RED_TEAMUSER ) do
			RemoveSkillFact(ACTOR_PC,value,54,2); 
			DoOut(value) 
		end
		
		RegistTimer(10, DO_DESTROY_MAP, 0, 0, 0);
		
		Matching_Tournament_OUT_ALL(0);
		
	elseif   nParam0 == RESURRECT_TIMER then
		GameViewAroundTimer = -1;
		--if(BLUE_TEAM_FLAGNUM == nParam1 )then
			for key,value in pairs( BLUE_TEAMUSER ) do
				local position = key;
				SetPositionInstance(ACTOR_PC,value, 186, 0,  -120, 148); 
			end
		--else
			for key,value in pairs( RED_TEAMUSER ) do
				local position = key;
				SetPositionInstance(ACTOR_PC,value, 186, 0, 120 -1, -148 ); 
			end
		--end
		ReFillAllTeam();
		
	elseif nParam0 == GAME_OVER then
		ForceFinish(nParam1);
	elseif nParam0 == DO_DESTROY_MAP then
		DoDestroy();
	end
end


isSendResualt = false;

isSetScore = false;

function EventDie(nDieActorType, nDieActorID, nKillActorType, nKillActorID)  -- 어떠한 PC,MOB이 죽었을 때 실행되는 부분;
	if(Score == nil)then
		Score = {};
	end
	--사람이 사람죽인경우
	if ( ( nDieActorType == ACTOR_PC ) and ( nKillActorType == ACTOR_PC ) ) then 
		--죽인놈 펙션
		local AttackerType ,AttackerFaction = GetFaction(nKillActorType,nKillActorID);
		-- 죽은넘 펙션
		local DamagedType, DamagedFaction = GetFaction(nDieActorType,nDieActorID);
		
		--스킬 부활 허용
		SetResurrectionDisable(nDieActorID,false)
		--POWER 사망대기
		--SetStageState(ACTOR_PC,nDieActorID,ERESULT_FAILED,true)
		
		--GetHP
		
		
		local NoHPTeam = -1;
		local NoHP = true;
		for key,value in pairs( BLUE_TEAMUSER ) do
			if GetHP(ACTOR_PC,value) ~= 0 then
				NoHP = false;
			end
		end
		
		if NoHP == true then
			NoHPTeam = BLUE_TEAM_FLAGNUM;
		end
		
		NoHP = true;
		for key,value in pairs( RED_TEAMUSER ) do
			if GetHP(ACTOR_PC,value) ~= 0 then
				NoHP = false;
			end
		end
		
		if NoHP == true then
			NoHPTeam = RED_TEAM_FLAGNUM;
		end
		
		
		if NoHPTeam == -1 then
			return true;
		end
		
		
		
		if(Score[AttackerFaction] == nil)then 
			Score[AttackerFaction] = 0;
		end
		
		if  isSetScore == false then
			Score[AttackerFaction] = Score[AttackerFaction] + 1;
			isSetScore = true;
		end
		
		forcewinner(NoHPTeam,AttackerFaction);
		
		
	end
	
	return true;
end

function forcewinner(NoHPTeam, AttackerFaction)

	if(Score[AttackerFaction] >= WinCounting) then
		
		if NoHPTeam == RED_TEAM_FLAGNUM then
			for key,value in pairs( BLUE_TEAMUSER ) do
				Matching_Tournament_GamInfo_Win(value,TOURNAMENT_PLAYER)
			end
			for key,value in pairs( RED_TEAMUSER ) do
				Matching_Tournament_GamInfo_Lose(value,TOURNAMENT_PLAYER)
				SetResurrectionForcedInstance(value, 186, 0,0,0,100.0);
			end
			--RegistTimer(3, RESURRECT_TIMER, BLUE_TEAM_FLAGNUM, -140, 0);
		else
			for key,value in pairs( BLUE_TEAMUSER ) do
				Matching_Tournament_GamInfo_Lose(value,TOURNAMENT_PLAYER)
			end
			for key,value in pairs( RED_TEAMUSER ) do
				Matching_Tournament_GamInfo_Win(value,TOURNAMENT_PLAYER)
				SetResurrectionForcedInstance(value, 186, 0,0,0,100.0);
			end
			--RegistTimer(3, RESURRECT_TIMER, RED_TEAM_FLAGNUM, 140, 0);
		end
		
		for key,value in pairs( Score ) do
			SendGameTeamInfo(value,key);
		end
		
		
		ForceFinish(AttackerFaction);
		
			
		
		--RegistTimer(3, GAME_OVER, AttackerFaction,0,0);
	else
		
		for key,value in pairs( BLUE_TEAMUSER ) do
			local position = key;
			if GetHP(ACTOR_PC,value) == 0 then
				SetResurrectionForcedInstance(value, 186, 0,0,3,100.0);
			end
		end

		for key,value in pairs( RED_TEAMUSER ) do
			local position = key;
			if GetHP(ACTOR_PC,value) == 0 then
				SetResurrectionForcedInstance(value, 186, 0,1,3,100.0);
			end
		end


		-- 이겼으면 이겼다고 UI 띄워주기
		-- 졌으면 졌다고 UI 띄워주기	
		if NoHPTeam == RED_TEAM_FLAGNUM then
			for key,value in pairs( BLUE_TEAMUSER ) do
				Matching_Tournament_GamInfo_Win(value,TOURNAMENT_PLAYER)
			end
			for key,value in pairs( RED_TEAMUSER ) do
				Matching_Tournament_GamInfo_Lose(value,TOURNAMENT_PLAYER)
			end
			RegistTimer(3, RESURRECT_TIMER, BLUE_TEAM_FLAGNUM, -120, 0);
		else
			for key,value in pairs( BLUE_TEAMUSER ) do
				Matching_Tournament_GamInfo_Lose(value,TOURNAMENT_PLAYER)
			end
			for key,value in pairs( RED_TEAMUSER ) do
				Matching_Tournament_GamInfo_Win(value,TOURNAMENT_PLAYER)
			end
			RegistTimer(3, RESURRECT_TIMER, RED_TEAM_FLAGNUM, 120, 0);
		end
		
		Canfight = false;
		GameTimer= 0
		GameBeginTimer = 0;
		
		for key,value in pairs( Score ) do
			SendGameTeamInfo(value,key);
		end
	end
	
end

function EventReceiveDamage(nDamagedActorType, nDamagedActorID, nAttackActorType, nAttackActorID, nDamage, nDamageFlag)  -- 어떠한 PC,MOB이 데미지를 받으면 실행되는 부분;
	if( DamageTable[nAttackActorID] == nil )then
		DamageTable[nAttackActorID] = 0;
	end
		
	DamageTable[nAttackActorID] = DamageTable[nAttackActorID]  + nDamage;

	if( Canfight == false)then
		return false,0;
	end
	
	return true,nDamage;
end

function EventResurrect(nResurrectType, nResurretActorType, nResurrectActorID, nKillActorType, nKillActorID)
	--AllTeamPosisionSetting();
	ApplySkill(ACTOR_PC,nResurrectActorID,54,2,2,180*5,EMSKILLFACT_TYPE_NORMAL);
	return true;
end

function EventReceiveHeal(nReceiveActorType, nReceiveActorID, nHealActorType, nHealActorID, nHeal, nHealFlag)  -- 어떠한 PC,MOB이 회복를 받으면 실행되는 부분;
	if( HealTable[nHealActorID] == nil )then
		HealTable[nHealActorID] = 0;
	end
		
	HealTable[nHealActorID] = HealTable[nHealActorID]  + nHeal;	
	return true,nHeal; 
end

function EventCustomMessage(nSenderType, nSenderID, nParam1, nParam2, nParam3, nParam4)
	if ( nParam1 == PARAM1_EXIT_BUTTON ) then -- 나가기 버튼 클릭;		
	
		for key,value in pairs( BLUE_TEAMUSER ) do
			RemoveSkillFact(ACTOR_PC,value,54,2); 
		end
		
		for key,value in pairs( RED_TEAMUSER ) do
			RemoveSkillFact(ACTOR_PC,value,54,2); 
		end
		
		SetVisible(ACTOR_PC,nSenderID,true);
		Matching_Tournament_OUT(nSenderID)
		REFILLFULL(nSenderID)
		
		DoOut(nSenderID);
	end
end


function EventPartyJoin(nPartyID, bExpedition, nJoinerID, bInSameInstance)
	for  key,value in pairs( BLUE_TEAMUSER ) do
		if(nJoinerID == value)then
			ForceFinish( RED_TEAM_FLAGNUM )
			break;
		end
	end
	for  key,value in pairs( RED_TEAMUSER ) do
		if(nJoinerID == value)then
			ForceFinish( BLUE_TEAM_FLAGNUM )
			break;
		end
	end
end

function EventPartyOut(nPartyID, bExpedition, nOuterID, bInSameInstance)
	for  key,value in pairs( BLUE_TEAMUSER ) do
		if(nOuterID == value)then
			ForceFinish( RED_TEAM_FLAGNUM )
			break;
		end
	end
	for  key,value in pairs( RED_TEAMUSER ) do
		if(nOuterID == value)then
			ForceFinish( BLUE_TEAM_FLAGNUM )
			break;
		end
	end
end