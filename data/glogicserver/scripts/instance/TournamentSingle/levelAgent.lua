--
-- 주의) 
-- 1) 대/소 문자를 확실하게 구분해야 합니다.
-- 2) true, false 를 확실하게 구분해야 합니다.
-- 3) UCS-2 Little Endian 로 저장해야 합니다.
-------------------------------------------------------------------------------
-- 버전 표기는 반드시 있어야 하며, 로직단과 반드시 일치해야 한다;
SCRIPT_VERSION = 1.3;

-- 초기화시 저절로 기입되는 변수로 절대 선언 되면 안됩니다.;
--keyMapID = 선언금지 ( 초기화 이후 저절로 세팅 됨 );
--instanceMapID = 선언금지 ( 초기화 이후 저절로 세팅 됨 );

-- emAuthority
AUTHRITY_NOMAL          = 0;    -- 일반적인 입장;
AUTHRITY_NOMAL_FORCED   = 1;    -- 시스템이 입장시킨 일반적인 입장;
AUTHRITY_GM             = 2;    -- GM명령어로 입장;
AUTHRITY_OBSERVER       = 3;    -- 관전모드로 입장;
-------------------
-- ColorCode
	COLOR_BLACK				= {0,0,0};
	COLOR_WHITE				= {255,255,255};
	COLOR_RED				= {255,0,0};
	COLOR_GREEN				= {0,255,0};
	COLOR_BLUE				= {128,194,235};
--------------------------------------------------------------------------------

function luaAgentRequestJoin(nPlayerDbNum, emAuthority)
    if ( emAuthority == AUTHRITY_OBSERVER ) then
        if ( IsJoinGameofALL(nPlayerDbNum) == true ) or ( IsJoinOtherContents(nPlayerDbNum) == true ) then
            PrintChatXmlMsg(nPlayerDbNum,{"TOURNAMENT_SYSTEM_OBSERVE", 1},COLOR_BLUE);
            return false;
        end
        PrintChatXmlMsg(nPlayerDbNum,{"TOURNAMENT_SYSTEM_OBSERVE", 2,ID2GAMEINTEXT("TOURNAMENT_PVP_SINGLE_MAPNAME")},COLOR_BLUE);
    end
    return true;
end

function luaAgentRequestEntry(nPlayerDbNum, bRequestByGate)
	return true;
end