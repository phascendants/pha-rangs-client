--
-- 주의) 
-- 1) 대/소 문자를 확실하게 구분해야 합니다.
-- 2) true, false 를 확실하게 구분해야 합니다.
-- 3) UCS-2 Little Endian 로 저장해야 합니다.
-------------------------------------------------------------------------------
-- 버전 표기는 반드시 있어야 하며, 로직단과 반드시 일치해야 한다;
SCRIPT_VERSION = 
{
    "AlphaServer",     -- main version
    0,    -- sub version
};

-- 초기화시 저절로 기입되는 변수로 절대 선언 되면 안됩니다.;
--keyMapID = 선언금지 ( 초기화 이후 저절로 세팅 됨 );

-- ActorType
	ACTOR_PC				= 0;
	ACTOR_NPC				= 1;
	ACTOR_MOB				= 2;
	ACTOR_SUMMON			= 6;
	ACTOR_NULL				= -1;
--------------------------------------------------------------------------------	
	-- ColorCode
	COLOR_BLACK				= {0,0,0};
	COLOR_WHITE				= {255,255,255};
	COLOR_RED				= {255,0,0};
	COLOR_GREEN				= {0,255,0};
	COLOR_BLUE				= {128,194,235};
	COLOR_MAGENTA			= {240,100,170}; 
	COLOR_YELLOW				= {255,200,0};
--------------------------------------------------------------------------------
--[[
function luaFieldRequestJoin2(nPlayerDbNum, bRequestByGate)

	-- 파티원들의 입장 조건 검사;
	-- 파티원중 입장 불가 인원이 있는가?
	local _partyMaster = GetPartyMaster(_partyID);
	if ( checkRequire(_partyMember) == false ) then
		return false;
	end

	return true;
end
--]]

function luaFieldRequestJoin(CharDbNum, bRequestByGate )
	local _Itemcount = GetItemCount (CharDbNum,701,462);
	if (_Itemcount ~= nil) and (_Itemcount >= 1 ) then 
		return true;		
	end
	return false;
end