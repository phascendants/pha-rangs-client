--
-- 주의) 
-- 1) 대/소 문자를 확실하게 구분해야 합니다.
-- 2) true, false 를 확실하게 구분해야 합니다.
-- 3) UCS-2 Little Endian 로 저장해야 합니다.
-------------------------------------------------------------------------------
-- 버전 표기는 반드시 있어야 하며, 로직단과 반드시 일치해야 한다;
SCRIPT_VERSION = 
{
    "AlphaServer",     -- main version
    0,    -- sub version
};

-- 초기화시 저절로 기입되는 변수로 절대 선언 되면 안됩니다.;
--keyMapID = 선언금지 ( 초기화 이후 저절로 세팅 됨 );
--instanceMapID = 선언금지 ( 초기화 이후 저절로 세팅 됨 );

-- ActorType
	ACTOR_PC				= 0;
	ACTOR_NPC				= 1;
	ACTOR_MOB				= 2;
	ACTOR_SUMMON			= 6;
	ACTOR_NULL				= -1;
--------------------------------------------------------------------------------
-- FactionType
	FACTION_TEAM			= 0;
	FACTION_PARTY			= 1;
	FACTION_CLUB			= 2;
	FACTION_EXPEDITION 	= 3;
	FACTION_CLUB_SAFE	= 4;
--------------------------------------------------------------------------------
-- RelationType
	RELATION_ENEMY			= 0;
	RELATION_ALLY			= 1;
	RELATION_NEUTRAL		= 2;
--------------------------------------------------------------------------------
-- TriggerType
	TRIGGER_PC				= 1;
	TRIGGER_MOB				= 2;
	TRIGGER_SUMMON			= 4;
	
	TRIGGER_PC_MOB		    = 3;	
	TRIGGER_PC_SUMMON       = 5;
--------------------------------------------------------------------------------
	PARAM3_MSGBOX_OK		= 0;
	PARAM3_MSGBOX_CANCEL	= 1;
	PARAM3_MSGBOX_TIMEOVER	= 2;
--------------------------------------------------------------------------------
-- ColorCode
	COLOR_BLACK				= {0,0,0};
	COLOR_WHITE				= {255,255,255};
	COLOR_RED				= {255,0,0};
	COLOR_GREEN				= {0,255,0};
	COLOR_BLUE				= {128,194,235};
	COLOR_MAGENTA			= {240,100,170}; 
	COLOR_YELLOW			= {255,200,0};
--------------------------------------------------------------------------------

---이벤트 변수 모음----
EventMasterKeyMapID = 308;

Boss_CROW_ID = 	{89, 66};    		-- 몬스터 데이터 mid,sid; (캐릭터 레벨 30 ~ 100)
Boss_CROW2_ID = {89, 67};    		-- 몬스터 데이터 mid,sid; (캐릭터 레벨 101 ~ 130)
Boss_CROW3_ID = {89, 68};    		-- 몬스터 데이터 mid,sid; (캐릭터 레벨 131 ~ 160)
Boss_CROW4_ID = {89, 69};    		-- 몬스터 데이터 mid,sid; (캐릭터 레벨 161 ~ 180)
Boss_CROW5_ID = {89, 70};    		-- 몬스터 데이터 mid,sid; (캐릭터 레벨 181 ~ 210)
Boss_CROW6_ID = {89, 71};    		-- 몬스터 데이터 mid,sid; (캐릭터 레벨 211 ~ 260)
Boss_CROW7_ID = {89, 64};    		-- NPC 데이터 mid,sid; (모든 캐릭터 레벨)


Boss_Pos =  {0, 0};     		-- 봉인 몬스터 위치;
Boss1_ActorID = {};				-- 봉인 몬스터1 ID
Char_level = nil;				-- 캐릭터 레벨

---시간 관련 변수---
Game_Start = 18;		-- 게임 시작
Game_Play = 20;			-- 게임 진행
Game_Reward = 22;		-- 게임 성공
Game_Destroy = 24;		-- 게임 종료 (인던 파괴)
Game_CountDown = 26;	-- 게임 남은시간 알림
Boss_Live = 27;			-- 보스 소환
Kick_user = 28;			-- 유저 이동

-- 시간 --
Play_Time = 120;		-- 게임 진행 시간
Reward_Time = 10;		-- 게임 성공 시간
CountDownTime = 120;		-- 남은시간 표시 시간
NOTIFY_GAME_END_TIME = {100,80,60,40,20,10};   -- 게임 종료 알림 주기;


-- 맵 ID 관련 --
BattleMapID = { 308, 0 };			-- 이벤트 던전 MapID;

IsSummon_Boss = false;			-- 보스 소환 여부
IsSummon_nomore = false;		-- 보스 소환 중지 여부

---------------------------------

-- 초기화 ( 인던 생성 직후 );
function EventInitialize()
    return true;
end

-- 입장 요청 발생시
function EventRequestJoin(actorID)
	return true;
end

-- 유저 입장시
function EventJoin(actorID)
	return true;
end

function EventRequestJoinGM(actorID)
	return false; -- GM 명령어로 들어온 경우 무조건 허용;
end

-- 실제 맵에 진입할 때 실행되는 부분
function EventMapEnter(actorID, mapMid, mapSid)

		PrintSimpleXMLMsgAll(5, {"2015HALLOWEEN_SYSTEM_MESSAGE", 0}, COLOR_WHITE);	-- 화면중단 메시지 출력 2015HALLOWEEN_SYSTEM_MESSAGE, 0 -- 대단하군! 여길 찾아 들어오다니! 쫓아내주지! 
		PrintChatXMLMsgAll({"2015HALLOWEEN_SYSTEM_MESSAGE", 0}, COLOR_BLUE);	-- 채팅 메시지 출력 2015HALLOWEEN_SYSTEM_MESSAGE, 0 -- 대단하군! 여길 찾아 들어오다니! 쫓아내주지! 
		RemoveItem (actorID, 701, 462, 1); -- 입장권(1) 아이템을 삭제 한다. 
	
		Char_level = GetLevel(ACTOR_PC, actorID);		
		RegistTimer(5,Game_Play,actorID);
end

-- 인던을 나갈 때 실행되는 부분
function EventOut(actorID)
			DoDestroy();	-- 인던 삭제
		return true;
end


-- 게이트를 통한 이동 시 실행되는 부분
function EventRequestMoveGate(nActorID, nMapMainID, nMapSubID, nGateID, nMainMapIDTarget, nSubMapIDTarget, nGateIDTarget)
	SetGatePosition(nActorID, nMainMapIDTarget, nSubMapIDTarget, nGateIDTarget);
	return true;
end


-- 타이머가 정한 시간이 되었을때 실행되는 부분;
function EventTimer(nTimerHandle, nParam0, nParam1, nParam2, nParam3)
	if (nParam0 == Game_Play) then -- 게임 진행 중
		PrintSimpleXMLMsgAll(5, {"2015HALLOWEEN_SYSTEM_MESSAGE", 1}, COLOR_WHITE);	-- 화면중단 메시지 출력 2015HALLOWEEN_SYSTEM_MESSAGE, 1 -- 호박은 랜덤장소에 나타난다구! 잘 찾아봐!
		PrintChatXMLMsgAll({"2015HALLOWEEN_SYSTEM_MESSAGE", 1}, COLOR_BLUE);	-- 채팅 메시지 출력 2015HALLOWEEN_SYSTEM_MESSAGE, 1 -- 호박은 랜덤장소에 나타난다구! 잘 찾아봐!
		
		RegistTimer (Play_Time, Game_Reward,nParam1);
		-- 10초 카운트 다운 시간 알림 (게임 종료 알림 메시지 등록);
			for key,value in pairs( NOTIFY_GAME_END_TIME ) do
				RegistTimer(Play_Time - (value), Game_CountDown, value);
			end			
		RegistTimer (0,Boss_Live);	
		
	elseif ( nParam0 == Game_CountDown ) then
		PrintSimpleXMLMsgAll(5, {"2015HALLOWEEN_SYSTEM_MESSAGE", 2,nParam1}, COLOR_WHITE);	-- 화면중단 메시지 출력 2015HALLOWEEN_SYSTEM_MESSAGE, 2 -- 널 쫓아 내기까지 %1%초 남았지! 하하하!
		PrintChatXMLMsgAll({"2015HALLOWEEN_SYSTEM_MESSAGE", 2,nParam1}, COLOR_BLUE);	-- 채팅 메시지 출력 2015HALLOWEEN_SYSTEM_MESSAGE, 2 -- 널 쫓아 내기까지 %1%초 남았지! 하하하!			

			
	elseif (nParam0 == Boss_Live) then -- 보스 소환
		if(BattleMapID[1] == 308) then

			local _bossID;
			if (Char_level >= 30) and (Char_level <= 100) then -- 캐릭터 레벨 30 ~ 100
					local _randomdirection_x = math.random(-200,200);			
					local _randomdirection_z = math.random (-200,200);
				if( IsSummon_Boss == false ) and (IsSummon_nomore == false) then
					_bossID = DropCrow(BattleMapID[1], BattleMapID[2], Boss_CROW_ID[1], Boss_CROW_ID[2],Boss_Pos[1]+_randomdirection_x, Boss_Pos[2]+_randomdirection_z); -- 몬스터 위치 시킴;
					Boss1_ActorID[_bossID] = _bossID;
				IsSummon_Boss = true;
				end
				
			elseif (Char_level >= 101) and (Char_level <= 130) then -- 캐릭터 레벨 101 ~ 130
					local _randomdirection_x = math.random(-200,200);			
					local _randomdirection_z = math.random (-200,200);
				if( IsSummon_Boss == false ) and (IsSummon_nomore == false) then
					_bossID = DropCrow(BattleMapID[1], BattleMapID[2], Boss_CROW2_ID[1], Boss_CROW2_ID[2],Boss_Pos[1]+_randomdirection_x, Boss_Pos[2]+_randomdirection_z); -- 몬스터 위치 시킴;
					Boss1_ActorID[_bossID] = _bossID;					
					IsSummon_Boss = true;
				end
				
			elseif (Char_level >= 131) and (Char_level <= 160) then -- 캐릭터 레벨 131 ~ 160
					local _randomdirection_x = math.random(-200,200);			
					local _randomdirection_z = math.random (-200,200);
				if( IsSummon_Boss == false ) and (IsSummon_nomore == false) then
					_bossID = DropCrow(BattleMapID[1], BattleMapID[2], Boss_CROW3_ID[1], Boss_CROW3_ID[2],Boss_Pos[1]+_randomdirection_x, Boss_Pos[2]+_randomdirection_z); -- 몬스터 위치 시킴;
					Boss1_ActorID[_bossID] = _bossID;
					IsSummon_Boss = true;
				end
				
			elseif (Char_level >= 161) and (Char_level <= 180) then -- 캐릭터 레벨 161 ~ 180
					local _randomdirection_x = math.random(-200,200);			
					local _randomdirection_z = math.random (-200,200);
				if( IsSummon_Boss == false ) and (IsSummon_nomore == false) then
					_bossID = DropCrow(BattleMapID[1], BattleMapID[2], Boss_CROW4_ID[1], Boss_CROW4_ID[2],Boss_Pos[1]+_randomdirection_x, Boss_Pos[2]+_randomdirection_z); -- 몬스터 위치 시킴;
					Boss1_ActorID[_bossID] = _bossID;
					IsSummon_Boss = true;
				end
				
			elseif (Char_level >= 181) and (Char_level <= 210) then -- 캐릭터 레벨 181 ~ 210
					local _randomdirection_x = math.random(-200,200);			
					local _randomdirection_z = math.random (-200,200);
				if( IsSummon_Boss == false ) and (IsSummon_nomore == false) then
					_bossID = DropCrow(BattleMapID[1], BattleMapID[2], Boss_CROW5_ID[1], Boss_CROW5_ID[2],Boss_Pos[1]+_randomdirection_x, Boss_Pos[2]+_randomdirection_z); -- 몬스터 위치 시킴;
					Boss1_ActorID[_bossID] = _bossID;
					IsSummon_Boss = true;
				end
				
			elseif (Char_level >= 211) and (Char_level <= 260) then -- 캐릭터 레벨 211 ~ 260
					local _randomdirection_x = math.random(-200,200);			
					local _randomdirection_z = math.random (-200,200);			
				if( IsSummon_Boss == false ) and (IsSummon_nomore == false) then
					_bossID = DropCrow(BattleMapID[1], BattleMapID[2], Boss_CROW6_ID[1], Boss_CROW6_ID[2],Boss_Pos[1]+_randomdirection_x, Boss_Pos[2]+_randomdirection_z); -- 몬스터 위치 시킴;
					Boss1_ActorID[_bossID] = _bossID;
					IsSummon_Boss = true;
				end
			end	
		end	

	elseif (nParam0 == Game_Reward) then -- 게임 성공 시간
		PrintSimpleXMLMsgAll(10, {"2015HALLOWEEN_SYSTEM_MESSAGE", 3}, COLOR_WHITE);	-- 화면중단 메시지 출력 2015HALLOWEEN_SYSTEM_MESSAGE, 3 -- 하하하! 너도 여기까지다! 또 찾아와보시지!
		PrintChatXMLMsgAll({"2015HALLOWEEN_SYSTEM_MESSAGE", 3}, COLOR_BLUE);	-- 채팅 메시지 출력 2015HALLOWEEN_SYSTEM_MESSAGE, 3 -- 하하하! 너도 여기까지다! 또 찾아와보시지!
		IsSummon_nomore = true;	
		
		local _bossID;
			_bossID = DropCrow(BattleMapID[1], BattleMapID[2], Boss_CROW7_ID[1], Boss_CROW7_ID[2],Boss_Pos[1],Boss_Pos[2]); -- NPC 위치 시킴;
		
		RegistTimer(Reward_Time,Kick_user,nParam1);

	elseif (nParam0 == Kick_user) then -- 인던 파괴	
		SetGatePosition  (nParam1,22,0,0); 

		RegistTimer(3,Game_Destroy);
	
	elseif (nParam0 == Game_Destroy) then -- 인던 파괴	
		DoDestroy();	-- 인던 삭제
	end
end


-- 어떠한 PC,MOB이 데미지를 받으면 실행되는 부분;
function EventReceiveDamage(nDamagedActorType, nDamagedActorID, nAttackActorType, nAttackActorID, nDamage, nDamageFlag)
	return true;
end


-- 어떠한 PC,MOB이 죽었을 때 실행되는 부분;
function EventDie(nDieActorType, nDieActorID, nKillActorType, nKillActorID)
	if (nDieActorID == Boss1_ActorID [nDieActorID]) then -- 몬스터 1이 죽었을 때
		PrintSimpleXMLMsgAll(2, {"2015HALLOWEEN_SYSTEM_MESSAGE", 4}, COLOR_WHITE);	-- 화면중단 메시지 출력 2015HALLOWEEN_SYSTEM_MESSAGE, 4 -- 아얏! 내 호박~~!! 
		PrintChatXMLMsgAll({"2015HALLOWEEN_SYSTEM_MESSAGE", 4}, COLOR_RED);	-- 채팅 메시지 출력 2015HALLOWEEN_SYSTEM_MESSAGE, 4 -- 아얏! 내 호박~~!! 
	
		IsSummon_Boss = false;

		if( IsSummon_Boss == false ) then
			RegistTimer(0,Boss_Live);
		end
		return true;
	end	
end

--[[
-- 부활 이벤트가 발생할 때 실행되는 부분
function EventResurrect(nResurrectType, nResurretActorType, nResurrectActorID, nKillActorType, nKillActorID)
end

--function EventActorTriggerIn(nTriggerType, nTriggerID, nInnerType, nInnerID)
--end

--function EventActorTriggerOut(nTriggerType, nTriggerID, nOuterType, nOuterID)
--end

-- 클릭 트리거를 클릭했을 때 실행되는 부분
function EventClickTrigger(nTriggerType, nTriggerID, nClickerType, nClickerID)
end

-- 모션이 종료될 때 실행되는 부분
function EventMotionFinish(nActorType, nActorID, nMotionMid, nMotionSid, nMotionRemainSec)
end

function EventPartyJoin(nPartyID, bExpedition, nJoinerID, bInSameInstance)
end

function EventCustomMessage(nSenderType, nSenderID, nParam1, nParam2, nParam3, nParam4)
end
 
function EventUseItem(CharDbNum, ItemMID, ItemSID, nParam, fParam) -- 유저가 어떠한 Item을 사용하였을때 실행되는 부분;
	return true;
end
      
function EventRequestMoveGate(nActorID, nMapMainID, nMapSubID, nGateID, nMainMapIDTarget, nSubMapIDTarget, nGateIDTarget)
	return true;
end

function EventReceiveHeal(nReceiveActorType, nReceiveActorID, nHealActorType, nHealActorID, nHeal, nHealFlag)  -- 어떠한 PC,MOB이 회복를 받으면 실행되는 부분;
	return true,nHeal; 
end

function EventPartyChangeMaster(nPartyID, bExpedition, nNewMasterID, bInMapNewMaster, nOldMaster, bInMapOldMaster)
end

function EventPartyOut(nPartyID, bExpedition, nOuterID, bInSameInstance)
end
]]--