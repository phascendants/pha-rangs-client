--
-- 주의) 
-- 1) 대/소 문자를 확실하게 구분해야 합니다.
-- 2) true, false 를 확실하게 구분해야 합니다.
-- 3) UCS-2 Little Endian 로 저장해야 합니다.
---------------------
----------------------------------------------------------
-- 버전 표기는 반드시 있어야 하며, 로직단과 반드시 일치해야 한다;
SCRIPT_VERSION = 
{
    "AlphaServer",     -- main version
    0,    -- sub version
};

-- 초기화시 저절로 기입되는 변수로 절대 선언 되면 안됩니다.;
--keyMapID = 선언금지 ( 초기화 이후 저절로 세팅 됨 );
--------------------------------------------------------------------------------
-- ColorCode
	COLOR_BLACK				= {0,0,0};
	COLOR_WHITE				= {255,255,255};
	COLOR_RED				= {255,0,0};
	COLOR_GREEN				= {0,255,0};
	COLOR_BLUE				= {128,194,235};
-------------------------------------------------------------------------------
-- CustomMessage Sender Type
    MSG_TO_USER             = 0;    -- 특정 유저;   SenderID : CharDbNum;
    MSG_TO_MY_INSTANCE      = 1;    -- 현재 자신이 속한 필드서버 인던 객체 (LevelField)  SenderID : 의미없음;
    MSG_TO_FIELD_BASE       = 2;    -- 필드서버의 특정 인던 컨텐츠 (PublicField);  SenderID : Instance KeyMap ID;
    MSG_TO_FIELD_INSTANCE   = 3;    -- 필드서버의 특정 인던 객체 (LevelField);  SenderID : Instance Map ID;
    MSG_TO_AGENT_BASE       = 4;    -- 에이전트서버의 특정 인던 컨텐츠 (PublicAgent);  SenderID : Instance KeyMap ID;
    MSG_TO_AGENT_INSTANCE   = 5;    -- 에이전트서버의 특정 인던 객체 (LevelAgent);  SenderID : Instance Map ID;
--------------------------------------------------------------------------------
-- CutsomParameter list
PARAM1_EXIT_BUTTON          = 200;
PARAM1_EVENT_JOIN           = 201;
PARAM1_EVENT_OUT            = 202;
PARAM1_EVENT_CREATEMAP      = 203;
PARAM1_MEMBER_NUM           = 204;
PARAM1_EVENT_SET_GUIDANCE  = 205;
EVENT_SKILL_RESURRECT = 8;
EVENT_SYSTEM_RESURRECT = 16;
PARAM1_BATTLE_OPEN_NOTIFY   = 206;
PARAM1_BATTLE_OPEN          = 207;
PARAM1_BATTLE_CLOSE         = 208;
--------------------------------------------------------------------------------
EventMasterKeyMapID = 308;

InstanceMapIDTable = {};
PartyMemberNum = {};

function luaAgentEventInitialize()
end

--function luaAgentEventTimer(nTimerHandle, nParam1, nParam2, nParam3, nParam4)
--end

function luaAgentEventAbsTimer(nTimerHandle, nParam1, nParam2, nParam3, nParam4)
end

function luaAgentRequestCreate(nPlayerDbNum, bRequestByGate)
	local PartyID = GetPartyID(nPlayerDbNum);
	if ( PartyID ~= nil ) then
		return false;
	end 
	
	return true;
end

function luaAgentRequestMapID(nPlayerDbNum)
	return 308,0;
end

function luaAgentEventCustomMessage(nSenderType, nSenderID, Param1, Param2, Param3, Param4)
	if ( Param1 == PARAM1_EVENT_CREATEMAP ) then		-- 맵 생성
        table.insert(InstanceMapIDTable, Param2);
	
--[[
	elseif ( Param1 == PARAM1_EVENT_JOIN ) then		-- 유저 입장 시 파티멤버 체크
        if ( PartyMemberNum[Param2] == nil ) then
            PartyMemberNum[Param2] = 1;
        else 
            PartyMemberNum[Param2] = PartyMemberNum[Param2] + 1;
        end
        
		ServerLog("PartyMemberNum : "..tostring(PartyMemberNum[Param2]));
		
        --for key, value in pairs(InstanceMapIDTable) do
            SendMessage(MSG_TO_FIELD_INSTANCE, EventMasterKeyMapID, PARAM1_MEMBER_NUM, Param2, PartyMemberNum[Param2]);
        --end        
        
    elseif ( Param1 == PARAM1_EVENT_OUT ) then		-- 유저 이탈 시 파티멤버 체크
        if ( PartyMemberNum[Param2] ~= nil ) then
            PartyMemberNum[Param2] = PartyMemberNum[Param2] - 1;
        end
        
		ServerLog("PartyMemberNumPublic : "..tostring(PartyMemberNum[Param2]));
		
        --for key, value in pairs(InstanceMapIDTable) do
            SendMessage(MSG_TO_FIELD_INSTANCE, EventMasterKeyMapID, PARAM1_MEMBER_NUM, Param2, PartyMemberNum[Param2]);
        --end
--]]
	end
	
end

