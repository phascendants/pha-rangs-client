--
-- 주의) 
-- 1) 대/소 문자를 확실하게 구분해야 합니다.
-- 2) true, false 를 확실하게 구분해야 합니다.
-- 3) UCS-2 Little Endian 로 저장해야 합니다.
-------------------------------------------------------------------------------
-- 버전 표기는 반드시 있어야 하며, 로직단과 반드시 일치해야 한다;
SCRIPT_VERSION = 
{
    "AlphaServer",     -- main version
    0,    -- sub version
};

-- 초기화시 저절로 기입되는 변수로 절대 선언 되면 안됩니다.;
--keyMapID = 선언금지 ( 초기화 이후 저절로 세팅 됨 );
--instanceMapID = 선언금지 ( 초기화 이후 저절로 세팅 됨 );

keyParty = nil;

function luaAgentRequestEntry(nPlayerDbNum, bRequestByGate)
	local PartyID = GetPartyID(nPlayerDbNum);
	if ( ( PartyID == nil ) or ( PartyID ~= keyParty ) ) then
		return false;
	end
	return true;
end

function luaAgentEventJoin(actorID, emAuthority)
end

function luaAgentEventPartyJoin(masterDbNum, membetDbNum)
	return false;
end

function luaAgentEventPartryOut(playerDbNum)
	return false;
end

function luaAgentEventPartryDissolution(playerDbNum)
	return false;
end

--function luaAgentEventClubOut(nClubDbNum, nPlayerDbNum, nKickActionActorDbNum)
--end

function luaAgentEventCreate(nstanceMapID, Param1, Param2, Param3, Param4)
	ServerLog("EventCreate : "..nstanceMapID..Param1..Param2..Param3..Param4)
	keyParty = Param1;
end