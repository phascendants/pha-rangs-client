--
-- 주의) 
-- 1) 대/소 문자를 확실하게 구분해야 합니다.
-- 2) true, false 를 확실하게 구분해야 합니다.
-- 3) UCS-2 Little Endian 로 저장해야 합니다.
-------------------------------------------------------------------------------
-- 버전 표기는 반드시 있어야 하며, 로직단과 반드시 일치해야 한다;
SCRIPT_VERSION = 
{
    "AlphaServer",     -- main version
    0,    -- sub version
};

-- 초기화시 저절로 기입되는 변수로 절대 선언 되면 안됩니다.;
--keyMapID = 선언금지 ( 초기화 이후 저절로 세팅 됨 );
--instanceMapID = 선언금지 ( 초기화 이후 저절로 세팅 됨 );

------ 깃발전 설정 변수 ---------
RedTeamList = {}                    -- 레드팀의 팀원 명단 ( 재입장 처리에 사용 );
BlueTeamList = {}                   -- 블루팀의 팀원 명단 ( 재입장 처리에 사용 );
---------------------------------

function luaAgentRequestEntry(nPlayerDbNum, bRequestByGate)
	return true;
end

function luaAgentEventJoin(actorID, emAuthority)
end

function luaAgentEventPartyJoin(masterDbNum, membetDbNum)
    return false;
end

function luaAgentEventPartryOut(playerDbNum)
    return false;
end

function luaAgentEventPartryDissolution(playerDbNum)
    return false;
end