--
-- 주의) 
-- 1) 대/소 문자를 확실하게 구분해야 합니다.
-- 2) true, false 를 확실하게 구분해야 합니다.
-- 3) UCS-2 Little Endian 로 저장해야 합니다.
-------------------------------------------------------------------------------
-- 버전 표기는 반드시 있어야 하며, 로직단과 반드시 일치해야 한다;
SCRIPT_VERSION = 
{
    "AlphaServer",     -- main version
    0,    -- sub version
};

-- 초기화시 저절로 기입되는 변수로 절대 선언 되면 안됩니다.;
--keyMapID = 선언금지 ( 초기화 이후 저절로 세팅 됨 );
--instanceMapID = 선언금지 ( 초기화 이후 저절로 세팅 됨 );

function luaAgentRequestEntry(nPlayerDbNum, bRequestByGate)
	return true;
end

function luaAgentEventJoin(actorID, emAuthority)
	 local _clubID = GetClubID(actorID);  -- 소속 클럽을 확인;
    if ( _clubID == nil ) then
        return;
    end
	
	local partyMember = GetParty(actorID);		-- 입장 시 파티원의 클럽이 다를 경우 파티 해산
	if ( partyMember ~= nil ) then
		for key, value in pairs(partyMember) do
			local partyMemberClub = GetClubID(value);
			if ( partyMemberClub ~= _clubID ) then
				ClearParty(GetPartyID(actorID));
				break;
			end
		end
	end
end

function luaAgentEventPartyJoin(masterDbNum, membetDbNum)
	local _partyMasterclubID = GetClubID(masterDbNum);
	local _partyMemberclubID = GetClubID(membetDbNum);
	if ( _partyMasterclubID == _partyMemberclubID) then		-- 파티 신청자와 파티장의 클럽이 같을 경우에만 파티 승인
		return true;
	else
		return false;
	end
end

function luaAgentEventPartryOut(playerDbNum)
	return true;
end

function luaAgentEventPartryDissolution(playerDbNum)
	return true;
end

function luaAgentEventClubOut(nClubDbNum, nPlayerDbNum, nKickActionActorDbNum)
	DoOut(nPlayerDbNum);
end