--
-- 주의) 
-- 1) 대/소 문자를 확실하게 구분해야 합니다.
-- 2) true, false 를 확실하게 구분해야 합니다.
-- 3) UCS-2 Little Endian 로 저장해야 합니다.
-------------------------------------------------------------------------------

name = L"GuidanceClubBattleA"; -- 절대 중복 되어선 안된다;
keyMapMainID = 191; -- 절대 중복 되어선 안된다(subID는 사용하지 않음);
keyMapSubID = 0;
maxPlayer = 1000;
maxCreate = 1;
isVisible = true; -- 클라이언트에 보여줄건지;
isUse = true; -- 이 인던을 사용할건지;

publicAgentScript =
{
	L"GuidanceClubBattle\\GroundA\\GuidanceClubBattle_PublicAgent_A.lua",
};

publicFieldScript =
{
	L"GuidanceClubBattle\\GroundA\\GuidanceClubBattle_PublicField_A.lua",
};

levelAgentScript =
{
	L"GuidanceClubBattle\\GroundA\\GuidanceClubBattle_LevelAgent_A.lua",
};

levelFieldScript =
{
	L"GuidanceClubBattle\\GroundA\\GuidanceClubBattle_LevelField_A.lua",
};
