
--------------------------------------------------------------------------------
-- 버전 표기는 반드시 있어야 하며, 로직단과 반드시 일치해야 한다;
SCRIPT_VERSION = 
{
    "AlphaServer",		-- main version
    0,					-- sub version
};


-- 초기화시 저절로 기입되는 변수로 절대 선언 되면 안됩니다;
--keyMapID = 선언금지 ( 초기화 이후 저절로 세팅 됨 );
--------------------------------------------------------------------------------
-- Color Code
COLOR_BLACK											= { 0, 0, 0 }
COLOR_WHITE											= { 255, 255, 255 }
COLOR_RED											= { 255, 0, 0 }
COLOR_GREEN											= { 0, 255, 0 }
COLOR_BLUE											= { 128, 194, 235 }
COLOR_MAGENTA										= { 240, 100, 170 } 
COLOR_YELLOW										= { 255, 200, 0 }


-------------------------------------------------------------------------------
-- Custom Message Sender Type
MSG_TO_USER											= 0			-- 특정 유저;   SenderID : CharDbNum;
MSG_TO_MY_INSTANCE									= 1			-- 현재 자신이 속한 필드서버 인던 객체 (LevelField)  SenderID : 의미없음;
MSG_TO_FIELD_BASE									= 2			-- 필드서버의 특정 인던 컨텐츠 (PublicField);  SenderID : Instance KeyMap ID;
MSG_TO_FIELD_INSTANCE								= 3			-- 필드서버의 특정 인던 객체 (LevelField);  SenderID : Instance Map ID;
MSG_TO_AGENT_BASE									= 4			-- 에이전트서버의 특정 인던 컨텐츠 (PublicAgent);  SenderID : Instance KeyMap ID;
MSG_TO_AGENT_INSTANCE								= 5			-- 에이전트서버의 특정 인던 객체 (LevelAgent);  SenderID : Instance Map ID;


--------------------------------------------------------------------------------
-- AbsTimer list
PARAM1_ABSTIMER_BATTLE_OPEN_NOTIFY					= 1
PARAM1_ABSTIMER_BATTLE_OPEN							= 2


--------------------------------------------------------------------------------
-- Cutsom Message list
PARAM1_EXIT_BUTTON									= 1

PARAM1_EVENT_UPDATE_CLUB_POINT						= 2
PARAM1_EVENT_UPDATE_CERTIFYMACHINE					= 3
PARAM1_EVENT_UPDATE_PROGRESS_TIME					= 4

PARAM1_EVENT_CREATEMAP_FA							= 5
PARAM1_EVENT_JOIN_FA								= 6
PARAM1_EVENT_OUT_FA									= 7
PARAM1_EVENT_BATTLE_CLOSE_FA						= 8

PARAM1_EVENT_SET_GUIDANCE_FA						= 9




----------------------------------------------------------------------------
-- Game
TIME_GAME_DURATION									= 3000		-- 게임 진행 시간;


--------------------------------------------------------------------------------
-- 인증기 인던을 나타내는 Class 이다;

CCertifyInstanceDungeonAgent = {}
CCertifyInstanceDungeonAgent.new = function ( )

	----------------------------------------------------------------------------
	-- CCertifyInstanceDungeonAgent instance
	local instance									= {}
	
	
	----------------------------------------------------------------------------
	-- Constant Variable
	local m_TIME_GAME_DURATION						= 0							-- 최초 설정 시간 상수;


	----------------------------------------------------------------------------
	-- Private Variable
	local m_bOpen 									= false						-- 오픈 상태;
	local m_nOpenCount 								= 0							-- 오픈 카운트;
	local m_nInstanceMapID							= 0							-- 인던 ID;
	
	local m_nKeyMapID								= 0							-- 인던 Key ID;
	local m_strInstanceDungeonName					= ""						-- 인던 이름;
	
	local m_nProgressTime							= 0							-- 인던 진행시간;
	
	local m_tableScore								= {}						-- 진영별 점수;
	local m_tableCertifyCount						= {}						-- 진영별 인증횟수;
	local m_tableMemberSize							= {}						-- 진영별 멤버수;

	local m_tableCertifiedID						= {}						-- 인증기 목록;
	local m_tableCertifiedAddPoint					= {}						-- 각 인증기 포인트;
	
	
	----------------------------------------------------------------------------
	-- Function List
	
	-- 인던을 초기화한다;
	instance.Initialize = function ( _nKeyMapID, _nTime, _strInstanceDungeonName )
		
		m_bOpen = false
		m_nOpenCount = 0
		m_nInstanceMapID = 0
		m_nKeyMapID = _nKeyMapID
		m_strInstanceDungeonName = _strInstanceDungeonName
		
		m_TIME_GAME_DURATION = _nTime
		m_nProgressTime = _nTime
		
		for nKey, nValue in pairs ( m_tableScore ) do
			m_tableScore[ nKey ] = nil
		end
		
		for nKey, nValue in pairs ( m_tableCertifyCount ) do
			m_tableCertifyCount[ nKey ] = nil
		end
		
		for nKey, nValue in pairs ( m_tableMemberSize ) do
			m_tableMemberSize[ nKey ] = nil
		end
		
		for nKey, nValue in pairs ( m_tableCertifiedID ) do
			m_tableCertifiedID[ nKey ] = nil
		end
		
		for nKey, nValue in pairs ( m_tableCertifiedAddPoint ) do
			m_tableCertifiedAddPoint[ nKey ] = nil
		end
	
	end
	
	-- 인던을 리셋한다;
	instance.Reset = function ( )
	
		m_bOpen = false
		m_nOpenCount = 0
		m_nInstanceMapID = 0
		
		m_nProgressTime = m_TIME_GAME_DURATION
		
		for nKey, nValue in pairs ( m_tableScore ) do
			m_tableScore[ nKey ] = nil
		end
		
		for nKey, nValue in pairs ( m_tableCertifyCount ) do
			m_tableCertifyCount[ nKey ] = nil
		end
		
		for nKey, nValue in pairs ( m_tableMemberSize ) do
			m_tableMemberSize[ nKey ] = nil
		end
		
		for nKey, nValue in pairs ( m_tableCertifiedID ) do
			m_tableCertifiedID[ nKey ] = nil
		end
		
		for nKey, nValue in pairs ( m_tableCertifiedAddPoint ) do
			m_tableCertifiedAddPoint[ nKey ] = nil
		end
		
	end
	
	-- 인던을 오픈한다;
	instance.Open = function ( )
	
		if ( m_bOpen == true ) then
	
			return
		
		end
		
		instance.Reset()

		m_bOpen = true
		m_nOpenCount = m_nOpenCount + 1
		
		-- 지역 생성 및 초기화
		DoCreateInstance( 0, m_nKeyMapID, 0 )
		
		ServerLog( "[ "..m_strInstanceDungeonName.." ] [ Open InstanceDungeon ]" )
	
	end
	
	-- 인던을 이어한다;
	instance.Continue = function ( )
	
		if ( m_bOpen == false ) then
	
			return
			
		end
		
		m_nOpenCount = m_nOpenCount + 1
		
		ServerLog( "[ "..m_strInstanceDungeonName.." ] [ Request Recreate InstanceDungeon ]" )
	
	end
	
	-- 인던을 닫는다;
	instance.Close = function ( )
	
		if ( m_bOpen == false ) then
	
			return
			
		end
		
		instance.Reset()
		
		ServerLog( "[ "..m_strInstanceDungeonName.." ] [ Close InstanceDungeon ]" )
	
	end
	
	-- 인던에 입장한다;
	instance.JoinUser = function ( _nNum )
	
		if ( m_tableMemberSize[ _nNum ] ~= nil ) then
		
			m_tableMemberSize[ _nNum ] = m_tableMemberSize[ _nNum ] + 1
				
		end
		
		if ( instance.IsOpen() ) then
			
			ServerLog( "[ "..m_strInstanceDungeonName.." ] [ Num : "..tostring( _nNum )..", Member Size : "..tostring( m_tableMemberSize[ _nNum ] ) )
				
		end
		
	end
	
	-- 인던에서 퇴장한다;
	instance.OutUser = function ( _nNum )
	
		if ( m_tableMemberSize[ _nNum ] ~= nil ) then
		
			m_tableMemberSize[ _nNum ] = m_tableMemberSize[ _nNum ] - 1
				
		end
		
		if ( instance.IsOpen() ) then
			
			ServerLog( "[ "..m_strInstanceDungeonName.." ] [ Num : "..tostring( _nNum )..", Member Size : "..tostring( m_tableMemberSize[ _nNum ] ) )
			
		end
		
	end
	
	-- 필드에서 맵 생성시 동기화한다;
	instance.SyncMap = function ( _nInstanceMapID )
	
		m_nInstanceMapID = _nInstanceMapID
	
		if ( _nInstanceMapID ~= 0 ) then
			
			for nNum, nScore in pairs ( m_tableScore ) do
			
				SendMessage( MSG_TO_FIELD_INSTANCE, _nInstanceMapID, PARAM1_EVENT_UPDATE_CLUB_POINT, nNum, nScore, m_tableCertifyCount[ nNum ] )
				ServerLog( "[ "..m_strInstanceDungeonName.." ] [ Sync Point, "..tostring( nNum ).." / "..tostring( nScore ).." / "..tostring( m_tableCertifyCount[ nNum ] ).." ]" )
				
			end
			
			for nKey, nCertifiedID in pairs ( m_tableCertifiedID ) do
			
				if ( nCertifiedID ~= 0 ) then
				
					SendMessage( MSG_TO_FIELD_INSTANCE, _nInstanceMapID, PARAM1_EVENT_UPDATE_CERTIFYMACHINE, nKey, m_tableCertifiedID[ nKey ], m_tableCertifiedAddPoint[ nKey ] )
					ServerLog( "[ "..m_strInstanceDungeonName.." ] [ Sync Certified Machine Info, "..tostring( nKey ).." / "..tostring( m_tableCertifiedID[ nKey ] ).." / "..tostring( m_tableCertifiedAddPoint[ nKey ] ).." ]" )
				
				end
				
			end
			
			SendMessage( MSG_TO_FIELD_INSTANCE, _nInstanceMapID, PARAM1_EVENT_UPDATE_PROGRESS_TIME, m_nProgressTime, TIME_GAME_DURATION, m_nOpenCount )
			ServerLog( "[ "..m_strInstanceDungeonName.." ] [ Initialize InstanceDungeon, "..tostring( _nInstanceMapID ).." / "..tostring( m_nProgressTime ).." / "..tostring( m_nOpenCount ).." ]" )
		
		end
		
	end
	
	-- 각 진영별 포인트를 동기화한다;
	instance.SyncPoint = function ( _nNum, _nScore, _nCount )
	
		m_tableScore[ _nNum ] = _nScore
		m_tableCertifyCount[ _nNum ] = _nCount
		
		ServerLog( "[ "..m_strInstanceDungeonName.." ] [ Update Point, "..tostring( _nNum ).." / "..tostring( _nScore ).." / "..tostring( _nCount ).." ]" )
	
	end
	
	-- 각 인증기 상태를 동기화한다;
	instance.SyncCertifyMachine = function ( _nCertifyID, _nNum, _nAddCount )
	
		m_tableCertifiedID[ _nCertifyID ] = _nNum
		m_tableCertifiedAddPoint[ _nCertifyID ] = _nAddCount
			
		ServerLog( "[ "..m_strInstanceDungeonName.." ] [ Update Certified Machine Info, "..tostring( _nCertifyID ).." / "..tostring( _nNum ).." / "..tostring( _nAddCount ).." ]" )
		
	end
	
	-- 진행시간 및 최초인증상태를 동기화한다;
	instance.SyncProgressTime = function ( _nProgressTime )
	
		m_nProgressTime = _nProgressTime
		
		ServerLog( "[ "..m_strInstanceDungeonName.." ] [ Update Progress Time, "..tostring( _nProgressTime ).." / "..tostring( _nFirstCertifySuccess ).." ]" )
		
	end
	
	-- 현재 Open 상태인지 확인한다;
	instance.IsOpen = function ( )
	
		if ( m_nInstanceMapID == 0 ) then
	
			return false
			
		end

		return m_bOpen
	
	end
	
	----------------------------------------------------------------------------
	
	return instance
	
end


--------------------------------------------------------------------------------
-- 선도전 인던을 생성한다;
g_sGuidanceClub = CCertifyInstanceDungeonAgent.new()


--------------------------------------------------------------------------------
-- Agent 서버가 올라갈 때의 초기화 Callback;
function luaAgentEventInitialize ( )

	-- 기존 선도클럽을 초기화한다;
	DisableOldGuidance()
	
	-- 선도전을 초기화한다;
	g_sGuidanceClub.Initialize( 191, TIME_GAME_DURATION, "Guidance Club Battle A" )
	
	-- 선도전 시작 알림 AbsTimer 등록 (1:일, 2:월, 3:화, 4:수, 5:목, 6:금, 7:토)
	RegistAbsTimer( 7, nil, 22, 30, 0, PARAM1_ABSTIMER_BATTLE_OPEN_NOTIFY, 30, 0, 0, true )
	RegistAbsTimer( 7, nil, 22, 50, 0, PARAM1_ABSTIMER_BATTLE_OPEN_NOTIFY, 10, 0, 0, true )
	RegistAbsTimer( 7, nil, 22, 55, 0, PARAM1_ABSTIMER_BATTLE_OPEN_NOTIFY, 5, 0, 0, true )
	RegistAbsTimer( 7, nil, 22, 57, 0, PARAM1_ABSTIMER_BATTLE_OPEN_NOTIFY, 3, 0, 0, true )
	RegistAbsTimer( 7, nil, 22, 59, 0, PARAM1_ABSTIMER_BATTLE_OPEN_NOTIFY, 1, 0, 0, true )

	RegistAbsTimer( 7, nil, 23, 0, 0, PARAM1_ABSTIMER_BATTLE_OPEN, 0, 0, 0, true )
	
end


--------------------------------------------------------------------------------
-- 시간 Timer 처리 Callback;
function luaAgentEventAbsTimer ( _nTimerHandle, _nParam1, _nParam2, _nParam3, _nParam4 )

    if ( _nParam1 == PARAM1_ABSTIMER_BATTLE_OPEN_NOTIFY ) then
	
		-- 메시지를 한번만 호출하기 위하여 선도전 A 에서만 호출한다;
        PrintSimpleXmlMsg_Broad( 10, { "PVP_GUIDANCE_SYSTEM_MESSAGE", 0, _nParam2 }, COLOR_WHITE )
		PrintChatXmlMsg_Broad( { "PVP_GUIDANCE_SYSTEM_MESSAGE", 0, _nParam2 }, COLOR_BLUE )
		
    elseif ( _nParam1 == PARAM1_ABSTIMER_BATTLE_OPEN ) then
	
		if ( false == g_sGuidanceClub:IsOpen() ) then
		
			-- 메시지를 한번만 호출하기 위하여 선도전 A 에서만 호출한다;
			PrintSimpleXmlMsg_Broad( 10, { "PVP_GUIDANCE_SYSTEM_MESSAGE", 1 }, COLOR_WHITE )
			PrintChatXmlMsg_Broad( { "PVP_GUIDANCE_SYSTEM_MESSAGE", 1 }, COLOR_BLUE )
			
			g_sGuidanceClub.Open()
			
			-- 선도지역을 초기화시킨다;
			SetGuidanceClub( 1, 0 )
			
		end
		
    end
	
end


--------------------------------------------------------------------------------
-- 전장 생성 요청 Callback;
function luaAgentRequestCreate ( _nPlayerDbNum, _bRequestByGate )
	
	if ( false == g_sGuidanceClub.IsOpen() ) then
	
		PrintChatXmlMsg( _nPlayerDbNum, { "PVP_GUIDANCE_SYSTEM_MESSAGE", 24 }, COLOR_RED )
	
		-- 열려있지 않다면 생성하지 않는다;
		-- 시간이 되면 자동생성이 이루어진다;
		return false
		
	else
	
		-- 열려있는 상태라면 이어서한다;
		g_sGuidanceClub.Continue()
		
		return true
		
	end
	
end


--------------------------------------------------------------------------------
-- 중복생성 Callback;
function luaAgentRequestDuplicateCreate ( )

	-- 전장의 중복 생성을 허용하지 않는다;
	return false
	
end


--------------------------------------------------------------------------------
-- Map ID 요청 ( 설정되어 있는 MapID 정보가 같아야한다 );
function luaAgentRequestMapID ( _nPlayerDbNum )

	return 191, 0
	
end


--------------------------------------------------------------------------------
-- Message Procedure;
function luaAgentEventCustomMessage ( _nSenderType, _nSenderID, _nParam1, _nParam2, _nParam3, _nParam4 )

	-- 유저에게서 agent 로 메시지를 보내는 일은 있을 수 없다;
	if ( _nSenderType == MSG_TO_USER ) then
	
		return
		
	end
	
	-- 유저가 입장하면 입장클럽의 인원을 증가시킨다;
	if ( _nParam1 == PARAM1_EVENT_JOIN_FA ) then

		g_sGuidanceClub.JoinUser( _nParam2 )
        
	-- 유저가 입장하면 입장클럽의 인원을 감소시킨다;
    elseif ( _nParam1 == PARAM1_EVENT_OUT_FA ) then
	
		g_sGuidanceClub.OutUser( _nParam2 )
	 	 
	-- 전장 생성시 호출된다;
    elseif ( _nParam1 == PARAM1_EVENT_CREATEMAP_FA ) then
	
		g_sGuidanceClub.SyncMap( _nParam2 )
        
	-- 선도 클럽을 결정한다;
	elseif ( _nParam1 == PARAM1_EVENT_SET_GUIDANCE_FA  ) then
	
		g_sGuidanceClub.Close()
		
		if ( SetGuidanceClub( 1, _nParam4 ) ) then
		
			-- 제대로 세팅이 되었으면 출력한다;
			PrintChatXmlMsg_Broad( { "PVP_GUIDANCE_SYSTEM_MESSAGE", 17, GetClubName( _nParam4 ) }, COLOR_BLUE )
			
		else
		
			-- 이미 다른 전장에서 승리한 선도클럽이라면 출력한다;
			PrintChatXmlMsg_Broad( { "PVP_GUIDANCE_SYSTEM_MESSAGE", 25 }, COLOR_BLUE )
			
		end
	
	-- 종료한다;
	elseif ( _nParam1 == PARAM1_EVENT_BATTLE_CLOSE_FA ) then
		
		g_sGuidanceClub.Close()
		
		PrintChatXmlMsg_Broad( { "PVP_GUIDANCE_SYSTEM_MESSAGE", 25 }, COLOR_BLUE )
		
	-- 선도전 포인트를 동기화한다;
	elseif ( _nParam1 == PARAM1_EVENT_UPDATE_CLUB_POINT ) then
	
		g_sGuidanceClub.SyncPoint( _nParam2, _nParam3, _nParam4 )
	
	-- 선도전 인증기 정보를 동기화한다;
	elseif ( _nParam1 == PARAM1_EVENT_UPDATE_CERTIFYMACHINE ) then
	
		g_sGuidanceClub.SyncCertifyMachine( _nParam2, _nParam3, _nParam4 )
	
	-- 선도전 진행시간을 동기화한다;
	elseif ( _nParam1 == PARAM1_EVENT_UPDATE_PROGRESS_TIME ) then
	
		g_sGuidanceClub.SyncProgressTime( _nParam2 )
		
    end
	
end


--------------------------------------------------------------------------------