--
-- 주의) 
-- 1) 대/소 문자를 확실하게 구분해야 합니다.
-- 2) true, false 를 확실하게 구분해야 합니다.
-- 3) UCS-2 Little Endian 로 저장해야 합니다.
-------------------------------------------------------------------------------

instanceTable =
{	
	L"club_death_match.lua",
	L"capture_the_flag_A.lua",		--깃발전(Lv.200 ~ 229)
	L"capture_the_flag_B.lua",		--깃발전(Lv.230 ~)
    L"GuidanceClubBattle\\guidance_club_battle_A.lua",    --선도전
    L"GuidanceClubBattle\\guidance_club_battle_B.lua",    --선도전
    L"GuidanceClubBattle\\guidance_club_battle_C.lua",    --선도전
	--L"CaptureTheIsland\\capture_the_island_battlezone.lua",    --난사군도 점령전 (전장)
	L"CaptureTheIsland\\capture_the_island_rewardzone.lua",    --난사군도 점령전 (보상사냥터)	
	--L"2015halloween.lua",				-- 할로윈인던 이벤트
	--L"capture_the_flag.lua",
	--L"tournament_tgame.lua",
	L"PremiumMap\\premium_airport.lua",    --프리미엄맵 1차 (세인트사설공항맵)
	L"PremiumMap\\premium_bank.lua",    --프리미엄맵 2차 (은행맵)
	L"DimensionLab.lua",    		--차원실험실	
}
