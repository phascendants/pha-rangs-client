--
-- 주의) 
-- 1) 대/소 문자를 확실하게 구분해야 합니다.
-- 2) true, false 를 확실하게 구분해야 합니다.
-- 3) UCS-2 Little Endian 로 저장해야 합니다.
-------------------------------------------------------------------------------
-- 버전 표기는 반드시 있어야 하며, 로직단과 반드시 일치해야 한다;
SCRIPT_VERSION = 
{
    "AlphaServer",     -- main version
    0,    -- sub version
};

-- 초기화시 저절로 기입되는 변수로 절대 선언 되면 안됩니다.;
--keyMapID = 선언금지 ( 초기화 이후 저절로 세팅 됨 );
--instanceMapID = 선언금지 ( 초기화 이후 저절로 세팅 됨 );

-- ActorType
	ACTOR_PC				= 0;
	ACTOR_NPC				= 1;
	ACTOR_MOB				= 2;
	ACTOR_SUMMON			= 6;
	ACTOR_NULL				= -1;
--------------------------------------------------------------------------------
-- FactionType
	FACTION_TEAM			= 0;
	FACTION_PARTY			= 1;
	FACTION_CLUB			= 2;
	FACTION_EXPEDITION 	= 3;
	FACTION_CLUB_SAFE	= 4;
--------------------------------------------------------------------------------
-- RelationType
	RELATION_ENEMY			= 0;
	RELATION_ALLY			= 1;
	RELATION_NEUTRAL		= 2;
--------------------------------------------------------------------------------
-- TriggerType
	TRIGGER_PC				= 1;
	TRIGGER_MOB				= 2;
	TRIGGER_SUMMON			= 4;
	
	TRIGGER_PC_MOB		    = 3;	
	TRIGGER_PC_SUMMON       = 5;
--------------------------------------------------------------------------------
	PARAM3_MSGBOX_OK		= 0;
	PARAM3_MSGBOX_CANCEL	= 1;
	PARAM3_MSGBOX_TIMEOVER	= 2;
--------------------------------------------------------------------------------
-- ColorCode
	COLOR_BLACK				= {0,0,0};
	COLOR_WHITE				= {255,255,255};
	COLOR_RED				= {255,0,0};
	COLOR_GREEN				= {0,255,0};
	COLOR_BLUE				= {128,194,235};
	COLOR_MAGENTA			= {240,100,170}; 
	COLOR_YELLOW				= {255,200,0};
--------------------------------------------------------------------------------
-- CutsomParameter list
PARAM1_EXIT_BUTTON          = 200;
PARAM1_EVENT_JOIN           = 201;
PARAM1_EVENT_OUT            = 202;
PARAM1_EVENT_CREATEMAP      = 203;
PARAM1_MEMBER_NUM           = 204;
PARAM1_EVENT_SET_GUIDANCE  = 205;
EVENT_SKILL_RESURRECT = 8;
EVENT_SYSTEM_RESURRECT = 16;
PARAM1_BATTLE_OPEN_NOTIFY   = 206;
PARAM1_BATTLE_OPEN          = 207;
PARAM1_BATTLE_CLOSE         = 208;
--------------------------------------------------------------------------------
-- Timer Event Code
T_CheckTicket = 300;		-- 티켓 보유 체크
--------------------------------------------------------------------------------
-- 프리미엄 맵 설정 변수
CheckTicketCycle = 10;		-- 티켓 유무 체크 주기

PremiumMapID = {80, 0};		-- 인던 Base Map ID

TicketID = { {117, 218},		--24시간
				 {117, 216},		--12시간
				 {454, 21},		--9시간(서버 통합 이벤트)				 				 
				 {117, 214},		--6시간
				 {117, 159},		--6시간 (구)
				 {117, 212}, };		--3시간

TicketNum = 6;				-- 티켓 테이블 갯수

-- 현재 유효한 유저수;
g_nPartyCount							= 0

-- 인던 인원 관리 Table ( 인원 관리도 개선 할 필요가 있다 );
g_tablePartyMember 					= {}						-- 입장 파티 리스트;
g_tablePartyMemberStatus 			= {}						-- 파티원의 플레이 상태 여부;
----------------------------------

-- 초기화 ( 인던 생성 직후 );
function EventInitialize()
	RegistTimer(CheckTicketCycle, T_CheckTicket, nil, nil, nil, 1);		-- 티켓 체크 타이머 등록 (CheckTicketCycle 주기로 반복)
	
	return true;
end

-- 입장 요청 발생시
function EventRequestJoin(actorID)
	return true;
end

-- GM 입장 요청 발생시
function EventRequestJoinGM(actorID)
	return true;		-- GM 명령어로 들어온 경우 무조건 허용;
end

-- 유저 입장시
function EventJoin(actorID)
	-- 최초 입장 시 파티 멤버 테이블에 등록한다.
	if ( g_tablePartyMemberStatus[actorID] == nil ) then
		
		table.insert( g_tablePartyMember, actorID )
		
	end
	
	functionMemberAdjust(actorID,1);			--인던안의 맴버수를 체크하기위한 임의함수(접속한 캐릭터의 Db Num 저장)(1이 증가, 2가 감소) **유저가 진입할경우 인원카운트를 1올린다.
	
	Log_EnterUser( "Premium_Airport", PremiumMapID[1], actorID, 0, #g_tablePartyMember, g_nPartyCount, 0 )
		
	return true;
end

-- 실제 맵에 진입할 때 실행되는 부분
function EventMapEnter(actorID, mapMid, mapSid)
	return true;
end

-- 인던을 나갈 때 실행되는 부분
function EventOut(actorID) 
	
	functionMemberAdjust(actorID, 2);
	
	Log_OutUser( "Premium_Airport", PremiumMapID[1], actorID, 0, #g_tablePartyMember, g_nPartyCount, 0 )
	
	if (g_nPartyCount <= 0) or ( g_nPartyCount == nil) then -- 인던에 0명인 경우 인던 파괴
		DoDestroy();	-- 인던 삭제
	end
	
	return true;
end

-- 게이트를 통한 이동 시 실행되는 부분
function EventRequestMoveGate(nActorID, nMapMainID, nMapSubID, nGateID, nMainMapIDTarget, nSubMapIDTarget, nGateIDTarget)
	if ( nMainMapIDTarget == 22 ) then		-- 이동할 맵이 학원광장이라면 (학원광장은 c 코드에 명시되어 있으므로 변경 금지)
		SetPositionField(nActorID, nMainMapIDTarget, nSubMapIDTarget, 0, 0);		-- 학광으로 이동하는 게이트 사용 시 인던이 아닌 필드로 보냄
		return false;
	end
	return true;
end

-- 타이머가 정한 시간이 되었을때 실행되는 부분;
function EventTimer(nTimerHandle, nParam0, nParam1, nParam2, nParam3)
	if ( nParam0 == T_CheckTicket ) then
		for key, value in pairs (g_tablePartyMemberStatus) do		-- 인던 내부에 있는 유저수만큼 인벤 내 티켓 유무 체크
			if ( g_tablePartyMemberStatus[key] ~= nil ) then
				functionCheckTicket(key);
			end
		end	
	end
end

function EventPartyOut(nPartyID, bExpedition, nOuterID, bInSameInstance)
	-- 파티를 탈퇴할 경우 탈퇴자를 내쫒는다.;
		DoOut(nOuterID); 
end

--선언 함수--
function functionMemberAdjust(MemberDbNum,AdjustType)	--인던안의 인원수 가감을 위한 임의함수(유저넘버,가감타입)
	if (AdjustType == 1) then
		
		--유저수 증가
		g_nPartyCount = g_nPartyCount + 1;
		
		-- 플레이 상태를 활성화 한다;
		g_tablePartyMemberStatus[MemberDbNum] = true;
				
	elseif (AdjustType == 2) then
		
		--유저수 감소
		g_nPartyCount = g_nPartyCount - 1;
		
		-- 플레이 상태를 비활성화 한다;
		g_tablePartyMemberStatus[MemberDbNum] = false;
		
	end
end

-- 어떠한 PC,MOB이 데미지를 받으면 실행되는 부분;
function EventReceiveDamage(nDamagedActorType, nDamagedActorID, nAttackActorType, nAttackActorID, nDamage, nDamageFlag)
	return true;
end

-- 어떠한 PC,MOB이 죽었을 때 실행되는 부분;
function EventDie(nDieActorType, nDieActorID, nKillActorType, nKillActorID)
	return true;
end

--[[
-- 부활 이벤트가 발생할 때 실행되는 부분
function EventResurrect(nResurrectType, nResurretActorType, nResurrectActorID, nKillActorType, nKillActorID)
end

--function EventActorTriggerIn(nTriggerType, nTriggerID, nInnerType, nInnerID)
--end

--function EventActorTriggerOut(nTriggerType, nTriggerID, nOuterType, nOuterID)
--end

-- 클릭 트리거를 클릭했을 때 실행되는 부분
function EventClickTrigger(nTriggerType, nTriggerID, nClickerType, nClickerID)
end

-- 모션이 종료될 때 실행되는 부분
function EventMotionFinish(nActorType, nActorID, nMotionMid, nMotionSid, nMotionRemainSec)
end

function EventPartyJoin(nPartyID, bExpedition, nJoinerID, bInSameInstance)
end

function EventCustomMessage(nSenderType, nSenderID, nParam1, nParam2, nParam3, nParam4)
end
 
function EventUseItem(CharDbNum, ItemMID, ItemSID, nParam, fParam) -- 유저가 어떠한 Item을 사용하였을때 실행되는 부분;
	return true;
end
      
function EventRequestMoveGate(nActorID, nMapMainID, nMapSubID, nGateID, nMainMapIDTarget, nSubMapIDTarget, nGateIDTarget)
	return true;
end

function EventReceiveHeal(nReceiveActorType, nReceiveActorID, nHealActorType, nHealActorID, nHeal, nHealFlag)  -- 어떠한 PC,MOB이 회복를 받으면 실행되는 부분;
	return true,nHeal; 
end

-- 파티 마스터가 변경될 경우 호출되는 함수;
function EventPartyChangeMaster(nPartyID, bExpedition, nNewMasterID, bInMapNewMaster, nOldMaster, bInMapOldMaster)
end
]]--

function functionCheckTicket(actorID)
	local _isHaveTicket;
	for i=1, TicketNum, 1 do
		-- 유저 인벤에 보유한 티켓이 있는지 검사
		if ( GetItemCount(actorID, TicketID[i][1], TicketID[i][2]) ~= 0 ) then
			--ServerLog("Item Checked"..tostring(i));
			_isHaveTicket = true;
			break;
		else
			_isHaveTicket = false;
		end
	end
	
	if ( _isHaveTicket == false ) then
		-- 티켓이 없다면 퇴장 시킴
		PrintChatXMLMsg(actorID, {"PREMIUM_MAP_FORCED_EXIT", 0}, COLOR_RED);
		DoOut(actorID);
	end
end



