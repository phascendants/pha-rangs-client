--
-- 주의) 
-- 1) 대/소 문자를 확실하게 구분해야 합니다.
-- 2) true, false 를 확실하게 구분해야 합니다.
-- 3) UCS-2 Little Endian 로 저장해야 합니다.
-------------------------------------------------------------------------------

name = L"Premium_Airport"; -- 절대 중복 되어선 안된다;
keyMapMainID = 80; -- 절대 중복 되어선 안된다(subID는 사용하지 않음);
keyMapSubID = 0;
maxPlayer = 100;
maxCreate = 200;
isVisible = true; -- 클라이언트에 보여줄건지;
isUse = true; -- 이 인던을 사용할건지;

publicAgentScript =
{
	L"PremiumMap\\Premium_Airport\\Premium_Airport_PublicAgent.lua",
};

publicFieldScript =
{
	L"PremiumMap\\Premium_Airport\\Premium_Airport_PublicField.lua",
};

levelAgentScript =
{
	L"PremiumMap\\Premium_Airport\\Premium_Airport_LevelAgent.lua",
};

levelFieldScript =
{
	L"PremiumMap\\Premium_Airport\\Premium_Airport_LevelField.lua",
};
