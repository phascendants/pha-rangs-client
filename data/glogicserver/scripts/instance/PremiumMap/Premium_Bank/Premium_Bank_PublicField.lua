--
-- 주의) 
-- 1) 대/소 문자를 확실하게 구분해야 합니다.
-- 2) true, false 를 확실하게 구분해야 합니다.
-- 3) UCS-2 Little Endian 로 저장해야 합니다.
-------------------------------------------------------------------------------
-- 버전 표기는 반드시 있어야 하며, 로직단과 반드시 일치해야 한다;
SCRIPT_VERSION = 
{
    "AlphaServer",     -- main version
    0,    -- sub version
};

-- 초기화시 저절로 기입되는 변수로 절대 선언 되면 안됩니다.;
--keyMapID = 선언금지 ( 초기화 이후 저절로 세팅 됨 );

-- ActorType
	ACTOR_PC				= 0;
	ACTOR_NPC				= 1;
	ACTOR_MOB				= 2;
	ACTOR_SUMMON			= 6;
	ACTOR_NULL				= -1;
--------------------------------------------------------------------------------	
	-- ColorCode
	COLOR_BLACK				= {0,0,0};
	COLOR_WHITE				= {255,255,255};
	COLOR_RED				= {255,0,0};
	COLOR_GREEN				= {0,255,0};
	COLOR_BLUE				= {128,194,235};
	COLOR_MAGENTA			= {240,100,170}; 
	COLOR_YELLOW				= {255,200,0};
--------------------------------------------------------------------------------
TicketID = {	{117, 218},		--24시간
					{117, 216},		--12시간
					{454, 21},		--9시간(서버 통합 이벤트)					
					{117, 214},		--6시간
					{117, 159},		--6시간 (구)
					{117, 212}, };		--3시간

	
TicketNum = 6;				-- 티켓 테이블 갯수

function luaFieldRequestJoin(CharDbNum, bRequestByGate )
	for i=1, TicketNum, 1 do		-- 입장 요청이 왔을 때, 입장권이 있는지 여부를 체크
		ServerLog("TicketID[i][1] : "..tostring(TicketID[i][1]).." ".."TicketID[i][2] : "..tostring(TicketID[i][2]));
		ServerLog("Item : "..tostring(GetItemCount(CharDbNum, TicketID[i][1], TicketID[i][2])));
		if ( GetItemCount(CharDbNum, TicketID[i][1], TicketID[i][2]) ~= 0 ) then
			return true;
		end
	end
	
	PrintChatXMLMsg(CharDbNum, {"PREMIUM_MAP_NOT_ENOUGH_TICKET", 0}, COLOR_RED);
	return false;
end