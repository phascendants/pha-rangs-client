/**
	PVE 한계의 사막
	서버 메인 스크립트       작성자 김판수
	
 */

// 맵 정보
root <- MapInfoTrigger();
root.SetMap(246, 0);			// 맵ID
root.SetGateID(2);			// 맵 시작 게이트 ID
root.SetDailyLimit(2);			// 하루 입장횟수 (라이브 적용 시 -> 2)
root.SetOwnTime(6000);			// 귀속 시간  100분 (테스트 및 라이브 100분)
root.SetPerson(2);			// 인원 제한 (테스트 및 라이브 2)
root.SetWaitEntrance(10);		// 입장 확인 대기 시간

// 보통
level0 <- LevelTrigger();
level0.SetFile("InfinitystairsBLev0.nut");
level0.SetLevel(0);		
level0.SetUserLevel(245, 300);
level0.Load();					
root.InsertSibling(level0);