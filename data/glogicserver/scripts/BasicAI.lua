Type_Max_Count = 1

GLAT_IDLE = 0
GLAT_MOVE = 1
GLAT_ATTACK = 2
GLAT_SKILL = 3
GLAT_SHOCK = 4
GLAT_PUSHPULL = 5
GLAT_GATHERING = 6
GLAT_TALK = 7
GLAT_CONFT_END = 8
GLAT_MOTION = 9
GLAT_AUTHED = 10
GLAT_FALLING = 11
GLAT_DIE = 12
GLAT_SKILL_WAIT = 13

function ChangeState( me, owner )
	if ( owner:GetAction() == me:GetAction() ) then
		return;
	end
		
	if ( owner:GetAction() == GLAT_IDLE ) then
		me:ChangeAIState( "State_Normal" )
	elseif ( owner:GetAction() == GLAT_MOVE ) then
		me:ChangeAIState( "State_Move" )
	elseif ( owner:GetAction() == GLAT_SKILL ) or ( owner:GetAction() == GLAT_ATTACK ) then
		me:ChangeAIState( "State_Attack" )
	end
end


-- Idle State
State_Normal = {}

State_Normal[ "Begin" ] = function( me )
end

State_Normal[ "Update" ] = function( me, fElapsedTime )
	-- 상태 체크 및 변화;
	ChangeState( me, me:GetOwner() )
	
	-- 주인과의 거리 체크 및 변화;
	if ( me:IsValidOwnerDistance() == false ) then
		me:RunAwayAction()
	end
	
	-- 액션 시작;
	me:StartTargetAction()
	
	-- 주인 추적;
	me:TracingOwner()
end

State_Normal[ "End" ] = function( me )
end

State_Normal[ "Attack" ] = function( me )
end

State_Normal[ "Attacked" ] = function( me, attacker )
	return true
end

State_Normal[ "OwnerAttack" ] = function( me )
end

State_Normal[ "OwnerAttacked" ] = function( me, attacker )
	return true
end


-- Move State
State_Move = {}

State_Move[ "Begin" ] = function( me )
end

State_Move[ "Update" ] = function( me, fElapsedTime )
	-- 상태 체크 및 변화;
	ChangeState( me, me:GetOwner() )
	
	-- 주인과의 거리 체크 및 변화;
	if ( me:IsValidOwnerDistance() == false ) then
		me:RunAwayAction()
	end
	
	-- 액션 시작;
	me:StartTargetAction()
	
	-- 주인 추적;
	me:TracingOwner()
end

State_Move[ "End" ] = function( me )
end

State_Move[ "Attack" ] = function( me )
end

State_Move[ "Attacked" ] = function( me, attacker )
	return true
end

State_Move[ "OwnerAttack" ] = function( me )
end

State_Move[ "OwnerAttacked" ] = function( me, attacker )
	return true
end


-- Attack State
State_Attack = {}

State_Attack[ "Begin" ] = function( me )
end

State_Attack[ "Update" ] = function( me, fElapsedTime )
	-- 상태 체크 및 변화;
	ChangeState( me, me:GetOwner() )
	
	-- 주인과의 거리 체크 및 변화;
	if ( me:IsValidOwnerDistance() == false ) then
		me:RunAwayAction()
	end
	
	-- 액션 시작;
	me:StartTargetAction()
	
	-- 주인 추적;
	me:TracingOwner()
end

State_Attack[ "End" ] = function( me )
end

State_Attack[ "Attack" ] = function( me )
end

State_Attack[ "Attacked" ] = function( me, attacker )
	return false
end

State_Attack[ "OwnerAttack" ] = function( me )
	if ( me:IsTargetExcludeOwner() == false ) then
		me:SetOwnerTarget()
	end
end

State_Attack[ "OwnerAttacked" ] = function( me, attacker )
	return false
end