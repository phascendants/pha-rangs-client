/**
	PVE 통곡의 빌딩
	서버 메인 스크립트       작성자 이익세
	
 */

// 맵 정보
root <- MapInfoTrigger();
root.SetMap(285, 0);			// 맵ID
root.SetGateID(2);			// 맵의 게이트ID
root.SetDailyLimit(1);			// 하루 입장 제한
root.SetOwnTime(6000);			// 귀속 시간  2시간 -> 1시간 20분 by wowkill (라이브용)
root.SetPerson(2);			// 인원 제한
root.SetWaitEntrance(10);		// 입장 확인 대기 시간

// 보통
level0 <- LevelTrigger();
level0.SetFile("infinitystairsc0lev0.nut");
level0.SetLevel(0);		
level0.SetUserLevel(230, 300);
level0.Load();			
root.InsertSibling(level0);

// 어려움
//level1 <- LevelTrigger();
//level1.SetFile("InfinitystairsLev1.nut");
//level1.SetLevel(1);
//level1.SetUserLevel(220, 260);
//level1.Load();
//root.InsertSibling(level1);

// 상
//level2 <- LevelTrigger();
//level2.SetFile("InfinitystairsLev2.nut");
//level2.SetLevel(2);
//level2.SetUserLevel(230, 260);
//level2.Load();
//root.InsertSibling(level2);
