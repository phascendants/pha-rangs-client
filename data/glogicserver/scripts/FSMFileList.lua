--
-- 주의) 
-- 1) 대/소 문자를 확실하게 구분해야 합니다.
-- 2) true, false 를 확실하게 구분해야 합니다.
-- 3) UCS-2 Little Endian 로 저장해야 합니다.
-------------------------------------------------------------------------------

List = 
{
	"BasicAI.lua",							-- 기본 AI;
	"ChannelingAI.lua",						-- 채널링 AI;
	"DelayedInvokeAI.lua",					-- 지연 AI;
	"TrapAI.lua"							-- 트랩 AI;
}

Basic = 
{
	hire_summon_level_interval	= 300,		-- 고용 소환수 레벨 간격;
	summon_speed_up_value		= 50,		-- 소환수 스피드업시의 속도;
	summon_event_skill_range	= 200,		-- 이벤트 스킬 거리;
	summon_owner_range			= 200,		-- 주인과 떨어질 수 있는 거리 ( RunAway시에 사용 );
}