-------------------------------------------------------------------------------
-- HideSet 세팅 파일
-- 2012-02-22 khLee;
--
-- 주의) 
-- 1) 대/소 문자를 확실하게 구분해야 합니다.
-- 2) true, false 를 확실하게 구분해야 합니다.
-- 3) UCS-2 Little Endian 로 저장해야 합니다.
-- 4) 새로운 항목 추가시 RnaLogic/GLHideSe_Define.h, RnaLogic/GLHideSet.cpp 두 파일에도 추가해야 합니다.
-------------------------------------------------------------------------------
--버젼;
HideSet_Version = 100;

HideSet_Settings =
{
	fInvisiblePerceiveAngle = 100.0, -- 자동 은신 감지 시야각 (도);
	fInvisiblePerceiveRadius = 100.0, -- 자동 은신 감지 기준 범위(거리);
	fInvisiblePerceiveLevel = 200.0, -- 자동 은신 감지 레벨 : 감지확률 = (내레벨 - 상대레벨) / fInvisiblePerceiveLevel * 100;
	fInvisibleUpdateTime = 3.0, -- 자동 은신 감지 갱신 시간 (초);
}
-- 옵션;
HideSet_Option = 
{	
	-- 일반;
	EMHIDE_PICKUP_ITEM = true, -- 아이템 줍기
	EMHIDE_GETON_VEHICLE = true, -- 탈것 타기
	EMHIDE_GETOFF_VEHICLE = true, -- 탈것 내리기
	EMHIDE_DEATH = true, -- 사망
	EMHIDE_UPDATE_EXP = false, -- 경험치 획득
	EMHIDE_LEVELUP = false, -- 레벨업
	EMHIDE_LEVELDOWN = false, -- 레벨 다운

	-- 필드 이동;
	EMHIDE_TURN_WALK = false, -- 걷기 전환
	EMHIDE_TURN_RUN = true, -- 뛰기 전환
	EMHIDE_ON_VEHICLE = true, -- 탈것 탑승 상태
	EMHIDE_BUS = true, -- 버스
	EMHIDE_TAXI = true, -- 택시

	-- 전투;
	EMHIDE_START_ATTACK = true, -- 일반 공격 시작
	EMHIDE_USE_SKILL = true, -- 스킬 사용
	EMHIDE_TAKE_BUFF = false, -- 자신에게 버프 적용
	EMHIDE_TAKE_LIMIT = true, -- 자신에게 리미트 스킬
	EMHIDE_TAKE_DEBUFF = true, -- 자신에게 디버프 스킬 적용
	EMHIDE_ON_HIT = true, -- 적의 공격에 적중되어 HP가 감소함
	EMHIDE_TAKE_DAMAGE_BY_DEBUFF = true, -- 적의 지속데미지 스킬에 적중되어 HP가 감소함
	EMHIDE_AVOID = false, --공격 회피

	-- Q박스 효과 적용 여부;
	EMHIDE_SPEEDUP = false,
	EMHIDE_SPEEDUP_MAX = false,
	EMHIDE_CRAZY = false,
	EMHIDE_MADNESS = false,
	EMHIDE_EXP_TIME = false,
	EMHIDE_EXP_GET = false,
	EMHIDE_POWERUP = false,
	EMHIDE_POWERUP_MAX = false,
	EMHIDE_BOMB = false,
	EMHIDE_HEAL = false,
	EMHIDE_LUCKY = false,
	EMHIDE_SUMMON = true,
	
	-- 아이템 사용 여부;
	EMHIDE_USE_SKILLBOOK = false, -- 스킬북 사용
	EMHIDE_USE_ACCREDIT = true, -- 인증서 사용(인증 시작)
	EMHIDE_RESET_SKILL_STAT = false, -- 스킬/스탯 리셋 사용, 사용 완료
	EMHIDE_USE_GRINDER = false, -- 연마제 사용
	EMHIDE_USE_INVEN_EXTENSION = false, -- 인벤 확장 사용
	EMHIDE_USE_STORAGE_EXTENSION = false, -- 창고 확장 사용
	EMHIDE_USE_STORAGE_LINK = false, -- 창고 연결 사용
	EMHIDE_USE_MARKET_GRANT = false, -- 개인상점 허가권 사용 시작
	EMHIDE_USE_DISGUISE_SPLIT = false, -- 코스튬 분리 사용
	EMHIDE_USE_DISGUISE_REMOVE = false, -- 세제(코스튬 제거) 사용
	EMHIDE_USE_LOOK_CHANGE = false, -- 얼굴/헤어스타일/헤어컬러 변경
	EMHIDE_USE_PET_CHANGE = false, -- 펫 이름/컬러 변경 및 먹이 사용
	EMHIDE_USE_LUNCH = false, -- 도시락 사용
	EMHIDE_USE_POINT_CHARGE = false, -- 포인트 카드로 인게임 포인트 충전
	EMHIDE_USE_POST_CONNECT = false, -- 우편 연결 및 휴지통 사용	
	
	-- 탈것 개조;
	EMHIDE_REMODEL_VEHICLE = false, -- 탈 것 아이템 탈착
	
	-- 대련;
	EMHIDE_REQ_SELF = true, -- 개인 대련 신청 시
	EMHIDE_REQ_PARTY = true, -- 파티 대련 신청 시
	EMHIDE_REQ_CLUB = true, -- 클럽 대련 신청 시
	EMHIDE_BEING_SELF = false, -- 개인 대련 시작/대련 중
	EMHIDE_BEING_PARTY = false, -- 파티 대련 시작/대련 중
	EMHIDE_BEING_CLUB = false, -- 클럽 대련 시작/대련 중
	
	-- 클럽;
	EMHIDE_RECEIVE_INVITE = false, -- 클럽 초대 받음
	EMHIDE_CLUB_LEAVE = false, -- 클럽 탈퇴 완료
	EMHIDE_CLUB_CREATE = false, -- 클럽 창설 완료
	EMHIDE_CLUB_DISBAND = false, -- 클럽 해체
	
	-- 개인 상점;
	EMHIDE_STORE_OPEN = true, -- 개인 상점 설치
	EMHIDE_STORE_CLOSE = true, -- 개인 상점 접기
	
	-- NPC;
	EMHIDE_TALK_START = true, -- 새로 대화 시작(말걸기)
	EMHIDE_TALK_END = true, -- 현재 NPC와의 거래 완료(혹은 대화 완료, 창 닫힘)
	EMHIDE_QUEST_ACCEPT = true, -- NPC 대화로 퀘스트 수락
	EMHIDE_QUEST_COMPLETE = true, -- NPC 대화로 퀘스트 완료
	
	-- 개인거래;
	EMHIDE_DEAL_ACCEPT_SELF = true, -- 개인 거래 신청 수락함
	EMHIDE_DEAL_ACCEPT_TARGET = true, -- 개인 거래 신청 수락 받음
	EMHIDE_DEAL_REQUET_SEND = true, -- 개인 거래 신청 함
	EMHIDE_DEAL_REQUEST_RECEIVE = false, -- 개인 거래 신청 받음	
	
	-- 파티 초대;
	EMHIDE_PARTY_ACCEPT = false, -- 파티 초대 받음, 파티 초대 수락함
	EMHIDE_PARTY_REQUEST = false, -- 파티 신청(결과에 상관 없이)
	EMHIDE_PARTY_LEAVE = false, -- 파티 탈퇴
	EMHIDE_FRIEND_REQUEST = false, -- 친구 신청
	EMHIDE_FRIEND_REMOVE = false, -- 친구 삭제
	EMHIDE_VIEWINFO = false, -- 정보 보기(예정)
	EMHIDE_BLOCK = false, -- 차단
	
	-- 펫; -- 은신 중일때는 펫도 은신되게;
	EMHIDE_PET_SUMMON = false, -- 펫 소환
	EMHIDE_PET_SUMMONNING = false, -- 펫 소환 중
	EMHIDE_PET_SUMMON_RELEASE = false, -- 펫 소환 해제
	EMHIDE_PET_SKILL_LEARN = false, -- 펫 스킬 배우기
	
	-- 선도전;
	EMHIDE_GUIDANCE_CERTIFY_HIDECANCLE = true, -- 인증하려는 캐릭터가 은신중이라면 은신이 풀리게할 것인가?
}
