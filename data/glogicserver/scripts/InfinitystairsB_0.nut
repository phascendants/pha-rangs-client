/**
	PVE 한계의 사막
	서버 공통 콤포넌트 스크립트    작성자 김판수
	
 */
 
// 입장 대기
root <- ReadyTrigger();
root.SetCount(8);
root.SetLifeTime(7);

// 파티 관리
party <- PartyTrigger();
party.SetAutoOut(true);
root.InsertSibling(party);

// 인지 시간
recognize <- RecognizeTrigger();
recognize.SetRecognizeTime(0.3);
root.InsertSibling(recognize);

// 보상 처리
reward <- RewardTrigger();
reward.SetWaitTime(60);
reward.SetMoney(0);
reward.SetMinStage(5);
root.InsertSibling(reward);

// 재도전, 부활 처리
retry <- RetryTrigger();
retry.SetTime(60);
retry.SetMoney(2500);
retry.SetControl(0.5);
root.InsertSibling(retry);

// 게이트 OFF 처리
close1 <- PortalCloseTrigger();
close1.SetFromGateID(0);
root.InsertSibling(close1);
close2 <- PortalCloseTrigger();
close2.SetFromGateID(1);
root.InsertSibling(close2);
close3 <- PortalCloseTrigger();
close3.SetFromGateID(2);
root.InsertSibling(close3);

// 난이도 설정
difficulty <- DifficultyTrigger();
root.InsertSibling(difficulty);

// 스테이지 시작 처리
start <- StartStageTrigger();
start.SetLifeTime(1);
root.InsertChild(start);

// 스테이지 정보 출력
info <- MsgTrigger();
start.InsertSibling(info);

// 스테이지 처리
stage <- StageTrigger();
stage.SetMission(1);

// 몬스터 클리어 미션
clear <- MonsterClearTrigger();

// 이동 미션
pos <- PositioningClearTrigger();
pos.SetAttach(stage);
pos.SetCount(2);

// 스테이지 후처리
post <- PostStageTrigger();
post.SetTime(3);
root.InsertSibling(post);

// 스테이지 정보 출력
postinfo <- MsgTrigger();
post.InsertChild(postinfo);

// 게이트 ON 처리
open <- PortalOpenTrigger();
postinfo.InsertChild(open);

// 맵 레이어 변경
layer <- LayerTrigger();
root.InsertSibling(layer);