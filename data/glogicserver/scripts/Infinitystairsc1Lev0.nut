/**
	PVE 무한의 계단 
	서버 스크립트
	작성자 이익세 (수정하지말것)
 */
 
/*move to pos*/
script1 <- ::import("data/glogicserver/scripts/infinitystairsC1_0.nut", {});
script1.difficulty.SetMultipleHP(2.5);
script1.difficulty.SetMultipleAttack(1.2);
script1.difficulty.SetMultipleDefense(1);
script1.difficulty.SetMultipleExp(0.7);
script1.difficulty.SetMultipleDrop(1);
script1.layer.SetLayer(0); /*es_pyramid2_a.lev, 0*/
script1.info.SetMessage("INFINITYSTAIRS_MESSAGE_57"); /* Entering Secret Pyramid's treasure room. If you are stressed out, just give up already. This mode is not for you. */
script1.info.SetLifeTime(4);
script1.stage.SetLifeTime(80); /* 80 sec. */
script1.start.InsertChild(script1.stage);
script1.start.InsertChild(script1.pos);
script1.pos.SetMin(16, 15); /*x,y coordinate*/
script1.pos.SetMax(16, 15); /*x,y coordinate*/
script1.postinfo.SetMessage("INFINITYSTAIRS_MESSAGE_60"); /*Please proceed to the next area through the gate.*/
script1.postinfo.SetLifeTime(4);
script1.open.SetFromGateID(1); /*exit gate*/
script1.open.SetToMapID(294, 0);
script1.open.SetToGateID(2);
script1.reward.SetMVPIncrease(2);
script1.reward.SetEXPIncrease(5);
stage1 <- script1.root;

/*move to pos*/
script2 <- ::import("data/glogicserver/scripts/infinitystairsC1_0.nut", {});
script2.difficulty.SetMultipleHP(2.5);
script2.difficulty.SetMultipleAttack(1.2);
script2.difficulty.SetMultipleDefense(1);
script2.difficulty.SetMultipleExp(0.7);
script2.difficulty.SetMultipleDrop(1);
script2.layer.SetLayer(0); /*es_pyramid2_b.lev, 0*/
script2.info.SetMessage("INFINITYSTAIRS_MESSAGE_58"); /*Try to evade the traps and reach the destination.*/
script2.info.SetLifeTime(4);
script2.stage.SetLifeTime(90); /* 90 sec. */
script2.start.InsertChild(script2.stage);
script2.start.InsertChild(script2.pos);
script2.pos.SetMin(24, 19); /*x,y coordinate*/
script2.pos.SetMax(24, 19); /*x,y coordinate*/
script2.postinfo.SetMessage("INFINITYSTAIRS_MESSAGE_60"); /*Please proceed to the next area through the gate.*/
script2.postinfo.SetLifeTime(4);
script2.open.SetFromGateID(1); /*exit gate*/
script2.open.SetToMapID(295, 0);
script2.open.SetToGateID(2); /*entry gate on next map*/
script2.reward.SetMVPIncrease(2);
script2.reward.SetEXPIncrease(5);
stage2 <- script2.root;

/*move to pos*/
script3 <- ::import("data/glogicserver/scripts/infinitystairsC1_0.nut", {});
script3.difficulty.SetMultipleHP(2.5);
script3.difficulty.SetMultipleAttack(2);
script3.difficulty.SetMultipleDefense(1);
script3.difficulty.SetMultipleExp(0.7);
script3.difficulty.SetMultipleDrop(1);
script3.layer.SetLayer(0); /*es_pyramid2_c.lev, 0*/
script3.info.SetMessage("INFINITYSTAIRS_MESSAGE_58"); /*Try to evade the traps and reach the destination.*/
script3.info.SetLifeTime(4);
script3.stage.SetLifeTime(76); /* 76 sec. */
script3.start.InsertChild(script3.stage);
script3.start.InsertChild(script3.pos);
script3.pos.SetMin(12, 28); /*x,y coordinate*/
script3.pos.SetMax(12, 28); /*x,y coordinate*/
script3.postinfo.SetMessage("INFINITYSTAIRS_MESSAGE_60"); /*Please proceed to the next area through the gate.*/
script3.postinfo.SetLifeTime(4);
script3.open.SetFromGateID(1); /*exit gate*/
script3.open.SetToMapID(293, 0);
script3.open.SetToGateID(2); /*entry gate on next map*/
script3.reward.SetMVPIncrease(2);
script3.reward.SetEXPIncrease(5);
stage3 <- script3.root;

/*move to pos*/
script4 <- ::import("data/glogicserver/scripts/infinitystairsC1_0.nut", {});
script4.difficulty.SetMultipleHP(2.5);
script4.difficulty.SetMultipleAttack(2);
script4.difficulty.SetMultipleDefense(1);
script4.difficulty.SetMultipleExp(1);
script4.difficulty.SetMultipleDrop(1);
script4.layer.SetLayer(1); /*es_pyramid2_a.lev, 1*/
script4.info.SetMessage("INFINITYSTAIRS_MESSAGE_58"); /*Try to evade the traps and reach the destination.*/
script4.info.SetLifeTime(4);
script4.stage.SetLifeTime(120); /* 120 sec. */
script4.start.InsertChild(script4.stage);
script4.start.InsertChild(script4.pos);
script4.pos.SetMin(16, 15); /*x,y coordinate*/
script4.pos.SetMax(16, 15); /*x,y coordinate*/
script4.postinfo.SetMessage("INFINITYSTAIRS_MESSAGE_60"); /*Please proceed to the next area through the gate.*/
script4.postinfo.SetLifeTime(4);
script4.open.SetFromGateID(1);
script4.open.SetToMapID(294, 0);
script4.open.SetToGateID(2);
script4.reward.SetMVPIncrease(2);
script4.reward.SetEXPIncrease(5);
stage4 <- script4.root;

/* evade trap and move to pos */
script5 <- ::import("data/glogicserver/scripts/infinitystairsC1_0.nut", {});
script5.difficulty.SetMultipleHP(2.5);
script5.difficulty.SetMultipleAttack(2);
script5.difficulty.SetMultipleDefense(1);
script5.difficulty.SetMultipleExp(1);
script5.difficulty.SetMultipleDrop(1);
script5.layer.SetLayer(1); /*es_pyramid2_b.lev, 1*/
script5.info.SetMessage("INFINITYSTAIRS_MESSAGE_58"); /*Try to evade the traps and reach the destination.*/
script5.info.SetLifeTime(4);
script5.stage.SetLifeTime(120);
script5.start.InsertChild(script5.stage);
script5.start.InsertChild(script5.pos);
script5.pos.SetMin(24, 19); /*x,y coordinate*/
script5.pos.SetMax(24, 19); /*x,y coordinate*/
script5.postinfo.SetMessage("INFINITYSTAIRS_MESSAGE_60"); /*Please proceed to the next area through the gate.*/
script5.postinfo.SetLifeTime(4);
script5.open.SetFromGateID(1);
script5.open.SetToMapID(295, 0);
script5.open.SetToGateID(2);
script5.reward.SetMVPIncrease(2);
script5.reward.SetEXPIncrease(5);
stage5 <- script5.root;

/* evade trap and move to pos */
script6 <- ::import("data/glogicserver/scripts/infinitystairsC1_0.nut", {});
script6.difficulty.SetMultipleHP(2.5);
script6.difficulty.SetMultipleAttack(2);
script6.difficulty.SetMultipleDefense(1);
script6.difficulty.SetMultipleExp(0.8);
script6.difficulty.SetMultipleDrop(1);
script6.layer.SetLayer(1); /*es_pyramid2_c.lev, 1*/
script6.info.SetMessage("INFINITYSTAIRS_MESSAGE_58"); /*Try to evade the traps and reach the destination.*/
script6.info.SetLifeTime(4);
script6.stage.SetLifeTime(150);
script6.start.InsertChild(script6.stage);
script6.start.InsertChild(script6.pos);
script6.pos.SetMin(12, 28); /*x,y coordinate*/
script6.pos.SetMax(12, 28); /*x,y coordinate*/
script6.postinfo.SetMessage("INFINITYSTAIRS_MESSAGE_60"); /*Please proceed to the next area through the gate.*/
script6.postinfo.SetLifeTime(4);
script6.open.SetFromGateID(1);
script6.open.SetToMapID(296, 0);
script6.open.SetToGateID(2);
script6.reward.SetMVPIncrease(2);
script6.reward.SetEXPIncrease(5);
stage6 <- script6.root;

/* evade trap and move to pos */
script7 <- ::import("data/glogicserver/scripts/infinitystairsC1_0.nut", {});
script7.difficulty.SetMultipleHP(3);
script7.difficulty.SetMultipleAttack(2);
script7.difficulty.SetMultipleDefense(1);
script7.difficulty.SetMultipleExp(0.8);
script7.difficulty.SetMultipleDrop(1);
script7.layer.SetLayer(0); /*es_pyramid2_d.lev, 0*/
script7.info.SetMessage("INFINITYSTAIRS_MESSAGE_58"); /*Try to evade the traps and reach the destination.*/
script7.info.SetLifeTime(4);
script7.stage.SetLifeTime(60);
script7.start.InsertChild(script7.stage);
script7.start.InsertChild(script7.pos);
script7.pos.SetMin(16, 15); /*x,y coordinate*/
script7.pos.SetMax(16, 15); /*x,y coordinate*/
script7.postinfo.SetMessage("INFINITYSTAIRS_MESSAGE_60"); /*Please proceed to the next area through the gate.*/
script7.postinfo.SetLifeTime(4);
script7.open.SetFromGateID(1);
script7.open.SetToMapID(296, 0);
script7.open.SetToGateID(2);
script7.reward.SetMVPIncrease(2);
script7.reward.SetEXPIncrease(5);
stage7 <- script7.root;

/* evade trap and move to pos */
script8 <- ::import("data/glogicserver/scripts/infinitystairsC1_0.nut", {});
script8.difficulty.SetMultipleHP(2);
script8.difficulty.SetMultipleAttack(2);
script8.difficulty.SetMultipleDefense(1);
script8.difficulty.SetMultipleExp(1);
script8.difficulty.SetMultipleDrop(1);
script8.layer.SetLayer(1); /*es_pyramid2_d.lev, 1*/
script8.info.SetMessage("INFINITYSTAIRS_MESSAGE_58"); /*Try to evade the traps and reach the destination.*/
script8.info.SetLifeTime(4);
script8.stage.SetLifeTime(100); /*100 sec*/
script8.start.InsertChild(script8.stage);
script8.start.InsertChild(script8.pos);
script8.pos.SetMin(16, 15); /*x,y coordinate*/
script8.pos.SetMax(16, 15); /*x,y coordinate*/
script8.postinfo.SetMessage("INFINITYSTAIRS_MESSAGE_60"); /*Please proceed to the next area through the gate.*/
script8.postinfo.SetLifeTime(4);
script8.open.SetFromGateID(1);
script8.open.SetToMapID(295, 0);
script8.open.SetToGateID(2);
script8.reward.SetMVPIncrease(2);
script8.reward.SetEXPIncrease(5);
stage8 <- script8.root;

/* evade trap and move to pos */
script9 <- ::import("data/glogicserver/scripts/infinitystairsC1_0.nut", {});
script9.difficulty.SetMultipleHP(4);
script9.difficulty.SetMultipleAttack(2);
script9.difficulty.SetMultipleDefense(1);
script9.difficulty.SetMultipleExp(1);
script9.difficulty.SetMultipleDrop(1);
script9.layer.SetLayer(2); /*es_pyramid2_c.lev, 2*/
script9.info.SetMessage("INFINITYSTAIRS_MESSAGE_58"); /*Try to evade the traps and reach the destination.*/
script9.info.SetLifeTime(4);
script9.stage.SetLifeTime(100); /*100 sec*/
script9.start.InsertChild(script9.stage);
script9.start.InsertChild(script9.pos);
script9.pos.SetMin(12, 28); /*x,y coordinate*/
script9.pos.SetMax(12, 28); /*x,y coordinate*/
script9.postinfo.SetMessage("INFINITYSTAIRS_MESSAGE_60"); /*Please proceed to the next area through the gate.*/
script9.postinfo.SetLifeTime(4);
script9.open.SetFromGateID(1);
script9.open.SetToMapID(299, 0);
script9.open.SetToGateID(2);
script9.reward.SetMVPIncrease(2);
script9.reward.SetEXPIncrease(5);
stage9 <- script9.root;

/* kill 1 boss */
script10 <- ::import("data/glogicserver/scripts/infinitystairsC1_0.nut", {});
script10.difficulty.SetMultipleHP(2);
script10.difficulty.SetMultipleAttack(1.2);
script10.difficulty.SetMultipleDefense(1);
script10.difficulty.SetMultipleExp(1);
script10.difficulty.SetMultipleDrop(1);
script10.layer.SetLayer(0); /*es_pyramid2_g.lev, 0*/
script10.info.SetMessage("INFINITYSTAIRS_MESSAGE_59"); /*The treasure box that belongs to you is across the river. Please select one among them.*/
script10.info.SetLifeTime(4);
script10.stage.SetLifeTime(120);
script10.start.InsertChild(script10.stage);
script10.start.InsertChild(script10.clear);
script10.clear.SetCount(1); /* kill 1 boss */
script10.postinfo.SetMessage("INFINITYSTAIRS_MESSAGE_62"); /*ALL CLEAR! Your mouse control deserves respect. Press 'ESC' button to close the result window and take the drop items.*/
script10.postinfo.SetLifeTime(4);
script10.reward.SetMVPIncrease(2);
script10.reward.SetEXPIncrease(5);
script10.stage.SetLastStage(); /*end stage*/
stage10 <- script10.root;