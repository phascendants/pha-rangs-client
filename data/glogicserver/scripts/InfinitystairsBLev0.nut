/**
	PVE 한계의 사막
	서버 스크립트
	작성자 김판수 (수정하지말것)
 */
 

script1 <- ::import("data/glogicserver/scripts/infinitystairsB_0.nut", {});
script1.difficulty.SetMultipleHP(1.5);		// 전체몹 HP증가율
script1.difficulty.SetMultipleAttack(1.8);	// 전체몹 공격력 증가율
script1.difficulty.SetMultipleDefense(2);	// 전체몹 방어력 증가율
script1.difficulty.SetMultipleExp(1.02);		// 전체몹 설정된 경험치의 증가율 (1 = 현 설정값)
script1.difficulty.SetMultipleDrop(1);		// 전체몹 설정된 드랍율
script1.layer.SetLayer(0);			// 시작 레이어
script1.info.SetMessage("INFINITYSTAIRS_MESSAGE_39");	// 시작 출력 메시지
script1.info.SetLifeTime(4);				// 시작 출력 메시지 시간
script1.stage.SetLifeTime(4800);				// 스테이지 클리어 시간
script1.start.InsertChild(script1.stage);
script1.start.InsertChild(script1.pos);
script1.pos.SetMin(9, 25);
script1.pos.SetMax(9, 25);
script1.postinfo.SetMessage("INFINITYSTAIRS_MESSAGE_41");	// 클리어 시 출력 메시지
script1.postinfo.SetLifeTime(4);				// 클리어 시 출력 메시지 시간
script1.open.SetFromGateID(1);					// 오픈 할 게이트
script1.open.SetToMapID(247, 0);				// 다음 스테이지 맵 정보
script1.open.SetToGateID(2);					// 다음 이동 맵의 게이트 정보
stage1 <- script1.root;



script2 <- ::import("data/glogicserver/scripts/infinitystairsB_0.nut", {});
script2.difficulty.SetMultipleHP(1.3);
script2.difficulty.SetMultipleAttack(4.5);
script2.difficulty.SetMultipleDefense(2);
script2.difficulty.SetMultipleExp(1);			
script2.difficulty.SetMultipleDrop(1);
script2.layer.SetLayer(0);
script2.info.SetMessage("INFINITYSTAIRS_MESSAGE_40");
script2.info.SetLifeTime(4);
script2.stage.SetLifeTime(600);
script2.start.InsertChild(script2.stage);
script2.start.InsertChild(script2.clear);
script2.clear.SetCount(1);
script2.postinfo.SetMessage("INFINITYSTAIRS_MESSAGE_42");
script2.postinfo.SetLifeTime(60);
script2.stage.SetLastStage();
stage2 <- script2.root;
