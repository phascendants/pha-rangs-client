--
-- 주의) 
-- 1) 대/소 문자를 확실하게 구분해야 합니다.
-- 2) true, false 를 확실하게 구분해야 합니다.
-- 3) UCS-2 Little Endian 로 저장해야 합니다.
-------------------------------------------------------------------------------
-- 버젼;
Lotto_System_Version = 100;

-- 로또 시스템 전체의 전역 상수;
Basic = 
{
	view_list_num = 5,						-- 당첨 기록 확인 개수;
	view_char_num = 2,						-- 당첨 캐릭터 글자 표시 제한;
	aggregate_time = 172800,					-- 누적금액 대기시간(추첨완료 후);
	accumulate_money_time = 15,				-- 누적 당첨금 갱신시간(초);
}

-- 각 로또 시스템의 전역 상수;
LottoSystem =
{
	-- Game Money Lotto System;
	{
		lotto_total_num = 35,				-- Lotto가 가지는 총 번호 개수;
		drawing_total_num = 7,				-- 추첨할 번호의 총 개수;
		drawing_main_num = 6,				-- 추첨할 메인 번호 개수;
		drawing_bonus_num = 1,				-- 추첨할 보너스 번호 개수;
	
		use = 1,							-- 사용 여부( 0: 사용안함, 1: 사용 );
		use_sort = 1,						-- 사용 여부( 0: 사용안함, 1: 사용 );
		drawingType = 0,					-- 추첨 방식( 0: 랜덤, 1: 최소인원 );
		
		-- 요일 ( 0 : NONE, 1 : Sunday, 2 : Monday ... );
		-- 추첨일( 요일, 시, 분, 초 );
		drawing_date = {
			{  2, 1, 00, 0  },				-- 월요일 01:00분 추첨
		},
		drawing_ready_time = 20,			-- 추첨 대기 시간(초);
		drawing_time = 90,					-- 추첨 시간(초);
		
		-- 화폐 종류( 0 : Game Money, 1 : Ran Point );
		money_type = 0,
		lotto_ticket_money = 100000,		-- 1장당 구매 가격;
		
		buy_level = 1,						-- 구매 제한 레벨;
		buy_multi_num = 100,					-- 구매 가능 수(장);
		buy_inven_money = 100000,			-- 인벤의 최소 재산(필요 머니);
		
		ticket_fee = 50,					-- 수수료(%), 구매시 수수료를 뺀 금액을 누적;
		
		-- 승리 타입( Main num, Bonus num, 지급 타입, 타입에 따른 지금 금액, 보상아이템 MainID, 보상아이템 SubID, 보상아이템 개수 );
		-- 지급 타입( 0 :  누적 금액, 1 : 고정 금액 );
		-- 누적 금액시 퍼센트, 고정 금액시 실제 지급액;
		win_type = {
			{ 6, 0, 0, 65, 458, 540, 1 },
			{ 5, 1, 0, 10, 65535, 65535, 1 },
			{ 5, 0, 0, 10, 65535, 65535, 1 },
			{ 4, 0, 0, 15, 65535, 65535, 1 },
		},
	},
	
	-- Ran Point Lotto System;
	-- 위의 주석과 같음;
   {
		lotto_total_num = 25,				-- Lotto가 가지는 총 번호 개수;
		drawing_total_num = 7,				-- 추첨할 번호의 총 개수;
		drawing_main_num = 6,				-- 추첨할 메인 번호 개수;
		drawing_bonus_num = 1,				-- 추첨할 보너스 번호 개수;
		
		use = 1,							-- 사용 여부( 0: 사용안함, 1: 사용 );
		use_sort = 1,                       -- 사용 여부( 0: 사용안함, 1: 사용 );
		drawingType = 0,                    -- 추첨 방식( 0: 랜덤, 1: 최소인원 );
		
		-- 요일 ( 0 : NONE, 1 : Sunday, 2 : Monday ... );
		-- 추첨일( 요일, 시, 분, 초 );
		drawing_date = {
			{ 5, 1, 00, 0 },				-- 목요일 01:00분 추첨
		},
		drawing_ready_time = 20,            -- 추첨 대기 시간(초);
		drawing_time = 90,                  -- 추첨 시간(초);
		
		-- 화폐 종류( 0 : Game Money, 1 : Ran Point );
		money_type = 1,
		lotto_ticket_money = 5,			-- 1장당 구매 가격;
		
		buy_level = 1,						-- 구매 제한 레벨;
		buy_multi_num = 100,					-- 구매 가능 수;
		buy_inven_money = 5,				-- 인벤의 최소 재산(필요 머니);
		
		ticket_fee = 50,					-- 수수료(%);
		
		-- 승리 타입( Main num, Bonus num, 지급 타입, 타입에 따른 지금 금액, 보상아이템 MainID, 보상아이템 SubID, 보상아이템 개수 );
		-- 지급 타입( 0 :  누적 금액, 1 : 고정 금액 );
		-- 누적 금액시 퍼센트, 고정 금액시 실제 지급액;
		win_type = { 
			{ 6, 0, 0, 65, 458, 642, 1 },
			{ 5, 1, 0, 10, 65535, 65535, 0 },
			{ 5, 0, 0, 10, 65535, 65535, 0 },
			{ 4, 0, 0, 15, 65535, 65535, 0 },
		},
	},
}
